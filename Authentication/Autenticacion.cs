﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace AppTUMISMITOPE.Authentication
{


    public interface IAutenticacion
    {

        void IniciarSesion(String userName);
        void CloseSession();
        String GetCurrrentUser();
    }

    public class Autenticacion : IAutenticacion
    {

        private readonly IHttpContextAccessor _httpContextAccessor;

        public Autenticacion(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public void CloseSession()
        {
            _httpContextAccessor.HttpContext.SignOutAsync();
        }

        public string GetCurrrentUser()
        {
            var claim = _httpContextAccessor.HttpContext.User.Claims.First();

            return claim.Value;
        }

        public void IniciarSesion(String userName)
        {

            var claims = new List<Claim> {
                new Claim(ClaimTypes.Name,userName) };

            var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
            var claimsPrincipal = new ClaimsPrincipal(claimsIdentity);

            _httpContextAccessor.HttpContext.SignInAsync(claimsPrincipal);

        }

     
        

        
    }
}
