﻿using AppTUMISMITOPE.DB;
using AppTUMISMITOPE.Models;
using AppTUMISMITOPE.Repository;
using AppTUMISMITOPE.Test.RepositoryTest.Mocks;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace AppTUMISMITOPE.Test.RepositoryTest
{
    public class PostulacionRepositoryTest
    {

        [Test]
        public void FindByIdCase01()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.FindById(1);


            Assert.IsNotNull(result);


        }
        [Test]
        public void FindByIdCase02()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.FindById(3);


            Assert.IsNotNull(result);


        }
        [Test]
        public void FindByIdCase03()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.FindById(5);


            Assert.IsNotNull(result);


        }
        [Test]
        public void FindByIdCase04()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.FindById(6);


            Assert.IsNotNull(result);


        }
        [Test]
        public void FindByIdCase05()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.FindById(7);


            Assert.IsNotNull(result);


        }

        //


        [Test]
        public void FindByIdCase06()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.FindById(1);


            Assert.IsInstanceOf<Postulacion>(result);
            Assert.NotZero(result.idPublicacion);
            Assert.IsNotEmpty(result.descripcion);


        }
        [Test]
        public void FindByIdCase07()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.FindById(3);


            Assert.IsInstanceOf<Postulacion>(result);
            Assert.NotZero(result.idPublicacion);
            Assert.IsNotEmpty(result.descripcion);



        }
        [Test]
        public void FindByIdCase08()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.FindById(5);


            Assert.IsInstanceOf<Postulacion>(result);
            Assert.NotZero(result.idPublicacion);
            Assert.IsNotEmpty(result.descripcion);


        }
        [Test]
        public void FindByIdCase09()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.FindById(6);

            Assert.IsInstanceOf<Postulacion>(result);
            Assert.NotZero(result.idPublicacion);
            Assert.IsNotEmpty(result.descripcion);


        }
        [Test]
        public void FindByIdCase10()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.FindById(7);


            Assert.IsInstanceOf<Postulacion>(result);
            Assert.NotZero(result.idPublicacion);
            Assert.IsNotEmpty(result.descripcion);


        }

        //
        [Test]
        public void FindByIdCase11()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.FindById(7);


            Assert.AreEqual(7,result.id);
            Assert.AreEqual(900, result.monto);
            Assert.AreEqual(3, result.estado);

        }
        [Test]
        public void FindByIdCase12()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.FindById(1);


            Assert.AreEqual(1, result.id);
            Assert.AreEqual(380, result.monto);
            Assert.AreEqual(0, result.estado);



        }
        [Test]
        public void FindByIdCase13()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.FindById(6);


            Assert.AreEqual(6, result.id);
            Assert.AreEqual(180, result.monto);
            Assert.AreEqual(1, result.estado);


        }
        [Test]
        public void FindByIdCase14()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.FindById(2);

            Assert.AreEqual(2, result.id);
            Assert.AreEqual(400, result.monto);
            Assert.AreEqual(0, result.estado);


        }
        [Test]
        public void FindByIdCase15()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.FindById(5);


            Assert.AreEqual(5, result.id);
            Assert.AreEqual(360, result.monto);
            Assert.AreEqual(3, result.estado);


        }
        //

        [Test]
        public void FindByIdCase16()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.FindById(7);


            Assert.AreEqual(2, result.likesPostulacion.Count);

        }
        [Test]
        public void FindByIdCase17()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.FindById(1);


            Assert.AreEqual(3, result.likesPostulacion.Count);



        }
        [Test]
        public void FindByIdCase18()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.FindById(6);


            Assert.AreEqual(2, result.likesPostulacion.Count);


        }
        [Test]
        public void FindByIdCase19()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.FindById(2);

            Assert.AreEqual(1, result.likesPostulacion.Count);


        }
        [Test]
        public void FindByIdCase20()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.FindById(5);


            Assert.AreEqual(3, result.likesPostulacion.Count);

        }


        //GetPostulacionesByPublicacion

        [Test]
        public void GetPostulacionesByPublicacionCase01()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetPostulacionesByPublicacion(5);


            Assert.IsNotNull(result );
            Assert.IsNotEmpty(result);

        }
        [Test]
        public void GetPostulacionesByPublicacionCase02()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetPostulacionesByPublicacion(6);


            Assert.IsNotNull(result);
            Assert.IsNotEmpty(result);

        }
        [Test]
        public void GetPostulacionesByPublicacionCase03()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetPostulacionesByPublicacion(4);


            Assert.IsNotNull(result);
            Assert.IsNotEmpty(result);

        }
        [Test]
        public void GetPostulacionesByPublicacionCase04()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetPostulacionesByPublicacion(7);


            Assert.IsNotNull(result);
            Assert.IsNotEmpty(result);

        }
        [Test]
        public void GetPostulacionesByPublicacionCase05()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetPostulacionesByPublicacion(100000);


            Assert.IsNotNull(result);
            Assert.IsEmpty(result);

        }
        [Test]
        public void GetPostulacionesByPublicacionCase06()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetPostulacionesByPublicacion(0);


            Assert.IsNotNull(result);
            Assert.IsEmpty(result);

        }
        [Test]
        public void GetPostulacionesByPublicacionCase07()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetPostulacionesByPublicacion(1010101010);


            Assert.IsNotNull(result);
            Assert.IsEmpty(result);

        }
        //
        [Test]
        public void GetPostulacionesByPublicacionCase08()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetPostulacionesByPublicacion(1);


            Assert.AreEqual(2,result.Count);
            
        }
        [Test]
        public void GetPostulacionesByPublicacionCase09()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetPostulacionesByPublicacion(2);


            Assert.AreEqual(1, result.Count);

        }
        [Test]
        public void GetPostulacionesByPublicacionCase10()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetPostulacionesByPublicacion(3);


            Assert.AreEqual(0, result.Count);

        }
        [Test]
        public void GetPostulacionesByPublicacionCase11()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetPostulacionesByPublicacion(5);


            Assert.AreEqual(1, result.Count);

        }
        [Test]
        public void GetPostulacionesByPublicacionCase12()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetPostulacionesByPublicacion(6);


            Assert.AreEqual(1, result.Count);

        }
        [Test]
        public void GetPostulacionesByPublicacionCase13()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetPostulacionesByPublicacion(7);


            Assert.AreEqual(1, result.Count);

        }

        //

        [Test]
        public void GetPostulacionesByPublicacionCase14()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetPostulacionesByPublicacion(1);


            Assert.AreEqual(1, result[0].id);
            Assert.AreEqual("Postulacion01Pub01",result[0].descripcion);

        }

        [Test]
        public void GetPostulacionesByPublicacionCase15()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetPostulacionesByPublicacion(1);

            Assert.AreEqual(2, result[1].id);
            Assert.AreEqual("Postulacion02Pub01", result[1].descripcion);

        }
        [Test]
        public void GetPostulacionesByPublicacionCase16()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetPostulacionesByPublicacion(2);

            Assert.AreEqual(3, result[0].id);
            Assert.AreEqual("Postulacion01Pub02", result[0].descripcion);

        }
        [Test]
        public void GetPostulacionesByPublicacionCase17()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetPostulacionesByPublicacion(5);


            Assert.AreEqual(5, result[0].id);
            Assert.AreEqual("Postulacion01Pub05", result[0].descripcion);

        }

        [Test]
        public void GetPostulacionesByPublicacionCase18()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetPostulacionesByPublicacion(6);


            Assert.AreEqual(6, result[0].id);
            Assert.AreEqual("Postulacion01Pub06", result[0].descripcion);

        }


        [Test]
        public void GetPostulacionesByPublicacionCase19()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetPostulacionesByPublicacion(7);


            Assert.AreEqual(7, result[0].id);
            Assert.AreEqual("Postulacion01Pub07", result[0].descripcion);

        }

        //
        [Test]
        public void GetPostulacionesByPublicacionCase20()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetPostulacionesByPublicacion(1);


            Assert.AreEqual(1, result[0].id);
            Assert.AreEqual(3, result[0].likesPostulacion.Count);

        }

        [Test]
        public void GetPostulacionesByPublicacionCase21()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetPostulacionesByPublicacion(1);

            Assert.AreEqual(2, result[1].id);
            Assert.AreEqual(1, result[1].likesPostulacion.Count);

        }
        [Test]
        public void GetPostulacionesByPublicacionCase22()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetPostulacionesByPublicacion(2);

            Assert.AreEqual(3, result[0].id);
            Assert.AreEqual(2, result[0].likesPostulacion.Count);

        }
        [Test]
        public void GetPostulacionesByPublicacionCase23()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetPostulacionesByPublicacion(5);



            Assert.AreEqual(5, result[0].id);
            Assert.AreEqual(3, result[0].likesPostulacion.Count);

        }

        [Test]
        public void GetPostulacionesByPublicacionCase24()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetPostulacionesByPublicacion(6);


            Assert.AreEqual(6, result[0].id);
            Assert.AreEqual(2, result[0].likesPostulacion.Count);

        }


        [Test]
        public void GetPostulacionesByPublicacionCase25()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetPostulacionesByPublicacion(7);


            Assert.AreEqual(7, result[0].id);
            Assert.AreEqual(2, result[0].likesPostulacion.Count);


        }
        //
        [Test]
        public void GetPostulacionesByPublicacionCase26()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetPostulacionesByPublicacion(5);



            Assert.AreEqual(5, result[0].id);
            Assert.AreEqual(8, result[0].likesPostulacion[0].id);

        }

        [Test]
        public void GetPostulacionesByPublicacionCase27()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetPostulacionesByPublicacion(6);


            Assert.AreEqual(6, result[0].id);
            Assert.AreEqual(11, result[0].likesPostulacion[0].id);

        }
        [Test]
        public void GetPostulacionesByPublicacionCase28()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetPostulacionesByPublicacion(6);


            Assert.AreEqual(6, result[0].id);
            Assert.AreEqual(12, result[0].likesPostulacion[1].id);

        }

        [Test]
        public void GetPostulacionesByPublicacionCase29()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetPostulacionesByPublicacion(7);


            Assert.AreEqual(7, result[0].id);
            Assert.AreEqual(13, result[0].likesPostulacion[0].id);


        }

        [Test]
        public void GetPostulacionesByPublicacionCase30()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetPostulacionesByPublicacion(7);


            Assert.AreEqual(7, result[0].id);
            Assert.AreEqual(14, result[0].likesPostulacion[1].id);


        }


        // cases GetEnVotacionByIdPublicacion



        [Test]
        public void GetEnVotacionByIdPublicacionCase01()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetEnVotacionByIdPublicacion(5);


            Assert.IsNotNull(result);
            Assert.IsEmpty(result);

        }
        [Test]
        public void GetEnVotacionByIdPublicacionCase02()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetEnVotacionByIdPublicacion(6);


            Assert.IsNotNull(result);
            Assert.IsEmpty(result);

        }
        [Test]
        public void GetEnVotacionByIdPublicacionCase03()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetEnVotacionByIdPublicacion(4);


            Assert.IsNotNull(result);
            Assert.IsEmpty(result);

        }
        [Test]
        public void GetEnVotacionByIdPublicacionCase04()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetEnVotacionByIdPublicacion(7);


            Assert.IsNotNull(result);
            Assert.IsEmpty(result);

        }
        [Test]
        public void GetEnVotacionByIdPublicacionCase05()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetEnVotacionByIdPublicacion(100000);


            Assert.IsNotNull(result);
            Assert.IsEmpty(result);

        }
        [Test]
        public void GetEnVotacionByIdPublicacionCase06()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetEnVotacionByIdPublicacion(0);


            Assert.IsNotNull(result);
            Assert.IsEmpty(result);

        }
        [Test]
        public void GetEnVotacionByIdPublicacionCase07()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetEnVotacionByIdPublicacion(1010101010);


            Assert.IsNotNull(result);
            Assert.IsEmpty(result);

        }
        [Test]
        public void GetEnVotacionByIdPublicacionCase08()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetEnVotacionByIdPublicacion(1);


            Assert.IsNotNull(result);
            Assert.IsNotEmpty(result);

        }
 
        //
        [Test]
        public void GetEnVotacionByIdPublicacionCase09()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetEnVotacionByIdPublicacion(1);


            Assert.AreEqual(2, result.Count);

        }
        [Test]
        public void GetEnVotacionByIdPublicacionCase10()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetEnVotacionByIdPublicacion(2);


            Assert.AreEqual(0, result.Count);

        }
        [Test]
        public void GetEnVotacionByIdPublicacionCase11()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetEnVotacionByIdPublicacion(3);


            Assert.AreEqual(0, result.Count);

        }
        [Test]
        public void GetEnVotacionByIdPublicacionCase12()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetEnVotacionByIdPublicacion(5);


            Assert.AreEqual(0, result.Count);

        }
        [Test]
        public void GetEnVotacionByIdPublicacionCase13()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetEnVotacionByIdPublicacion(6);


            Assert.AreEqual(0, result.Count);

        }
        [Test]
        public void GetEnVotacionByIdPublicacionCase14()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetEnVotacionByIdPublicacion(7);


            Assert.AreEqual(0, result.Count);

        }

        //

        [Test]
        public void GetEnVotacionByIdPublicacionCase15()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetEnVotacionByIdPublicacion(1);


            Assert.AreEqual(1, result[0].id);
            Assert.AreEqual("Postulacion01Pub01", result[0].descripcion);

        }

        [Test]
        public void GetEnVotacionByIdPublicacionCase16()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetEnVotacionByIdPublicacion(1);

            Assert.AreEqual(2, result[1].id);
            Assert.AreEqual("Postulacion02Pub01", result[1].descripcion);

        }

        [Test]
        public void GetEnVotacionByIdPublicacionCase17()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetEnVotacionByIdPublicacion(2);


            Assert.IsEmpty(result);
       
          

        }
        [Test]
        public void GetEnVotacionByIdPublicacionCase18()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetEnVotacionByIdPublicacion(1);


            Assert.AreEqual(2, result[1].id);

            Assert.IsInstanceOf<Postulacion>(result[1]);


        }

        [Test]
        public void GetEnVotacionByIdPublicacionCase19()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetEnVotacionByIdPublicacion(1);


            foreach (var item in result)
            {
                Assert.IsInstanceOf<Postulacion>(item);
            }


        }

  

        //
        [Test]
        public void GetEnVotacionByIdPublicacionCase20()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetEnVotacionByIdPublicacion(1);


            Assert.AreEqual(1, result[0].id);
            Assert.AreEqual(3, result[0].likesPostulacion.Count);

        }

        [Test]
        public void GetEnVotacionByIdPublicacionCase21()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetEnVotacionByIdPublicacion(1);

            Assert.AreEqual(2, result[1].id);
            Assert.AreEqual(1, result[1].likesPostulacion.Count);

        }
      


        [Test]
        public void GetEnVotacionByIdPublicacionCase22()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetEnVotacionByIdPublicacion(1);


        
            Assert.AreEqual(1, result[0].likesPostulacion[0].id);


        }

        [Test]
        public void GetEnVotacionByIdPublicacionCase23()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetEnVotacionByIdPublicacion(1);



            Assert.AreEqual(3, result[0].likesPostulacion[2].id);


        }

        [Test]
        public void GetEnVotacionByIdPublicacionCase24()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetEnVotacionByIdPublicacion(1);



            Assert.AreEqual(4, result[1].likesPostulacion[0].id);


        }

        [Test]
        public void GetEnVotacionByIdPublicacionCase25()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetEnVotacionByIdPublicacion(1);



            Assert.AreEqual(2, result[1].likesPostulacion[0].idPostulacion);


            Assert.AreEqual(new DateTime(2021, 03, 10), result[1].likesPostulacion[0].fechaLike);
        }

        //cases GetNoElejidos






        [Test]
        public void GetNoElejidosCase01()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetNoElejidos(5);


            Assert.IsNotNull(result);
            Assert.IsNotEmpty(result);

        }
        [Test]
        public void GetNoElejidosCase02()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetNoElejidos(6);


            Assert.IsNotNull(result);
            Assert.IsEmpty(result);

        }
        [Test]
        public void GetNoElejidosCase03()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetNoElejidos(4);


            Assert.IsNotNull(result);
            Assert.IsEmpty(result);

        }
        [Test]
        public void GetNoElejidosCase04()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetNoElejidos(7);


            Assert.IsNotNull(result);
            Assert.IsNotEmpty(result);

        }
        [Test]
        public void GetNoElejidosCase05()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetNoElejidos(100000);


            Assert.IsNotNull(result);
            Assert.IsEmpty(result);

        }
        [Test]
        public void GetNoElejidosCase06()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetNoElejidos(0);


            Assert.IsNotNull(result);
            Assert.IsEmpty(result);

        }
        [Test]
        public void GetNoElejidosCase07()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetNoElejidos(1010101010);


            Assert.IsNotNull(result);
            Assert.IsEmpty(result);

        }
        [Test]
        public void GetNoElejidosCase08()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetNoElejidos(1);


            Assert.IsNotNull(result);
            Assert.IsNotEmpty(result);

        }


        [Test]
        public void GetNoElejidosCase09()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetNoElejidos(1);


            Assert.IsInstanceOf<List<Postulacion>>(result);

            foreach (var item in result)
            {
                Assert.IsInstanceOf<Postulacion>(item);
            }

        }
        [Test]
        public void GetNoElejidosCase10()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetNoElejidos(5);


            Assert.IsInstanceOf<List<Postulacion>>(result);
            foreach (var item in result)
            {
                Assert.IsInstanceOf<Postulacion>(item);
            }

        }


        [Test]
        public void GetNoElejidosCase11()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetNoElejidos(2);


            Assert.IsInstanceOf<List<Postulacion>>(result);
            foreach (var item in result)
            {
                Assert.IsInstanceOf<Postulacion>(item);
            }

        }
        [Test]
        public void GetNoElejidosCase12()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetNoElejidos(1);


            Assert.IsInstanceOf<List<Postulacion>>(result);
            foreach (var item in result)
            {
                Assert.IsInstanceOf<Postulacion>(item);
            }

        }
        //
        [Test]
        public void GetNoElejidosCase13()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetNoElejidos(1);


            Assert.AreEqual(2, result.Count);

        }
        [Test]
        public void GetNoElejidosCase14()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetNoElejidos(2);


            Assert.AreEqual(1, result.Count);

        }
        [Test]
        public void GetNoElejidosCase15()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetNoElejidos(3);


            Assert.AreEqual(0, result.Count);
        }
        [Test]
        public void GetNoElejidosCase16()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetNoElejidos(5);


            Assert.AreEqual(1, result.Count);

        }
        [Test]
        public void GetNoElejidosCase17()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetNoElejidos(6);


            Assert.AreEqual(0, result.Count);

        }
        [Test]
        public void GetNoElejidosCase18()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetNoElejidos(7);


            Assert.AreEqual(1, result.Count);

        }
        [Test]
        public void GetNoElejidosCase19()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetNoElejidos(4);


            Assert.AreEqual(0, result.Count);

        }

        //

        [Test]
        public void GetNoElejidosCase20()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetNoElejidos(1);


            Assert.AreEqual(1, result[0].id);
            Assert.AreEqual("Postulacion01Pub01", result[0].descripcion);

        }

        [Test]
        public void GetNoElejidosCase21()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetNoElejidos(1);

            Assert.AreEqual(2, result[1].id);
            Assert.AreEqual("Postulacion02Pub01", result[1].descripcion);

        }

        [Test]
        public void GetNoElejidosCase22()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetNoElejidos(2);

            Assert.AreEqual(3, result[0].id);

            Assert.AreEqual("Postulacion01Pub02", result[0].descripcion);


        }

        [Test]
        public void GetNoElejidosCase23()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetNoElejidos(7);

            Assert.AreEqual(7, result[0].id);
            Assert.AreEqual("Postulacion01Pub07", result[0].descripcion);


        }
        [Test]
        public void GetNoElejidosCase24()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetNoElejidos(5);

            Assert.AreEqual(5, result[0].id);
            Assert.AreEqual("Postulacion01Pub05", result[0].descripcion);


        }


        //
        [Test]
        public void GetNoElejidosCase25()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetNoElejidos(1);


            Assert.AreEqual(1, result[0].id);
            Assert.AreEqual(3, result[0].likesPostulacion.Count);

        }

        [Test]
        public void GetNoElejidosCase26()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetNoElejidos(1);

            Assert.AreEqual(2, result[1].id);
            Assert.AreEqual(1, result[1].likesPostulacion.Count);

        }



        [Test]
        public void GetNoElejidosCase27()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetNoElejidos(1);



            Assert.AreEqual(1, result[0].likesPostulacion[0].id);


        }

        [Test]
        public void GetNoElejidosCase28()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetNoElejidos(1);



            Assert.AreEqual(3, result[0].likesPostulacion[2].id);


        }

        [Test]
        public void GetNoElejidosCase29()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetNoElejidos(1);



            Assert.AreEqual(4, result[1].likesPostulacion[0].id);


        }

        [Test]
        public void GetNoElejidosCase30()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.GetNoElejidos(1);
            Assert.AreEqual(2, result[1].likesPostulacion[0].idPostulacion);


            Assert.AreEqual(new DateTime(2021, 03, 10), result[1].likesPostulacion[0].fechaLike);
        }




        //cases FindByPublicacionUsuario


        [Test]
        public void FindByPublicacionUsuarioCase01()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.FindByPublicacionUsuario(1,2);


            Assert.IsNotNull(result);



        }
        [Test]
        public void FindByPublicacionUsuarioCase02()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.FindByPublicacionUsuario(3,100);


            Assert.IsNull(result);


        }
        [Test]
        public void FindByPublicacionUsuarioCase03()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.FindByPublicacionUsuario(5,9);


            Assert.IsNotNull(result);


        }
        [Test]
        public void FindByPublicacionUsuarioCase04()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.FindByPublicacionUsuario(6,5);


            Assert.IsNotNull(result);


        }
        [Test]
        public void FindByPublicacionUsuarioCase05()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.FindByPublicacionUsuario(7,7);


            Assert.IsNotNull(result);


        }

        //


        [Test]
        public void FindByPublicacionUsuarioCase06()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.FindByPublicacionUsuario(1,9);


            Assert.IsInstanceOf<Postulacion>(result);
            Assert.NotZero(result.idPublicacion);
            Assert.IsNotEmpty(result.descripcion);


        }
        [Test]
        public void FindByPublicacionUsuarioCase07()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.FindByPublicacionUsuario(2,5);


            Assert.IsInstanceOf<Postulacion>(result);
            Assert.NotZero(result.idPublicacion);
            Assert.IsNotEmpty(result.descripcion);



        }
        [Test]
        public void FindByPublicacionUsuarioCase08()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.FindByPublicacionUsuario(5,9);


            Assert.IsInstanceOf<Postulacion>(result);
            Assert.NotZero(result.idPublicacion);
            Assert.IsNotEmpty(result.descripcion);


        }
        [Test]
        public void FindByPublicacionUsuarioCase09()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.FindByPublicacionUsuario(6,5);

            Assert.IsInstanceOf<Postulacion>(result);
            Assert.NotZero(result.idPublicacion);
            Assert.IsNotEmpty(result.descripcion);


        }
        [Test]
        public void FindByPublicacionUsuarioCase10()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.FindByPublicacionUsuario(7,7);


            Assert.IsInstanceOf<Postulacion>(result);
            Assert.NotZero(result.idPublicacion);
            Assert.IsNotEmpty(result.descripcion);


        }

        //
        [Test]
        public void FindByPublicacionUsuarioCase11()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.FindByPublicacionUsuario(7,7);


            Assert.AreEqual(7, result.id);
            Assert.AreEqual(900, result.monto);
            Assert.AreEqual(3, result.estado);

        }
        [Test]
        public void FindByPublicacionUsuarioCase12()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.FindByPublicacionUsuario(1,2);


            Assert.AreEqual(1, result.id);
            Assert.AreEqual(380, result.monto);
            Assert.AreEqual(0, result.estado);



        }
        [Test]
        public void FindByPublicacionUsuarioCase13()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.FindByPublicacionUsuario(6,5);


            Assert.AreEqual(6, result.id);
            Assert.AreEqual(180, result.monto);
            Assert.AreEqual(1, result.estado);


        }
        [Test]
        public void FindByPublicacionUsuarioCase14()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.FindByPublicacionUsuario(1,9);

            Assert.AreEqual(2, result.id);
            Assert.AreEqual(400, result.monto);
            Assert.AreEqual(0, result.estado);


        }
        [Test]
        public void FindByPublicacionUsuarioCase15()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.FindByPublicacionUsuario(5,9);


            Assert.AreEqual(5, result.id);
            Assert.AreEqual(360, result.monto);
            Assert.AreEqual(3, result.estado);


        }
        //

        [Test]
        public void FindByPublicacionUsuarioCase16()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.FindByPublicacionUsuario(7,7);


            Assert.AreEqual(2, result.likesPostulacion.Count);

        }
        [Test]
        public void FindByPublicacionUsuarioCase17()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.FindByPublicacionUsuario(1,2);


            Assert.AreEqual(3, result.likesPostulacion.Count);



        }
        [Test]
        public void FindByPublicacionUsuarioCase18()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.FindByPublicacionUsuario(6,5);


            Assert.AreEqual(2, result.likesPostulacion.Count);


        }
        [Test]
        public void FindByPublicacionUsuarioCase19()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.FindByPublicacionUsuario(1,9);

            Assert.AreEqual(1, result.likesPostulacion.Count);


        }
        [Test]
        public void FindByPublicacionUsuarioCase20()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            var result = pr.FindByPublicacionUsuario(5,9);


            Assert.AreEqual(3, result.likesPostulacion.Count);

        }


        [Test]
        public void AddCase01()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            pr.Add(new Postulacion());

        }

        [Test]
        public void DeleteRangeCase01()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            pr.DeleteRange(new List<Postulacion>());


        }
        [Test]
        public void UpdateChangesCase01()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            PostulacionRepository pr = new PostulacionRepository(mockContext.Object);

            pr.UpdateChanges();


        }

    }
}
