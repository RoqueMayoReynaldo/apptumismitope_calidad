﻿using AppTUMISMITOPE.DB;
using AppTUMISMITOPE.Models;
using AppTUMISMITOPE.Repository;
using AppTUMISMITOPE.Test.RepositoryTest.Mocks;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace AppTUMISMITOPE.Test.RepositoryTest
{
    public class PublicacionRepositoryTest
    {

        [Test]
        public void GetAllPublicacionesCase01()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);


            var result = pr.GetAllPublicaciones();

            //solo se obtienen los que tienen estado 0 y 3 

            Assert.IsNotNull(result);

        }
        [Test]
        public void GetAllPublicacionesCase02()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);


            var result = pr.GetAllPublicaciones();

            //solo se obtienen los que tienen estado 0 y 3 

            Assert.IsInstanceOf<List<Publicacion>>(result);

        }
        [Test]
        public void GetAllPublicacionesCase03()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            //solo se obtienen los que tienen estado 0 y 3 

            Assert.AreEqual(4, pr.GetAllPublicaciones().Count);

        }

        [Test]
        public void GetAllPublicacionesCase04()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetAllPublicaciones();


            Assert.AreEqual(1,result[3].id);

        }

        [Test]
        public void GetAllPublicacionesCase05()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetAllPublicaciones();


            Assert.AreEqual(7, result[0].id);

        }

        [Test]
        public void GetAllPublicacionesCase06()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetAllPublicaciones();


            Assert.AreEqual(5, result[1].id);

        }

        [Test]
        public void GetAllPublicacionesCase07()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetAllPublicaciones();


            Assert.AreEqual(3, result[2].id);

        }


        [Test]
        public void GetAllPublicacionesCase08()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetAllPublicaciones();


            foreach (var item in result)
            {
                Assert.IsNotNull(item.usuario);
            }
        }

        [Test]
        public void GetAllPublicacionesCase09()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetAllPublicaciones();


            foreach (var item in result)
            {
                Assert.IsNotNull(item.fotos);
            }
        }
        [Test]
        public void GetAllPublicacionesCase10()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetAllPublicaciones();

            Assert.AreEqual(4, result[2].fotos.Count);

        }
        [Test]
        public void GetAllPublicacionesCase11()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetAllPublicaciones();

            Assert.AreEqual(2, result[0].fotos.Count);

        }

        [Test]
        public void GetAllPublicacionesCase12()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetAllPublicaciones();

            Assert.AreEqual(1, result[1].fotos.Count);

        }

        [Test]
        public void GetAllPublicacionesCase13()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetAllPublicaciones();

            Assert.AreEqual(5, result[3].fotos.Count);

        }
        [Test]
        public void GetAllPublicacionesCase14()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetAllPublicaciones();

            Assert.AreEqual(4, result[0].donaciones.Count);

        }
        [Test]
        public void GetAllPublicacionesCase15()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetAllPublicaciones();

            Assert.AreEqual(2, result[1].donaciones.Count);

        }


        [Test]
        public void GetAllPublicacionesCase16()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetAllPublicaciones();

            Assert.AreEqual(0, result[2].donaciones.Count);

        }
        [Test]
        public void GetAllPublicacionesCase17()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetAllPublicaciones();

            Assert.AreEqual(3, result[3].donaciones.Count);

        }

        //
        [Test]
        public void GetAllPublicacionesCase18()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetAllPublicaciones();

            Assert.AreEqual(2, result[0].comentarios.Count);

        }
        [Test]
        public void GetAllPublicacionesCase19()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetAllPublicaciones();

            Assert.AreEqual(3, result[1].comentarios.Count);

        }


        [Test]
        public void GetAllPublicacionesCase20()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetAllPublicaciones();

            Assert.AreEqual(4, result[2].comentarios.Count);

        }
        [Test]
        public void GetAllPublicacionesCase21()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetAllPublicaciones();

            Assert.AreEqual(3, result[3].comentarios.Count);

        }


        //
        [Test]
        public void GetAllPublicacionesCase22()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetAllPublicaciones();

            Assert.AreEqual(4, result[0].reacciones.Count);

        }
        [Test]
        public void GetAllPublicacionesCase23()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetAllPublicaciones();

            Assert.AreEqual(3, result[1].reacciones.Count);

        }


        [Test]
        public void GetAllPublicacionesCase24()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetAllPublicaciones();

            Assert.AreEqual(4, result[2].reacciones.Count);

        }
        [Test]
        public void GetAllPublicacionesCase25()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetAllPublicaciones();

            Assert.AreEqual(4, result[3].reacciones.Count);

        }

        [Test]
        public void GetAllPublicacionesCase26()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetAllPublicaciones();

            Assert.AreEqual("/files/fotosPublicaciones/foto01Pub03.png", result[2].fotos[0].contenido);

        }

        [Test]
        public void GetAllPublicacionesCase27()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetAllPublicaciones();

            Assert.AreEqual("/files/fotosPublicaciones/foto02Pub01.png", result[3].fotos[1].contenido);

        }
        [Test]
        public void GetAllPublicacionesCase28()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetAllPublicaciones();

            Assert.AreEqual("/files/fotosPublicaciones/foto01Pub07.png", result[0].fotos[0].contenido);

        }

        //
        [Test]
        public void GetAllPublicacionesCase29()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetAllPublicaciones();

            Assert.AreEqual("Comentario01Pub07", result[0].comentarios[0].contenido);

        }

        [Test]
        public void GetAllPublicacionesCase30()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetAllPublicaciones();

            Assert.AreEqual("Comentario01Pub05", result[1].comentarios[0].contenido);

        }

        [Test]
        public void GetAllPublicacionesCase31()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetAllPublicaciones();

            Assert.AreEqual("Comentario01Pub03", result[2].comentarios[0].contenido);

        }
        [Test]
        public void GetAllPublicacionesCase32()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetAllPublicaciones();

            Assert.AreEqual("Comentario01Pub01", result[3].comentarios[0].contenido);

        }


        //GetPublicaciones


        [Test]
        public void GetPublicacionesSinCulminar01()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);


            var result = pr.GetPublicacionesSinCulminar();

            //solo se obtienen los que tienen estado 0 y 3 

            Assert.IsNotNull(result);

        }
        [Test]
        public void GetPublicacionesSinCulminar02()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);


            var result = pr.GetPublicacionesSinCulminar();

            //solo se obtienen los que tienen estado 0 y 3 

            Assert.IsInstanceOf<List<Publicacion>>(result);

        }
        [Test]
        public void GetPublicacionesSinCulminar03()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            //solo se obtienen los que tienen estado 0 ,1,2 y 3 

            Assert.AreEqual(6, pr.GetPublicacionesSinCulminar().Count);

        }

        [Test]
        public void GetPublicacionesSinCulminar04()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetPublicacionesSinCulminar();


            Assert.AreEqual(1, result[5].id);

        }

        [Test]
        public void GetPublicacionesSinCulminar05()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetPublicacionesSinCulminar();


            Assert.AreEqual(7, result[0].id);

        }

        [Test]
        public void GetPublicacionesSinCulminar06()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetPublicacionesSinCulminar();


            Assert.AreEqual(6, result[1].id);

        }

        [Test]
        public void GetPublicacionesSinCulminar07()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetPublicacionesSinCulminar();


            Assert.AreEqual(5, result[2].id);

        }


        [Test]
        public void GetPublicacionesSinCulminar08()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetPublicacionesSinCulminar();


            foreach (var item in result)
            {
                Assert.IsNotNull(item.usuario);
            }
        }

        [Test]
        public void GetPublicacionesSinCulminar09()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetPublicacionesSinCulminar();


            foreach (var item in result)
            {
                Assert.IsNotNull(item.fotos);
            }
        }
        [Test]
        public void GetPublicacionesSinCulminar10()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetPublicacionesSinCulminar();

            Assert.AreEqual(4, result[4].fotos.Count);

        }
        [Test]
        public void GetPublicacionesSinCulminar11()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetPublicacionesSinCulminar();

            Assert.AreEqual(2, result[0].fotos.Count);

        }

        [Test]
        public void GetPublicacionesSinCulminar12()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetPublicacionesSinCulminar();

            Assert.AreEqual(1, result[2].fotos.Count);

        }

        [Test]
        public void GetPublicacionesSinCulminar13()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetPublicacionesSinCulminar();

            Assert.AreEqual(5, result[5].fotos.Count);

        }
        [Test]
        public void GetPublicacionesSinCulminar14()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetPublicacionesSinCulminar();

            Assert.AreEqual(4, result[0].donaciones.Count);

        }
        [Test]
        public void GetPublicacionesSinCulminar15()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetPublicacionesSinCulminar();

            Assert.AreEqual(2, result[2].donaciones.Count);

        }


        [Test]
        public void GetPublicacionesSinCulminar16()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetPublicacionesSinCulminar();

            Assert.AreEqual(0, result[4].donaciones.Count);

        }
        [Test]
        public void GetPublicacionesSinCulminar17()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetPublicacionesSinCulminar();

            Assert.AreEqual(3, result[5].donaciones.Count);

        }

        //
        [Test]
        public void GetPublicacionesSinCulminar18()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetPublicacionesSinCulminar();

            Assert.AreEqual(2, result[0].comentarios.Count);

        }
        [Test]
        public void GetPublicacionesSinCulminar19()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetPublicacionesSinCulminar();

            Assert.AreEqual(3, result[2].comentarios.Count);

        }


        [Test]
        public void GetPublicacionesSinCulminar20()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetPublicacionesSinCulminar();

            Assert.AreEqual(4, result[4].comentarios.Count);

        }
        [Test]
        public void GetPublicacionesSinCulminar21()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetPublicacionesSinCulminar();

            Assert.AreEqual(3, result[5].comentarios.Count);

        }


        //
        [Test]
        public void GetPublicacionesSinCulminar22()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetPublicacionesSinCulminar();

            Assert.AreEqual(4, result[0].reacciones.Count);

        }
        [Test]
        public void GetPublicacionesSinCulminar23()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetPublicacionesSinCulminar();

            Assert.AreEqual(3, result[2].reacciones.Count);

        }


        [Test]
        public void GetPublicacionesSinCulminar24()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetPublicacionesSinCulminar();

            Assert.AreEqual(4, result[4].reacciones.Count);

        }
        [Test]
        public void GetPublicacionesSinCulminar25()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetPublicacionesSinCulminar();

            Assert.AreEqual(4, result[5].reacciones.Count);

        }

        [Test]
        public void GetPublicacionesSinCulminar26()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetPublicacionesSinCulminar();

            Assert.AreEqual("/files/fotosPublicaciones/foto01Pub03.png", result[4].fotos[0].contenido);

        }

        [Test]
        public void GetPublicacionesSinCulminar27()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetPublicacionesSinCulminar();

            Assert.AreEqual("/files/fotosPublicaciones/foto02Pub01.png", result[5].fotos[1].contenido);

        }
        [Test]
        public void GetPublicacionesSinCulminar28()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetPublicacionesSinCulminar();

            Assert.AreEqual("/files/fotosPublicaciones/foto01Pub07.png", result[0].fotos[0].contenido);

        }

        
        [Test]
        public void GetPublicacionesSinCulminar29()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetPublicacionesSinCulminar();

            Assert.AreEqual("Comentario02Pub07", result[0].comentarios[1].contenido);

        }

        [Test]
        public void GetPublicacionesSinCulminar30()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetPublicacionesSinCulminar();

            Assert.AreEqual("Comentario01Pub05", result[2].comentarios[0].contenido);

        }

        [Test]
        public void GetPublicacionesSinCulminar31()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetPublicacionesSinCulminar();

            Assert.AreEqual("Comentario01Pub03", result[4].comentarios[0].contenido);

        }
        [Test]
        public void GetPublicacionesSinCulminar32()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetPublicacionesSinCulminar();

            Assert.AreEqual("Comentario01Pub01", result[5].comentarios[0].contenido);

        }

        //GetCulminados


        [Test]
        public void GetCulminadosCase01()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);


            var result = pr.GetCulminados();

            //solo se obtienen los que tienen estado 0 y 3 

            Assert.IsNotNull(result);

        }
        [Test]
        public void GetCulminadosCase02()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);


            var result = pr.GetCulminados();

            //solo se obtienen los que tienen estado 0 y 3 

            Assert.IsInstanceOf<List<Publicacion>>(result);

        }
        [Test]
        public void GetCulminadosCase03()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            //solo se obtienen los que tienen estado 0 ,1,2 y 3 

            Assert.AreEqual(1, pr.GetCulminados().Count);

        }

        [Test]
        public void GetCulminadosCase04()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetCulminados();


            Assert.AreEqual(2, result[0].id);

        }


        [Test]
        public void GetCulminadosCase05()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetCulminados();


            foreach (var item in result)
            {
                Assert.IsNotNull(item.usuario);
            }
        }

        [Test]
        public void GetCulminadosCase06()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetCulminados();


            foreach (var item in result)
            {
                Assert.IsNotNull(item.fotos);
            }
        }
        [Test]
        public void GetCulminadosCase07()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetCulminados();

            Assert.AreEqual(4, result[0].fotos.Count);

        }
    
     
    

        [Test]
        public void GetCulminadosCase08()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetCulminados();

            Assert.AreEqual(2, result[0].donaciones.Count);

        }

   


        //
        [Test]
        public void GetCulminadosCase09()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetCulminados();

            Assert.AreEqual(1,result[0].comentarios.Count);

        }
     



        //
        [Test]
        public void GetCulminadosCase10()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetCulminados();

            Assert.AreEqual(4, result[0].reacciones.Count);

        }
 

        [Test]
        public void GetCulminadosCase11()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetCulminados();

            Assert.AreEqual("/files/fotosPublicaciones/foto01Pub02.png", result[0].fotos[0].contenido);

        }

        [Test]
        public void GetCulminadosCase12()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetCulminados();

            Assert.AreEqual("/files/fotosPublicaciones/foto02Pub02.png", result[0].fotos[1].contenido);

        }
        [Test]
        public void GetCulminadosCase13()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetCulminados();

            Assert.AreEqual("/files/fotosPublicaciones/foto03Pub02.png", result[0].fotos[2].contenido);

        }


        [Test]
        public void GetCulminadosCase14()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetCulminados();

            Assert.AreEqual("/files/fotosPublicaciones/foto04Pub02.png", result[0].fotos[3].contenido);

        }

        [Test]
        public void GetCulminadosCase15()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetCulminados();

            Assert.AreEqual(3, result[0].postulaciones[0].id);

        }
        [Test]
        public void GetCulminadosCase16()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetCulminados();

            Assert.IsNotNull(result[0].postulaciones[0].evidencia);

        }

        [Test]
        public void GetCulminadosCase17()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetCulminados();

            Assert.AreEqual(3,result[0].postulaciones[0].evidencia.idPostulacion);

        }

        [Test]
        public void GetCulminadosCase18()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetCulminados();

            Assert.AreEqual(6, result[0].postulaciones[0].evidencia.fotoEvidencias.Count);

        }


        //cases GetPublicacionById



        [Test]
        public void GetPublicacionByIdCase01()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);


            var result = pr.GetPublicacionById(2);

            //solo se obtienen los que tienen estado 0 y 3 

            Assert.IsNotNull(result);

        }
        [Test]
        public void GetPublicacionByIdCase02()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);


            var result = pr.GetPublicacionById(7);

            //solo se obtienen los que tienen estado 0 y 3 

            Assert.IsInstanceOf<Publicacion>(result);

        }
  

        [Test]
        public void GetPublicacionById03()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetPublicacionById(2);


            Assert.AreEqual(2, result.id);
            Assert.AreEqual("Publicacion02", result.titulo);
        }



        [Test]
        public void GetPublicacionById04()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetPublicacionById(5);


            Assert.AreEqual(5, result.id);
            Assert.AreEqual("Publicacion05", result.titulo);
        }
        [Test]
        public void GetPublicacionById05()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetPublicacionById(3);


            Assert.AreEqual(3, result.id);
            Assert.AreEqual(0, result.vista);
            Assert.AreEqual("Publicacion03", result.titulo);
        }

        [Test]
        public void GetPublicacionById06()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetPublicacionById(1);


            Assert.AreEqual(1, result.id);
            Assert.AreEqual(13, result.vista);
            Assert.AreEqual("Publicacion01", result.titulo);
        }
        [Test]
        public void GetPublicacionById07()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetPublicacionById(3);

            Assert.AreEqual(4, result.fotos.Count);

        }


        [Test]
        public void GetPublicacionById08()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetPublicacionById(1);

            Assert.AreEqual(5, result.fotos.Count);

        }
        [Test]
        public void GetPublicacionById09()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetPublicacionById(2);

            Assert.AreEqual(4, result.fotos.Count);

        }

        [Test]
        public void GetPublicacionById10()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetPublicacionById(4);

            Assert.AreEqual(2, result.fotos.Count);

        }


        [Test]
        public void GetPublicacionById11()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetPublicacionById(7);

            Assert.AreEqual(2, result.fotos.Count);

        }


        [Test]
        public void GetPublicacionById12()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetPublicacionById(1);

            Assert.AreEqual(3, result.donaciones.Count);

        }



        [Test]
        public void GetPublicacionById13()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetPublicacionById(2);

            Assert.AreEqual(2, result.donaciones.Count);

        }


        [Test]
        public void GetPublicacionById14()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetPublicacionById(3);

            Assert.AreEqual(0, result.donaciones.Count);

        }


        [Test]
        public void GetPublicacionById15()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetPublicacionById(4);

            Assert.AreEqual(2, result.donaciones.Count);

        }

        [Test]
        public void GetPublicacionById16()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetPublicacionById(5);

            Assert.AreEqual(2, result.donaciones.Count);

        }

        //
        [Test]
        public void GetPublicacionById17()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetPublicacionById(1);

            Assert.AreEqual(3, result.comentarios.Count);

        }

        [Test]
        public void GetPublicacionById18()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetPublicacionById(2);

            Assert.AreEqual(1, result.comentarios.Count);

        }
        [Test]
        public void GetPublicacionById19()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetPublicacionById(3);

            Assert.AreEqual(4, result.comentarios.Count);

        }
        [Test]
        public void GetPublicacionById20()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetPublicacionById(4);

            Assert.AreEqual(2, result.comentarios.Count);

        }
        [Test]
        public void GetPublicacionById21()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetPublicacionById(5);

            Assert.AreEqual(3, result.comentarios.Count);

        }
        [Test]
        public void GetPublicacionById22()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetPublicacionById(6);

            Assert.AreEqual(1, result.comentarios.Count);
        }
        //
        [Test]
        public void GetPublicacionById23()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetPublicacionById(1);

            Assert.AreEqual(4, result.reacciones.Count);

        }
        [Test]
        public void GetPublicacionById24()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetPublicacionById(3);

            Assert.AreEqual(4, result.reacciones.Count);

        }
        [Test]
        public void GetPublicacionById25()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetPublicacionById(6);

            Assert.AreEqual(3, result.reacciones.Count);

        }
        [Test]
        public void GetPublicacionById26()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetPublicacionById(4);

            Assert.AreEqual(1, result.reacciones.Count);

        }
        [Test]
        public void GetPublicacionById27()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetPublicacionById(1);

            Assert.AreEqual("/files/fotosPublicaciones/foto01Pub01.png", result.fotos[0].contenido);

        }

        [Test]
        public void GetPublicacionById28()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetPublicacionById(3);

            Assert.AreEqual("/files/fotosPublicaciones/foto02Pub03.png", result.fotos[1].contenido);

        }
        [Test]
        public void GetPublicacionById29()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetPublicacionById(7);

            Assert.AreEqual("/files/fotosPublicaciones/foto02Pub07.png", result.fotos[1].contenido);


        }


    

        [Test]
        public void GetPublicacionById30()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetPublicacionById(1);

            Assert.AreEqual(1, result.postulaciones[0].id);

        }
        [Test]
        public void GetPublicacionById31()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetPublicacionById(4);

            Assert.AreEqual(4, result.postulaciones[0].id);

        }
        [Test]
        public void GetPublicacionById32()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            var result = pr.GetPublicacionById(7);

            Assert.AreEqual(7, result.postulaciones[0].id);

        }
        [Test]
        public void AddAndSavePublicacionCase01()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            pr.AddAndSavePublicacion(new Publicacion());

        }
        [Test]
        public void UpdateChangesCase01()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            PublicacionRepository pr = new PublicacionRepository(mockContext.Object);

            pr.UpdateChanges();

        }
    }
}
