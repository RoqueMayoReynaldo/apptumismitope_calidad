﻿using AppTUMISMITOPE.DB;
using AppTUMISMITOPE.Models;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppTUMISMITOPE.Test.RepositoryTest.Mocks
{
    public static class AppTumismitoPeContextMock
    {
      

    public static Mock<AppTumismitoPeContext> GetContextMock()
        {
          



            //perfilProfesional
            IQueryable<PerfilProfesional> perfilProData = GetPerfilProfesionalData();

            var mockSetPerfilPro = new Mock<DbSet<PerfilProfesional>>();

            mockSetPerfilPro.As<IQueryable<PerfilProfesional>>().Setup(m => m.Provider).Returns(perfilProData.Provider);
            mockSetPerfilPro.As<IQueryable<PerfilProfesional>>().Setup(m => m.Expression).Returns(perfilProData.Expression);
            mockSetPerfilPro.As<IQueryable<PerfilProfesional>>().Setup(m => m.ElementType).Returns(perfilProData.ElementType);
            mockSetPerfilPro.As<IQueryable<PerfilProfesional>>().Setup(m => m.GetEnumerator()).Returns(perfilProData.GetEnumerator());

            //añadimos esto para los querys
            mockSetPerfilPro.Setup(m => m.AsQueryable()).Returns(perfilProData);







            IQueryable<Usuario> userData = GetUserData();

            var mockSetUsuario = new Mock<DbSet<Usuario>>();

            mockSetUsuario.As<IQueryable<Usuario>>().Setup(m => m.Provider).Returns(userData.Provider);
            mockSetUsuario.As<IQueryable<Usuario>>().Setup(m => m.Expression).Returns(userData.Expression);
            mockSetUsuario.As<IQueryable<Usuario>>().Setup(m => m.ElementType).Returns(userData.ElementType);
            mockSetUsuario.As<IQueryable<Usuario>>().Setup(m => m.GetEnumerator()).Returns(userData.GetEnumerator());

            //añadimos esto para los querys
            mockSetUsuario.Setup(m => m.AsQueryable()).Returns(userData);







            //Foto

            IQueryable<Foto> fotoData = GetFotoData();
            var mockSetFoto = new Mock<DbSet<Foto>>();

            mockSetFoto.As<IQueryable<Foto>>().Setup(m => m.Provider).Returns(fotoData.Provider);
            mockSetFoto.As<IQueryable<Foto>>().Setup(m => m.Expression).Returns(fotoData.Expression);
            mockSetFoto.As<IQueryable<Foto>>().Setup(m => m.ElementType).Returns(fotoData.ElementType);
            mockSetFoto.As<IQueryable<Foto>>().Setup(m => m.GetEnumerator()).Returns(fotoData.GetEnumerator());


            mockSetFoto.Setup(m => m.AsQueryable()).Returns(fotoData);


            ////Postulacion

            IQueryable<Postulacion> postulacionData = GetPostulacionData();
            var mockSetPostulacion = new Mock<DbSet<Postulacion>>();

            mockSetPostulacion.As<IQueryable<Postulacion>>().Setup(m => m.Provider).Returns(postulacionData.Provider);
            mockSetPostulacion.As<IQueryable<Postulacion>>().Setup(m => m.Expression).Returns(postulacionData.Expression);
            mockSetPostulacion.As<IQueryable<Postulacion>>().Setup(m => m.ElementType).Returns(postulacionData.ElementType);
            mockSetPostulacion.As<IQueryable<Postulacion>>().Setup(m => m.GetEnumerator()).Returns(postulacionData.GetEnumerator());


            mockSetPostulacion.Setup(m => m.AsQueryable()).Returns(postulacionData);


            //Evidencia

            IQueryable<Evidencia> evidenciaData = GetEvidenciaData();
            var mockSetEvidencia = new Mock<DbSet<Evidencia>>();

            mockSetEvidencia.As<IQueryable<Evidencia>>().Setup(m => m.Provider).Returns(evidenciaData.Provider);
            mockSetEvidencia.As<IQueryable<Evidencia>>().Setup(m => m.Expression).Returns(evidenciaData.Expression);
            mockSetEvidencia.As<IQueryable<Evidencia>>().Setup(m => m.ElementType).Returns(evidenciaData.ElementType);
            mockSetEvidencia.As<IQueryable<Evidencia>>().Setup(m => m.GetEnumerator()).Returns(evidenciaData.GetEnumerator());


            mockSetEvidencia.Setup(m => m.AsQueryable()).Returns(evidenciaData);



            //FotoEvidencia

            IQueryable<FotoEvidencia> fotoEvidenciaData = GetFotoEvidenciaData();
            var mockSetFotoEvidencia = new Mock<DbSet<FotoEvidencia>>();

            mockSetFotoEvidencia.As<IQueryable<FotoEvidencia>>().Setup(m => m.Provider).Returns(fotoEvidenciaData.Provider);
            mockSetFotoEvidencia.As<IQueryable<FotoEvidencia>>().Setup(m => m.Expression).Returns(fotoEvidenciaData.Expression);
            mockSetFotoEvidencia.As<IQueryable<FotoEvidencia>>().Setup(m => m.ElementType).Returns(fotoEvidenciaData.ElementType);
            mockSetFotoEvidencia.As<IQueryable<FotoEvidencia>>().Setup(m => m.GetEnumerator()).Returns(fotoEvidenciaData.GetEnumerator());


            mockSetFotoEvidencia.Setup(m => m.AsQueryable()).Returns(fotoEvidenciaData);



            //LikePostulacion

            IQueryable<LikePostulacion> likePostulacionData = GetLikePostulacionData();
            var mockSetLikePostulacion = new Mock<DbSet<LikePostulacion>>();

            mockSetLikePostulacion.As<IQueryable<LikePostulacion>>().Setup(m => m.Provider).Returns(likePostulacionData.Provider);
            mockSetLikePostulacion.As<IQueryable<LikePostulacion>>().Setup(m => m.Expression).Returns(likePostulacionData.Expression);
            mockSetLikePostulacion.As<IQueryable<LikePostulacion>>().Setup(m => m.ElementType).Returns(likePostulacionData.ElementType);
            mockSetLikePostulacion.As<IQueryable<LikePostulacion>>().Setup(m => m.GetEnumerator()).Returns(likePostulacionData.GetEnumerator());


            mockSetLikePostulacion.Setup(m => m.AsQueryable()).Returns(likePostulacionData);


            ////Donacion

            IQueryable<Donacion> donacionData = GetDonacionData();
            var mockSetDonacion = new Mock<DbSet<Donacion>>();

            mockSetDonacion.As<IQueryable<Donacion>>().Setup(m => m.Provider).Returns(donacionData.Provider);
            mockSetDonacion.As<IQueryable<Donacion>>().Setup(m => m.Expression).Returns(donacionData.Expression);
            mockSetDonacion.As<IQueryable<Donacion>>().Setup(m => m.ElementType).Returns(donacionData.ElementType);
            mockSetDonacion.As<IQueryable<Donacion>>().Setup(m => m.GetEnumerator()).Returns(donacionData.GetEnumerator());


            mockSetDonacion.Setup(m => m.AsQueryable()).Returns(donacionData);



            ////ReaccionPublicacion

            IQueryable<ReaccionPublicacion> reaccionPublicacionData = GetReaccionPublicacionData();
            var mockSetReaccionPublicacion = new Mock<DbSet<ReaccionPublicacion>>();

            mockSetReaccionPublicacion.As<IQueryable<ReaccionPublicacion>>().Setup(m => m.Provider).Returns(reaccionPublicacionData.Provider);
            mockSetReaccionPublicacion.As<IQueryable<ReaccionPublicacion>>().Setup(m => m.Expression).Returns(reaccionPublicacionData.Expression);
            mockSetReaccionPublicacion.As<IQueryable<ReaccionPublicacion>>().Setup(m => m.ElementType).Returns(reaccionPublicacionData.ElementType);
            mockSetReaccionPublicacion.As<IQueryable<ReaccionPublicacion>>().Setup(m => m.GetEnumerator()).Returns(reaccionPublicacionData.GetEnumerator());


            mockSetReaccionPublicacion.Setup(m => m.AsQueryable()).Returns(reaccionPublicacionData);





            //Comentario

            IQueryable<Comentario> comentarioData = GetComentarioData();
            var mockSetComentario = new Mock<DbSet<Comentario>>();

            mockSetComentario.As<IQueryable<Comentario>>().Setup(m => m.Provider).Returns(comentarioData.Provider);
            mockSetComentario.As<IQueryable<Comentario>>().Setup(m => m.Expression).Returns(comentarioData.Expression);
            mockSetComentario.As<IQueryable<Comentario>>().Setup(m => m.ElementType).Returns(comentarioData.ElementType);
            mockSetComentario.As<IQueryable<Comentario>>().Setup(m => m.GetEnumerator()).Returns(comentarioData.GetEnumerator());


            mockSetComentario.Setup(m => m.AsQueryable()).Returns(comentarioData);


            //ReplyComentario

            IQueryable<ReplyComentario> replyComentarioData = GetReplyComentarioData();
            var mockSetReplyComentario = new Mock<DbSet<ReplyComentario>>();

            mockSetReplyComentario.As<IQueryable<ReplyComentario>>().Setup(m => m.Provider).Returns(replyComentarioData.Provider);
            mockSetReplyComentario.As<IQueryable<ReplyComentario>>().Setup(m => m.Expression).Returns(replyComentarioData.Expression);
            mockSetReplyComentario.As<IQueryable<ReplyComentario>>().Setup(m => m.ElementType).Returns(replyComentarioData.ElementType);
            mockSetReplyComentario.As<IQueryable<ReplyComentario>>().Setup(m => m.GetEnumerator()).Returns(replyComentarioData.GetEnumerator());


            mockSetReplyComentario.Setup(m => m.AsQueryable()).Returns(replyComentarioData);

            //Caja

            IQueryable<Caja> CajaData = GetCajaData();
            var mockSetCaja = new Mock<DbSet<Caja>>();

            mockSetCaja.As<IQueryable<Caja>>().Setup(m => m.Provider).Returns(CajaData.Provider);
            mockSetCaja.As<IQueryable<Caja>>().Setup(m => m.Expression).Returns(CajaData.Expression);
            mockSetCaja.As<IQueryable<Caja>>().Setup(m => m.ElementType).Returns(CajaData.ElementType);
            mockSetCaja.As<IQueryable<Caja>>().Setup(m => m.GetEnumerator()).Returns(CajaData.GetEnumerator());


            mockSetCaja.Setup(m => m.AsQueryable()).Returns(CajaData);


            //publicacion

            IQueryable<Publicacion> publicacionData = GetPublicacionData();
            var mockSetPublicacion = new Mock<DbSet<Publicacion>>();

            mockSetPublicacion.As<IQueryable<Publicacion>>().Setup(m => m.Provider).Returns(publicacionData.Provider);
            mockSetPublicacion.As<IQueryable<Publicacion>>().Setup(m => m.Expression).Returns(publicacionData.Expression);
            mockSetPublicacion.As<IQueryable<Publicacion>>().Setup(m => m.ElementType).Returns(publicacionData.ElementType);
            mockSetPublicacion.As<IQueryable<Publicacion>>().Setup(m => m.GetEnumerator()).Returns(publicacionData.GetEnumerator());

            mockSetPublicacion.Setup(m => m.AsQueryable()).Returns(publicacionData);


            ////Tarjeta

            IQueryable<Tarjeta> tarjetaData = GetTarjetaData();
            var mockSetTarjeta = new Mock<DbSet<Tarjeta>>();

            mockSetTarjeta.As<IQueryable<Tarjeta>>().Setup(m => m.Provider).Returns(tarjetaData.Provider);
            mockSetTarjeta.As<IQueryable<Tarjeta>>().Setup(m => m.Expression).Returns(tarjetaData.Expression);
            mockSetTarjeta.As<IQueryable<Tarjeta>>().Setup(m => m.ElementType).Returns(tarjetaData.ElementType);
            mockSetTarjeta.As<IQueryable<Tarjeta>>().Setup(m => m.GetEnumerator()).Returns(tarjetaData.GetEnumerator());


            mockSetTarjeta.Setup(m => m.AsQueryable()).Returns(tarjetaData);







            ////creamos el mock del context , enviando su parametro necesario
            var mockContext = new Mock<AppTumismitoPeContext>(new DbContextOptions<AppTumismitoPeContext>());




            mockContext.Setup(c => c.Publicaciones).Returns(mockSetPublicacion.Object);

            mockContext.Setup(c => c.Cajas).Returns(mockSetCaja.Object);

            mockContext.Setup(c => c.Usuarios).Returns(mockSetUsuario.Object);
        
            mockContext.Setup(c => c.Comentarios).Returns(mockSetComentario.Object);
            mockContext.Setup(c => c.Donaciones).Returns(mockSetDonacion.Object);
            mockContext.Setup(c => c.Evidencias).Returns(mockSetEvidencia.Object);
            mockContext.Setup(c => c.FotoEvidencias).Returns(mockSetFotoEvidencia.Object);
            mockContext.Setup(c => c.Fotos).Returns(mockSetFoto.Object);
            mockContext.Setup(c => c.Postulaciones).Returns(mockSetPostulacion.Object);
            mockContext.Setup(c => c.LikePostulaciones).Returns(mockSetLikePostulacion.Object);
            mockContext.Setup(c => c.PerfilProfesionales).Returns(mockSetPerfilPro.Object);
            mockContext.Setup(c => c.ReaccionPublicaciones).Returns(mockSetReaccionPublicacion.Object);
            mockContext.Setup(c => c.ReplyComentarios).Returns(mockSetReplyComentario.Object);
            mockContext.Setup(c => c.Tarjetas).Returns(mockSetTarjeta.Object);








            return mockContext;
        }


        private static IQueryable<Caja> GetCajaData()
        {
            return new List<Caja>
            {


                new Caja{id=1,monto=200}




            }.AsQueryable();
        }

        private static IQueryable<FotoEvidencia> GetFotoEvidenciaData()
        {
            return new List<FotoEvidencia>
            {


                new FotoEvidencia{ id=1,contenido="/files/fotosEvicencias/foto01Post01.png",idEvidencia=1},
                new FotoEvidencia{ id=2,contenido="/files/fotosEvicencias/foto02Post01.png",idEvidencia=1},
                new FotoEvidencia{ id=3,contenido="/files/fotosEvicencias/foto03Post01.png",idEvidencia=1},
                new FotoEvidencia{ id=4,contenido="/files/fotosEvicencias/foto04Post01.png",idEvidencia=1},
                new FotoEvidencia{ id=5,contenido="/files/fotosEvicencias/foto05Post01.png",idEvidencia=1},
                new FotoEvidencia{ id=6,contenido="/files/fotosEvicencias/foto06Post01.png",idEvidencia=1},
                

                //new FotoEvidencia{ id=7,contenido="/files/fotosEvicencias/foto01Post02.png",idEvidencia=2},
                //new FotoEvidencia{ id=8,contenido="/files/fotosEvicencias/foto01Post02.png",idEvidencia=2},
                //new FotoEvidencia{ id=9,contenido="/files/fotosEvicencias/foto01Post02.png",idEvidencia=2},
                //new FotoEvidencia{ id=10,contenido="/files/fotosEvicencias/foto01Post02.png",idEvidencia=2},
                //new FotoEvidencia{ id=11,contenido="/files/fotosEvicencias/foto01Post02.png",idEvidencia=2},
                //new FotoEvidencia{ id=12,contenido="/files/fotosEvicencias/foto01Post02.png",idEvidencia=2},
                //new FotoEvidencia{ id=13,contenido="/files/fotosEvicencias/foto01Post02.png",idEvidencia=2},




            }.AsQueryable();
        }

        private static IQueryable<Foto> GetFotoData()
        {

            return new List<Foto>
            {

                new Foto{ id=1,contenido="/files/fotosPublicaciones/foto01Pub01.png",detalle="foto01Pub01.png",idPublicacion=1},
                new Foto{ id=2,contenido="/files/fotosPublicaciones/foto02Pub01.png",detalle="foto02Pub01.png",idPublicacion=1},
                new Foto{ id=3,contenido="/files/fotosPublicaciones/foto03Pub01.png",detalle="foto03Pub01.png",idPublicacion=1},
                new Foto{ id=4,contenido="/files/fotosPublicaciones/foto04Pub01.png",detalle="foto04Pub01.png",idPublicacion=1},
                new Foto{ id=5,contenido="/files/fotosPublicaciones/foto05Pub01.png",detalle="foto05Pub01.png",idPublicacion=1},

                new Foto{ id=6,contenido="/files/fotosPublicaciones/foto01Pub02.png",detalle="foto01Pub02.png",idPublicacion=2},
                new Foto{ id=7,contenido="/files/fotosPublicaciones/foto02Pub02.png",detalle="foto02Pub02.png",idPublicacion=2},
                new Foto{ id=8,contenido="/files/fotosPublicaciones/foto03Pub02.png",detalle="foto03Pub02.png",idPublicacion=2},
                new Foto{ id=9,contenido="/files/fotosPublicaciones/foto04Pub02.png",detalle="foto04Pub02.png",idPublicacion=2},

                new Foto{ id=10,contenido="/files/fotosPublicaciones/foto01Pub03.png",detalle="foto01Pub03.png",idPublicacion=3},
                new Foto{ id=11,contenido="/files/fotosPublicaciones/foto02Pub03.png",detalle="foto02Pub03.png",idPublicacion=3},
                new Foto{ id=12,contenido="/files/fotosPublicaciones/foto03Pub03.png",detalle="foto03Pub03.png",idPublicacion=3},
                new Foto{ id=13,contenido="/files/fotosPublicaciones/foto04Pub03.png",detalle="foto04Pub03.png",idPublicacion=3},

                new Foto{ id=14,contenido="/files/fotosPublicaciones/foto01Pub04.png",detalle="foto01Pub04.png",idPublicacion=4},
                new Foto{ id=15,contenido="/files/fotosPublicaciones/foto02Pub04.png",detalle="foto02Pub04.png",idPublicacion=4},

                new Foto{ id=16,contenido="/files/fotosPublicaciones/foto01Pub05.png",detalle="foto01Pub05.png",idPublicacion=5},

                new Foto{ id=17,contenido="/files/fotosPublicaciones/foto01Pub06.png",detalle="foto01Pub06.png",idPublicacion=6},

                new Foto{ id=18,contenido="/files/fotosPublicaciones/foto01Pub07.png",detalle="foto01Pub07.png",idPublicacion=7},
                new Foto{ id=19,contenido="/files/fotosPublicaciones/foto02Pub07.png",detalle="foto02Pub07.png",idPublicacion=7},

            }.AsQueryable();
        }

        private static IQueryable<Evidencia> GetEvidenciaData()
        {
            return new List<Evidencia>
            {
                new Evidencia{id=1,idPostulacion=3,descripcion="Postulacion03Evidencia",fotoEvidencias=GetFotoEvidenciaData().Where(o=>o.idEvidencia==1).ToList() },
                //new Evidencia{id=2,idPostulacion=5,descripcion="Postulacion05Evidencia",fotoEvidencias=GetFotoEvidenciaData().Where(o=>o.idEvidencia==2).ToList() }

            }.AsQueryable();
        }
        private static IQueryable<LikePostulacion> GetLikePostulacionData()
        {
            return new List<LikePostulacion>
            {


                new LikePostulacion{ id=1,idPostulacion=1,idUsuario=10,fechaLike=new DateTime(2021,03,09)},
                new LikePostulacion{ id=2,idPostulacion=1,idUsuario=8,fechaLike=new DateTime(2021,03,09)},
                new LikePostulacion{ id=3,idPostulacion=1,idUsuario=7,fechaLike=new DateTime(2021,03,10)},

                new LikePostulacion{ id=4,idPostulacion=2,idUsuario=5,fechaLike=new DateTime(2021,03,10)},

                new LikePostulacion{ id=5,idPostulacion=3,idUsuario=8,fechaLike=new DateTime(2021,03,14)},
                new LikePostulacion{ id=6,idPostulacion=3,idUsuario=3,fechaLike=new DateTime(2021,03,15)},

                new LikePostulacion{ id=7,idPostulacion=4,idUsuario=5,fechaLike=new DateTime(2021,04,07)},

                new LikePostulacion{ id=8,idPostulacion=5,idUsuario=1,fechaLike=new DateTime(2021,04,23)},
                new LikePostulacion{ id=9,idPostulacion=5,idUsuario=3,fechaLike=new DateTime(2021,04,25)},
                new LikePostulacion{ id=10,idPostulacion=5,idUsuario=8,fechaLike=new DateTime(2021,04,25)},

                new LikePostulacion{ id=11,idPostulacion=6,idUsuario=6,fechaLike=new DateTime(2021,04,27)},
                new LikePostulacion{ id=12,idPostulacion=6,idUsuario=9,fechaLike=new DateTime(2021,04,27)},

                new LikePostulacion{ id=13,idPostulacion=7,idUsuario=4,fechaLike=new DateTime(2021,05,05)},
                new LikePostulacion{ id=14,idPostulacion=7,idUsuario=3,fechaLike=new DateTime(2021,05,08)},


            }.AsQueryable();
        }













        private static IQueryable<Usuario> GetUserData()
        {
            //tipo 0 es comun y 1 trabajdor
            return new List<Usuario>
            {
                new Usuario { id=1,userName="alexRoque",pass="123456",tipo=0,estado=1,correo="alexRoque@gmail.com",fechaCreacion=new DateTime(2021,04,14),foto="https://imageAlexRoque",nombre="Alex",apellido="Roque",fechaNacimiento=new DateTime(1950,01,14),sexo="Masculino",direccion="Jr.Los Perdidos #354"                 /*  ,postulaciones=GetPostulacionData().Where(o=>o.idUsuario==1).ToList()*/,perfilProfesional=GetPerfilProfesionalData().FirstOrDefault(o=>o.idUsuario==1)},
                new Usuario { id=2,userName="rosmerySanchez",pass="123456",tipo=1,estado=1,correo="rosmeryS@gmail.com",fechaCreacion=new DateTime(2021,05,10),foto="https://imageRosmery",nombre="Rosmery",apellido="Sanchez",fechaNacimiento=new DateTime(1990,04,11),sexo="Femenino",direccion="Jr.Puno #124"                   /*  ,postulaciones=GetPostulacionData().Where(o=>o.idUsuario==2).ToList()*/,perfilProfesional=GetPerfilProfesionalData().FirstOrDefault(o=>o.idUsuario==2)},
                new Usuario { id=3,userName="yanMinchan",pass="123456",tipo=0,estado=1,correo="yanMinchan@gmail.com",fechaCreacion=new DateTime(2021,05,13),foto="https://imageYan",nombre="Yan",apellido="Minchan",fechaNacimiento=new DateTime(1949,02,11),sexo="Masculino",direccion="Jr.Esperanza #111"                       /*  ,postulaciones=GetPostulacionData().Where(o=>o.idUsuario==3).ToList()*/,perfilProfesional=GetPerfilProfesionalData().FirstOrDefault(o=>o.idUsuario==3)},
                new Usuario { id=4,userName="seleneOblitas",pass="123456",tipo=0,estado=1,correo="seleneOblitas@gmail.com",fechaCreacion=new DateTime(2021,06,01),foto="https://imageseleneOblitas",nombre="Selene",apellido="Oblitas",fechaNacimiento=new DateTime(1920,05,09),sexo="Femenino",direccion="Jr.Grau #963"          /*  ,postulaciones=GetPostulacionData().Where(o=>o.idUsuario==4).ToList()*/,perfilProfesional=GetPerfilProfesionalData().FirstOrDefault(o=>o.idUsuario==4)},
                new Usuario { id=5,userName="victorTeran",pass="123456",tipo=1,estado=1,correo="victorTeran@gmail.com",fechaCreacion=new DateTime(2021,06,06),foto="https://imagevictorTeran",nombre="Victor",apellido="Teran",fechaNacimiento=new DateTime(1955,05,21),sexo="Masculino",direccion="Jr.La Mar #513"               /*  ,postulaciones=GetPostulacionData().Where(o=>o.idUsuario==5).ToList()*/,perfilProfesional=GetPerfilProfesionalData().FirstOrDefault(o=>o.idUsuario==5)},
                new Usuario { id=6,userName="jorgeTorres",pass="123456",tipo=0,estado=1,correo="jorgeTorres@gmail.com",fechaCreacion=new DateTime(2021,06,23),foto="https://imagejorgeTorres",nombre="Jorge",apellido="Torres",fechaNacimiento=new DateTime(1920,06,25),sexo="Masculino",direccion="Jr.Angamos #578"              /*  ,postulaciones=GetPostulacionData().Where(o=>o.idUsuario==6).ToList()*/,perfilProfesional=GetPerfilProfesionalData().FirstOrDefault(o=>o.idUsuario==6)},
                new Usuario { id=7,userName="mariaRobles",pass="123456",tipo=1,estado=1,correo="mariaRobles@gmail.com",fechaCreacion=new DateTime(2021,06,23),foto="https://imagemariaRobles",nombre="Maria",apellido="Robles",fechaNacimiento=new DateTime(1950,04,14),sexo="Femenino",direccion="Jr.Peru #356"                  /*  ,postulaciones=GetPostulacionData().Where(o=>o.idUsuario==7).ToList()*/,perfilProfesional=GetPerfilProfesionalData().FirstOrDefault(o=>o.idUsuario==7)},
                new Usuario { id=8,userName="miguelSanchez",pass="123456",tipo=0,estado=1,correo="miguelSanchez@gmail.com",fechaCreacion=new DateTime(2021,06,25),foto="https://imagemiguelSanchez",nombre="Miguel",apellido="Sanchez",fechaNacimiento=new DateTime(1960,07,08),sexo="Masculino",direccion="Jr.Los Perdidos #264" /*  ,postulaciones=GetPostulacionData().Where(o=>o.idUsuario==8).ToList()*/,perfilProfesional=GetPerfilProfesionalData().FirstOrDefault(o=>o.idUsuario==8)},
                new Usuario { id=9,userName="lucyTeran",pass="123456",tipo=1,estado=1,correo="lucyTeran@gmail.com",fechaCreacion=new DateTime(2021,06,28),foto="https://imagelucyTeran",nombre="Lucy",apellido="Teran",fechaNacimiento=new DateTime(1930,11,22),sexo="Femenino",direccion="Jr.Los Esperanza #151"                 /*  ,postulaciones=GetPostulacionData().Where(o=>o.idUsuario==9).ToList()*/,perfilProfesional=GetPerfilProfesionalData().FirstOrDefault(o=>o.idUsuario==9)},
                new Usuario { id=10,userName="andresIniesta",pass="123456",tipo=0,estado=0,correo="andresIniesta@gmail.com",fechaCreacion=new DateTime(2021,06,28),foto="https://imageandresIniesta",nombre="Andres",apellido="Iniesta",fechaNacimiento=new DateTime(1933,08,06),sexo="Masculino",direccion="Jr.Los Chile #894"   /*,postulaciones=GetPostulacionData().Where(o=>o.idUsuario==10) .ToList()*/,perfilProfesional=GetPerfilProfesionalData().FirstOrDefault(o=>o.idUsuario==10)}

            }.AsQueryable();
        }

        private static IQueryable<Postulacion> GetPostulacionData()
        {

            //estado 0: esta en votacion
            //estado 1: fue elegido pero aun no completan monto
            //estado 2: ya debe estar trabjaando
            //estado 3: ya culmino







            return new List<Postulacion>
            {

                new Postulacion{ id=1,fechaCreacion=new DateTime(2021,03,08,1,30,30),idUsuario=2,idPublicacion=1,estado=0,monto=380,diasRealizacion=64,descripcion="Postulacion01Pub01",usuario=GetUserData().FirstOrDefault(o=>o.id==2)/*,publicacion= GetPublicacionData().FirstOrDefault(o=>o.id==1)*/,evidencia=GetEvidenciaData().FirstOrDefault(o=>o.idPostulacion==1),likesPostulacion=GetLikePostulacionData().Where(o=>o.idPostulacion==1).ToList()},
                new Postulacion{ id=2,fechaCreacion=new DateTime(2021,03,08,1,35,35),idUsuario=9,idPublicacion=1,estado=0,monto=400,diasRealizacion=58,descripcion="Postulacion02Pub01",usuario=GetUserData().FirstOrDefault(o=>o.id==9)/*,publicacion= GetPublicacionData().FirstOrDefault(o=>o.id==1)*/,evidencia=GetEvidenciaData().FirstOrDefault(o=>o.idPostulacion==2),likesPostulacion=GetLikePostulacionData().Where(o=>o.idPostulacion==2).ToList()},
                new Postulacion{ id=3,fechaCreacion=new DateTime(2021,03,13,2,31,31),idUsuario=5,idPublicacion=2,estado=3,monto=500,diasRealizacion=15,descripcion="Postulacion01Pub02",usuario=GetUserData().FirstOrDefault(o=>o.id==5)/*,publicacion= GetPublicacionData().FirstOrDefault(o=>o.id==2)*/,evidencia=GetEvidenciaData().FirstOrDefault(o=>o.idPostulacion==3),likesPostulacion=GetLikePostulacionData().Where(o=>o.idPostulacion==3).ToList()},
                new Postulacion{ id=4,fechaCreacion=new DateTime(2021,04,05,3,15,15),idUsuario=7,idPublicacion=4,estado=1,monto=300,diasRealizacion=18,descripcion="Postulacion01Pub04",usuario=GetUserData().FirstOrDefault(o=>o.id==7)/*,publicacion= GetPublicacionData().FirstOrDefault(o=>o.id==4)*/,evidencia=GetEvidenciaData().FirstOrDefault(o=>o.idPostulacion==4),likesPostulacion=GetLikePostulacionData().Where(o=>o.idPostulacion==4).ToList()},
                new Postulacion{ id=5,fechaCreacion=new DateTime(2021,04,23,3,11,11),idUsuario=9,idPublicacion=5,estado=3,monto=360,diasRealizacion=50,descripcion="Postulacion01Pub05",usuario=GetUserData().FirstOrDefault(o=>o.id==9)/*,publicacion= GetPublicacionData().FirstOrDefault(o=>o.id==5)*/,evidencia=GetEvidenciaData().FirstOrDefault(o=>o.idPostulacion==5),likesPostulacion=GetLikePostulacionData().Where(o=>o.idPostulacion==5).ToList()},
                new Postulacion{ id=6,fechaCreacion=new DateTime(2021,04,26,4,35,35),idUsuario=5,idPublicacion=6,estado=1,monto=180,diasRealizacion=11,descripcion="Postulacion01Pub06",usuario=GetUserData().FirstOrDefault(o=>o.id==5)/*,publicacion= GetPublicacionData().FirstOrDefault(o=>o.id==6)*/,evidencia=GetEvidenciaData().FirstOrDefault(o=>o.idPostulacion==6),likesPostulacion=GetLikePostulacionData().Where(o=>o.idPostulacion==6).ToList()},
                new Postulacion{ id=7,fechaCreacion=new DateTime(2021,05,05,3,06,06),idUsuario=7,idPublicacion=7,estado=3,monto=900,diasRealizacion=8, descripcion="Postulacion01Pub07" ,usuario=GetUserData().FirstOrDefault(o=>o.id==7)/*,publicacion= GetPublicacionData().FirstOrDefault(o=>o.id==7)*/,evidencia=GetEvidenciaData().FirstOrDefault(o=>o.idPostulacion==7),likesPostulacion=GetLikePostulacionData().Where(o=>o.idPostulacion==7).ToList()},


            }.AsQueryable();
        }
        
        private static IQueryable<Postulacion> GetPostulacionData02()
        {

            //estado 0: esta en votacion
            //estado 1: fue elegido pero aun no completan monto
            //estado 2: ya debe estar trabjaando
            //estado 3: ya culmino

    




           
            return new List<Postulacion>
            {

                new Postulacion{ id=1,fechaCreacion=new DateTime(2021,03,08,1,30,30),monto=380,diasRealizacion=64,estado=0,descripcion="Postulacion01Pub01",idUsuario=2,idPublicacion=1,usuario=GetUserData().FirstOrDefault(o=>o.id==2),publicacion= GetPublicacionData().FirstOrDefault(o=>o.id==1),evidencia=GetEvidenciaData().FirstOrDefault(o=>o.idPostulacion==1),likesPostulacion=GetLikePostulacionData().Where(o=>o.idPostulacion==1).ToList()},
                new Postulacion{ id=2,fechaCreacion=new DateTime(2021,03,08,1,35,35),monto=400,diasRealizacion=58,estado=0,descripcion="Postulacion02Pub01",idUsuario=9,idPublicacion=1,usuario=GetUserData().FirstOrDefault(o=>o.id==9),publicacion= GetPublicacionData().FirstOrDefault(o=>o.id==1),evidencia=GetEvidenciaData().FirstOrDefault(o=>o.idPostulacion==2),likesPostulacion=GetLikePostulacionData().Where(o=>o.idPostulacion==2).ToList()},
                new Postulacion{ id=3,fechaCreacion=new DateTime(2021,03,13,2,31,31),monto=500,diasRealizacion=15,estado=3,descripcion="Postulacion01Pub02",idUsuario=5,idPublicacion=2,usuario=GetUserData().FirstOrDefault(o=>o.id==5),publicacion= GetPublicacionData().FirstOrDefault(o=>o.id==2),evidencia=GetEvidenciaData().FirstOrDefault(o=>o.idPostulacion==3),likesPostulacion=GetLikePostulacionData().Where(o=>o.idPostulacion==3).ToList()},
                new Postulacion{ id=4,fechaCreacion=new DateTime(2021,04,05,3,15,15),monto=300,diasRealizacion=18,estado=1,descripcion="Postulacion01Pub04",idUsuario=7,idPublicacion=4,usuario=GetUserData().FirstOrDefault(o=>o.id==7),publicacion= GetPublicacionData().FirstOrDefault(o=>o.id==4),evidencia=GetEvidenciaData().FirstOrDefault(o=>o.idPostulacion==4),likesPostulacion=GetLikePostulacionData().Where(o=>o.idPostulacion==4).ToList()},
                new Postulacion{ id=5,fechaCreacion=new DateTime(2021,04,23,3,11,11),monto=360,diasRealizacion=50,estado=3,descripcion="Postulacion01Pub05",idUsuario=9,idPublicacion=5,usuario=GetUserData().FirstOrDefault(o=>o.id==9),publicacion= GetPublicacionData().FirstOrDefault(o=>o.id==5),evidencia=GetEvidenciaData().FirstOrDefault(o=>o.idPostulacion==5),likesPostulacion=GetLikePostulacionData().Where(o=>o.idPostulacion==5).ToList()},
                new Postulacion{ id=6,fechaCreacion=new DateTime(2021,04,26,4,35,35),monto=180,diasRealizacion=11,estado=1,descripcion="Postulacion01Pub06",idUsuario=5,idPublicacion=6,usuario=GetUserData().FirstOrDefault(o=>o.id==5),publicacion= GetPublicacionData().FirstOrDefault(o=>o.id==6),evidencia=GetEvidenciaData().FirstOrDefault(o=>o.idPostulacion==6),likesPostulacion=GetLikePostulacionData().Where(o=>o.idPostulacion==6).ToList()},
                new Postulacion{ id=7,fechaCreacion=new DateTime(2021,05,05,3,06,06),monto=900,diasRealizacion=8,estado=3,descripcion="Postulacion01Pub07",idUsuario=7,idPublicacion=7,usuario=GetUserData().FirstOrDefault(o=>o.id==7),publicacion= GetPublicacionData().FirstOrDefault(o=>o.id==7), evidencia=GetEvidenciaData().FirstOrDefault(o=>o.idPostulacion==7),likesPostulacion=GetLikePostulacionData().Where(o=>o.idPostulacion==7).ToList()},


            }.AsQueryable();
        }
        private static IQueryable<Publicacion> GetPublicacionData()
        {

            //estado 0= estado al crear
            //estado 1= publicacion culminada y con evidencia
            //estado 2= con postulacion fijada pero aun recaudando
            //estado 3= trabajando en publicacion pero aun no dada por culminada

            

            return new List<Publicacion>
            {

                new Publicacion{id=1,descripcion="descripcion de publicacion01",estado=0,vista=13,titulo="Publicacion01",fechaCreacion=new DateTime(2021,03,01),limitePostulaciones=new DateTime(2021,03,15,1,30,30),idUsuario=1,usuario=GetUserData().FirstOrDefault(o=>o.id==1),montoAsignado=0,postulaciones=GetPostulacionData().Where(o=>o.idPublicacion==1).ToList(),donaciones=GetDonacionData().Where(o=>o.idPublicacion==1).ToList(),reacciones=GetReaccionPublicacionData().Where(o=>o.idPublicacion==1).ToList() ,fotos=GetFotoData().Where(o=>o.idPublicacion==1).ToList() ,comentarios=GetComentarioData().Where(o=>o.idPublicacion==1).ToList()},
                new Publicacion{id=2,descripcion="descripcion de publicacion02",estado=1,vista=23,titulo="Publicacion02",fechaCreacion=new DateTime(2021,03,05),limitePostulaciones=new DateTime(2021,03,20,2,31,31),idUsuario=3,usuario=GetUserData().FirstOrDefault(o=>o.id==3),montoAsignado=500,postulaciones=GetPostulacionData().Where(o=>o.idPublicacion==2).ToList(),donaciones=GetDonacionData().Where(o=>o.idPublicacion==2).ToList(),reacciones=GetReaccionPublicacionData().Where(o=>o.idPublicacion==2).ToList(),fotos=GetFotoData().Where(o=>o.idPublicacion==2).ToList(),comentarios=GetComentarioData().Where(o=>o.idPublicacion==2).ToList() },

                new Publicacion{id=3,descripcion="descripcion de publicacion03",estado=0,vista=0,titulo="Publicacion03",fechaCreacion=new DateTime(2021,03,10),limitePostulaciones=new DateTime(9999,12,31,00,00,00),idUsuario=6,usuario=GetUserData().FirstOrDefault(o=>o.id==6),montoAsignado=0,postulaciones=GetPostulacionData().Where(o=>o.idPublicacion==3).ToList(),donaciones=GetDonacionData().Where(o=>o.idPublicacion==3).ToList(),reacciones=GetReaccionPublicacionData().Where(o=>o.idPublicacion==3).ToList(),fotos=GetFotoData().Where(o=>o.idPublicacion==3).ToList() ,comentarios=GetComentarioData().Where(o=>o.idPublicacion==3).ToList()},

                new Publicacion{id=4,descripcion="descripcion de publicacion04",estado=2,vista=97,titulo="Publicacion04",fechaCreacion=new DateTime(2021,03,25),limitePostulaciones=new DateTime(2021,04,12,3,15,15),idUsuario=8,usuario=GetUserData().FirstOrDefault(o=>o.id==8),montoAsignado=300,postulaciones=GetPostulacionData().Where(o=>o.idPublicacion==4).ToList(),donaciones=GetDonacionData().Where(o=>o.idPublicacion==4).ToList(),reacciones=GetReaccionPublicacionData().Where(o=>o.idPublicacion==4).ToList(),fotos=GetFotoData().Where(o=>o.idPublicacion==4).ToList(),comentarios=GetComentarioData().Where(o=>o.idPublicacion==4).ToList() },
                new Publicacion{id=5,descripcion="descripcion de publicacion05",estado=3,vista=30,titulo="Publicacion05",fechaCreacion=new DateTime(2021,04,15),limitePostulaciones=new DateTime(2021,04,30,3,11,11),idUsuario=10,usuario=GetUserData().FirstOrDefault(o=>o.id==10),montoAsignado=360 ,postulaciones=GetPostulacionData().Where(o=>o.idPublicacion==5).ToList(),donaciones=GetDonacionData().Where(o=>o.idPublicacion==5).ToList(),reacciones=GetReaccionPublicacionData().Where(o=>o.idPublicacion==5).ToList(),fotos=GetFotoData().Where(o=>o.idPublicacion==5).ToList(),comentarios=GetComentarioData().Where(o=>o.idPublicacion==5).ToList()},
                new Publicacion{id=6,descripcion="descripcion de publicacion06",estado=2,vista=51,titulo="Publicacion06",fechaCreacion=new DateTime(2021,04,18),limitePostulaciones=new DateTime(2021,05,03,4,35,35),idUsuario=3,usuario=GetUserData().FirstOrDefault(o=>o.id==3),montoAsignado=180,postulaciones=GetPostulacionData().Where(o=>o.idPublicacion==6).ToList() ,donaciones=GetDonacionData().Where(o=>o.idPublicacion==6).ToList(),reacciones=GetReaccionPublicacionData().Where(o=>o.idPublicacion==6).ToList(),fotos=GetFotoData().Where(o=>o.idPublicacion==6).ToList(),comentarios=GetComentarioData().Where(o=>o.idPublicacion==6).ToList()},
                new Publicacion{id=7,descripcion="descripcion de publicacion07",estado=3,vista=35,titulo="Publicacion07",fechaCreacion=new DateTime(2021,04,26),limitePostulaciones=new DateTime(2021,05,12,3,06,06),idUsuario=10,usuario=GetUserData().FirstOrDefault(o=>o.id==10),montoAsignado=900,postulaciones=GetPostulacionData().Where(o=>o.idPublicacion==7).ToList(),donaciones=GetDonacionData().Where(o=>o.idPublicacion==7).ToList(),reacciones=GetReaccionPublicacionData().Where(o=>o.idPublicacion==7).ToList() ,fotos=GetFotoData().Where(o=>o.idPublicacion==7).ToList(),comentarios=GetComentarioData().Where(o=>o.idPublicacion==7).ToList()},

            }.AsQueryable();
        }

       

        private static IQueryable<ReplyComentario> GetReplyComentarioData()
        {
            return new List<ReplyComentario>
            {


                new ReplyComentario{ id=1,contenido="Reply01Comentario01",fecha=new DateTime(2021,03,02),idUsuario=8,idComentario=1,usuario=GetUserData().FirstOrDefault(o=>o.id==8) },
                new ReplyComentario{ id=2,contenido="Reply02Comentario01",fecha=new DateTime(2021,03,03),idUsuario=5,idComentario=1,usuario=GetUserData().FirstOrDefault(o=>o.id==5) },

                new ReplyComentario{ id=3,contenido="Reply01Comentario02",fecha=new DateTime(2021,03,05),idUsuario=9,idComentario=2,usuario=GetUserData().FirstOrDefault(o=>o.id==9) },
                new ReplyComentario{ id=4,contenido="Reply02Comentario02",fecha=new DateTime(2021,03,08),idUsuario=2,idComentario=2,usuario=GetUserData().FirstOrDefault(o=>o.id==2) },
                new ReplyComentario{ id=5,contenido="Reply03Comentario02",fecha=new DateTime(2021,03,10),idUsuario=1,idComentario=2,usuario=GetUserData().FirstOrDefault(o=>o.id==1) },

                new ReplyComentario{ id=6,contenido="Reply01Comentario03",fecha=new DateTime(2021,03,08),idUsuario=7,idComentario=3,usuario=GetUserData().FirstOrDefault(o=>o.id==7) },

                new ReplyComentario{ id=7,contenido="Reply01Comentario04",fecha=new DateTime(2021,03,10),idUsuario=4,idComentario=4,usuario=GetUserData().FirstOrDefault(o=>o.id==4) },
                new ReplyComentario{ id=8,contenido="Reply02Comentario04",fecha=new DateTime(2021,03,10),idUsuario=8,idComentario=4,usuario=GetUserData().FirstOrDefault(o=>o.id==8) },

                new ReplyComentario{ id=9,contenido="Reply01Comentario05",fecha=new DateTime(2021,03,15),idUsuario=4,idComentario=5,usuario=GetUserData().FirstOrDefault(o=>o.id==4) },
                new ReplyComentario{ id=10,contenido="Reply02Comentario05",fecha=new DateTime(2021,03,17),idUsuario=3,idComentario=5,usuario=GetUserData().FirstOrDefault(o=>o.id==3) },
                new ReplyComentario{ id=11,contenido="Reply03Comentario05",fecha=new DateTime(2021,03,17),idUsuario=2,idComentario=5,usuario=GetUserData().FirstOrDefault(o=>o.id==2) },
                new ReplyComentario{ id=12,contenido="Reply04Comentario05",fecha=new DateTime(2021,03,18),idUsuario=6,idComentario=5,usuario=GetUserData().FirstOrDefault(o=>o.id==6) },

                new ReplyComentario{ id=13,contenido="Reply01Comentario06",fecha=new DateTime(2021,03,15),idUsuario=6,idComentario=6,usuario=GetUserData().FirstOrDefault(o=>o.id==6) },

                new ReplyComentario{ id=14,contenido="Reply01Comentario07",fecha=new DateTime(2021,03,14),idUsuario=1,idComentario=7,usuario=GetUserData().FirstOrDefault(o=>o.id==1) },
                new ReplyComentario{ id=15,contenido="Reply02Comentario07",fecha=new DateTime(2021,03,15),idUsuario=7,idComentario=7,usuario=GetUserData().FirstOrDefault(o=>o.id==7) },


                new ReplyComentario{ id=16,contenido="Reply01Comentario08",fecha=new DateTime(2021,03,21),idUsuario=6,idComentario=8,usuario=GetUserData().FirstOrDefault(o=>o.id==6) },

                //comenatrio 09 sin reply

                new ReplyComentario{ id=17,contenido="Reply01Comentario10",fecha=new DateTime(2021,04,05),idUsuario=4,idComentario=10,usuario=GetUserData().FirstOrDefault(o=>o.id==4) },


                new ReplyComentario{ id=18,contenido="Reply01Comentario11",fecha=new DateTime(2021,04,20),idUsuario=2,idComentario=11,usuario=GetUserData().FirstOrDefault(o=>o.id==2) },
                new ReplyComentario{ id=19,contenido="Reply02Comentario11",fecha=new DateTime(2021,04,21),idUsuario=8,idComentario=11,usuario=GetUserData().FirstOrDefault(o=>o.id==8) },

                new ReplyComentario{ id=20,contenido="Reply01Comentario12",fecha=new DateTime(2021,04,21),idUsuario=3,idComentario=12,usuario=GetUserData().FirstOrDefault(o=>o.id==3) },
                new ReplyComentario{ id=21,contenido="Reply02Comentario12",fecha=new DateTime(2021,04,21),idUsuario=1,idComentario=12,usuario=GetUserData().FirstOrDefault(o=>o.id==1) },
                new ReplyComentario{ id=22,contenido="Reply03Comentario12",fecha=new DateTime(2021,04,25),idUsuario=7,idComentario=12,usuario=GetUserData().FirstOrDefault(o=>o.id==7) },

                new ReplyComentario{ id=23,contenido="Reply01Comentario13",fecha=new DateTime(2021,04,25),idUsuario=5,idComentario=13,usuario=GetUserData().FirstOrDefault(o=>o.id==5) },

                new ReplyComentario{ id=24,contenido="Reply01Comentario14",fecha=new DateTime(2021,04,22),idUsuario=9,idComentario=14,usuario=GetUserData().FirstOrDefault(o=>o.id==9) },

                new ReplyComentario{ id=25,contenido="Reply01Comentario15",fecha=new DateTime(2021,05,01),idUsuario=6,idComentario=15,usuario=GetUserData().FirstOrDefault(o=>o.id==6) },
                new ReplyComentario{ id=26,contenido="Reply02Comentario15",fecha=new DateTime(2021,05,03),idUsuario=3,idComentario=15,usuario=GetUserData().FirstOrDefault(o=>o.id==3) },



                new ReplyComentario{ id=27,contenido="Reply01Comentario16",fecha=new DateTime(2021,05,05),idUsuario=8,idComentario=16,usuario=GetUserData().FirstOrDefault(o=>o.id==8) },

            }.AsQueryable();
        }

        private static IQueryable<Comentario> GetComentarioData()
        {
            return new List<Comentario>
            {
                new Comentario{ id=1,idPublicacion=1,idUsuario=3,contenido="Comentario01Pub01",usuario=GetUserData().FirstOrDefault(o=>o.id==3),fecha=new DateTime(2021,03,01),replyComentarios=GetReplyComentarioData().Where(o=>o.idComentario==1).ToList() },
                new Comentario{ id=2,idPublicacion=1,idUsuario=2,contenido="Comentario02Pub01",usuario=GetUserData().FirstOrDefault(o=>o.id==2),fecha=new DateTime(2021,03,02) ,replyComentarios=GetReplyComentarioData().Where(o=>o.idComentario==2).ToList() },
                new Comentario{ id=3,idPublicacion=1,idUsuario=6,contenido="Comentario03Pub01",usuario=GetUserData().FirstOrDefault(o=>o.id==6),fecha=new DateTime(2021,03,07),replyComentarios=GetReplyComentarioData().Where(o=>o.idComentario==3).ToList()  },

                new Comentario{ id=4,idPublicacion=2,idUsuario=9,contenido="Comentario01Pub02",usuario=GetUserData().FirstOrDefault(o=>o.id==9),fecha=new DateTime(2021,03,08) ,replyComentarios=GetReplyComentarioData().Where(o=>o.idComentario==4).ToList() },

                new Comentario{ id=5,idPublicacion=3,idUsuario=1,contenido="Comentario01Pub03",usuario=GetUserData().FirstOrDefault(o=>o.id==1),fecha=new DateTime(2021,03,11 ),replyComentarios=GetReplyComentarioData().Where(o=>o.idComentario==5).ToList() },
                new Comentario{ id=6,idPublicacion=3,idUsuario=7,contenido="Comentario02Pub03",usuario=GetUserData().FirstOrDefault(o=>o.id==7),fecha=new DateTime(2021,03,12) ,replyComentarios=GetReplyComentarioData().Where(o=>o.idComentario==6).ToList() },
                new Comentario{ id=7,idPublicacion=3,idUsuario=5,contenido="Comentario03Pub03",usuario=GetUserData().FirstOrDefault(o=>o.id==5),fecha=new DateTime(2021,03,12),replyComentarios=GetReplyComentarioData().Where(o=>o.idComentario==7).ToList()  },
                new Comentario{ id=8,idPublicacion=3,idUsuario=8,contenido="Comentario04Pub03",usuario=GetUserData().FirstOrDefault(o=>o.id==8),fecha=new DateTime(2021,03,18) ,replyComentarios=GetReplyComentarioData().Where(o=>o.idComentario==8).ToList() },

                new Comentario{ id=9,idPublicacion=4,idUsuario=6,contenido="Comentario01Pub04",usuario=GetUserData().FirstOrDefault(o=>o.id==6),fecha=new DateTime(2021,03,28) ,replyComentarios=GetReplyComentarioData().Where(o=>o.idComentario==9).ToList() },
                new Comentario{ id=10,idPublicacion=4,idUsuario=2,contenido="Comentario02Pub04",usuario=GetUserData().FirstOrDefault(o=>o.id==2),fecha=new DateTime(2021,04,01) ,replyComentarios=GetReplyComentarioData().Where(o=>o.idComentario==10).ToList() },

                new Comentario{ id=11,idPublicacion=5,idUsuario=1,contenido="Comentario01Pub05",usuario=GetUserData().FirstOrDefault(o=>o.id==1),fecha=new DateTime(2021,04,18) ,replyComentarios=GetReplyComentarioData().Where(o=>o.idComentario==11).ToList() },
                new Comentario{ id=12,idPublicacion=5,idUsuario=4,contenido="Comentario02Pub05",usuario=GetUserData().FirstOrDefault(o=>o.id==4),fecha=new DateTime(2021,04,19) ,replyComentarios=GetReplyComentarioData().Where(o=>o.idComentario==12).ToList() },
                new Comentario{ id=13,idPublicacion=5,idUsuario=5,contenido="Comentario03Pub05",usuario=GetUserData().FirstOrDefault(o=>o.id==5),fecha=new DateTime(2021,04,24) ,replyComentarios=GetReplyComentarioData().Where(o=>o.idComentario==13).ToList() },

                new Comentario{ id=14,idPublicacion=6,idUsuario=7,contenido="Comentario01Pub06",usuario=GetUserData().FirstOrDefault(o=>o.id==7),fecha=new DateTime(2021,04,21),replyComentarios=GetReplyComentarioData().Where(o=>o.idComentario==14).ToList()  },

                new Comentario{ id=15,idPublicacion=7,idUsuario=9,contenido="Comentario01Pub07",usuario=GetUserData().FirstOrDefault(o=>o.id==9),fecha=new DateTime(2021,04,30) ,replyComentarios=GetReplyComentarioData().Where(o=>o.idComentario==15).ToList() },
                new Comentario{ id=16,idPublicacion=7,idUsuario=8,contenido="Comentario02Pub07",usuario=GetUserData().FirstOrDefault(o=>o.id==8),fecha=new DateTime(2021,05,02) ,replyComentarios=GetReplyComentarioData().Where(o=>o.idComentario==16).ToList() }

            }.AsQueryable();
        }

       



        private static IQueryable<ReaccionPublicacion> GetReaccionPublicacionData()
        {
            //0 dislike, 1 like

            return new List<ReaccionPublicacion>
            {
                new ReaccionPublicacion{ id=1,tipo=0,fechaCreacion=new DateTime(2021,03,02),idPublicacion=1,idUsuario=8,usuario=GetUserData().FirstOrDefault(o=>o.id==8)},
                new ReaccionPublicacion{ id=2,tipo=1,fechaCreacion=new DateTime(2021,03,03),idPublicacion=1,idUsuario=2,usuario=GetUserData().FirstOrDefault(o=>o.id==2)},
                new ReaccionPublicacion{ id=3,tipo=1,fechaCreacion=new DateTime(2021,03,04),idPublicacion=1,idUsuario=1,usuario=GetUserData().FirstOrDefault(o=>o.id==1)},
                new ReaccionPublicacion{ id=4,tipo=1,fechaCreacion=new DateTime(2021,03,04),idPublicacion=1,idUsuario=9,usuario=GetUserData().FirstOrDefault(o=>o.id==9)},

                new ReaccionPublicacion{ id=5,tipo=0,fechaCreacion=new DateTime(2021,03,06),idPublicacion=2,idUsuario=1,usuario=GetUserData().FirstOrDefault(o=>o.id==1)},
                new ReaccionPublicacion{ id=6,tipo=0,fechaCreacion=new DateTime(2021,03,06),idPublicacion=2,idUsuario=9,usuario=GetUserData().FirstOrDefault(o=>o.id==9)},
                new ReaccionPublicacion{ id=7,tipo=0,fechaCreacion=new DateTime(2021,03,07),idPublicacion=2,idUsuario=6,usuario=GetUserData().FirstOrDefault(o=>o.id==6)},
                new ReaccionPublicacion{ id=8,tipo=1,fechaCreacion=new DateTime(2021,03,08),idPublicacion=2,idUsuario=4,usuario=GetUserData().FirstOrDefault(o=>o.id==4)},

                new ReaccionPublicacion{ id=9,tipo=1,fechaCreacion=new DateTime(2021,03,10),idPublicacion=3,idUsuario=6,usuario=GetUserData().FirstOrDefault(o=>o.id==6)},
                new ReaccionPublicacion{ id=10,tipo=1,fechaCreacion=new DateTime(2021,03,11),idPublicacion=3,idUsuario=3,usuario=GetUserData().FirstOrDefault(o=>o.id==3)},
                new ReaccionPublicacion{ id=11,tipo=1,fechaCreacion=new DateTime(2021,03,15),idPublicacion=3,idUsuario=2,usuario=GetUserData().FirstOrDefault(o=>o.id==2)},
                new ReaccionPublicacion{ id=12,tipo=1,fechaCreacion=new DateTime(2021,03,17),idPublicacion=3,idUsuario=7,usuario=GetUserData().FirstOrDefault(o=>o.id==7)},


                new ReaccionPublicacion{ id=13,tipo=1,fechaCreacion=new DateTime(2021,03,28),idPublicacion=4,idUsuario=8,usuario=GetUserData().FirstOrDefault(o=>o.id==8)},


                new ReaccionPublicacion{ id=14,tipo=0,fechaCreacion=new DateTime(2021,04,20),idPublicacion=5,idUsuario=1,usuario=GetUserData().FirstOrDefault(o=>o.id==1)},
                new ReaccionPublicacion{ id=15,tipo=0,fechaCreacion=new DateTime(2021,04,21),idPublicacion=5,idUsuario=4,usuario=GetUserData().FirstOrDefault(o=>o.id==4)},
                new ReaccionPublicacion{ id=16,tipo=1,fechaCreacion=new DateTime(2021,04,22),idPublicacion=5,idUsuario=8,usuario=GetUserData().FirstOrDefault(o=>o.id==8)},


                new ReaccionPublicacion{ id=17,tipo=0,fechaCreacion=new DateTime(2021,04,22),idPublicacion=6,idUsuario=3,usuario=GetUserData().FirstOrDefault(o=>o.id==3)},
                new ReaccionPublicacion{ id=18,tipo=0,fechaCreacion=new DateTime(2021,04,22),idPublicacion=6,idUsuario=6,usuario=GetUserData().FirstOrDefault(o=>o.id==6)},
                new ReaccionPublicacion{ id=19,tipo=0,fechaCreacion=new DateTime(2021,04,23),idPublicacion=6,idUsuario=9,usuario=GetUserData().FirstOrDefault(o=>o.id==9)},


                new ReaccionPublicacion{ id=20,tipo=0,fechaCreacion=new DateTime(2021,04,26),idPublicacion=7,idUsuario=7,usuario=GetUserData().FirstOrDefault(o=>o.id==7)},
                new ReaccionPublicacion{ id=21,tipo=0,fechaCreacion=new DateTime(2021,04,27),idPublicacion=7,idUsuario=3,usuario=GetUserData().FirstOrDefault(o=>o.id==3)},
                new ReaccionPublicacion{ id=22,tipo=1,fechaCreacion=new DateTime(2021,04,27),idPublicacion=7,idUsuario=2,usuario=GetUserData().FirstOrDefault(o=>o.id==2)},
                new ReaccionPublicacion{ id=23,tipo=0,fechaCreacion=new DateTime(2021,04,29),idPublicacion=7,idUsuario=5,usuario=GetUserData().FirstOrDefault(o=>o.id==5)}

            }.AsQueryable();
        }
        private static IQueryable<Donacion> GetDonacionData()
        {
            return new List<Donacion>
            {

                new Donacion{ id=1,monto=100,canalPago="Yape",idUsuario=6,idPublicacion=1,  fecha=new DateTime(2021,03,02) ,usuario=GetUserData().FirstOrDefault(o=>o.id==6)},
                new Donacion{ id=2,monto=10,canalPago="PayPal",idUsuario=9,idPublicacion=1, fecha=new DateTime(2021,03,03),usuario=GetUserData().FirstOrDefault(o=>o.id==9) },
                new Donacion{ id=3,monto=50,canalPago="Yape",idUsuario=7,idPublicacion=1,   fecha=new DateTime(2021,03,03),usuario=GetUserData().FirstOrDefault(o=>o.id==7) },

                new Donacion{ id=4,monto=300,canalPago="Tunki",idUsuario=5,idPublicacion=2 ,fecha=new DateTime(2021,03,05),usuario=GetUserData().FirstOrDefault(o=>o.id==5)},
                new Donacion{ id=5,monto=200,canalPago="PayPal",idUsuario=1,idPublicacion=2,fecha=new DateTime(2021,03,06) ,usuario=GetUserData().FirstOrDefault(o=>o.id==1)},

                new Donacion{ id=6,monto=100,canalPago="Yape",idUsuario=8,idPublicacion=4,  fecha=new DateTime(2021,03,25) ,usuario=GetUserData().FirstOrDefault(o=>o.id==8)},
                new Donacion{ id=7,monto=50,canalPago="Tunki",idUsuario=3,idPublicacion=4 , fecha=new DateTime(2021,03,26),usuario=GetUserData().FirstOrDefault(o=>o.id==3)},

                new Donacion{ id=8,monto=300,canalPago="Yape",idUsuario=1,idPublicacion=5,  fecha=new DateTime(2021,04,16) ,usuario=GetUserData().FirstOrDefault(o=>o.id==1)},
                new Donacion{ id=9,monto=60,canalPago="Tunki",idUsuario=2,idPublicacion=5,  fecha=new DateTime(2021,04,16),usuario=GetUserData().FirstOrDefault(o=>o.id==2) },


                new Donacion{ id=10,monto=90,canalPago="PayPal",idUsuario=4,idPublicacion=6 ,fecha=new DateTime(2021,04,18),usuario=GetUserData().FirstOrDefault(o=>o.id==4)},
                new Donacion{ id=11,monto=30,canalPago="Tunki",idUsuario=2,idPublicacion=6 , fecha=new DateTime(2021,04,20),usuario=GetUserData().FirstOrDefault(o=>o.id==2)},



                new Donacion{ id=12,monto=300,canalPago="PayPal",idUsuario=1,idPublicacion=7,fecha=new DateTime(2021,04,28),usuario=GetUserData().FirstOrDefault(o=>o.id==1) },
                new Donacion{ id=13,monto=100,canalPago="Tunki",idUsuario=2,idPublicacion=7 ,fecha=new DateTime(2021,04,29),usuario=GetUserData().FirstOrDefault(o=>o.id==2)},
                new Donacion{ id=14,monto=320,canalPago="PayPal",idUsuario=6,idPublicacion=7,fecha=new DateTime(2021,04,29),usuario=GetUserData().FirstOrDefault(o=>o.id==6) },
                new Donacion{ id=15,monto=180,canalPago="Yape",idUsuario=5,idPublicacion=7 , fecha=new DateTime(2021,04,29),usuario=GetUserData().FirstOrDefault(o=>o.id==5)},

            }.AsQueryable();
        }
        private static IQueryable<PerfilProfesional> GetPerfilProfesionalData()
        {
            return new List<PerfilProfesional>
            {
                new PerfilProfesional{ id=1,dni=22222222,telefono=929292922,descripcion="Bueno",reputacion=80,estado=1,especialidad="Electricista",documentacion="http:/2.doc",idUsuario=2},
                new PerfilProfesional{ id=2,dni=55555555,telefono=959595955,descripcion="Regular",reputacion=50,estado=1,especialidad="Carpintero",documentacion="http:/5.doc",idUsuario=5},
                new PerfilProfesional{ id=3,dni=77777777,telefono=979797977,descripcion="Bueno",reputacion=80,estado=1,especialidad="Electricista",documentacion="http:/7.doc",idUsuario=7},
                new PerfilProfesional{ id=4,dni=99999999,telefono=999999999,descripcion="Muy Bueno",reputacion=100,estado=1,especialidad="Gasfitero",documentacion="http:/9.doc",idUsuario=9}
            }.AsQueryable();
        }


        private static IQueryable<Tarjeta> GetTarjetaData()
        {
            return new List<Tarjeta>
            {


                new Tarjeta{ id=1,numero=12345678,nombre="Alex",apellido="Roque",mesVencimiento=10,anioVencimiento=22,codigoSeguridad=123,saldo=5000},
                new Tarjeta{ id=2,numero=22222222,nombre="Rosmery",apellido="Sanchez",mesVencimiento=11,anioVencimiento=22,codigoSeguridad=123,saldo=100000},
                new Tarjeta{ id=3,numero=33333333,nombre="Yan",apellido="Minchan",mesVencimiento=10,anioVencimiento=25,codigoSeguridad=123,saldo=3000},
                new Tarjeta{ id=4,numero=44444444,nombre="Selene",apellido="Oblitas",mesVencimiento=15,anioVencimiento=23,codigoSeguridad=123,saldo=2000},
                new Tarjeta{ id=5,numero=55555555,nombre="Victor",apellido="Teran",mesVencimiento=05,anioVencimiento=26,codigoSeguridad=123,saldo=1000},
                new Tarjeta{ id=6,numero=66666666,nombre="Jorge",apellido="Torres",mesVencimiento=06,anioVencimiento=23,codigoSeguridad=123,saldo=800},
                new Tarjeta{ id=7,numero=77777777,nombre="Maria",apellido="Robles",mesVencimiento=01,anioVencimiento=28,codigoSeguridad=123,saldo=4000},
                new Tarjeta{ id=8,numero=88888888,nombre="Miguel",apellido="Sanchez",mesVencimiento=07,anioVencimiento=23,codigoSeguridad=123,saldo=6500},
                new Tarjeta{ id=9,numero=99999999,nombre="Lucy",apellido="Teran",mesVencimiento=12,anioVencimiento=25,codigoSeguridad=123,saldo=2050},
                new Tarjeta{ id=10,numero=11111111,nombre="Andres",apellido="Iniesta",mesVencimiento=11,anioVencimiento=24,codigoSeguridad=123,saldo=8530},




            }.AsQueryable();
        }


    }
}
