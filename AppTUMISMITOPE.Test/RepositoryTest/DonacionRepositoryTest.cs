﻿using AppTUMISMITOPE.DB;
using AppTUMISMITOPE.Models;
using AppTUMISMITOPE.Repository;
using AppTUMISMITOPE.Test.RepositoryTest.Mocks;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace AppTUMISMITOPE.Test.RepositoryTest
{
    public class DonacionRepositoryTest
    {
        [Test]
        public void GetByIdPublicacionCase01() {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            DonacionRepository dr = new DonacionRepository(mockContext.Object);

            var result = dr.GetByIdPublicacion(1);

            Assert.IsNotNull(result);
            Assert.IsNotEmpty(result);

        }
        [Test]
        public void GetByIdPublicacionCase02()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            DonacionRepository dr = new DonacionRepository(mockContext.Object);

            var result = dr.GetByIdPublicacion(2);

            Assert.IsNotNull(result);
            Assert.IsNotEmpty(result);

        }

        [Test]
        public void GetByIdPublicacionCase03()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            DonacionRepository dr = new DonacionRepository(mockContext.Object);

            var result = dr.GetByIdPublicacion(3);

            Assert.IsNotNull(result);
            Assert.IsEmpty(result);

        }

        [Test]
        public void GetByIdPublicacionCase04()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            DonacionRepository dr = new DonacionRepository(mockContext.Object);

            var result = dr.GetByIdPublicacion(4);

            Assert.IsNotNull(result);
            Assert.IsNotEmpty(result);

        }

        [Test]
        public void GetByIdPublicacionCase05()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            DonacionRepository dr = new DonacionRepository(mockContext.Object);

            var result = dr.GetByIdPublicacion(5);

            Assert.IsNotNull(result);
            Assert.IsNotEmpty(result);

        }
        [Test]
        public void GetByIdPublicacionCase06()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            DonacionRepository dr = new DonacionRepository(mockContext.Object);

            var result = dr.GetByIdPublicacion(6);

            Assert.IsNotNull(result);
            Assert.IsNotEmpty(result);

        }

        [Test]
        public void GetByIdPublicacionCase07()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            DonacionRepository dr = new DonacionRepository(mockContext.Object);

            var result = dr.GetByIdPublicacion(7);

            Assert.IsNotNull(result);
            Assert.IsNotEmpty(result);

        }

        //

        [Test]
        public void GetByIdPublicacionCase08()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            DonacionRepository dr = new DonacionRepository(mockContext.Object);

            var result = dr.GetByIdPublicacion(1);

            Assert.IsInstanceOf<List<Donacion>>(result);

            foreach (var item in result ) {

                Assert.IsInstanceOf<Donacion>(item);
            }


        }
        [Test]
        public void GetByIdPublicacionCase09()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            DonacionRepository dr = new DonacionRepository(mockContext.Object);

            var result = dr.GetByIdPublicacion(2);

            Assert.IsInstanceOf<List<Donacion>>(result);

            foreach (var item in result)
            {

                Assert.IsInstanceOf<Donacion>(item);
            }

        }


        [Test]
        public void GetByIdPublicacionCase10()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            DonacionRepository dr = new DonacionRepository(mockContext.Object);

            var result = dr.GetByIdPublicacion(4);
            Assert.IsInstanceOf<List<Donacion>>(result);

            foreach (var item in result)
            {

                Assert.IsInstanceOf<Donacion>(item);
            }

        }

        [Test]
        public void GetByIdPublicacionCase11()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            DonacionRepository dr = new DonacionRepository(mockContext.Object);

            var result = dr.GetByIdPublicacion(5);

            Assert.IsInstanceOf<List<Donacion>>(result);

            foreach (var item in result)
            {

                Assert.IsInstanceOf<Donacion>(item);
            }

        }
        [Test]
        public void GetByIdPublicacionCase12()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            DonacionRepository dr = new DonacionRepository(mockContext.Object);

            var result = dr.GetByIdPublicacion(6);

            Assert.IsInstanceOf<List<Donacion>>(result);

            foreach (var item in result)
            {

                Assert.IsInstanceOf<Donacion>(item);
            }

        }

        [Test]
        public void GetByIdPublicacionCase13()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            DonacionRepository dr = new DonacionRepository(mockContext.Object);

            var result = dr.GetByIdPublicacion(7);

            Assert.IsInstanceOf<List<Donacion>>(result);

            foreach (var item in result)
            {

                Assert.IsInstanceOf<Donacion>(item);
            }

        }

        //


        [Test]
        public void GetByIdPublicacionCase14()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            DonacionRepository dr = new DonacionRepository(mockContext.Object);

            var result = dr.GetByIdPublicacion(1);


            Assert.NotZero(result.Count);
            Assert.AreEqual(3,result.Count);
        }
        [Test]
        public void GetByIdPublicacionCase15()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            DonacionRepository dr = new DonacionRepository(mockContext.Object);

            var result = dr.GetByIdPublicacion(2);


            Assert.NotZero(result.Count);
            Assert.AreEqual(2, result.Count);
        }
        [Test]
        public void GetByIdPublicacionCase16()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            DonacionRepository dr = new DonacionRepository(mockContext.Object);

            var result = dr.GetByIdPublicacion(4);


            Assert.NotZero(result.Count);
            Assert.AreEqual(2, result.Count);
        }
        [Test]
        public void GetByIdPublicacionCase17()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            DonacionRepository dr = new DonacionRepository(mockContext.Object);

            var result = dr.GetByIdPublicacion(5);


            Assert.NotZero(result.Count);
            Assert.AreEqual(2, result.Count);
        }
        [Test]
        public void GetByIdPublicacionCase18()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            DonacionRepository dr = new DonacionRepository(mockContext.Object);

            var result = dr.GetByIdPublicacion(6);


            Assert.NotZero(result.Count);
            Assert.AreEqual(2, result.Count);
        }
        [Test]
        public void GetByIdPublicacionCase19()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            DonacionRepository dr = new DonacionRepository(mockContext.Object);

            var result = dr.GetByIdPublicacion(7);


            Assert.NotZero(result.Count);
            Assert.AreEqual(4, result.Count);
        }

        //
        [Test]
        public void GetByIdPublicacionCase20()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            DonacionRepository dr = new DonacionRepository(mockContext.Object);

            var result = dr.GetByIdPublicacion(7);

            Assert.AreEqual(12,result[0].id);
            Assert.AreEqual("PayPal", result[0].canalPago);
            Assert.AreEqual(300,result[0].monto);
        }
        [Test]
        public void GetByIdPublicacionCase21()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            DonacionRepository dr = new DonacionRepository(mockContext.Object);

            var result = dr.GetByIdPublicacion(7);

            Assert.AreEqual(14, result[2].id);
            Assert.AreEqual("PayPal", result[2].canalPago);
            Assert.AreEqual(320, result[2].monto);
        }
        [Test]
        public void GetByIdPublicacionCase22()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            DonacionRepository dr = new DonacionRepository(mockContext.Object);

            var result = dr.GetByIdPublicacion(4);

            Assert.AreEqual(7, result[1].id);
            Assert.AreEqual("Tunki", result[1].canalPago);
            Assert.AreEqual(50, result[1].monto);
        }
        [Test]
        public void GetByIdPublicacionCase23()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            DonacionRepository dr = new DonacionRepository(mockContext.Object);

            var result = dr.GetByIdPublicacion(5);

            Assert.AreEqual(8, result[0].id);
            Assert.AreEqual("Yape", result[0].canalPago);
            Assert.AreEqual(300, result[0].monto);
        }
        [Test]
        public void GetByIdPublicacionCase24()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            DonacionRepository dr = new DonacionRepository(mockContext.Object);

            var result = dr.GetByIdPublicacion(6);

            Assert.AreEqual(10, result[0].id);
            Assert.AreEqual("PayPal", result[0].canalPago);
            Assert.AreEqual(90, result[0].monto);
        }
        [Test]
        public void GetByIdPublicacionCase25()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            DonacionRepository dr = new DonacionRepository(mockContext.Object);

            var result = dr.GetByIdPublicacion(7);

            Assert.AreEqual(14, result[2].id);
            Assert.AreEqual("PayPal", result[2].canalPago);
            Assert.AreEqual(320, result[2].monto);
        }

        [Test]
        public void AddCase01()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            DonacionRepository dr = new DonacionRepository(mockContext.Object);

            dr.Add(new Donacion());
        }
    }
}
