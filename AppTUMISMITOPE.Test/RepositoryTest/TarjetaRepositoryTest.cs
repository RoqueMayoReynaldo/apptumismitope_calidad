﻿using AppTUMISMITOPE.DB;
using AppTUMISMITOPE.Models;
using AppTUMISMITOPE.Repository;
using AppTUMISMITOPE.Test.RepositoryTest.Mocks;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace AppTUMISMITOPE.Test.RepositoryTest
{
    public class TarjetaRepositoryTest
    {

        [Test]
        public void GetTarjetaByModelCase01()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            TarjetaRepository tr = new TarjetaRepository(mockContext.Object);


            Tarjeta tarjeta = new Tarjeta { numero=12345678,nombre="Alex",apellido="Roque",mesVencimiento=10,
            anioVencimiento=22,codigoSeguridad=123 }; 

            var result = tr.GetTarjetaByModel(tarjeta);

            Assert.IsNotNull(result);


        }
        [Test]
        public void GetTarjetaByModelCase02()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            TarjetaRepository tr = new TarjetaRepository(mockContext.Object);


            Tarjeta tarjeta = new Tarjeta
            {
                numero = 77777777,
                nombre = "Maria",
                apellido = "Robles",
                mesVencimiento = 01,
                anioVencimiento = 28,
                codigoSeguridad = 123
            };

            var result = tr.GetTarjetaByModel(tarjeta);

            Assert.IsNotNull(result);


        }
        [Test]
        public void GetTarjetaByModelCase03()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            TarjetaRepository tr = new TarjetaRepository(mockContext.Object);


            Tarjeta tarjeta = new Tarjeta
            {
                numero = 11111111,
                nombre = "Andres",
                apellido = "Iniesta",
                mesVencimiento = 11,
                anioVencimiento = 24,
                codigoSeguridad = 123
            };

            var result = tr.GetTarjetaByModel(tarjeta);

            Assert.IsNotNull(result);


        }

        [Test]
        public void GetTarjetaByModelCase04()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            TarjetaRepository tr = new TarjetaRepository(mockContext.Object);


            Tarjeta tarjeta = new Tarjeta
            {
                numero = 66666666,
                nombre = "Jorge",
                apellido = "Torres",
                mesVencimiento = 06,
                anioVencimiento = 23,
                codigoSeguridad = 123
            };

            var result = tr.GetTarjetaByModel(tarjeta);

            Assert.IsNotNull(result);

        }
        
        [Test]
        public void GetTarjetaByModelCase05()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            TarjetaRepository tr = new TarjetaRepository(mockContext.Object);


            Tarjeta tarjeta = new Tarjeta
            {
                numero = 99999999,
                nombre = "Lucy",
                apellido = "Teran",
                mesVencimiento = 12,
                anioVencimiento = 25,
                codigoSeguridad = 123
            };

            var result = tr.GetTarjetaByModel(tarjeta);

            Assert.IsNotNull(result);


        }
        //
        [Test]
        public void GetTarjetaByModelCase06()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            TarjetaRepository tr = new TarjetaRepository(mockContext.Object);


            Tarjeta tarjeta = new Tarjeta
            {
                numero = 12345678,
                nombre = "Alex",
                apellido = "Roque",
                mesVencimiento = 10,
                anioVencimiento = 22,
                codigoSeguridad = 123
            };

            var result = tr.GetTarjetaByModel(tarjeta);

            Assert.IsInstanceOf<Tarjeta>(result);
            Assert.IsNotEmpty(result.nombre);
            Assert.NotZero(result.numero);

        }
        [Test]
        public void GetTarjetaByModelCase07()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            TarjetaRepository tr = new TarjetaRepository(mockContext.Object);


            Tarjeta tarjeta = new Tarjeta
            {
                numero = 77777777,
                nombre = "Maria",
                apellido = "Robles",
                mesVencimiento = 01,
                anioVencimiento = 28,
                codigoSeguridad = 123
            };

            var result = tr.GetTarjetaByModel(tarjeta);


            Assert.IsInstanceOf<Tarjeta>(result);
            Assert.IsNotEmpty(result.nombre);
            Assert.NotZero(result.numero);



        }
        [Test]
        public void GetTarjetaByModelCase08()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            TarjetaRepository tr = new TarjetaRepository(mockContext.Object);


            Tarjeta tarjeta = new Tarjeta
            {
                numero = 11111111,
                nombre = "Andres",
                apellido = "Iniesta",
                mesVencimiento = 11,
                anioVencimiento = 24,
                codigoSeguridad = 123
            };

            var result = tr.GetTarjetaByModel(tarjeta);
            Assert.IsInstanceOf<Tarjeta>(result);
            Assert.IsNotEmpty(result.nombre);
            Assert.NotZero(result.numero);


        }

        [Test]
        public void GetTarjetaByModelCase09()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            TarjetaRepository tr = new TarjetaRepository(mockContext.Object);


            Tarjeta tarjeta = new Tarjeta
            {
                numero = 66666666,
                nombre = "Jorge",
                apellido = "Torres",
                mesVencimiento = 06,
                anioVencimiento = 23,
                codigoSeguridad = 123
            };

            var result = tr.GetTarjetaByModel(tarjeta);
            Assert.IsInstanceOf<Tarjeta>(result);
            Assert.IsNotEmpty(result.nombre);
            Assert.NotZero(result.numero);



        }

        [Test]
        public void GetTarjetaByModelCase10()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            TarjetaRepository tr = new TarjetaRepository(mockContext.Object);


            Tarjeta tarjeta = new Tarjeta
            {
                numero = 99999999,
                nombre = "Lucy",
                apellido = "Teran",
                mesVencimiento = 12,
                anioVencimiento = 25,
                codigoSeguridad = 123
            };

            var result = tr.GetTarjetaByModel(tarjeta);
            Assert.IsInstanceOf<Tarjeta>(result);
            Assert.IsNotEmpty(result.nombre);
            Assert.NotZero(result.numero);


        }

        //
        [Test]
        public void GetTarjetaByModelCase11()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            TarjetaRepository tr = new TarjetaRepository(mockContext.Object);


            Tarjeta tarjeta = new Tarjeta
            {
                numero = 12345678,
                nombre = "Alex",
                apellido = "Roque",
                mesVencimiento = 10,
                anioVencimiento = 22,
                codigoSeguridad = 123
            };

            var result = tr.GetTarjetaByModel(tarjeta);

            Assert.AreEqual(1,result.id);
            Assert.AreEqual(5000, result.saldo);
            Assert.AreEqual("Alex", result.nombre);

        }
        [Test]
        public void GetTarjetaByModelCase12()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            TarjetaRepository tr = new TarjetaRepository(mockContext.Object);


            Tarjeta tarjeta = new Tarjeta
            {
                numero = 77777777,
                nombre = "Maria",
                apellido = "Robles",
                mesVencimiento = 01,
                anioVencimiento = 28,
                codigoSeguridad = 123
            };

            var result = tr.GetTarjetaByModel(tarjeta);


            Assert.AreEqual(7, result.id);
            Assert.AreEqual(4000, result.saldo);
            Assert.AreEqual("Maria", result.nombre);



        }
        [Test]
        public void GetTarjetaByModelCase13()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            TarjetaRepository tr = new TarjetaRepository(mockContext.Object);


            Tarjeta tarjeta = new Tarjeta
            {
                numero = 11111111,
                nombre = "Andres",
                apellido = "Iniesta",
                mesVencimiento = 11,
                anioVencimiento = 24,
                codigoSeguridad = 123
            };

            var result = tr.GetTarjetaByModel(tarjeta);
            Assert.AreEqual(10, result.id);
            Assert.AreEqual(8530, result.saldo);
            Assert.AreEqual("Andres", result.nombre);


        }

        [Test]
        public void GetTarjetaByModelCase14()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            TarjetaRepository tr = new TarjetaRepository(mockContext.Object);


            Tarjeta tarjeta = new Tarjeta
            {
                numero = 66666666,
                nombre = "Jorge",
                apellido = "Torres",
                mesVencimiento = 06,
                anioVencimiento = 23,
                codigoSeguridad = 123
            };

            var result = tr.GetTarjetaByModel(tarjeta);
            Assert.AreEqual(6, result.id);
            Assert.AreEqual(800, result.saldo);
            Assert.AreEqual("Jorge", result.nombre);



        }

        [Test]
        public void GetTarjetaByModelCase15()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            TarjetaRepository tr = new TarjetaRepository(mockContext.Object);


            Tarjeta tarjeta = new Tarjeta
            {
                numero = 99999999,
                nombre = "Lucy",
                apellido = "Teran",
                mesVencimiento = 12,
                anioVencimiento = 25,
                codigoSeguridad = 123
            };


            var result = tr.GetTarjetaByModel(tarjeta);
            Assert.AreEqual(9, result.id);
            Assert.AreEqual(2050, result.saldo);
            Assert.AreEqual("Lucy", result.nombre);


        }

        //
        [Test]
        public void GetTarjetaByModelFailCase16()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            TarjetaRepository tr = new TarjetaRepository(mockContext.Object);


            Tarjeta tarjeta = new Tarjeta
            {
                numero = 12345678,
                nombre = "Alexx",
                apellido = "Roque",
                mesVencimiento = 10,
                anioVencimiento = 22,
                codigoSeguridad = 123
            };

            var result = tr.GetTarjetaByModel(tarjeta);

            Assert.IsNull(result);

        }
        [Test]
        public void GetTarjetaByModelFailCase17()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            TarjetaRepository tr = new TarjetaRepository(mockContext.Object);


            Tarjeta tarjeta = new Tarjeta
            {
                numero = 77777777,
                nombre = "Maria",
                apellido = "Robles",
                mesVencimiento = 05,
                anioVencimiento = 28,
                codigoSeguridad = 123
            };

            var result = tr.GetTarjetaByModel(tarjeta);


            Assert.IsNull(result);



        }
        [Test]
        public void GetTarjetaByModelFailCase18()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            TarjetaRepository tr = new TarjetaRepository(mockContext.Object);


            Tarjeta tarjeta = new Tarjeta
            {
                numero = 11111111,
                nombre = "Andres",
                apellido = "Iniesta",
                mesVencimiento = 11,
                anioVencimiento = 24,
                codigoSeguridad = 321
            };

            var result = tr.GetTarjetaByModel(tarjeta);

            Assert.IsNull(result);


        }

        [Test]
        public void GetTarjetaByModelFailCase19()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            TarjetaRepository tr = new TarjetaRepository(mockContext.Object);


            Tarjeta tarjeta = new Tarjeta
            {
                numero = 879644544,
                nombre = "Jorge",
                apellido = "Torres",
                mesVencimiento = 06,
                anioVencimiento = 23,
                codigoSeguridad = 123
            };

            var result = tr.GetTarjetaByModel(tarjeta);
            Assert.IsNull(result);




        }

        [Test]
        public void GetTarjetaByModelFailCase20()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            TarjetaRepository tr = new TarjetaRepository(mockContext.Object);


            Tarjeta tarjeta = new Tarjeta
            {
                numero = 99999999,
                nombre = "Teran",
                apellido = "Lucy",
                mesVencimiento = 12,
                anioVencimiento = 25,
                codigoSeguridad = 123
            };


            var result = tr.GetTarjetaByModel(tarjeta);
            Assert.IsNull(result);


        }

        [Test]
        public void UpdateChangesCase01()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            TarjetaRepository tr = new TarjetaRepository(mockContext.Object);

            tr.UpdateChanges();
          


        }

    }
}
