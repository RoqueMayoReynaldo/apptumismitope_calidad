﻿using AppTUMISMITOPE.DB;
using AppTUMISMITOPE.Models;
using AppTUMISMITOPE.Repository;
using AppTUMISMITOPE.Test.RepositoryTest.Mocks;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace AppTUMISMITOPE.Test.RepositoryTest
{
    public class ReplyComentarioRepositoryTest
    {

        [Test]
        public void FindByIdCase01()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            ReplyComentarioRepository rc = new ReplyComentarioRepository(mockContext.Object);

            var result = rc.FindById(1);

            Assert.IsNotNull(result);

            Assert.IsInstanceOf<ReplyComentario>(result);

        }
        [Test]
        public void FindByIdCase02()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            ReplyComentarioRepository rc = new ReplyComentarioRepository(mockContext.Object);

            var result = rc.FindById(2);

            Assert.IsNotNull(result);

            Assert.IsInstanceOf<ReplyComentario>(result);

        }
        [Test]
        public void FindByIdCase03()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            ReplyComentarioRepository rc = new ReplyComentarioRepository(mockContext.Object);

            var result = rc.FindById(3);

            Assert.IsNotNull(result);

            Assert.IsInstanceOf<ReplyComentario>(result);

        }
        [Test]
        public void FindByIdCase04()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            ReplyComentarioRepository rc = new ReplyComentarioRepository(mockContext.Object);

            var result = rc.FindById(4);

            Assert.IsNotNull(result);

            Assert.IsInstanceOf<ReplyComentario>(result);

        }
        [Test]
        public void FindByIdCase05()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            ReplyComentarioRepository rc = new ReplyComentarioRepository(mockContext.Object);

            var result = rc.FindById(5);

            Assert.IsNotNull(result);

            Assert.IsInstanceOf<ReplyComentario>(result);

        }
        [Test]
        public void FindByIdCase06()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            ReplyComentarioRepository rc = new ReplyComentarioRepository(mockContext.Object);

            var result = rc.FindById(6);

            Assert.IsNotNull(result);

            Assert.IsInstanceOf<ReplyComentario>(result);

        }
        [Test]
        public void FindByIdCase07()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            ReplyComentarioRepository rc = new ReplyComentarioRepository(mockContext.Object);

            var result = rc.FindById(7);

            Assert.IsNotNull(result);

            Assert.IsInstanceOf<ReplyComentario>(result);


        }
        [Test]
        public void FindByIdCase08()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            ReplyComentarioRepository rc = new ReplyComentarioRepository(mockContext.Object);

            var result = rc.FindById(13);

            Assert.IsNotNull(result);

            Assert.IsInstanceOf<ReplyComentario>(result);

        }
        [Test]
        public void FindByIdCase09()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            ReplyComentarioRepository rc = new ReplyComentarioRepository(mockContext.Object);

            var result = rc.FindById(14);

            Assert.IsNotNull(result);

            Assert.IsInstanceOf<ReplyComentario>(result);

        }
        [Test]
        public void FindByIdCase10()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            ReplyComentarioRepository rc = new ReplyComentarioRepository(mockContext.Object);

            var result = rc.FindById(26);

            Assert.IsNotNull(result);

            Assert.IsInstanceOf<ReplyComentario>(result);

        }

        //

        [Test]
        public void FindByIdCase11()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            ReplyComentarioRepository rc = new ReplyComentarioRepository(mockContext.Object);

            var result = rc.FindById(1);

            Assert.AreEqual("Reply01Comentario01",result.contenido);
            Assert.AreEqual(1,result.id);
            Assert.AreEqual(8, result.idUsuario);
            Assert.AreEqual(1,result.idComentario);

        }

        [Test]
        public void FindByIdCase12()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            ReplyComentarioRepository rc = new ReplyComentarioRepository(mockContext.Object);

            var result = rc.FindById(3);

            Assert.AreEqual("Reply01Comentario02", result.contenido);
            Assert.AreEqual(3, result.id);
            Assert.AreEqual(9, result.idUsuario);
            Assert.AreEqual(2, result.idComentario);

        }

        [Test]
        public void FindByIdCase13()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            ReplyComentarioRepository rc = new ReplyComentarioRepository(mockContext.Object);

            var result = rc.FindById(5);

            Assert.AreEqual("Reply03Comentario02", result.contenido);
            Assert.AreEqual(1, result.idUsuario);
            Assert.AreEqual(2, result.idComentario);

        }

        [Test]
        public void FindByIdCase14()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            ReplyComentarioRepository rc = new ReplyComentarioRepository(mockContext.Object);

            var result = rc.FindById(9);

            Assert.AreEqual("Reply01Comentario05", result.contenido);
            Assert.AreEqual(4, result.idUsuario);
            Assert.AreEqual(5, result.idComentario);

        }

        [Test]
        public void FindByIdCase15()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            ReplyComentarioRepository rc = new ReplyComentarioRepository(mockContext.Object);

            var result = rc.FindById(11);

            Assert.AreEqual("Reply03Comentario05", result.contenido);
            Assert.AreEqual(2, result.idUsuario);
            Assert.AreEqual(5, result.idComentario);

        }

        [Test]
        public void FindByIdCase16()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            ReplyComentarioRepository rc = new ReplyComentarioRepository(mockContext.Object);

            var result = rc.FindById(13);

            Assert.AreEqual("Reply01Comentario06", result.contenido);
            Assert.AreEqual(6, result.idUsuario);
            Assert.AreEqual(6, result.idComentario);

        }

        [Test]
        public void FindByIdCase17()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            ReplyComentarioRepository rc = new ReplyComentarioRepository(mockContext.Object);

            var result = rc.FindById(14);

            Assert.AreEqual("Reply01Comentario07", result.contenido);
            Assert.AreEqual(1, result.idUsuario);
            Assert.AreEqual(7, result.idComentario);

        }

        [Test]
        public void FindByIdCase18()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            ReplyComentarioRepository rc = new ReplyComentarioRepository(mockContext.Object);

            var result = rc.FindById(17);

            Assert.AreEqual("Reply01Comentario10", result.contenido);
            Assert.AreEqual(4, result.idUsuario);
            Assert.AreEqual(10, result.idComentario);

        }

        [Test]
        public void FindByIdCase19()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            ReplyComentarioRepository rc = new ReplyComentarioRepository(mockContext.Object);

            var result = rc.FindById(23);

            Assert.AreEqual("Reply01Comentario13", result.contenido);
            Assert.AreEqual(5, result.idUsuario);
            Assert.AreEqual(13, result.idComentario);

        }

        [Test]
        public void FindByIdCase20()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            ReplyComentarioRepository rc = new ReplyComentarioRepository(mockContext.Object);

            var result = rc.FindById(1);

            Assert.AreEqual("Reply01Comentario01", result.contenido);
            Assert.AreEqual(8, result.idUsuario);
            Assert.AreEqual(1, result.idComentario);

        }


        //

        [Test]
        public void FindByIdCase21()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            ReplyComentarioRepository rc = new ReplyComentarioRepository(mockContext.Object);

            var result = rc.FindById(1);


            Assert.IsNotNull(result.usuario);
            Assert.IsInstanceOf<Usuario>(result.usuario);
         

        }


        [Test]
        public void FindByIdCase22()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            ReplyComentarioRepository rc = new ReplyComentarioRepository(mockContext.Object);

            var result = rc.FindById(5);


            Assert.IsNotNull(result.usuario);
            Assert.IsInstanceOf<Usuario>(result.usuario);


        }

        [Test]
        public void FindByIdCase23()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            ReplyComentarioRepository rc = new ReplyComentarioRepository(mockContext.Object);

            var result = rc.FindById(7);


            Assert.IsNotNull(result.usuario);
            Assert.IsInstanceOf<Usuario>(result.usuario);


        }

        [Test]
        public void FindByIdCase24()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            ReplyComentarioRepository rc = new ReplyComentarioRepository(mockContext.Object);

            var result = rc.FindById(9);


            Assert.IsNotNull(result.usuario);
            Assert.IsInstanceOf<Usuario>(result.usuario);


        }

        [Test]
        public void FindByIdCase25()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            ReplyComentarioRepository rc = new ReplyComentarioRepository(mockContext.Object);

            var result = rc.FindById(25);


            Assert.IsNotNull(result.usuario);
            Assert.IsInstanceOf<Usuario>(result.usuario);


        }
        //



        [Test]
        public void FindByIdCase26()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            ReplyComentarioRepository rc = new ReplyComentarioRepository(mockContext.Object);

            var result = rc.FindById(13);


            Assert.AreEqual(6,result.usuario.id);
            Assert.AreEqual("jorgeTorres",result.usuario.userName);
            Assert.AreEqual("Torres",result.usuario.apellido);


        }
        [Test]
        public void FindByIdCase27()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            ReplyComentarioRepository rc = new ReplyComentarioRepository(mockContext.Object);

            var result = rc.FindById(15);


            Assert.AreEqual(7, result.usuario.id);
            Assert.AreEqual("mariaRobles", result.usuario.userName);
            Assert.AreEqual("Robles", result.usuario.apellido);


        }
        [Test]
        public void FindByIdCase28()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            ReplyComentarioRepository rc = new ReplyComentarioRepository(mockContext.Object);

            var result = rc.FindById(18);


            Assert.AreEqual(2, result.usuario.id);
            Assert.AreEqual("rosmerySanchez", result.usuario.userName);
            Assert.AreEqual("Sanchez", result.usuario.apellido);


        }
        [Test]
        public void FindByIdCase29()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            ReplyComentarioRepository rc = new ReplyComentarioRepository(mockContext.Object);

            var result = rc.FindById(21);


            Assert.AreEqual(1, result.usuario.id);
            Assert.AreEqual("alexRoque", result.usuario.userName);
            Assert.AreEqual("Roque", result.usuario.apellido);


        }
        [Test]
        public void FindByIdCase30()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            ReplyComentarioRepository rc = new ReplyComentarioRepository(mockContext.Object);

            var result = rc.FindById(27);


            Assert.AreEqual(8, result.usuario.id);
            Assert.AreEqual("miguelSanchez", result.usuario.userName);
            Assert.AreEqual("Sanchez", result.usuario.apellido);



        }

        [Test]
        public void AddReplyCase01()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            ReplyComentarioRepository rc = new ReplyComentarioRepository(mockContext.Object);

            rc.AddReply(new ReplyComentario());



        }


    }
}
