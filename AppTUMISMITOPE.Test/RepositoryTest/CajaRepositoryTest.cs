﻿using AppTUMISMITOPE.DB;
using AppTUMISMITOPE.Models;
using AppTUMISMITOPE.Repository;
using AppTUMISMITOPE.Test.RepositoryTest.Mocks;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace AppTUMISMITOPE.Test.RepositoryTest
{
    public class CajaRepositoryTest
    {

        [Test]
        public void GetCajaCase01()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            CajaRepository cr = new CajaRepository(mockContext.Object);

            var result = cr.GetCaja();

            Assert.IsNotNull(result);
         

        }
        [Test]
        public void GetCajaCase02()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            CajaRepository cr = new CajaRepository(mockContext.Object);
            var result = cr.GetCaja();

            Assert.IsInstanceOf<Caja>(result);

        }
        [Test]
        public void GetCajaCase03()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            CajaRepository cr = new CajaRepository(mockContext.Object);
            var result = cr.GetCaja();

            Assert.NotZero(result.id);
            Assert.IsInstanceOf<double>(result.monto);


        }
        [Test]
        public void GetCajaCase04()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            CajaRepository cr = new CajaRepository(mockContext.Object);
            var result = cr.GetCaja();

            Assert.AreEqual(1,result.id);
            Assert.AreEqual(200,result.monto,0);

        }
        [Test]
        public void GetCajaCase05()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            CajaRepository cr = new CajaRepository(mockContext.Object);
            var result = cr.GetCaja();

            Assert.AreEqual(0,result.monto-200);

        }
        [Test]
        public void GetCajaCase06()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            CajaRepository cr = new CajaRepository(mockContext.Object);
            var result = cr.GetCaja();

            Assert.AreEqual(1, result.monto / 200);

        }
        [Test]
        public void GetCajaCase07()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            CajaRepository cr = new CajaRepository(mockContext.Object);
            var result = cr.GetCaja();

            Assert.AreEqual(211.30, result.monto + 11.30);

        }
        [Test]
        public void GetCajaCase08()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            CajaRepository cr = new CajaRepository(mockContext.Object);
            var result = cr.GetCaja();

            Assert.AreEqual(400, result.monto + result.monto);

        }
        [Test]
        public void GetCajaCase09()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            CajaRepository cr = new CajaRepository(mockContext.Object);
            var result = cr.GetCaja();

            Assert.AreEqual(20, result.monto%180);

        }
        [Test]
        public void GetCajaCase10()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            CajaRepository cr = new CajaRepository(mockContext.Object);

            var result = cr.GetCaja().monto.ToString();

            Assert.AreEqual("200", result);

        }
    }
}
