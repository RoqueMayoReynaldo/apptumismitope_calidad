﻿using AppTUMISMITOPE.DB;
using AppTUMISMITOPE.Models;
using AppTUMISMITOPE.Repository;
using AppTUMISMITOPE.Test.RepositoryTest.Mocks;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace AppTUMISMITOPE.Test.RepositoryTest
{
    public class ComentarioRepositoryTest
    {
        [Test]
        public void FindByIdCase01()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            ComentarioRepository cr = new ComentarioRepository(mockContext.Object);

            var result = cr.FindById(1);


            Assert.IsNotNull(result);



        }
        [Test]
        public void FindByIdCase02()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            ComentarioRepository cr = new ComentarioRepository(mockContext.Object);

            var result = cr.FindById(13);


            Assert.IsNotNull(result);



        }
        [Test]
        public void FindByIdCase03()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            ComentarioRepository cr = new ComentarioRepository(mockContext.Object);

            var result = cr.FindById(5);


            Assert.IsNotNull(result);



        }
        [Test]
        public void FindByIdCase04()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            ComentarioRepository cr = new ComentarioRepository(mockContext.Object);

            var result = cr.FindById(7);


            Assert.IsNotNull(result);



        }
        [Test]
        public void FindByIdCase05()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            ComentarioRepository cr = new ComentarioRepository(mockContext.Object);

            var result = cr.FindById(11);


            Assert.IsNotNull(result);



        }

        //
        [Test]
        public void FindByIdCase06()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            ComentarioRepository cr = new ComentarioRepository(mockContext.Object);

            var result = cr.FindById(11);


            Assert.IsInstanceOf<Comentario>(result);
            Assert.IsNotEmpty(result.contenido);
            Assert.NotZero(result.idUsuario);



        }
        [Test]
        public void FindByIdCase07()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            ComentarioRepository cr = new ComentarioRepository(mockContext.Object);

            var result = cr.FindById(15);



            Assert.IsInstanceOf<Comentario>(result);
            Assert.IsNotEmpty(result.contenido);
            Assert.NotZero(result.idUsuario);



        }
        [Test]
        public void FindByIdCase08()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            ComentarioRepository cr = new ComentarioRepository(mockContext.Object);

            var result = cr.FindById(1);



            Assert.IsInstanceOf<Comentario>(result);
            Assert.IsNotEmpty(result.contenido);
            Assert.NotZero(result.idUsuario);



        }
        [Test]
        public void FindByIdCase09()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            ComentarioRepository cr = new ComentarioRepository(mockContext.Object);

            var result = cr.FindById(2);



            Assert.IsInstanceOf<Comentario>(result);
            Assert.IsNotEmpty(result.contenido);
            Assert.NotZero(result.idUsuario);



        }
        [Test]
        public void FindByIdCase10()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            ComentarioRepository cr = new ComentarioRepository(mockContext.Object);

            var result = cr.FindById(16);



            Assert.IsInstanceOf<Comentario>(result);
            Assert.IsNotEmpty(result.contenido);
            Assert.NotZero(result.idUsuario);



        }

        //

        [Test]
        public void FindByIdCase11()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            ComentarioRepository cr = new ComentarioRepository(mockContext.Object);

            var result = cr.FindById(11);


            Assert.AreEqual(5,result.idPublicacion);
            Assert.AreEqual(1, result.idUsuario);
            Assert.IsNotNull(result.usuario);


        }
        [Test]
        public void FindByIdCase12()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            ComentarioRepository cr = new ComentarioRepository(mockContext.Object);

            var result = cr.FindById(15);




            Assert.AreEqual(7, result.idPublicacion);
            Assert.AreEqual(9, result.idUsuario);
            Assert.IsNotNull(result.usuario);


        }
        [Test]
        public void FindByIdCase13() 
        { 
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            ComentarioRepository cr = new ComentarioRepository(mockContext.Object);

            var result = cr.FindById(1);



            Assert.AreEqual(1, result.idPublicacion);
            Assert.AreEqual(3, result.idUsuario);
            Assert.IsNotNull(result.usuario);


        }
        [Test]
        public void FindByIdCase14()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            ComentarioRepository cr = new ComentarioRepository(mockContext.Object);

            var result = cr.FindById(2);



            Assert.AreEqual(1, result.idPublicacion);
            Assert.AreEqual(2, result.idUsuario);
            Assert.IsNotNull(result.usuario);


        }
        [Test]
        public void FindByIdCase15()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            ComentarioRepository cr = new ComentarioRepository(mockContext.Object);

            var result = cr.FindById(16);




            Assert.AreEqual(7, result.idPublicacion);
            Assert.AreEqual(8, result.idUsuario);
            Assert.IsNotNull(result.usuario);


        }

        //

        [Test]
        public void FindByIdCase16()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            ComentarioRepository cr = new ComentarioRepository(mockContext.Object);

            var result = cr.FindById(11);




            Assert.AreEqual("alexRoque",result.usuario.userName);
            Assert.AreEqual("Roque", result.usuario.apellido);

        }
        [Test]
        public void FindByIdCase17()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            ComentarioRepository cr = new ComentarioRepository(mockContext.Object);

            var result = cr.FindById(15);





            Assert.AreEqual("lucyTeran", result.usuario.userName);
            Assert.AreEqual("Teran", result.usuario.apellido);


        }
        [Test]
        public void FindByIdCase18()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            ComentarioRepository cr = new ComentarioRepository(mockContext.Object);

            var result = cr.FindById(1);




            Assert.AreEqual("yanMinchan", result.usuario.userName);
            Assert.AreEqual("Minchan", result.usuario.apellido);


        }
        [Test]
        public void FindByIdCase19()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            ComentarioRepository cr = new ComentarioRepository(mockContext.Object);

            var result = cr.FindById(2);

            Assert.AreEqual("rosmerySanchez", result.usuario.userName);
            Assert.AreEqual("Sanchez", result.usuario.apellido);



        }
        [Test]
        public void FindByIdCase20()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            ComentarioRepository cr = new ComentarioRepository(mockContext.Object);

            var result = cr.FindById(16);




            Assert.AreEqual("miguelSanchez", result.usuario.userName);
            Assert.AreEqual("Sanchez", result.usuario.apellido);


        }

        [Test]
        public void FindByIdCase21()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            ComentarioRepository cr = new ComentarioRepository(mockContext.Object);

            var result = cr.FindById(17);




            Assert.IsNull(result);


        }

        [Test]
        public void FindByIdCase22()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            ComentarioRepository cr = new ComentarioRepository(mockContext.Object);

            var result = cr.FindById(18);




            Assert.IsNull(result);


        }
        [Test]
        public void FindByIdCase23()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            ComentarioRepository cr = new ComentarioRepository(mockContext.Object);

            var result = cr.FindById(50);




            Assert.IsNull(result);


        }
        [Test]
        public void AddComentarioCase01()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            ComentarioRepository cr = new ComentarioRepository(mockContext.Object);

            cr.AddComentario(new Comentario());


        }

    }
}
