﻿using AppTUMISMITOPE.DB;
using AppTUMISMITOPE.Models;
using AppTUMISMITOPE.Repository;
using AppTUMISMITOPE.Test.RepositoryTest.Mocks;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace AppTUMISMITOPE.Test.RepositoryTest
{
    
    public class UsuarioRepositoryTest
    {



     
        [Test]
        public void FindByIdCase01()
        {

            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindById(2);

            Assert.AreEqual(2, result.id);
            Assert.AreEqual("Rosmery", result.nombre);
            Assert.AreEqual("Sanchez", result.apellido);

        }
        [Test]
        public void FindByIdCase02()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindById(4);

            Assert.AreEqual(4, result.id);
            Assert.AreEqual("Selene", result.nombre);
            Assert.AreEqual("Oblitas", result.apellido);


        }
        [Test]
        public void FindByIdCase04()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindById(8);

            Assert.AreEqual(8, result.id);
            Assert.AreEqual("Miguel", result.nombre);
            Assert.AreEqual("Sanchez", result.apellido);


        }

        [Test]
        public void FindByIdCase05()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();


            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindById(10);

            Assert.AreEqual(10, result.id);
            Assert.AreEqual("Andres", result.nombre);
            Assert.AreEqual("Iniesta", result.apellido);


        }

        [Test]
        public void FindByIdCase06()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindById(2);

            Assert.AreEqual(2, result.id);
            Assert.AreEqual(1, result.perfilProfesional.id);
            Assert.AreEqual(2, result.perfilProfesional.idUsuario);

        }
        [Test]
        public void FindByIdCase07()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindById(5);

            Assert.AreEqual(5, result.id);
            Assert.AreEqual(2, result.perfilProfesional.id);
            Assert.AreEqual(5, result.perfilProfesional.idUsuario);

        }
        [Test]
        public void FindByIdCase08()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindById(7);

            Assert.AreEqual(7, result.id);
            Assert.AreEqual(3, result.perfilProfesional.id);
            Assert.AreEqual(7, result.perfilProfesional.idUsuario);

        }
        [Test]
        public void FindByIdCase09()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindById(9);

            Assert.AreEqual(9, result.id);
            Assert.AreEqual(4, result.perfilProfesional.id);
            Assert.AreEqual(9, result.perfilProfesional.idUsuario);

        }
        [Test]
        public void FindByIdCase10()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindById(10);

            Assert.AreEqual(10, result.id);
            Assert.AreEqual(0, result.estado);

        }
        [Test]
        public void FindByIdCase11()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindById(5);

            Assert.IsInstanceOf<Usuario>(result);

        }
        [Test]
        public void FindByIdCase12()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindById(2);

            Assert.IsInstanceOf<Usuario>(result);

        }
        [Test]
        public void FindByIdCase13()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindById(1);

            Assert.IsInstanceOf<Usuario>(result);

        }
        [Test]
        public void FindByIdCase14()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindById(8);

            Assert.IsInstanceOf<Usuario>(result);

        }
        [Test]
        public void FindByIdCase15()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindById(1);

            Assert.IsTrue(result.correo.Contains('@'));

        }
        [Test]
        public void FindByIdCase16()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindById(5);

            Assert.IsTrue(result.correo.Contains('@'));

        }
        [Test]
        public void FindByIdCase17()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindById(9);

            Assert.IsTrue(result.correo.Contains('@'));

        }
        [Test]
        public void FindByIdCase18()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindById(10);

            Assert.IsTrue(result.correo.Contains('@'));

        }
        [Test]
        public void FindByIdCase19()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindById(7);

            Assert.IsTrue(result.correo.Contains('@'));

        }



        [Test]
        public void FindByIdCase20()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindById(2);

            Assert.AreEqual("rosmerySanchez", result.userName);
            Assert.AreEqual("123456", result.pass);
        }
        [Test]
        public void FindByIdCase21()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindById(7);


            Assert.AreEqual("mariaRobles", result.userName);
            Assert.AreEqual("123456", result.pass);

        }
        [Test]
        public void FindByIdCase22()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindById(10);

            Assert.AreEqual("andresIniesta", result.userName);
            Assert.AreEqual("123456", result.pass);

        }
        [Test]
        public void FindByIdCase23()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindById(3);

            Assert.AreEqual("yanMinchan", result.userName);
            Assert.AreEqual("123456", result.pass);

        }
        [Test]
        public void FindByIdCase24()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindById(8);

            Assert.AreEqual("Masculino", result.sexo);

        }




        //FindByCredentials


        [Test]
        public void FindByCredentialsCase01()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindByCredentials("rosmerySanchez", "123456");

            Assert.AreEqual(2, result.id);
            Assert.AreEqual("Rosmery", result.nombre);
            Assert.AreEqual("Sanchez", result.apellido);


        }
        [Test]
        public void FindByCredentialsCase02()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindByCredentials("seleneOblitas", "123456");

            Assert.AreEqual(4, result.id);
            Assert.AreEqual("Selene", result.nombre);
            Assert.AreEqual("Oblitas", result.apellido);


        }
        [Test]
        public void FindByCredentialsCase03()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindByCredentials("miguelSanchez", "123456");

            Assert.AreEqual(8, result.id);
            Assert.AreEqual("Miguel", result.nombre);
            Assert.AreEqual("Sanchez", result.apellido);


        }
        [Test]
        public void FindByCredentialsCase04()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();

            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindByCredentials("andresIniesta", "123456");

            Assert.AreEqual(10, result.id);
            Assert.AreEqual("Andres", result.nombre);
            Assert.AreEqual("Iniesta", result.apellido);


        }

        [Test]
        public void FindByCredentialsCase05()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindByCredentials("rosmerySanchez", "123456");

            Assert.AreEqual(2, result.id);
            Assert.AreEqual(1, result.perfilProfesional.id);
            Assert.AreEqual(2, result.perfilProfesional.idUsuario);

        }
        [Test]
        public void FindByCredentialsCase06()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindByCredentials("victorTeran", "123456");

            Assert.AreEqual(5, result.id);
            Assert.AreEqual(2, result.perfilProfesional.id);
            Assert.AreEqual(5, result.perfilProfesional.idUsuario);

        }
        [Test]
        public void FindByCredentialsCase07()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);


            var result = ur.FindByCredentials("mariaRobles", "123456");

            Assert.AreEqual(7, result.id);
            Assert.AreEqual(3, result.perfilProfesional.id);
            Assert.AreEqual(7, result.perfilProfesional.idUsuario);

        }
        [Test]
        public void FindByCredentialsCase08()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindByCredentials("lucyTeran", "123456");

            Assert.AreEqual(9, result.id);
            Assert.AreEqual(4, result.perfilProfesional.id);
            Assert.AreEqual(9, result.perfilProfesional.idUsuario);

        }
        [Test]
        public void FindByCredentialsCase09()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindByCredentials("andresIniesta", "123456");

            Assert.AreEqual(10, result.id);
            Assert.AreEqual(0, result.estado);

        }
        [Test]
        public void FindByCredentialsCase10()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);
            var result = ur.FindByCredentials("victorTeran", "123456");

            Assert.IsInstanceOf<Usuario>(result);

        }
        [Test]
        public void FindByCredentialsCase11()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindByCredentials("miguelSanchez", "123456");

            Assert.IsInstanceOf<Usuario>(result);

        }
        [Test]
        public void FindByCredentialsCase12()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindByCredentials("yanMinchan", "123456");

            Assert.IsInstanceOf<Usuario>(result);

        }
        [Test]
        public void FindByCredentialsCase13()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindByCredentials("alexRoque", "123456");

            Assert.IsInstanceOf<Usuario>(result);

        }
        [Test]
        public void FindByCredentialsCase14()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindByCredentials("miguelSanchez", "123456");

            Assert.IsTrue(result.correo.Contains('@'));

        }
        [Test]
        public void FindByCredentialsCase15()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindByCredentials("jorgeTorres", "123456");

            Assert.IsTrue(result.correo.Contains('@'));

        }
        [Test]
        public void FindByCredentialsCase16()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindByCredentials("seleneOblitas", "123456");

            Assert.IsTrue(result.correo.Contains('@'));

        }
        [Test]
        public void FindByCredentialsCase17()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindByCredentials("yanMinchan", "123456");

            Assert.IsTrue(result.correo.Contains('@'));

        }
        [Test]
        public void FindByCredentialsCase18()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindByCredentials("andresIniesta", "123456");

            Assert.IsTrue(result.correo.Contains('@'));

        }



        [Test]
        public void FindByCredentialsCase19()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindByCredentials("rosmerySanchez", "123456");

            Assert.AreEqual("rosmerySanchez", result.userName);
            Assert.AreEqual("123456", result.pass);
        }
        [Test]
        public void FindByCredentialsCase20()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindByCredentials("mariaRobles", "123456");


            Assert.AreEqual("mariaRobles", result.userName);
            Assert.AreEqual("123456", result.pass);

        }
        [Test]
        public void FindByCredentialsCase21()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindByCredentials("andresIniesta", "123456");


            Assert.AreEqual("andresIniesta", result.userName);
            Assert.AreEqual("123456", result.pass);

        }
        [Test]
        public void FindByCredentialsCase22()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindByCredentials("yanMinchan", "123456");

            Assert.AreEqual("yanMinchan", result.userName);
            Assert.AreEqual("123456", result.pass);

        }
        [Test]
        public void FindByCredentialsCase23()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindByCredentials("alexRoque", "123456");

            Assert.AreEqual("Masculino", result.sexo);

        }

        //cases FindByUserName


        [Test]
        public void FindByUserNameCase01()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindByUserName("rosmerySanchez");

            Assert.AreEqual(2, result.id);
            Assert.AreEqual("Rosmery", result.nombre);
            Assert.AreEqual("Sanchez", result.apellido);


        }
        [Test]
        public void FindByUserNameCase02()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindByUserName("seleneOblitas");

            Assert.AreEqual(4, result.id);
            Assert.AreEqual("Selene", result.nombre);
            Assert.AreEqual("Oblitas", result.apellido);


        }
        [Test]
        public void FindByUserNameCase03()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindByUserName("miguelSanchez");

            Assert.AreEqual(8, result.id);
            Assert.AreEqual("Miguel", result.nombre);
            Assert.AreEqual("Sanchez", result.apellido);


        }
        [Test]
        public void FindByUserNameCase04()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);
            var result = ur.FindByUserName("andresIniesta");

            Assert.AreEqual(10, result.id);
            Assert.AreEqual("Andres", result.nombre);
            Assert.AreEqual("Iniesta", result.apellido);


        }

        [Test]
        public void FindByUserNameCase05()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindByUserName("rosmerySanchez");

            Assert.AreEqual(2, result.id);
            Assert.AreEqual(1, result.perfilProfesional.id);
            Assert.AreEqual(2, result.perfilProfesional.idUsuario);

        }
        [Test]
        public void FindByUserNameCase06()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindByUserName("victorTeran");

            Assert.AreEqual(5, result.id);
            Assert.AreEqual(2, result.perfilProfesional.id);
            Assert.AreEqual(5, result.perfilProfesional.idUsuario);

        }
        [Test]
        public void FindByUserNameCase07()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindByUserName("mariaRobles");

            Assert.AreEqual(7, result.id);
            Assert.AreEqual(3, result.perfilProfesional.id);
            Assert.AreEqual(7, result.perfilProfesional.idUsuario);

        }
        [Test]
        public void FindByUserNameCase08()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);


            var result = ur.FindByUserName("lucyTeran");

            Assert.AreEqual(9, result.id);
            Assert.AreEqual(4, result.perfilProfesional.id);
            Assert.AreEqual(9, result.perfilProfesional.idUsuario);

        }
        [Test]
        public void FindByUserNameCase09()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);
            var result = ur.FindByUserName("andresIniesta");

            Assert.AreEqual(10, result.id);
            Assert.AreEqual(0, result.estado);

        }
        [Test]
        public void FindByUserNameCase10()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindByUserName("rosmerySanchez");

            Assert.IsInstanceOf<Usuario>(result);

        }
        [Test]
        public void FindByUserNameCase11()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindByUserName("yanMinchan");

            Assert.IsInstanceOf<Usuario>(result);

        }
        [Test]
        public void FindByUserNameCase12()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindByUserName("seleneOblitas");

            Assert.IsInstanceOf<Usuario>(result);

        }
        [Test]
        public void FindByUserNameCase13()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindByUserName("victorTeran");

            Assert.IsInstanceOf<Usuario>(result);

        }
        [Test]
        public void FindByUserNameCase14()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindByUserName("andresIniesta");

            Assert.IsTrue(result.correo.Contains('@'));

        }
        [Test]
        public void FindByUserNameCase15()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);


            var result = ur.FindByUserName("lucyTeran");

            Assert.IsTrue(result.correo.Contains('@'));

        }
        [Test]
        public void FindByUserNameCase16()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindByUserName("miguelSanchez");

            Assert.IsTrue(result.correo.Contains('@'));

        }
        [Test]
        public void FindByUserNameCase17()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindByUserName("mariaRobles");

            Assert.IsTrue(result.correo.Contains('@'));

        }
        [Test]
        public void FindByUserNameCase18()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindByUserName("jorgeTorres");

            Assert.IsTrue(result.correo.Contains('@'));

        }



        [Test]
        public void FindByUserNameCase19()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindByUserName("rosmerySanchez");

            Assert.AreEqual("rosmerySanchez", result.userName);
            Assert.AreEqual("123456", result.pass);
        }
        [Test]
        public void FindByUserNameCase20()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindByUserName("mariaRobles");


            Assert.AreEqual("mariaRobles", result.userName);
            Assert.AreEqual("123456", result.pass);

        }
        [Test]
        public void FindByUserNameCase21()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);


            var result = ur.FindByUserName("andresIniesta");

            Assert.AreEqual("andresIniesta", result.userName);
            Assert.AreEqual("123456", result.pass);

        }
        [Test]
        public void FindByUserNameCase22()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindByUserName("yanMinchan");

            Assert.AreEqual("yanMinchan", result.userName);
            Assert.AreEqual("123456", result.pass);

        }
        [Test]
        public void FindByUserNameCase23()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.FindByUserName("alexRoque");

            Assert.AreEqual("Masculino", result.sexo);

        }


        //GetWithPostulacionessByUsername

        [Test]
        public void GetWithPostulacionessByUsernameCase01()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.GetWithPostulacionessByUsername("rosmerySanchez");

            Assert.AreEqual(2, result.id);
            Assert.AreEqual("Rosmery", result.nombre);
            Assert.AreEqual("Sanchez", result.apellido);


        }
        [Test]
        public void GetWithPostulacionessByUsernameCase02()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.GetWithPostulacionessByUsername("seleneOblitas");

            Assert.AreEqual(4, result.id);
            Assert.AreEqual("Selene", result.nombre);
            Assert.AreEqual("Oblitas", result.apellido);


        }
        [Test]
        public void GetWithPostulacionessByUsernameCase03()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.GetWithPostulacionessByUsername("miguelSanchez");

            Assert.AreEqual(8, result.id);
            Assert.AreEqual("Miguel", result.nombre);
            Assert.AreEqual("Sanchez", result.apellido);


        }
        [Test]
        public void GetWithPostulacionessByUsernameCase04()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);
            var result = ur.GetWithPostulacionessByUsername("andresIniesta");

            Assert.AreEqual(10, result.id);
            Assert.AreEqual("Andres", result.nombre);
            Assert.AreEqual("Iniesta", result.apellido);


        }

        [Test]
        public void GetWithPostulacionessByUsernameCase05()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.GetWithPostulacionessByUsername("rosmerySanchez");

            Assert.AreEqual(2, result.id);
            Assert.AreEqual(1, result.perfilProfesional.id);
            Assert.AreEqual(2, result.perfilProfesional.idUsuario);

        }
        [Test]
        public void GetWithPostulacionessByUsernameCase06()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.GetWithPostulacionessByUsername("victorTeran");

            Assert.AreEqual(5, result.id);
            Assert.AreEqual(2, result.perfilProfesional.id);
            Assert.AreEqual(5, result.perfilProfesional.idUsuario);

        }
        [Test]
        public void GetWithPostulacionessByUsernameCase07()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.GetWithPostulacionessByUsername("mariaRobles");

            Assert.AreEqual(7, result.id);
            Assert.AreEqual(3, result.perfilProfesional.id);
            Assert.AreEqual(7, result.perfilProfesional.idUsuario);

        }
        [Test]
        public void GetWithPostulacionessByUsernameCase08()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);


            var result = ur.GetWithPostulacionessByUsername("lucyTeran");

            Assert.AreEqual(9, result.id);
            Assert.AreEqual(4, result.perfilProfesional.id);
            Assert.AreEqual(9, result.perfilProfesional.idUsuario);

        }
        [Test]
        public void GetWithPostulacionessByUsernameCase09()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);
            var result = ur.GetWithPostulacionessByUsername("andresIniesta");

            Assert.AreEqual(10, result.id);
            Assert.AreEqual(0, result.estado);

        }
        [Test]
        public void GetWithPostulacionessByUsernameCase10()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.GetWithPostulacionessByUsername("rosmerySanchez");

            Assert.IsInstanceOf<Usuario>(result);

        }
        [Test]
        public void GetWithPostulacionessByUsernameCase11()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.GetWithPostulacionessByUsername("yanMinchan");

            Assert.IsInstanceOf<Usuario>(result);

        }
        [Test]
        public void GetWithPostulacionessByUsernameCase12()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.GetWithPostulacionessByUsername("seleneOblitas");

            Assert.IsInstanceOf<Usuario>(result);

        }
        [Test]
        public void GetWithPostulacionessByUsernameCase13()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.GetWithPostulacionessByUsername("victorTeran");

            Assert.IsInstanceOf<Usuario>(result);

        }
        [Test]
        public void GetWithPostulacionessByUsernameCase14()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.GetWithPostulacionessByUsername("andresIniesta");

            Assert.IsTrue(result.correo.Contains('@'));

        }
        [Test]
        public void GetWithPostulacionessByUsernameCase15()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);


            var result = ur.GetWithPostulacionessByUsername("lucyTeran");

            Assert.IsTrue(result.correo.Contains('@'));

        }
        [Test]
        public void GetWithPostulacionessByUsernameCase16()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.GetWithPostulacionessByUsername("miguelSanchez");

            Assert.IsTrue(result.correo.Contains('@'));

        }
        [Test]
        public void GetWithPostulacionessByUsernameCase17()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.GetWithPostulacionessByUsername("mariaRobles");

            Assert.IsTrue(result.correo.Contains('@'));

        }
        [Test]
        public void GetWithPostulacionessByUsernameCase18()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.GetWithPostulacionessByUsername("jorgeTorres");

            Assert.IsTrue(result.correo.Contains('@'));

        }



        [Test]
        public void GetWithPostulacionessByUsernameCase19()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.GetWithPostulacionessByUsername("rosmerySanchez");

            Assert.AreEqual("rosmerySanchez", result.userName);
            Assert.AreEqual("123456", result.pass);
        }
        [Test]
        public void GetWithPostulacionessByUsernameCase20()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.GetWithPostulacionessByUsername("mariaRobles");


            Assert.AreEqual("mariaRobles", result.userName);
            Assert.AreEqual("123456", result.pass);

        }
        [Test]
        public void GetWithPostulacionessByUsernameCase21()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);


            var result = ur.GetWithPostulacionessByUsername("andresIniesta");

            Assert.AreEqual("andresIniesta", result.userName);
            Assert.AreEqual("123456", result.pass);

        }
        [Test]
        public void GetWithPostulacionessByUsernameCase22()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.GetWithPostulacionessByUsername("yanMinchan");

            Assert.AreEqual("yanMinchan", result.userName);
            Assert.AreEqual("123456", result.pass);

        }
        [Test]
        public void GetWithPostulacionessByUsernameCase23()
        {
            Mock<AppTumismitoPeContext> mockContext;
            mockContext = AppTumismitoPeContextMock.GetContextMock();
            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var result = ur.GetWithPostulacionessByUsername("alexRoque");

            Assert.AreEqual("Masculino", result.sexo);

        }


      

    }
}
