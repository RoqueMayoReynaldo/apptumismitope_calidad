﻿using AppTUMISMITOPE.Authentication;
using AppTUMISMITOPE.Controllers;
using AppTUMISMITOPE.Models;
using AppTUMISMITOPE.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace AppTUMISMITOPE.Test.ControllersTest
{

    public class WelcomeControllerTest
    {

        
        [Test]
        public void SingInGetCaso01()
        {
            WelcomeController wc = new WelcomeController(null, null);



            var result = wc.SingIn();
            Assert.IsInstanceOf<ViewResult>(result);

        }
        //probamos que el nombre de la vista sea nulo debido a que no lo establecemos ,pero el framework considera que es el mismo que el metodo

        [Test]
        public void SingInGetCaso02()
        {
            WelcomeController wc = new WelcomeController(null, null);



            var result = wc.SingIn() as ViewResult;
            Assert.IsNotNull(result.ViewName);

        }
        [Test]
        public void SingInGetCaso03()
        {
            WelcomeController wc = new WelcomeController(null, null);



            var result = wc.SingIn() as ViewResult;
            Assert.IsNotNull(result);

        }

        [Test]
        public void SingInGetCaso04()
        {
            WelcomeController wc = new WelcomeController(null, null);



            var result = wc.SingIn() as ViewResult;
            Assert.IsNull(result.TempData);

        }
        [Test]
        public void SingInGetCaso05()
        {
            WelcomeController wc = new WelcomeController(null, null);



            var result = wc.SingIn() as ViewResult;
            Assert.AreEqual("SingIn", result.ViewName);

        }

        [Test]
        public void SingInGetCaso06()
        {
            WelcomeController wc = new WelcomeController(null, null);



            var result = wc.SingIn() as ViewResult;
            Assert.IsNull(result.Model);

        }
        [Test]
        public void SingInGetCaso07()
        {
            WelcomeController wc = new WelcomeController(null, null);



            var result = wc.SingIn() as ViewResult;
            Assert.IsTrue(result.ViewData.ModelState.IsValid);

        }
        [Test]
        public void SingInGetCaso08()
        {
            WelcomeController wc = new WelcomeController(null, null);



            var result = wc.SingIn();
            Assert.IsNotInstanceOf<RedirectToActionResult>(result);

        }


        [Test]
        public void SingInPostSuccesCaso01()
        {

            var mockUsuarioRepo = new Mock<IUsuarioRepository>();

            mockUsuarioRepo.
                Setup(o=>o.FindByCredentials(It.IsAny<String>(), It.IsAny<String>()))
                .Returns(new Usuario());


            var mockAutenticacion = new Mock<IAutenticacion>();

            WelcomeController wc = new 
                WelcomeController(mockUsuarioRepo.Object, mockAutenticacion.Object);

            var result = wc.SingIn("Usuario123","123456");

            Assert.IsInstanceOf<RedirectToActionResult>(result);

        }

        [Test]
        public void SingInPostSuccesCaso02()
        {

            var mockUsuarioRepo = new Mock<IUsuarioRepository>();

            mockUsuarioRepo.
                Setup(o => o.FindByCredentials(It.IsAny<String>(), It.IsAny<String>()))
                .Returns(new Usuario());


            var mockAutenticacion = new Mock<IAutenticacion>();

            WelcomeController wc = new
                WelcomeController(mockUsuarioRepo.Object, mockAutenticacion.Object);

            var result = wc.SingIn("Usuario123", "123456");

            Assert.IsNotInstanceOf<ViewResult>(result);

        }

        [Test]
        public void SingInPostSuccesCaso03()
        {

            var mockUsuarioRepo = new Mock<IUsuarioRepository>();

            mockUsuarioRepo.
                Setup(o => o.FindByCredentials(It.IsAny<String>(), It.IsAny<String>()))
                .Returns(new Usuario());
            var mockTempData = new Mock<ITempDataDictionary>();



            var mockAutenticacion = new Mock<IAutenticacion>();

            WelcomeController wc = new
                WelcomeController(mockUsuarioRepo.Object, mockAutenticacion.Object);


            wc.TempData = mockTempData.Object;



            var result = wc.SingIn("Usuario123", "123456") as RedirectToActionResult;



            Assert.IsTrue(result.ActionName.StartsWith("I"));

        }


        [Test]
        public void SingInPostSuccesCaso04()
        {

            var mockUsuarioRepo = new Mock<IUsuarioRepository>();

            mockUsuarioRepo.
                Setup(o => o.FindByCredentials(It.IsAny<String>(), It.IsAny<String>()))
                .Returns(new Usuario());
            var mockTempData = new Mock<ITempDataDictionary>();



            var mockAutenticacion = new Mock<IAutenticacion>();

            WelcomeController wc = new
                WelcomeController(mockUsuarioRepo.Object, mockAutenticacion.Object);


            wc.TempData = mockTempData.Object;



            var result = wc.SingIn("Usuario123", "123456") as RedirectToActionResult;



            Assert.IsTrue(result.ActionName.EndsWith("x"));

        }


        [Test]
        public void SingInPostSuccesCaso05()
        {

            var mockUsuarioRepo = new Mock<IUsuarioRepository>();

            mockUsuarioRepo.
                Setup(o => o.FindByCredentials(It.IsAny<String>(), It.IsAny<String>()))
                .Returns(new Usuario());


            var mockAutenticacion = new Mock<IAutenticacion>();

            WelcomeController wc = new
                WelcomeController(mockUsuarioRepo.Object, mockAutenticacion.Object);

            var result = wc.SingIn("Usuario123", "123456") as RedirectToActionResult;

            Assert.AreEqual("Index",result.ActionName);

        }
        [Test]
        public void SingInPostSuccesCaso06()
        {

            var mockUsuarioRepo = new Mock<IUsuarioRepository>();

            mockUsuarioRepo.
                Setup(o => o.FindByCredentials(It.IsAny<String>(), It.IsAny<String>()))
                .Returns(new Usuario());


            var mockAutenticacion = new Mock<IAutenticacion>();

            WelcomeController wc = new
                WelcomeController(mockUsuarioRepo.Object, mockAutenticacion.Object);

            var result = wc.SingIn("Usuario123", "123456") as RedirectToActionResult;

            Assert.IsTrue(result.ControllerName.StartsWith("H"));

        }
        [Test]
        public void SingInPostSuccesCaso07()
        {

            var mockUsuarioRepo = new Mock<IUsuarioRepository>();

            mockUsuarioRepo.
                Setup(o => o.FindByCredentials(It.IsAny<String>(), It.IsAny<String>()))
                .Returns(new Usuario());


            var mockAutenticacion = new Mock<IAutenticacion>();

            WelcomeController wc = new
                WelcomeController(mockUsuarioRepo.Object, mockAutenticacion.Object);

            var result = wc.SingIn("Usuario123", "123456") as RedirectToActionResult;

            Assert.IsTrue(result.ControllerName.EndsWith("e"));

        }

        [Test]
        public void SingInPostSuccesCaso08()
        {

            var mockUsuarioRepo = new Mock<IUsuarioRepository>();

            mockUsuarioRepo.
                Setup(o => o.FindByCredentials(It.IsAny<String>(), It.IsAny<String>()))
                .Returns(new Usuario());


            var mockAutenticacion = new Mock<IAutenticacion>();

            WelcomeController wc = new
                WelcomeController(mockUsuarioRepo.Object, mockAutenticacion.Object);

            var result = wc.SingIn("Usuario123", "123456") as RedirectToActionResult;

            Assert.AreEqual("Home", result.ControllerName);

        }
        [Test]
        public void SingInPostSuccesCaso09()
        {

            var mockUsuarioRepo = new Mock<IUsuarioRepository>();

            mockUsuarioRepo.
                Setup(o => o.FindByCredentials(It.IsAny<String>(), It.IsAny<String>()))
                .Returns(new Usuario());


            var mockAutenticacion = new Mock<IAutenticacion>();

            WelcomeController wc = new
                WelcomeController(mockUsuarioRepo.Object, mockAutenticacion.Object);

            var result = wc.SingIn("Usuario123", "123456") as RedirectToActionResult;

            Assert.IsTrue(wc.ModelState.IsValid) ;

        }
        [Test]
        public void SingInPostFailCaso01()
        {

            var mockUsuarioRepo = new Mock<IUsuarioRepository>();

            mockUsuarioRepo.
                Setup(o => o.FindByCredentials("Raul", "1234"))
                .Returns(new Usuario());


            var mockAutenticacion = new Mock<IAutenticacion>();

            var mockTempData = new Mock<ITempDataDictionary>();

            WelcomeController wc = new
                WelcomeController(mockUsuarioRepo.Object, mockAutenticacion.Object);


            wc.TempData = mockTempData.Object;
            var result = wc.SingIn("jose", "5678");

            Assert.IsNotNull(result);
        }

        [Test]
        public void SingInPostFailCaso02()
        {

            var mockUsuarioRepo = new Mock<IUsuarioRepository>();

            mockUsuarioRepo.
                Setup(o => o.FindByCredentials("Raul", "1234"))
                .Returns(new Usuario());


            var mockAutenticacion = new Mock<IAutenticacion>();

            var mockTempData = new Mock<ITempDataDictionary>();

            WelcomeController wc = new
                WelcomeController(mockUsuarioRepo.Object, mockAutenticacion.Object);


            wc.TempData = mockTempData.Object;
            var result = wc.SingIn("jose", "5678");

            Assert.IsInstanceOf<RedirectToActionResult>(result);
        }


        [Test]
        public void SingInPostFailCaso03()
        {

            var mockUsuarioRepo = new Mock<IUsuarioRepository>();

            mockUsuarioRepo.
                Setup(o => o.FindByCredentials("Raul", "1234"))
                .Returns(new Usuario());


            var mockAutenticacion = new Mock<IAutenticacion>();

            var mockTempData = new Mock<ITempDataDictionary>();

            WelcomeController wc = new
                WelcomeController(mockUsuarioRepo.Object, mockAutenticacion.Object);


            wc.TempData = mockTempData.Object;
            var result = wc.SingIn("jose", "5678");

            Assert.IsNotInstanceOf<ViewResult>(result);
        }
   
        [Test]
        public void SingInPostFailCaso04()
        {

            var mockUsuarioRepo = new Mock<IUsuarioRepository>();

            mockUsuarioRepo.
                Setup(o => o.FindByCredentials("Raul", "1234"))
                .Returns(new Usuario());


            var mockAutenticacion = new Mock<IAutenticacion>();

            var mockTempData = new Mock<ITempDataDictionary>();

            WelcomeController wc = new
                WelcomeController(mockUsuarioRepo.Object, mockAutenticacion.Object);


            wc.TempData = mockTempData.Object;
            var result = wc.SingIn("jose", "5678") as RedirectToActionResult;

            Assert.IsNotNull(wc.TempData);

            Assert.IsTrue( result.ActionName.StartsWith("S"));

        }




        [Test]
        public void SingInPostFailCaso05()
        {

            var mockUsuarioRepo = new Mock<IUsuarioRepository>();

            mockUsuarioRepo.
                Setup(o => o.FindByCredentials("Raul", "1234"))
                .Returns(new Usuario());


            var mockAutenticacion = new Mock<IAutenticacion>();

            var mockTempData = new Mock<ITempDataDictionary>();

            WelcomeController wc = new
                WelcomeController(mockUsuarioRepo.Object, mockAutenticacion.Object);


            wc.TempData = mockTempData.Object;
            var result = wc.SingIn("jose", "5678") as RedirectToActionResult ;

            Assert.IsNotNull(wc.TempData);

            Assert.IsTrue(result.ActionName.EndsWith("n"));

        }

        [Test]
        public void SingInPostFailCaso06()
        {

            var mockUsuarioRepo = new Mock<IUsuarioRepository>();

            mockUsuarioRepo.
                Setup(o => o.FindByCredentials("Raul", "1234"))
                .Returns(new Usuario());


            var mockAutenticacion = new Mock<IAutenticacion>();

            var mockTempData = new Mock<ITempDataDictionary>();

            WelcomeController wc = new
                WelcomeController(mockUsuarioRepo.Object, mockAutenticacion.Object);


            wc.TempData = mockTempData.Object;
            var result = wc.SingIn("jose", "5678") as RedirectToActionResult;

            Assert.IsNotNull(wc.TempData);

            Assert.AreEqual("SingIn", result.ActionName);

        }

        [Test]
        public void SingInPostFailCaso08()
        {

            var mockUsuarioRepo = new Mock<IUsuarioRepository>();

            mockUsuarioRepo.
                Setup(o => o.FindByCredentials("Raul", "1234"))
                .Returns(new Usuario());


            var mockAutenticacion = new Mock<IAutenticacion>();

            var mockTempData = new Mock<ITempDataDictionary>();

            WelcomeController wc = new
                WelcomeController(mockUsuarioRepo.Object, mockAutenticacion.Object);


            wc.TempData = mockTempData.Object;
            var result = wc.SingIn("jose", "5678") as RedirectToActionResult;

            Assert.IsNotNull(wc.TempData);

            Assert.IsNull(result.ControllerName);

        }
        [Test]
        public void SingInPostFailCaso09()
        {

            var mockUsuarioRepo = new Mock<IUsuarioRepository>();

            mockUsuarioRepo.
                Setup(o => o.FindByCredentials("Raul", "1234"))
                .Returns(new Usuario());


            var mockAutenticacion = new Mock<IAutenticacion>();

            var mockTempData = new Mock<ITempDataDictionary>();
         
           
           

            WelcomeController wc = new
                WelcomeController(mockUsuarioRepo.Object, mockAutenticacion.Object);


            wc.TempData = mockTempData.Object;
            

            var result = wc.SingIn("jose", "5678") as RedirectToActionResult;

           

            Assert.IsFalse(wc.ModelState.IsValid);

        }

        
    }
}
