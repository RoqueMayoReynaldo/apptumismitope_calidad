﻿using AppTUMISMITOPE.Authentication;
using AppTUMISMITOPE.Controllers;
using AppTUMISMITOPE.FilesService;
using AppTUMISMITOPE.Models;
using AppTUMISMITOPE.Repository;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace AppTUMISMITOPE.Test.ControllersTest
{
    public class HomeControllerTest
    {

        [Test]
        public void IndexCaso01()
        {

            var mockAutenticacionRepo = new Mock<IAutenticacion>();
            var mockPublicacionRepo = new Mock<IPublicacionRepository>();
            var mockUsuarioRepo = new Mock<IUsuarioRepository>();

            HomeController hc = new HomeController(mockAutenticacionRepo.Object, mockPublicacionRepo.Object, mockUsuarioRepo.Object, 
                null,null,null);

            var result = hc.Index();

            Assert.IsInstanceOf<ViewResult>(result);



        }
        [Test]
        public void IndexCaso02()
        {

            var mockAutenticacionRepo = new Mock<IAutenticacion>();
            var mockPublicacionRepo = new Mock<IPublicacionRepository>();
            var mockUsuarioRepo = new Mock<IUsuarioRepository>();

            HomeController hc = new HomeController(mockAutenticacionRepo.Object, mockPublicacionRepo.Object, mockUsuarioRepo.Object,
                null, null,null);

            var result = hc.Index() as ViewResult;

            Assert.IsNotNull(result.Model);



        }

        [Test]
        public void IndexCaso03()
        {

            var mockAutenticacionRepo = new Mock<IAutenticacion>();
            var mockPublicacionRepo = new Mock<IPublicacionRepository>();
            var mockUsuarioRepo = new Mock<IUsuarioRepository>();

            HomeController hc = new HomeController(mockAutenticacionRepo.Object, mockPublicacionRepo.Object, mockUsuarioRepo.Object,
                null, null,null);

            var result = hc.Index() as ViewResult;

            Assert.AreEqual("Index",result.ViewName);

        }
        [Test]
        public void IndexCaso04()
        {

            var mockAutenticacionRepo = new Mock<IAutenticacion>();
            var mockPublicacionRepo = new Mock<IPublicacionRepository>();
            var mockUsuarioRepo = new Mock<IUsuarioRepository>();

            HomeController hc = new HomeController(mockAutenticacionRepo.Object, mockPublicacionRepo.Object, mockUsuarioRepo.Object,
                null, null, null);

            var result = hc.Index() as ViewResult;

            Assert.IsNull(result.TempData);

        }

        [Test]
        public void IndexCaso05()
        {

            var mockAutenticacionRepo = new Mock<IAutenticacion>();
            var mockPublicacionRepo = new Mock<IPublicacionRepository>();
            var mockUsuarioRepo = new Mock<IUsuarioRepository>();

            HomeController hc = new HomeController(mockAutenticacionRepo.Object, mockPublicacionRepo.Object, mockUsuarioRepo.Object,
                null, null, null);

            var result = hc.Index() as ViewResult;

            Assert.IsNotNull(result);

        }
        [Test]
        public void IndexCaso06()
        {

            var mockAutenticacionRepo = new Mock<IAutenticacion>();
            var mockPublicacionRepo = new Mock<IPublicacionRepository>();
            var mockUsuarioRepo = new Mock<IUsuarioRepository>();

            HomeController hc = new HomeController(mockAutenticacionRepo.Object, mockPublicacionRepo.Object, mockUsuarioRepo.Object,
                null, null, null);

            var result = hc.Index() as ViewResult;
           
            Assert.IsTrue(result.ViewData.ModelState.IsValid);

        }
        [Test]
        public void IndexCaso07()
        {

            var mockAutenticacionRepo = new Mock<IAutenticacion>();
            var mockPublicacionRepo = new Mock<IPublicacionRepository>();
            var mockUsuarioRepo = new Mock<IUsuarioRepository>();

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<string>())).Returns(new Usuario {nombre="Juancito",apellido="Perez",correo="juancito@gmail.com" ,id=15}); ;
            mockPublicacionRepo.Setup(o => o.GetPublicacionesSinCulminar()).Returns(new List<Publicacion> { new Publicacion { id = 59, descripcion = "Muy buena publicacion", idUsuario = 100 }
            ,new Publicacion{id = 80, descripcion = "Muy mala publicacion", idUsuario = 500 } });



            HomeController hc = new HomeController(mockAutenticacionRepo.Object, mockPublicacionRepo.Object, mockUsuarioRepo.Object,
                null, null, null);

            var result = hc.Index() as ViewResult  ;
            var data = (IndexModel)result.ViewData.Model;
            Assert.AreEqual(2,data.pubs.Count);
            Assert.AreEqual("Juancito", data.UsuarioActivo.nombre);


        }
        [Test]
        public void IndexCaso08()
        {

            var mockAutenticacionRepo = new Mock<IAutenticacion>();
            var mockPublicacionRepo = new Mock<IPublicacionRepository>();
            var mockUsuarioRepo = new Mock<IUsuarioRepository>();

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<string>())).Returns(new Usuario { nombre = "Admin", apellido = "Admin", correo = "Admin@gmail.com", id = 1 }); ;
            mockPublicacionRepo.Setup(o => o.GetPublicacionesSinCulminar()).Returns(new List<Publicacion>());



            HomeController hc = new HomeController(mockAutenticacionRepo.Object, mockPublicacionRepo.Object, mockUsuarioRepo.Object,
                null, null, null);

            var result = hc.Index() as ViewResult;
            var data = (IndexModel)result.ViewData.Model;
            Assert.AreEqual(0, data.pubs.Count);
            Assert.AreEqual("Admin", data.UsuarioActivo.nombre);
            Assert.AreEqual(1, data.UsuarioActivo.id);


        }
        [Test]
        public void IndexCaso09()
        {

            var mockAutenticacionRepo = new Mock<IAutenticacion>();
            var mockPublicacionRepo = new Mock<IPublicacionRepository>();
            var mockUsuarioRepo = new Mock<IUsuarioRepository>();

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<string>())).Returns(new Usuario { nombre = "Admin", apellido = "Admin", correo = "Admin@gmail.com", id = 1 }); ;
            mockPublicacionRepo.Setup(o => o.GetPublicacionesSinCulminar()).Returns(new List<Publicacion>());



            HomeController hc = new HomeController(mockAutenticacionRepo.Object, mockPublicacionRepo.Object, mockUsuarioRepo.Object,
                null, null, null);

            var result = hc.Index() as ViewResult;
            var data = (IndexModel)result.ViewData.Model;
            Assert.AreEqual(0, data.pubs.Count);
            Assert.AreEqual("Admin", data.UsuarioActivo.nombre);
            Assert.AreEqual(1, data.UsuarioActivo.id);


        }


        [Test]
        public void CrearPublicacionCaso01()
        {
            Publicacion pub = new Publicacion();
            List<IFormFile> files = new List<IFormFile>();

           
            var mockPublicacionRepo = new Mock<IPublicacionRepository>();
            var mockFotoRepo = new Mock<IFotoRepository>();


            HomeController hc = new HomeController(null,
                mockPublicacionRepo.Object, null,mockFotoRepo.Object, 
                null,null);


            var result = hc.CrearPublicacion(pub,files);

            Assert.IsNotNull(result);
           
        }

        [Test]
        public void CrearPublicacionCaso02()
        {
            Publicacion pub = new Publicacion();
            List<IFormFile> files = new List<IFormFile>();


            var mockPublicacionRepo = new Mock<IPublicacionRepository>();
            var mockFotoRepo = new Mock<IFotoRepository>();


            HomeController hc = new HomeController(null,
                mockPublicacionRepo.Object, null, mockFotoRepo.Object,
                null,null);


            var result = hc.CrearPublicacion(pub, files);

            Assert.IsInstanceOf<RedirectToActionResult>(result);

        }
        [Test]
        public void CrearPublicacionCaso03()
        {
            Publicacion pub = new Publicacion();
            List<IFormFile> files = new List<IFormFile>();


            var mockPublicacionRepo = new Mock<IPublicacionRepository>();
            var mockFotoRepo = new Mock<IFotoRepository>();


            HomeController hc = new HomeController(null,
                mockPublicacionRepo.Object, null, mockFotoRepo.Object,
                null,null);


            var result = hc.CrearPublicacion(pub, files) as RedirectToActionResult;

            Assert.AreEqual("Index",result.ActionName);

        }
        [Test]
        public void CrearPublicacionCaso04()
        {
            Publicacion pub = new Publicacion();
            List<IFormFile> files = new List<IFormFile>();


            var mockPublicacionRepo = new Mock<IPublicacionRepository>();
            var mockFotoRepo = new Mock<IFotoRepository>();


            HomeController hc = new HomeController(null,
                mockPublicacionRepo.Object, null, mockFotoRepo.Object,
                null,null);


            var result = hc.CrearPublicacion(pub, files) as RedirectToActionResult;

            Assert.AreEqual(null, result.ControllerName);

        }

        [Test]
        public void CrearPublicacionCaso05()
        {
            Publicacion pub = new Publicacion();
            List<IFormFile> files = new List<IFormFile>();


            var mockPublicacionRepo = new Mock<IPublicacionRepository>();
            var mockFotoRepo = new Mock<IFotoRepository>();


            HomeController hc = new HomeController(null,
                mockPublicacionRepo.Object, null, mockFotoRepo.Object,
                null, null);




            Assert.Throws(typeof(Exception), () => hc.CrearPublicacion(null, files));

        }

        [Test]
        public void CrearPublicacionCaso06()
        {
            Publicacion pub = new Publicacion();
            List<IFormFile> files = new List<IFormFile>();


            var mockPublicacionRepo = new Mock<IPublicacionRepository>();
            var mockFotoRepo = new Mock<IFotoRepository>();


            HomeController hc = new HomeController(null,
                mockPublicacionRepo.Object, null, mockFotoRepo.Object,
                null, null);

            var exception = Assert.Throws<Exception>(() => hc.CrearPublicacion(null, files));
            Assert.AreEqual("error for parameter null", exception.Message);

        }

        [Test]
        public void CrearPublicacionCaso07()
        {
            Publicacion pub = new Publicacion();
            List<IFormFile> files = new List<IFormFile>();


            var mockPublicacionRepo = new Mock<IPublicacionRepository>();
            var mockFotoRepo = new Mock<IFotoRepository>();


            HomeController hc = new HomeController(null,
                mockPublicacionRepo.Object, null, mockFotoRepo.Object,
                null, null);




            Assert.Throws(typeof(Exception), () => hc.CrearPublicacion(new Publicacion(), null));

        }
        [Test]
        public void CrearPublicacionCaso08()
        {
            Publicacion pub = new Publicacion();
            List<IFormFile> files = new List<IFormFile>();


            var mockPublicacionRepo = new Mock<IPublicacionRepository>();
            var mockFotoRepo = new Mock<IFotoRepository>();


            HomeController hc = new HomeController(null,
                mockPublicacionRepo.Object, null, mockFotoRepo.Object,
                null, null);

            var exception = Assert.Throws<Exception>(() => hc.CrearPublicacion(new Publicacion(), null));
            Assert.AreEqual("error for parameter null", exception.Message);

        }

        [Test]
        public void CrearPublicacionCaso09()
        {
            Publicacion pub = new Publicacion();
            List<IFormFile> files = new List<IFormFile>();


            var mockPublicacionRepo = new Mock<IPublicacionRepository>();
            var mockFotoRepo = new Mock<IFotoRepository>();


            HomeController hc = new HomeController(null,
                mockPublicacionRepo.Object, null, mockFotoRepo.Object,
                null, null);




            Assert.Throws(typeof(Exception), () => hc.CrearPublicacion(null, null));

        }


        [Test]
        public void CrearPublicacionCaso10()
        {
            Publicacion pub = new Publicacion();
            List<IFormFile> files = new List<IFormFile>();


            var mockPublicacionRepo = new Mock<IPublicacionRepository>();
            var mockFotoRepo = new Mock<IFotoRepository>();


            HomeController hc = new HomeController(null,
                mockPublicacionRepo.Object, null, mockFotoRepo.Object,
                null, null);

            var exception = Assert.Throws<Exception>(() => hc.CrearPublicacion(null, null));
            Assert.AreEqual("error for parameter null", exception.Message);

        }

    

        [Test]
        public void LogoutCaso01()
        {
            
            var mockAutRepo = new Mock<IAutenticacion>();      
            HomeController hc = new HomeController(mockAutRepo.Object, null, null, null,null,null);

            var result = hc.Logout();

            Assert.IsNotNull(result);

        }
        [Test]
        public void LogoutCaso02()
        {

            var mockAutRepo = new Mock<IAutenticacion>();
            HomeController hc = new HomeController(mockAutRepo.Object, null, null, null, null,null);

            var result = hc.Logout();

            Assert.IsInstanceOf<RedirectToActionResult>(result);

        }
        [Test]
        public void LogoutCaso03()
        {

            var mockAutRepo = new Mock<IAutenticacion>();
            HomeController hc = new HomeController(mockAutRepo.Object, null, null, null, null, null);

            var result = hc.Logout() as RedirectToActionResult;

            Assert.IsTrue(result.ActionName.StartsWith("S"));

        }
        [Test]
        public void LogoutCaso04()
        {

            var mockAutRepo = new Mock<IAutenticacion>();
            HomeController hc = new HomeController(mockAutRepo.Object, null, null, null, null, null);

            var result = hc.Logout() as RedirectToActionResult;

            Assert.IsTrue(result.ActionName.EndsWith("n"));

        }

        public void LogoutCaso05()
        {

            var mockAutRepo = new Mock<IAutenticacion>();
            HomeController hc = new HomeController(mockAutRepo.Object, null, null, null, null,null);

            var result = hc.Logout() as RedirectToActionResult;

            Assert.AreEqual("SingIn",result.ActionName);

        }

        [Test]
        public void LogoutCaso06()
        {

            var mockAutRepo = new Mock<IAutenticacion>();
            HomeController hc = new HomeController(mockAutRepo.Object, null, null, null, null, null);

            var result = hc.Logout() as RedirectToActionResult;

            Assert.IsTrue(result.ControllerName.StartsWith("W"));

        }
        [Test]
        public void LogoutCaso07()
        {

            var mockAutRepo = new Mock<IAutenticacion>();
            HomeController hc = new HomeController(mockAutRepo.Object, null, null, null, null, null);

            var result = hc.Logout() as RedirectToActionResult;

            Assert.IsTrue(result.ControllerName.EndsWith("e"));

        }
        [Test]
        public void LogoutCaso08()
        {

            var mockAutRepo = new Mock<IAutenticacion>();
            HomeController hc = new HomeController(mockAutRepo.Object, null, null, null, null,null);

            var result = hc.Logout() as RedirectToActionResult;

            Assert.AreEqual("Welcome", result.ControllerName);

        }
        [Test]
        public void LogoutCaso09()
        {

            var mockAutRepo = new Mock<IAutenticacion>();
            HomeController hc = new HomeController(mockAutRepo.Object, null, null, null, null, null);

            var result = hc.Logout();

            Assert.IsNotInstanceOf<ViewResult>(result);

        }


        [Test]

        public void SaveFileCaso01()
        {
            var mockFile = new Mock<IFormFile>();
            //mockFile.Setup(o=>o.Length==1);
            mockFile.Setup(o => o.Length).Returns(1);
            mockFile.Setup(o => o.ContentType).Returns("image/png") ;
            mockFile.Setup(o => o.FileName).Returns("foto1.png") ;
        
            var mockIWebHostEnvironment = new Mock<IWebHostEnvironment>();
            mockIWebHostEnvironment.Setup(o=>o.WebRootPath).Returns("C: \\Users\\Alex\\source\repos\\AppTUMISMITOPE.Test\\wwwrootTest");

            var mockManageFiles = new Mock<IManageFiles>();
            mockManageFiles.Setup(o => o.CombinePaths(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns("files\\fotosPublicaciones\\foto1.png");
            mockManageFiles.Setup(o => o.CombinePaths(It.IsAny<string>(), It.IsAny<string>())).Returns("C:\\Users\\Alex\\source\\repos\\AppTUMISMITOPE.Test\\wwwroootTest\\files\\fotosPublicaciones\\foto1.png");
            String esperado = "/files/fotosPublicaciones/foto1.png";
     

            HomeController hc = new HomeController(null,null,null,null, mockManageFiles.Object,mockIWebHostEnvironment.Object);

            var result = hc.SaveFile(mockFile.Object);

            Assert.AreEqual(esperado,result);

        }
        [Test]
        public void SaveFileCaso02()
        {
            var mockFile = new Mock<IFormFile>();
            //mockFile.Setup(o=>o.Length==1);
            mockFile.Setup(o => o.Length).Returns(1);
            mockFile.Setup(o => o.ContentType).Returns("image/png");
            mockFile.Setup(o => o.FileName).Returns("foto_50.png");

            var mockIWebHostEnvironment = new Mock<IWebHostEnvironment>();
            mockIWebHostEnvironment.Setup(o => o.WebRootPath).Returns("C: \\Users\\Alex\\source\repos\\AppTUMISMITOPE.Test\\wwwrootTest");

            var mockManageFiles = new Mock<IManageFiles>();
            mockManageFiles.Setup(o => o.CombinePaths("files", "fotosPublicaciones", mockFile.Object.FileName)).Returns("files\\fotosPublicaciones\\foto_50.png");
            mockManageFiles.Setup(o => o.CombinePaths(It.IsAny<string>(), "files\\fotosPublicaciones\\foto_50.png")).Returns("C:\\Users\\Alex\\source\\repos\\AppTUMISMITOPE.Test\\wwwroootTest\\files\\fotosPublicaciones\\foto_50.png");
            String esperado = "/files/fotosPublicaciones/foto_50.png";


            HomeController hc = new HomeController(null, null, null, null, mockManageFiles.Object, mockIWebHostEnvironment.Object);

            var result = hc.SaveFile(mockFile.Object);

            Assert.AreEqual(esperado, result);

        }
        [Test]
        public void SaveFileCaso03()
        {
            var mockFile = new Mock<IFormFile>();
            //mockFile.Setup(o=>o.Length==1);
            mockFile.Setup(o => o.Length).Returns(1);
            mockFile.Setup(o => o.ContentType).Returns("image/png");
            mockFile.Setup(o => o.FileName).Returns("foto_50.png");

            var mockIWebHostEnvironment = new Mock<IWebHostEnvironment>();
            mockIWebHostEnvironment.Setup(o => o.WebRootPath).Returns("C: \\Users\\Alex\\source\repos\\AppTUMISMITOPE.Test\\wwwrootTest");

            var mockManageFiles = new Mock<IManageFiles>();
            mockManageFiles.Setup(o => o.CombinePaths("files", "fotosPublicaciones", mockFile.Object.FileName)).Returns("files\\fotosPublicaciones\\foto_50.png");
            mockManageFiles.Setup(o => o.CombinePaths(It.IsAny<string>(), "files\\fotosPublicaciones\\foto_50.png")).Returns("C:\\Users\\Alex\\source\\repos\\AppTUMISMITOPE.Test\\wwwroootTest\\files\\fotosPublicaciones\\foto_50.png");
           


            HomeController hc = new HomeController(null, null, null, null, mockManageFiles.Object, mockIWebHostEnvironment.Object);

            var result = hc.SaveFile(mockFile.Object);

            Assert.IsInstanceOf<String>(result);

        }
        [Test]
        public void SaveFileCaso04()
        {
            var mockFile = new Mock<IFormFile>();
            //mockFile.Setup(o=>o.Length==1);
            mockFile.Setup(o => o.Length).Returns(1);
            mockFile.Setup(o => o.ContentType).Returns("image/jpeg");
            mockFile.Setup(o => o.FileName).Returns("foto_50.jpeg");

            var mockIWebHostEnvironment = new Mock<IWebHostEnvironment>();
            mockIWebHostEnvironment.Setup(o => o.WebRootPath).Returns("C: \\Users\\Alex\\source\repos\\AppTUMISMITOPE.Test\\wwwrootTest");

            var mockManageFiles = new Mock<IManageFiles>();
            mockManageFiles.Setup(o => o.CombinePaths("files", "fotosPublicaciones", mockFile.Object.FileName)).Returns("files\\fotosPublicaciones\\foto_50.jpeg");
            mockManageFiles.Setup(o => o.CombinePaths(It.IsAny<string>(), "files\\fotosPublicaciones\\foto_50.jpeg")).Returns("C:\\Users\\Alex\\source\\repos\\AppTUMISMITOPE.Test\\wwwroootTest\\files\\fotosPublicaciones\\foto_50.jpeg");
            String esperado = "/files/fotosPublicaciones/foto_50.jpeg";


            HomeController hc = new HomeController(null, null, null, null, mockManageFiles.Object, mockIWebHostEnvironment.Object);

            var result = hc.SaveFile(mockFile.Object);

            Assert.AreEqual(esperado,result);

        }
        [Test]
        public void SaveFileCaso05()
        {
            var mockFile = new Mock<IFormFile>();
            //mockFile.Setup(o=>o.Length==1);
            mockFile.Setup(o => o.Length).Returns(1);
            mockFile.Setup(o => o.ContentType).Returns("image/jpeg");
            mockFile.Setup(o => o.FileName).Returns("12345678.jpeg");

            var mockIWebHostEnvironment = new Mock<IWebHostEnvironment>();
            mockIWebHostEnvironment.Setup(o => o.WebRootPath).Returns("C: \\Users\\Alex\\source\repos\\AppTUMISMITOPE.Test\\wwwrootTest");

            var mockManageFiles = new Mock<IManageFiles>();
            mockManageFiles.Setup(o => o.CombinePaths("files", "fotosPublicaciones", mockFile.Object.FileName)).Returns("files\\fotosPublicaciones\\12345678.jpeg");
            mockManageFiles.Setup(o => o.CombinePaths(It.IsAny<string>(), "files\\fotosPublicaciones\\12345678.jpeg")).Returns("C:\\Users\\Alex\\source\\repos\\AppTUMISMITOPE.Test\\wwwroootTest\\files\\fotosPublicaciones\\12345678.jpeg");



            HomeController hc = new HomeController(null, null, null, null, mockManageFiles.Object, mockIWebHostEnvironment.Object);

            var result = hc.SaveFile(mockFile.Object);

            Assert.IsNotNull(result);

        }
        [Test]
        public void SaveFileCaso06()
        {
            var mockFile = new Mock<IFormFile>();
            //mockFile.Setup(o=>o.Length==1);
            mockFile.Setup(o => o.Length).Returns(1);
            mockFile.Setup(o => o.ContentType).Returns("image/jpeg");
            mockFile.Setup(o => o.FileName).Returns("12345678.jpeg");

            var mockIWebHostEnvironment = new Mock<IWebHostEnvironment>();
            mockIWebHostEnvironment.Setup(o => o.WebRootPath).Returns("C: \\Users\\Alex\\source\repos\\AppTUMISMITOPE.Test\\wwwrootTest");

            var mockManageFiles = new Mock<IManageFiles>();
            mockManageFiles.Setup(o => o.CombinePaths("files", "fotosPublicaciones", mockFile.Object.FileName)).Returns("files\\fotosPublicaciones\\12345678.jpeg");
            mockManageFiles.Setup(o => o.CombinePaths(It.IsAny<string>(), "files\\fotosPublicaciones\\12345678.jpeg")).Returns("C:\\Users\\Alex\\source\\repos\\AppTUMISMITOPE.Test\\wwwroootTest\\files\\fotosPublicaciones\\12345678.jpeg");
            String esperado = "/files/fotosPublicaciones/12345678.jpeg";


            HomeController hc = new HomeController(null, null, null, null, mockManageFiles.Object, mockIWebHostEnvironment.Object);

            var result = hc.SaveFile(mockFile.Object);

            Assert.AreEqual(esperado,result);

        }

        [Test]
        public void SaveFileCaso07()
        {
            var mockFile = new Mock<IFormFile>();
            //mockFile.Setup(o=>o.Length==1);
            mockFile.Setup(o => o.Length).Returns(1);
            mockFile.Setup(o => o.ContentType).Returns("image/jpg");
            mockFile.Setup(o => o.FileName).Returns("12345678.jpg");

            var mockIWebHostEnvironment = new Mock<IWebHostEnvironment>();
          
            var mockManageFiles = new Mock<IManageFiles>();
          
            HomeController hc = new HomeController(null, null, null, null, mockManageFiles.Object, mockIWebHostEnvironment.Object);
     
            Assert.Throws(typeof(Exception), () => hc.SaveFile(mockFile.Object));

        }
        [Test]
        public void SaveFileCaso08()
        {
            var mockFile = new Mock<IFormFile>();
            //mockFile.Setup(o=>o.Length==1);
            mockFile.Setup(o => o.Length).Returns(1);
            mockFile.Setup(o => o.ContentType).Returns("image/jpg");
            mockFile.Setup(o => o.FileName).Returns("12345678.jpg");

            var mockIWebHostEnvironment = new Mock<IWebHostEnvironment>();

            var mockManageFiles = new Mock<IManageFiles>();

            HomeController hc = new HomeController(null, null, null, null, mockManageFiles.Object, mockIWebHostEnvironment.Object);

            var exception = Assert.Throws<Exception>(() => hc.SaveFile(mockFile.Object));

            Assert.AreEqual("file with error extension", exception.Message);

        }

        [Test]
        public void SaveFileCaso09()
        {
            var mockFile = new Mock<IFormFile>();
            //mockFile.Setup(o=>o.Length==1);
            mockFile.Setup(o => o.Length).Returns(1);
            mockFile.Setup(o => o.ContentType).Returns("image/SVG ");
            mockFile.Setup(o => o.FileName).Returns("12345678.SVG ");

            var mockIWebHostEnvironment = new Mock<IWebHostEnvironment>();

            var mockManageFiles = new Mock<IManageFiles>();

            HomeController hc = new HomeController(null, null, null, null, mockManageFiles.Object, mockIWebHostEnvironment.Object);

            Assert.Throws(typeof(Exception), () => hc.SaveFile(mockFile.Object));

        }
        [Test]
        public void SaveFileCaso10()
        {
            var mockFile = new Mock<IFormFile>();
            //mockFile.Setup(o=>o.Length==1);
            mockFile.Setup(o => o.Length).Returns(1);
            mockFile.Setup(o => o.ContentType).Returns("image/SVG ");
            mockFile.Setup(o => o.FileName).Returns("12345678.SVG ");

            var mockIWebHostEnvironment = new Mock<IWebHostEnvironment>();

            var mockManageFiles = new Mock<IManageFiles>();

            HomeController hc = new HomeController(null, null, null, null, mockManageFiles.Object, mockIWebHostEnvironment.Object);

            var exception = Assert.Throws<Exception>(() => hc.SaveFile(mockFile.Object));

            Assert.AreEqual("file with error extension", exception.Message);

        }



        [Test]
        public void SaveFileCaso11()
        {
            var mockFile = new Mock<IFormFile>();
            //mockFile.Setup(o=>o.Length==1);
            mockFile.Setup(o => o.Length).Returns(1);
            mockFile.Setup(o => o.ContentType).Returns("image/bpm ");
            mockFile.Setup(o => o.FileName).Returns("12345678.bpm ");

            var mockIWebHostEnvironment = new Mock<IWebHostEnvironment>();

            var mockManageFiles = new Mock<IManageFiles>();

            HomeController hc = new HomeController(null, null, null, null, mockManageFiles.Object, mockIWebHostEnvironment.Object);

            Assert.Throws(typeof(Exception), () => hc.SaveFile(mockFile.Object));

        }
        [Test]
        public void SaveFileCaso12()
        {
            var mockFile = new Mock<IFormFile>();
            //mockFile.Setup(o=>o.Length==1);
            mockFile.Setup(o => o.Length).Returns(1);
            mockFile.Setup(o => o.ContentType).Returns("image/bpm ");
            mockFile.Setup(o => o.FileName).Returns("12345678.bpm ");

            var mockIWebHostEnvironment = new Mock<IWebHostEnvironment>();

            var mockManageFiles = new Mock<IManageFiles>();

            HomeController hc = new HomeController(null, null, null, null, mockManageFiles.Object, mockIWebHostEnvironment.Object);

            var exception = Assert.Throws<Exception>(() => hc.SaveFile(mockFile.Object));

            Assert.AreEqual("file with error extension", exception.Message);

        }

        [Test]
        public void SaveFileCaso13()
        {
            var mockFile = new Mock<IFormFile>();
            //mockFile.Setup(o=>o.Length==1);
            mockFile.Setup(o => o.Length).Returns(1);
            mockFile.Setup(o => o.ContentType).Returns("image/gif ");
            mockFile.Setup(o => o.FileName).Returns("12345678.gif ");

            var mockIWebHostEnvironment = new Mock<IWebHostEnvironment>();

            var mockManageFiles = new Mock<IManageFiles>();

            HomeController hc = new HomeController(null, null, null, null, mockManageFiles.Object, mockIWebHostEnvironment.Object);

            Assert.Throws(typeof(Exception), () => hc.SaveFile(mockFile.Object));

        }
        [Test]
        public void SaveFileCaso14()
        {
            var mockFile = new Mock<IFormFile>();
            //mockFile.Setup(o=>o.Length==1);
            mockFile.Setup(o => o.Length).Returns(1);
            mockFile.Setup(o => o.ContentType).Returns("image/gif ");
            mockFile.Setup(o => o.FileName).Returns("12345678.gif ");

            var mockIWebHostEnvironment = new Mock<IWebHostEnvironment>();

            var mockManageFiles = new Mock<IManageFiles>();

            HomeController hc = new HomeController(null, null, null, null, mockManageFiles.Object, mockIWebHostEnvironment.Object);

            var exception = Assert.Throws<Exception>(() => hc.SaveFile(mockFile.Object));

            Assert.AreEqual("file with error extension", exception.Message);

        }

       
    }

        
}
