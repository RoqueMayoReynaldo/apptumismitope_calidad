﻿using AppTUMISMITOPE.Authentication;
using AppTUMISMITOPE.Controllers;
using AppTUMISMITOPE.DateServices;
using AppTUMISMITOPE.FilesService;
using AppTUMISMITOPE.Models;
using AppTUMISMITOPE.Repository;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AppTUMISMITOPE.Test.ControllersTest
{
    
    public class PublicacionControllerTest
    {
        PublicacionController pc;
        Mock<IWebHostEnvironment> mockIWebHostEnvironment;
        Mock<IAutenticacion> mockAutenticacionRepo;
        Mock<IUsuarioRepository> mockUsuarioRepo;
        Mock<IPublicacionRepository> mockPublicacionRepo;
        Mock<IPostulacionRepository> mockPostulacionRepo;
        Mock<IComentarioRepository> mockComentarioRepo;
        Mock<IReplyComentarioRepository> mockReplyRepo;
        Mock<ITarjetaRepository> mockTarjetaRepo;
        Mock<IDonacionRepository> mockDonacionRepo;
        Mock<ICajaRepository> mockCajaRepo;
        Mock<IEvidenciaRepository> mockEvidenciaRepo;
        Mock<IFotoEvidenciaRepository> mockFotoEvidenciaRepo;
        Mock<IManageFiles> mockManageFiles;
        Mock<IDateMethods> mockDateMethods;






        [SetUp]
        public void SetUp()
        {
            mockIWebHostEnvironment = new Mock<IWebHostEnvironment>();
            mockAutenticacionRepo = new Mock<IAutenticacion>();
            mockUsuarioRepo = new Mock<IUsuarioRepository>();
            mockPublicacionRepo = new Mock<IPublicacionRepository>();
            mockPostulacionRepo = new Mock<IPostulacionRepository>();
            mockComentarioRepo = new Mock<IComentarioRepository>();
            mockReplyRepo = new Mock<IReplyComentarioRepository>();
            mockTarjetaRepo = new Mock<ITarjetaRepository>();
            mockDonacionRepo = new Mock<IDonacionRepository>();
            mockCajaRepo = new Mock<ICajaRepository>();
            mockEvidenciaRepo = new Mock<IEvidenciaRepository>();
            mockFotoEvidenciaRepo = new Mock<IFotoEvidenciaRepository>();
            mockManageFiles = new Mock<IManageFiles>();
            mockDateMethods = new Mock<IDateMethods>();

            pc = new PublicacionController(
                mockIWebHostEnvironment.Object,
                mockAutenticacionRepo.Object,
                mockUsuarioRepo.Object,
                mockPublicacionRepo.Object,
                mockPostulacionRepo.Object,
                mockComentarioRepo.Object,
                mockReplyRepo.Object,
                mockTarjetaRepo.Object,
                mockDonacionRepo.Object,
                mockCajaRepo.Object,
                mockEvidenciaRepo.Object,
                mockFotoEvidenciaRepo.Object,
                mockManageFiles.Object,
                mockDateMethods.Object
                );

        }


        [Test]
        public void DetalleCase01()
        {

            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario());
            mockPostulacionRepo.Setup(o => o.FindByPublicacionUsuario(It.IsAny<int>(), It.IsAny<int>())).Returns(new Postulacion());

            var result = pc.Detalle(2);
            Assert.NotNull(result);

        }

        [Test]
        public void DetalleCase02()
        {

            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario());
            mockPostulacionRepo.Setup(o => o.FindByPublicacionUsuario(It.IsAny<int>(), It.IsAny<int>())).Returns(new Postulacion());

            var result = pc.Detalle(2);
            Assert.IsNotInstanceOf<RedirectToActionResult>(result);

        }

        [Test]
        public void DetalleCase03()
        {

            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario());
            mockPostulacionRepo.Setup(o => o.FindByPublicacionUsuario(It.IsAny<int>(), It.IsAny<int>())).Returns(new Postulacion());

            var result = pc.Detalle(2);
            Assert.IsInstanceOf<ViewResult>(result);

        }
        [Test]
        public void DetalleCase04()
        {

            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario());
            mockPostulacionRepo.Setup(o => o.FindByPublicacionUsuario(It.IsAny<int>(), It.IsAny<int>())).Returns(new Postulacion());

            var result = pc.Detalle(2) as ViewResult;
            Assert.NotNull(result.Model);

        }
        [Test]
        public void DetalleCase05()
        {

            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario());
            mockPostulacionRepo.Setup(o => o.FindByPublicacionUsuario(It.IsAny<int>(), It.IsAny<int>())).Returns(new Postulacion());

            var result = pc.Detalle(2) as ViewResult;
            Assert.IsTrue(result.ViewName.StartsWith("D"));

        }
        [Test]
        public void DetalleCase06()
        {

            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario());
            mockPostulacionRepo.Setup(o => o.FindByPublicacionUsuario(It.IsAny<int>(), It.IsAny<int>())).Returns(new Postulacion());

            var result = pc.Detalle(2) as ViewResult;
            Assert.IsTrue(result.ViewName.EndsWith("e"));

        }

        [Test]
        public void DetalleCase07()
        {

            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario());
            mockPostulacionRepo.Setup(o => o.FindByPublicacionUsuario(It.IsAny<int>(), It.IsAny<int>())).Returns(new Postulacion());

            var result = pc.Detalle(2) as ViewResult;
            Assert.AreEqual("Detalle", result.ViewName);

        }

        [Test]
        public void DetalleCase08()
        {

            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario());
            mockPostulacionRepo.Setup(o => o.FindByPublicacionUsuario(It.IsAny<int>(), It.IsAny<int>())).Returns(new Postulacion());

            var result = pc.Detalle(2) as ViewResult;
            Assert.IsNull(result.TempData);

        }

        [Test]
        public void DetalleCase09()
        {

            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario());
            mockPostulacionRepo.Setup(o => o.FindByPublicacionUsuario(It.IsAny<int>(), It.IsAny<int>())).Returns(new Postulacion());

            var result = pc.Detalle(2) as ViewResult;
            Assert.IsTrue(result.ViewData.ModelState.IsValid);

        }
        [Test]
        public void DetalleCase10()
        {

            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario());
            mockPostulacionRepo.Setup(o => o.FindByPublicacionUsuario(It.IsAny<int>(), It.IsAny<int>())).Returns(new Postulacion());

            var result = pc.Detalle(2) as ViewResult;
            Assert.AreEqual(true, result.ViewData["Postulo"]);

        }
        [Test]
        public void DetalleCase11()
        {

            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion { id = 5 });

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario { id = 1 });
            mockPostulacionRepo.Setup(o => o.FindByPublicacionUsuario(It.IsAny<int>(), It.IsAny<int>())).Returns(value: null);

            var result = pc.Detalle(2) as ViewResult;
            Assert.AreEqual(false, result.ViewData["Postulo"]);

        }
        [Test]
        public void DetalleCase12()
        {

            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion { id = 5 });

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario { id = 1 });
            mockPostulacionRepo.Setup(o => o.FindByPublicacionUsuario(It.IsAny<int>(), It.IsAny<int>())).Returns(value: null);

            var result = pc.Detalle(2) as ViewResult;
            Assert.IsInstanceOf<PubModel>(result.Model);

        }

        [Test]
        public void DetalleCase13()
        {

            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion { id = 5 });

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario { id = 1 });
            mockPostulacionRepo.Setup(o => o.FindByPublicacionUsuario(It.IsAny<int>(), It.IsAny<int>())).Returns(value: null);

            var result = pc.Detalle(2) as ViewResult;

            var Modelo = (PubModel)result.ViewData.Model;

            Assert.AreEqual(1, Modelo.UsuarioActivo.id);

        }

        [Test]
        public void DetalleCase14()
        {

            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion { id = 5 });

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario { id = 1 });
            mockPostulacionRepo.Setup(o => o.FindByPublicacionUsuario(It.IsAny<int>(), It.IsAny<int>())).Returns(value: null);

            var result = pc.Detalle(2) as ViewResult;

            var Modelo = (PubModel)result.ViewData.Model;

            Assert.AreEqual(5, Modelo.pub.id);

        }

        [Test]
        public void DetalleCase15()
        {

            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion { id = 24 });

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario { id = 99 });
            mockPostulacionRepo.Setup(o => o.FindByPublicacionUsuario(It.IsAny<int>(), It.IsAny<int>())).Returns(value: null);

            var result = pc.Detalle(2) as ViewResult;

            var Modelo = (PubModel)result.ViewData.Model;

            Assert.AreEqual(99, Modelo.UsuarioActivo.id);

        }

        [Test]
        public void DetalleCase16()
        {

            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion { id = 42 });

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario { id = 11 });
            mockPostulacionRepo.Setup(o => o.FindByPublicacionUsuario(It.IsAny<int>(), It.IsAny<int>())).Returns(value: null);

            var result = pc.Detalle(2) as ViewResult;

            var Modelo = (PubModel)result.ViewData.Model;

            Assert.AreEqual(42, Modelo.pub.id);

        }

        //Comentar cases

        [Test]
        public void ComentarCase01()
        {

            var result = pc.Comentar(new Comentario());

            Assert.IsNotNull(result);

        }


        [Test]
        public void ComentarCase02()
        {

            var result = pc.Comentar(new Comentario());

            Assert.IsNotInstanceOf<RedirectToActionResult>(result);

        }

        [Test]
        public void ComentarCase03()
        {

            var result = pc.Comentar(new Comentario());

            Assert.IsInstanceOf<ViewResult>(result);

        }
        [Test]
        public void ComentarCase04()
        {

            mockComentarioRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(value: null);

            var result = pc.Comentar(new Comentario());

            Assert.IsNull(result.Model);

        }
        [Test]
        public void ComentarCase05()
        {

            mockComentarioRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(new Comentario());

            var result = pc.Comentar(new Comentario());

            Assert.IsNotNull(result.Model);

        }
        [Test]
        public void ComentarCase06()
        {

            mockComentarioRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(new Comentario());

            var result = pc.Comentar(new Comentario()) as ViewResult;

            Assert.IsTrue(result.ViewName.StartsWith("C"));

        }
        [Test]
        public void ComentarCase07()
        {

            mockComentarioRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(new Comentario());

            var result = pc.Comentar(new Comentario()) as ViewResult;

            Assert.IsTrue(result.ViewName.EndsWith("r"));

        }

        [Test]
        public void ComentarCase08()
        {

            mockComentarioRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(new Comentario());

            var result = pc.Comentar(new Comentario()) as ViewResult;

            Assert.AreEqual("Comentar", result.ViewName);

        }
        [Test]
        public void ComentarCase09()
        {

            mockComentarioRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(new Comentario());

            var result = pc.Comentar(new Comentario()) as ViewResult;

            Assert.IsNull(result.TempData);

        }
        [Test]
        public void ComentarCase10()
        {

            mockComentarioRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(new Comentario());

            var result = pc.Comentar(new Comentario()) as ViewResult;

            Assert.IsTrue(result.ViewData.ModelState.IsValid);

        }

        [Test]
        public void ComentarCase11()
        {

            mockComentarioRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(new Comentario());

            var result = pc.Comentar(new Comentario()) as ViewResult;

            Assert.IsInstanceOf<Comentario>(result.Model);

        }

        //cases postulaciones





        [Test]
        public void PostulacionesCase01()
        {



            var result = pc.Postulaciones(2);
            Assert.NotNull(result);

        }

        [Test]
        public void PostulacionesCase02()
        {

            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario());


            var result = pc.Postulaciones(2);
            Assert.IsNotInstanceOf<RedirectToActionResult>(result);

        }

        [Test]
        public void PostulacionesCase03()
        {
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario());

            var result = pc.Postulaciones(5);
            Assert.IsInstanceOf<ViewResult>(result);

        }


        [Test]
        public void PostulacionesCase04()
        {


            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario());

            var result = pc.Postulaciones(11) as ViewResult;
            Assert.NotNull(result.Model);

        }
        [Test]
        public void PostulacionesCase05()
        {

            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario());

            var result = pc.Postulaciones(24) as ViewResult;
            Assert.IsTrue(result.ViewName.StartsWith("P"));

        }
        [Test]
        public void PostulacionesCase06()
        {

            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario());

            var result = pc.Postulaciones(55) as ViewResult;
            Assert.IsTrue(result.ViewName.EndsWith("s"));

        }

        [Test]
        public void PostulacionesCase07()
        {


            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario());

            var result = pc.Postulaciones(13) as ViewResult;
            Assert.AreEqual("Postulaciones", result.ViewName);

        }

        [Test]
        public void PostulacionesCase08()
        {

            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario());

            var result = pc.Postulaciones(77) as ViewResult;
            Assert.IsNull(result.TempData);

        }

        [Test]
        public void PostulacionesCase09()
        {

            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario());

            var result = pc.Postulaciones(53) as ViewResult;
            Assert.IsTrue(result.ViewData.ModelState.IsValid);

        }
        [Test]
        public void PostulacionesCase10()
        {
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario());

            var result = pc.Postulaciones(44) as ViewResult;

            //verificamos que no exista ningun viewbag
            Assert.IsEmpty(result.ViewData);

        }

        [Test]
        public void PostulacionesCase11()
        {

            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario());

            var result = pc.Postulaciones(76) as ViewResult;
            Assert.IsInstanceOf<PubModel>(result.Model);

        }

        [Test]
        public void PostulacionesCase12()
        {


            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario { id = 16 });

            var result = pc.Postulaciones(76) as ViewResult;

            var Modelo = (PubModel)result.ViewData.Model;

            Assert.AreEqual(16, Modelo.UsuarioActivo.id);

        }

        [Test]
        public void PostulacionesCase13()
        {

            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion { id = 55 });

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario { id = 16 });

            var result = pc.Postulaciones(85) as ViewResult;

            var Modelo = (PubModel)result.ViewData.Model;

            Assert.AreEqual(55, Modelo.pub.id);

        }

        [Test]
        public void PostulacionesCase14()
        {

            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion { id = 55 });

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario { id = 225 });

            var result = pc.Postulaciones(85) as ViewResult;

            var Modelo = (PubModel)result.ViewData.Model;

            Assert.AreEqual(225, Modelo.UsuarioActivo.id);

        }

        //cases formReply



        [Test]
        public void FormReplyCase01()
        {



            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario());

            var result = pc.FormReply(2, 4);
            Assert.NotNull(result);

        }

        [Test]
        public void FormReplyCase02()
        {



            var result = pc.FormReply(15, 5);

            Assert.IsNotInstanceOf<RedirectToActionResult>(result);

        }

        [Test]
        public void FormReplyCase03()
        {


            var result = pc.FormReply(11, 5);

            Assert.IsInstanceOf<ViewResult>(result);

        }
        [Test]
        public void FormReplyCase04()
        {

            mockUsuarioRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(new Usuario());



            var result = pc.FormReply(6, 7);
            Assert.NotNull(result.Model);

        }
        [Test]
        public void FormReplyCase05()
        {

            mockUsuarioRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(new Usuario());



            var result = pc.FormReply(8, 6) as ViewResult;
            Assert.IsTrue(result.ViewName.StartsWith("F"));

        }
        [Test]
        public void FormReplyCase06()
        {


            mockUsuarioRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(new Usuario());



            var result = pc.FormReply(5, 1) as ViewResult;
            Assert.IsTrue(result.ViewName.EndsWith("y"));

        }

        [Test]
        public void FormReplyCase07()
        {

            mockUsuarioRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(new Usuario());



            var result = pc.FormReply(5, 1) as ViewResult;
            Assert.AreEqual("FormReply", result.ViewName);

        }

        [Test]
        public void FormReplyCase08()
        {

            mockUsuarioRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(new Usuario());

            var result = pc.FormReply(5, 1) as ViewResult;
            Assert.IsNull(result.TempData);

        }

        [Test]
        public void FormReplyCase09()
        {
            mockUsuarioRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(new Usuario());

            var result = pc.FormReply(8, 16) as ViewResult;
            Assert.IsTrue(result.ViewData.ModelState.IsValid);

        }
        [Test]
        public void FormReplyCase10()
        {
            mockUsuarioRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(new Usuario());

            var result = pc.FormReply(68, 67) as ViewResult;
            Assert.IsNotEmpty(result.ViewData);

        }
        [Test]
        public void FormReplyCase11()
        {
            mockUsuarioRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(new Usuario());

            var result = pc.FormReply(68, 67) as ViewResult;
            Assert.AreEqual(68, result.ViewData["idComent"]);

        }

        [Test]
        public void FormReplyCase12()
        {

            mockUsuarioRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(new Usuario());

            var result = pc.FormReply(87, 5) as ViewResult;

            Assert.IsInstanceOf<Usuario>(result.Model);

        }

        [Test]
        public void FormReplyCase13()
        {

            mockUsuarioRepo.Setup(o => o.FindById(14)).Returns(new Usuario { id = 14 });

            var result = pc.FormReply(87, 14) as ViewResult;

            var Modelo = (Usuario)result.ViewData.Model;

            Assert.AreEqual(14, Modelo.id);

        }

        [Test]
        public void FormReplyCase14()
        {

            mockUsuarioRepo.Setup(o => o.FindById(5)).Returns(new Usuario { id = 50, nombre = "Berryck" });

            var result = pc.FormReply(11, 5) as ViewResult;

            var Modelo = (Usuario)result.ViewData.Model;

            Assert.AreEqual("Berryck", Modelo.nombre);

        }

        [Test]
        public void FormReplyCase15()
        {

            mockUsuarioRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(value: null);

            var result = pc.FormReply(7, 6) as ViewResult;

            var Modelo = (Usuario)result.ViewData.Model;

            Assert.IsNull(Modelo);

        }

        //cases sendReply



        [Test]
        public void SendReplyCase01()
        {


            mockReplyRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(new ReplyComentario());
            var result = pc.SendReply(new ReplyComentario());
            Assert.NotNull(result);

        }

        [Test]
        public void SendReplyCase02()
        {

            mockReplyRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(new ReplyComentario());
            var result = pc.SendReply(new ReplyComentario());

            Assert.IsNotInstanceOf<RedirectToActionResult>(result);

        }

        [Test]
        public void SendReplyCase03()
        {
            mockReplyRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(new ReplyComentario());

            var result = pc.SendReply(new ReplyComentario());

            Assert.IsInstanceOf<ViewResult>(result);

        }
        [Test]
        public void SendReplyCase04()
        {
            mockReplyRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(new ReplyComentario()) ;
            var result = pc.SendReply(new ReplyComentario());
            Assert.NotNull(result.Model);

        }
        [Test]
        public void SendReplyCase05()
        {


            var result = pc.SendReply(new ReplyComentario()) as ViewResult;
            Assert.IsTrue(result.ViewName.StartsWith("S"));

        }
        [Test]
        public void SendReplyCase06()
        {

            var result = pc.SendReply(new ReplyComentario()) as ViewResult;
            Assert.IsTrue(result.ViewName.EndsWith("y"));

        }

        [Test]
        public void SendReplyCase07()
        {

            var result = pc.SendReply(new ReplyComentario()) as ViewResult;
            Assert.AreEqual("SendReply", result.ViewName);

        }

        [Test]
        public void SendReplyCase08()
        {

            var result = pc.SendReply(new ReplyComentario()) as ViewResult;
            Assert.IsNull(result.TempData);

        }

        [Test]
        public void SendReplyCase09()
        {
            var result = pc.SendReply(new ReplyComentario()) as ViewResult;
            Assert.IsTrue(result.ViewData.ModelState.IsValid);

        }
        [Test]
        public void SendReplyCase10()
        {
            var result = pc.SendReply(new ReplyComentario()) as ViewResult;
            Assert.IsEmpty(result.ViewData);

        }


        [Test]
        public void SendReplyCase11()
        {
            mockReplyRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(new ReplyComentario());
            var result = pc.SendReply(new ReplyComentario()) as ViewResult;

            Assert.IsInstanceOf<ReplyComentario>(result.Model);

        }

        [Test]
        public void SendReplyCase12()
        {
            ReplyComentario rc = new ReplyComentario {id=16 };
            mockReplyRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(rc);
            var result = pc.SendReply(new ReplyComentario { id = 16 }) as ViewResult;

            var Modelo = (ReplyComentario)result.ViewData.Model;

            Assert.AreEqual(16, Modelo.id);

        }

        [Test]
        public void SendReplyCase13()
        {
            ReplyComentario rc = new ReplyComentario { id = 18,idUsuario=55,contenido= "Testeando ando" };
            mockReplyRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(rc);
            var result = pc.SendReply(new ReplyComentario { id = 18, idUsuario = 55, contenido = "Testeando ando" }) as ViewResult;


            var Modelo = (ReplyComentario)result.ViewData.Model;

            Assert.AreEqual("Testeando ando", Modelo.contenido);

        }

        [Test]
        public void SendReplyCase14()
        {





            Assert.Throws(typeof(Exception), () => pc.SendReply(null));

        }
        [Test]
        public void SendReplyCase15()
        {


            var exception = Assert.Throws<Exception>(() => pc.SendReply(null));

            Assert.AreEqual("error for parameter null", exception.Message);

        }


        //cases setDonacion





        [Test]
        public void setDonacionCase01()
        {

            mockTarjetaRepo.Setup(o => o.GetTarjetaByModel(It.IsAny<Tarjeta>())).Returns(new Tarjeta { saldo = 2400 });

            var result = pc.setDonacion(new Tarjeta(), new Donacion { monto = 200 });

            Assert.NotNull(result);


        }


        [Test]
        public void setDonacionCase02()
        {

            mockTarjetaRepo.Setup(o => o.GetTarjetaByModel(It.IsAny<Tarjeta>())).Returns(new Tarjeta { saldo = 5000 });

            var result = pc.setDonacion(new Tarjeta(), new Donacion { monto = 200 });


            Assert.IsNotInstanceOf<ViewResult>(result);

        }

        [Test]
        public void setDonacionCase03()
        {

            mockTarjetaRepo.Setup(o => o.GetTarjetaByModel(It.IsAny<Tarjeta>())).Returns(new Tarjeta { saldo = 5000 });

            var result = pc.setDonacion(new Tarjeta(), new Donacion { monto = 200 });
            Assert.IsInstanceOf<RedirectToActionResult>(result);

        }
        [Test]
        public void setDonacionCase04()
        {

            mockTarjetaRepo.Setup(o => o.GetTarjetaByModel(It.IsAny<Tarjeta>())).Returns(new Tarjeta { saldo = 5000 });

            var result = pc.setDonacion(new Tarjeta(), new Donacion { monto = 200 }) as RedirectToActionResult;

            Assert.NotNull(result.RouteValues);

        }
        [Test]
        public void setDonacionCase05()
        {

            mockTarjetaRepo.Setup(o => o.GetTarjetaByModel(It.IsAny<Tarjeta>())).Returns(new Tarjeta { saldo = 5000 });

            var result = pc.setDonacion(new Tarjeta(), new Donacion { monto = 200 }) as RedirectToActionResult;
            Assert.IsTrue(result.ActionName.StartsWith("D"));

        }
        [Test]
        public void setDonacionCase06()
        {
            mockTarjetaRepo.Setup(o => o.GetTarjetaByModel(It.IsAny<Tarjeta>())).Returns(new Tarjeta { saldo = 5000 });

            var result = pc.setDonacion(new Tarjeta(), new Donacion { monto = 200 }) as RedirectToActionResult;
            Assert.IsTrue(result.ActionName.EndsWith("e"));
        }

        [Test]
        public void setDonacionCase07()
        {
            mockTarjetaRepo.Setup(o => o.GetTarjetaByModel(It.IsAny<Tarjeta>())).Returns(new Tarjeta { saldo = 5000 });

            var result = pc.setDonacion(new Tarjeta(), new Donacion { monto = 200 }) as RedirectToActionResult;
            Assert.AreEqual("Detalle", result.ActionName);

        }
        [Test]
        public void setDonacionCase08()
        {
            mockTarjetaRepo.Setup(o => o.GetTarjetaByModel(It.IsAny<Tarjeta>())).Returns(new Tarjeta { saldo = 5000 });

            var result = pc.setDonacion(new Tarjeta(), new Donacion { monto = 200 }) as RedirectToActionResult;
            Assert.IsNull(result.ControllerName);

        }


        [Test]
        public void setDonacionCase09()
        {


            mockTarjetaRepo.Setup(o => o.GetTarjetaByModel(It.IsAny<Tarjeta>())).Returns(new Tarjeta { saldo = 5000 });

            var result = pc.setDonacion(new Tarjeta(), new Donacion { monto = 200 }) as RedirectToActionResult;
            Assert.IsTrue(pc.ModelState.IsValid);

        }
        [Test]
        public void setDonacionCase10()
        {

            mockTarjetaRepo.Setup(o => o.GetTarjetaByModel(It.IsAny<Tarjeta>())).Returns(new Tarjeta { saldo = 5000 });

            var result = pc.setDonacion(new Tarjeta(), new Donacion { idPublicacion = 14, monto = 200 }) as RedirectToActionResult;
            object valor;


            //verificamos si el valor pasado al redirectoaction es el mismo accediendo a los valores del route
            result.RouteValues.TryGetValue("id", out valor);

            Assert.AreEqual(14, valor);

        }
        [Test]
        public void setDonacionCase11()
        {

            mockTarjetaRepo.Setup(o => o.GetTarjetaByModel(It.IsAny<Tarjeta>())).Returns(new Tarjeta { saldo = 5000 });

            var result = pc.setDonacion(new Tarjeta(), new Donacion { monto = 200 }) as RedirectToActionResult;
            object valor;


            //verificamos si el valor pasado al redirectoaction es el mismo accedienco a los valores del route
            result.RouteValues.TryGetValue("id", out valor);

            Assert.AreEqual(0, valor);

        }

        [Test]
        public void setDonacionCase12()
        {
            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<string>())).Returns(new Usuario { id = 14 });
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion { id = 1 });

            //


            mockTarjetaRepo.Setup(o => o.GetTarjetaByModel(It.IsAny<Tarjeta>())).Returns(new Tarjeta { saldo = 0 });

            var result = pc.setDonacion(new Tarjeta(), new Donacion { monto = 200 }) as RedirectToActionResult;
            Assert.IsFalse(pc.ModelState.IsValid);

        }
        [Test]
        public void setDonacionCase13()
        {
            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<string>())).Returns(new Usuario { id = 14 });
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion { id = 1 });

            //


            mockTarjetaRepo.Setup(o => o.GetTarjetaByModel(It.IsAny<Tarjeta>())).Returns(new Tarjeta { saldo = 8000 });

            var result = pc.setDonacion(new Tarjeta(), new Donacion { monto = 9000000 }) as RedirectToActionResult;
            Assert.IsFalse(pc.ModelState.IsValid);

        }
        [Test]
        public void setDonacionCase14()
        {
            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<string>())).Returns(new Usuario { id = 14 });
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion { id = 1 });

            //


            mockTarjetaRepo.Setup(o => o.GetTarjetaByModel(It.IsAny<Tarjeta>())).Returns(new Tarjeta { saldo = 800 });

            var result = pc.setDonacion(new Tarjeta(), new Donacion { monto = 800.1 }) as RedirectToActionResult;
            Assert.IsFalse(pc.ModelState.IsValid);

        }


        [Test]
        public void setDonacionCase15()
        {
            Tarjeta tarjeta = new Tarjeta { id = 16, saldo = 2000 };

            mockTarjetaRepo.Setup(o => o.GetTarjetaByModel(It.IsAny<Tarjeta>())).Returns(tarjeta);

            var result = pc.setDonacion(new Tarjeta(), new Donacion { monto = 200 }) as RedirectToActionResult;

            Assert.AreEqual(1800, tarjeta.saldo);

        }


        [Test]
        public void setDonacionCase16()
        {



            Tarjeta tarjeta = new Tarjeta { id = 16, saldo = 2000 };

            mockTarjetaRepo.Setup(o => o.GetTarjetaByModel(It.IsAny<Tarjeta>())).Returns(tarjeta);

            var result = pc.setDonacion(new Tarjeta(), new Donacion { monto = 20.5 }) as RedirectToActionResult;

            Assert.AreEqual(1979.5, tarjeta.saldo);

        }

        [Test]
        public void setDonacionCase17()
        {

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<string>())).Returns(new Usuario { id = 14 });
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion { id = 1 });
            //

            Tarjeta tarjeta = new Tarjeta { id = 16, saldo = 0 };

            mockTarjetaRepo.Setup(o => o.GetTarjetaByModel(It.IsAny<Tarjeta>())).Returns(tarjeta);

            var result = pc.setDonacion(new Tarjeta(), new Donacion { monto = 20.5 }) as RedirectToActionResult;



            //verificamos mensaje de error del modelstate

            Assert.AreEqual("Saldo insuficiente", pc.ModelState["Mensaje"].Errors[0].ErrorMessage);

        }

        [Test]
        public void setDonacionCase18()
        {

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<string>())).Returns(new Usuario { id = 14 });
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion { id = 1 });
            //

            Tarjeta tarjeta = new Tarjeta { id = 16, saldo = 50000 };

            mockTarjetaRepo.Setup(o => o.GetTarjetaByModel(It.IsAny<Tarjeta>())).Returns(value: null);

            var result = pc.setDonacion(new Tarjeta(), new Donacion { monto = 20.5 }) as RedirectToActionResult;



            //verificamos mensaje de error del modelstate

            Assert.AreEqual("Verifique datos de tarjeta", pc.ModelState["Mensaje"].Errors[0].ErrorMessage);

        }

        [Test]
        public void setDonacionCase19()
        {

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<string>())).Returns(new Usuario { id = 14 });
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion { id = 1 });
            //

            Tarjeta tarjeta = new Tarjeta { id = 16, saldo = 50000 };

            mockTarjetaRepo.Setup(o => o.GetTarjetaByModel(It.IsAny<Tarjeta>())).Returns(value: null);

            var result = pc.setDonacion(new Tarjeta(), new Donacion { monto = 20.5 });



            //verificamos mensaje de error del modelstate


            Assert.IsNotInstanceOf<RedirectToActionResult>(result);

        }

        [Test]
        public void setDonacionCase20()
        {

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<string>())).Returns(new Usuario { id = 14 });
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion { id = 1 });
            //

            Tarjeta tarjeta = new Tarjeta { id = 16, saldo = 50000 };

            mockTarjetaRepo.Setup(o => o.GetTarjetaByModel(It.IsAny<Tarjeta>())).Returns(value: null);

            var result = pc.setDonacion(new Tarjeta(), new Donacion { monto = 20.5 });



            //verificamos mensaje de error del modelstate

            Assert.IsInstanceOf<ViewResult>(result);

        }

        [Test]
        public void setDonacionCase21()
        {

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<string>())).Returns(new Usuario { id = 14 });
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion { id = 1 });
            //

            Tarjeta tarjeta = new Tarjeta { id = 16, saldo = 0 };

            mockTarjetaRepo.Setup(o => o.GetTarjetaByModel(It.IsAny<Tarjeta>())).Returns(new Tarjeta { id = 154 });

            var result = pc.setDonacion(new Tarjeta(), new Donacion { monto = 20.5 });



            //verificamos mensaje de error del modelstate


            Assert.IsNotInstanceOf<RedirectToActionResult>(result);

        }

        [Test]
        public void setDonacionCase22()
        {

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<string>())).Returns(new Usuario { id = 14 });
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion { id = 1 });
            //

            Tarjeta tarjeta = new Tarjeta { id = 16, saldo = 1 };

            mockTarjetaRepo.Setup(o => o.GetTarjetaByModel(It.IsAny<Tarjeta>())).Returns(new Tarjeta { id = 12 });

            var result = pc.setDonacion(new Tarjeta(), new Donacion { monto = 20.5 });



            //verificamos mensaje de error del modelstate

            Assert.IsInstanceOf<ViewResult>(result);

        }


        [Test]
        public void setDonacionCase23()
        {

            Publicacion publicacion = new Publicacion { id = 5, vista = 44 };

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<string>())).Returns(new Usuario { id = 14 });
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(publicacion);
            //

            Tarjeta tarjeta = new Tarjeta { id = 16, saldo = 1 };

            mockTarjetaRepo.Setup(o => o.GetTarjetaByModel(It.IsAny<Tarjeta>())).Returns(new Tarjeta { id = 12 });

            var result = pc.setDonacion(new Tarjeta(), new Donacion { monto = 20.5 }) as ViewResult;



            Assert.AreEqual(45, publicacion.vista);

        }
        [Test]
        public void setDonacionCase24()
        {



            Publicacion publicacion = new Publicacion { id = 5, vista = 44 };

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<string>())).Returns(new Usuario { id = 14 });
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(publicacion);

            mockPostulacionRepo.Setup(o => o.FindByPublicacionUsuario(It.IsAny<int>(), It.IsAny<int>())).Returns(value: null);
            //  

            Tarjeta tarjeta = new Tarjeta { id = 16, saldo = 1 };

            mockTarjetaRepo.Setup(o => o.GetTarjetaByModel(It.IsAny<Tarjeta>())).Returns(new Tarjeta { id = 12 });

            var result = pc.setDonacion(new Tarjeta(), new Donacion { monto = 20.5 }) as ViewResult;


            //verificamos el valor del viewbag cooresponda con el resultado esperado al pasar ciertos valores


            Assert.AreEqual(false, result.ViewData["Postulo"]);

        }
        [Test]
        public void setDonacionCase25()
        {



            Publicacion publicacion = new Publicacion { id = 5, vista = 44 };

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<string>())).Returns(new Usuario { id = 14 });
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(publicacion);

            mockPostulacionRepo.Setup(o => o.FindByPublicacionUsuario(It.IsAny<int>(), It.IsAny<int>())).Returns(new Postulacion());
            //  

            Tarjeta tarjeta = new Tarjeta { id = 16, saldo = 1 };

            mockTarjetaRepo.Setup(o => o.GetTarjetaByModel(It.IsAny<Tarjeta>())).Returns(new Tarjeta { id = 12 });

            var result = pc.setDonacion(new Tarjeta(), new Donacion { monto = 20.5 }) as ViewResult;





            Assert.AreEqual(true, result.ViewData["Postulo"]);

        }
        [Test]
        public void setDonacionCase26()
        {



            Publicacion publicacion = new Publicacion { id = 5, vista = 44 };

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<string>())).Returns(new Usuario { id = 14 });
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(publicacion);

            mockPostulacionRepo.Setup(o => o.FindByPublicacionUsuario(It.IsAny<int>(), It.IsAny<int>())).Returns(new Postulacion());


            DateTime fecha = new DateTime(2021, 2, 13);
            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha);

            //  

            Tarjeta tarjeta = new Tarjeta { id = 16, saldo = 1 };

            mockTarjetaRepo.Setup(o => o.GetTarjetaByModel(It.IsAny<Tarjeta>())).Returns(new Tarjeta { id = 12 });

            var result = pc.setDonacion(new Tarjeta(), new Donacion { monto = 20.5 }) as ViewResult;

            var Modelo = result.Model as PubModel;




            Assert.AreEqual(fecha, Modelo.fechaActual);

        }
        [Test]
        public void setDonacionCase27()
        {



            Usuario usuario = new Usuario { id = 15, nombre = "Reynaldo" };

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<string>())).Returns(usuario);
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());

            mockPostulacionRepo.Setup(o => o.FindByPublicacionUsuario(It.IsAny<int>(), It.IsAny<int>())).Returns(new Postulacion());


            DateTime fecha = new DateTime(2021, 2, 13);
            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha);

            //  

            Tarjeta tarjeta = new Tarjeta { id = 16, saldo = 1 };

            mockTarjetaRepo.Setup(o => o.GetTarjetaByModel(It.IsAny<Tarjeta>())).Returns(new Tarjeta { id = 12 });

            var result = pc.setDonacion(new Tarjeta(), new Donacion { monto = 20.5 }) as ViewResult;

            var Modelo = result.Model as PubModel;




            Assert.AreEqual("Reynaldo", Modelo.UsuarioActivo.nombre);

        }
        [Test]
        public void setDonacionCase30()
        {



            Publicacion publicacion = new Publicacion { id = 66 };

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<string>())).Returns(new Usuario());
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(publicacion);

            mockPostulacionRepo.Setup(o => o.FindByPublicacionUsuario(It.IsAny<int>(), It.IsAny<int>())).Returns(new Postulacion());


            DateTime fecha = new DateTime(2021, 2, 13);
            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha);

            //  

            Tarjeta tarjeta = new Tarjeta { id = 16, saldo = 1 };

            mockTarjetaRepo.Setup(o => o.GetTarjetaByModel(It.IsAny<Tarjeta>())).Returns(new Tarjeta { id = 12 });

            var result = pc.setDonacion(new Tarjeta(), new Donacion { monto = 20.5 }) as ViewResult;

            var Modelo = result.Model as PubModel;




            Assert.AreEqual(66, Modelo.pub.id);

        }
        [Test]
        public void setDonacionCase31()
        {



            Publicacion publicacion = new Publicacion { id = 66 };

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<string>())).Returns(new Usuario());
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(publicacion);

            mockPostulacionRepo.Setup(o => o.FindByPublicacionUsuario(It.IsAny<int>(), It.IsAny<int>())).Returns(new Postulacion());


            DateTime fecha = new DateTime(2021, 2, 13);
            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha);

            //  

            Tarjeta tarjeta = new Tarjeta { id = 16, saldo = 1 };

            mockTarjetaRepo.Setup(o => o.GetTarjetaByModel(It.IsAny<Tarjeta>())).Returns(new Tarjeta { id = 12 });

            var result = pc.setDonacion(new Tarjeta(), new Donacion { monto = 20.5 }) as ViewResult;






            Assert.IsNotNull(result.Model);

        }

        [Test]
        public void setDonacionCase32()
        {



            Publicacion publicacion = new Publicacion { id = 66 };

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<string>())).Returns(new Usuario());
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(publicacion);

            mockPostulacionRepo.Setup(o => o.FindByPublicacionUsuario(It.IsAny<int>(), It.IsAny<int>())).Returns(new Postulacion());


            DateTime fecha = new DateTime(2021, 2, 13);
            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha);

            //  

            Tarjeta tarjeta = new Tarjeta { id = 16, saldo = 1 };

            mockTarjetaRepo.Setup(o => o.GetTarjetaByModel(It.IsAny<Tarjeta>())).Returns(new Tarjeta { id = 12 });

            var result = pc.setDonacion(new Tarjeta(), new Donacion { monto = 20.5 }) as ViewResult;





            Assert.AreEqual("Detalle", result.ViewName);


        }

        [Test]
        public void setDonacionCase33()
        {



            Publicacion publicacion = new Publicacion { id = 66 };

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<string>())).Returns(new Usuario());
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(publicacion);

            mockPostulacionRepo.Setup(o => o.FindByPublicacionUsuario(It.IsAny<int>(), It.IsAny<int>())).Returns(new Postulacion());


            DateTime fecha = new DateTime(2021, 2, 13);
            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha);

            //  

            Tarjeta tarjeta = new Tarjeta { id = 16, saldo = 1 };

            mockTarjetaRepo.Setup(o => o.GetTarjetaByModel(It.IsAny<Tarjeta>())).Returns(new Tarjeta { id = 12 });

            var result = pc.setDonacion(new Tarjeta(), new Donacion { monto = 20.5 }) as ViewResult;





            Assert.IsInstanceOf<PubModel>(result.Model);


        }
        //cases postular

        [Test]

        public void PostularCase01()
        {
            Postulacion p = new Postulacion { id = 14, idPublicacion = 3 };

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario());
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());


            var result = pc.Postular(p, 1);


            Assert.IsNotNull(result);
        }




        [Test]

        public void PostularCase02()
        {
            Postulacion p = new Postulacion { id = 14, idPublicacion = 3, monto = 100, diasRealizacion = 10, descripcion = "Trabajo garantizado" };

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario());
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());


            var result = pc.Postular(p, 1);






            Assert.IsNotInstanceOf<RedirectToActionResult>(result);
        }

        [Test]

        public void PostularCase03()
        {
            Postulacion p = new Postulacion { id = 14, idPublicacion = 3, monto = 100, diasRealizacion = 10, descripcion = "Trabajo garantizado" };

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario());
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());


            var result = pc.Postular(p, 1);






            Assert.IsInstanceOf<ViewResult>(result);
        }
        [Test]

        public void PostularCase04()
        {
            Postulacion p = new Postulacion { id = 14, idPublicacion = 3 };

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario());
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());


            var result = pc.Postular(p, 1);






            Assert.IsInstanceOf<ViewResult>(result);
        }
        [Test]

        public void PostularCase05()
        {
            Postulacion p = new Postulacion { id = 14, idPublicacion = 3 };

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario());
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());


            var result = pc.Postular(p, 1) as ViewResult;






            Assert.IsTrue(result.ViewName.StartsWith("D"));
            Assert.IsTrue(result.ViewName.EndsWith("e"));
            Assert.AreEqual("Detalle", result.ViewName);

        }

        [Test]

        public void PostularCase06()
        {
            Postulacion p = new Postulacion { id = 14, idPublicacion = 3 };

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario());
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());


            var result = pc.Postular(p, 1) as ViewResult;





            Assert.IsNotNull(result.Model);

        }
        [Test]

        public void PostularCase07()
        {
            Postulacion p = new Postulacion { id = 14, idPublicacion = 3 };

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario());
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());


            var result = pc.Postular(p, 1) as ViewResult;





            Assert.IsInstanceOf<PubModel>(result.Model);

        }

        [Test]

        public void PostularCase08()
        {
            Postulacion p = new Postulacion { id = 14, idPublicacion = 3 };

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario());
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());


            var result = pc.Postular(p, 1) as ViewResult;



            Assert.IsFalse(result.ViewData.ModelState.IsValid);

            Assert.AreEqual("Descripcion requerida", result.ViewData.ModelState["descripcion"].Errors[0].ErrorMessage);
            Assert.AreEqual("Monto requerido", result.ViewData.ModelState["monto"].Errors[0].ErrorMessage);
            Assert.AreEqual("Dias requerido", result.ViewData.ModelState["diasRealizacion"].Errors[0].ErrorMessage);
        }

        [Test]

        public void PostularCase09()
        {
            Postulacion p = new Postulacion { id = 14, idPublicacion = 3, descripcion = "Testeando" };

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario());
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());


            var result = pc.Postular(p, 1) as ViewResult;



            Assert.IsFalse(result.ViewData.ModelState.IsValid);
            Assert.IsNull(result.ViewData.ModelState["descripcion"]);
            Assert.AreEqual("Monto requerido", result.ViewData.ModelState["monto"].Errors[0].ErrorMessage);
            Assert.AreEqual("Dias requerido", result.ViewData.ModelState["diasRealizacion"].Errors[0].ErrorMessage);

        }
        [Test]
        public void PostularCase10()
        {
            Postulacion p = new Postulacion { id = 14, idPublicacion = 3, descripcion = "Testeando", diasRealizacion = 4 };

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario());
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());


            var result = pc.Postular(p, 1) as ViewResult;



            Assert.IsFalse(result.ViewData.ModelState.IsValid);
            Assert.IsNull(result.ViewData.ModelState["descripcion"]);
            Assert.IsNull(result.ViewData.ModelState["diasRealizacion"]);
            Assert.AreEqual("Monto requerido", result.ViewData.ModelState["monto"].Errors[0].ErrorMessage);


        }

        [Test]
        public void PostularCase11()
        {
            //cuando es valido

            Postulacion p = new Postulacion { id = 14, idPublicacion = 3, descripcion = "Testeando", diasRealizacion = 4, monto = 2 };

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario());
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());


            var result = pc.Postular(p, 1) as ViewResult;



            Assert.IsTrue(result.ViewData.ModelState.IsValid);
            Assert.IsNull(result.ViewData.ModelState["descripcion"]);
            Assert.IsNull(result.ViewData.ModelState["diasRealizacion"]);
            Assert.IsNull(result.ViewData.ModelState["monto"]);


        }

        [Test]
        public void PostularCase12()
        {


            Postulacion p = new Postulacion { id = 14, idPublicacion = 3, descripcion = "Testeando", monto = 2 };

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario());
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());


            var result = pc.Postular(p, 1) as ViewResult;



            Assert.IsFalse(result.ViewData.ModelState.IsValid);
            Assert.IsNull(result.ViewData.ModelState["descripcion"]);
            Assert.IsNull(result.ViewData.ModelState["monto"]);

            Assert.AreEqual("Dias requerido", result.ViewData.ModelState["diasRealizacion"].Errors[0].ErrorMessage);



        }

        [Test]
        public void PostularCase13()
        {


            Postulacion p = new Postulacion { id = 14, idPublicacion = 3, monto = 2 };

            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario());
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());


            var result = pc.Postular(p, 1) as ViewResult;



            Assert.IsFalse(result.ViewData.ModelState.IsValid);


            Assert.IsNull(result.ViewData.ModelState["monto"]);
            Assert.AreEqual("Descripcion requerida", result.ViewData.ModelState["descripcion"].Errors[0].ErrorMessage); ;

            Assert.AreEqual("Dias requerido", result.ViewData.ModelState["diasRealizacion"].Errors[0].ErrorMessage);



        }


        [Test]
        public void PostularCase14()
        {
            DateTime fecha = new DateTime(2020, 2, 16);

            Postulacion p = new Postulacion { id = 14, idPublicacion = 3, monto = 2, descripcion = "testeando", diasRealizacion = 14 };

            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha);
            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario());
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());


            var result = pc.Postular(p, 5) as ViewResult;



            Assert.IsTrue(result.ViewData.ModelState.IsValid);

            Assert.AreEqual(0, p.estado);
            Assert.AreEqual(fecha, p.fechaCreacion);


        }
        [Test]
        public void PostularCase15()
        {
            DateTime fecha = new DateTime(2052, 2, 10);

            Postulacion p = new Postulacion { id = 14, idPublicacion = 3, monto = 2, descripcion = "testeando", diasRealizacion = 14 };

            Publicacion publicacion = new Publicacion { id = 3, limitePostulaciones = fecha };


            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha);
            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario());
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(publicacion);




            var result = pc.Postular(p, 5) as ViewResult;



            Assert.IsTrue(result.ViewData.ModelState.IsValid);


            //verificamos que la fecha limite no cambie ya que pasamos el valor 5 como cantidad de postulaciones

            Assert.AreEqual(fecha, publicacion.limitePostulaciones);



        }
        [Test]
        public void PostularCase16()
        {
            DateTime fecha = new DateTime(2052, 2, 10);

            Postulacion p = new Postulacion { id = 14, idPublicacion = 3, monto = 2, descripcion = "testeando", diasRealizacion = 14 };

            Publicacion publicacion = new Publicacion { id = 3, limitePostulaciones = fecha };


            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha);
            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario());
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(publicacion);




            var result = pc.Postular(p, 0) as ViewResult;



            Assert.IsTrue(result.ViewData.ModelState.IsValid);


            //verificamos que la fecha limite SI cambie ya que pasamos el valor 5 como cantidad de postulaciones

            Assert.AreNotEqual(fecha, publicacion.limitePostulaciones);

            Assert.AreEqual(fecha.AddDays(7), publicacion.limitePostulaciones);



        }

        [Test]
        public void PostularCase17()
        {
            DateTime fecha = new DateTime(2052, 2, 10);

            Postulacion p = new Postulacion { id = 14, idPublicacion = 3 };

            Publicacion publicacion = new Publicacion { id = 3, limitePostulaciones = fecha };


            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha);
            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario());
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(publicacion);




            var result = pc.Postular(p, 0) as ViewResult;

            //verificamos que al no ser valido , no se le asigne una fecha a la postulacion

            Assert.IsFalse(result.ViewData.ModelState.IsValid);

            Assert.AreEqual(new DateTime(0001, 01, 01, 00, 00, 00), p.fechaCreacion);
            Assert.AreEqual(fecha, publicacion.limitePostulaciones);

        }
        [Test]
        public void PostularCase18()
        {
            DateTime fecha = new DateTime(2052, 2, 10);

            Postulacion p = new Postulacion { id = 14, idPublicacion = 3 };

            Publicacion publicacion = new Publicacion { id = 3, limitePostulaciones = fecha };


            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha);
            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario());
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(publicacion);




            var result = pc.Postular(p, 0) as ViewResult;

            //verificamos que al no ser valido , no se le asigne una fecha a la postulacion

            Assert.IsFalse(result.ViewData.ModelState.IsValid);

            Assert.AreEqual(new DateTime(0001, 01, 01, 00, 00, 00), p.fechaCreacion);
            Assert.AreEqual(fecha, publicacion.limitePostulaciones);

        }
        [Test]
        public void PostularCase19()
        {

            DateTime fecha = new DateTime(2052, 2, 10);

            Postulacion p = new Postulacion { id = 14, idPublicacion = 3 };

            Publicacion publicacion = new Publicacion { id = 3, limitePostulaciones = fecha, vista = 33 };


            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha);
            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario());
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(publicacion);




            var result = pc.Postular(p, 0) as ViewResult;





            Assert.AreEqual(34, publicacion.vista);

        }

        [Test]
        public void PostularCase20()
        {


            Postulacion p = new Postulacion { id = 14, idPublicacion = 3 };


            DateTime fecha = new DateTime(2052, 2, 10);



            Publicacion publicacion = new Publicacion { id = 3, limitePostulaciones = fecha, vista = 33 };


            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha);
            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario { id = 14 });
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(publicacion);

            mockPostulacionRepo.Setup(o => o.FindByPublicacionUsuario(It.IsAny<int>(), It.IsAny<int>())).Returns(value: null);


            var result = pc.Postular(p, 0) as ViewResult;

            //viewbag valor

            Assert.AreEqual(false, result.ViewData["Postulo"]);

        }
        [Test]
        public void PostularCase21()
        {


            Postulacion p = new Postulacion { id = 14, idPublicacion = 3 };


            DateTime fecha = new DateTime(2052, 2, 10);



            Publicacion publicacion = new Publicacion { id = 3, limitePostulaciones = fecha, vista = 33 };


            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha);
            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario { id = 14 });
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(publicacion);

            mockPostulacionRepo.Setup(o => o.FindByPublicacionUsuario(It.IsAny<int>(), It.IsAny<int>())).Returns(new Postulacion());


            var result = pc.Postular(p, 0) as ViewResult;

            //viewbag valor

            Assert.AreEqual(true, result.ViewData["Postulo"]);

        }
        [Test]
        public void PostularCase22()
        {


            Postulacion p = new Postulacion { id = 14, idPublicacion = 3 };


            DateTime fecha = new DateTime(2052, 2, 10);



            Publicacion publicacion = new Publicacion { id = 3, limitePostulaciones = fecha, vista = 33 };


            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha);
            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario { id = 14 });
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(publicacion);

            mockPostulacionRepo.Setup(o => o.FindByPublicacionUsuario(It.IsAny<int>(), It.IsAny<int>())).Returns(new Postulacion());


            var result = pc.Postular(p, 0) as ViewResult;

            var Modelo = result.Model as PubModel;




            Assert.AreEqual(fecha, Modelo.fechaActual);

        }
        [Test]
        public void PostularCase23()
        {




            Postulacion p = new Postulacion { id = 14, idPublicacion = 3 };


            DateTime fecha = new DateTime(2052, 2, 10);



            Publicacion publicacion = new Publicacion { id = 3, limitePostulaciones = fecha, vista = 33 };


            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha);
            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario { id = 14, nombre = "Ruben" });
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(publicacion);

            mockPostulacionRepo.Setup(o => o.FindByPublicacionUsuario(It.IsAny<int>(), It.IsAny<int>())).Returns(new Postulacion());


            var result = pc.Postular(p, 0) as ViewResult;

            var Modelo = result.Model as PubModel;




            Assert.AreEqual("Ruben", Modelo.UsuarioActivo.nombre);
            Assert.AreEqual(14, Modelo.UsuarioActivo.id);

        }

        //cases TodasPublicaciones
        [Test]
        public void TodasPublicacionesCase01()
        {

            var result = pc.TodasPublicaciones();

            Assert.NotNull(result);
        }
        [Test]
        public void TodasPublicacionesCase02()
        {

            var result = pc.TodasPublicaciones();

            Assert.IsNotInstanceOf<RedirectToActionResult>(result);
        }
        [Test]
        public void TodasPublicacionesCase03()
        {

            var result = pc.TodasPublicaciones();

            Assert.IsInstanceOf<ViewResult>(result);
        }
        [Test]
        public void TodasPublicacionesCase04()
        {

            var result = pc.TodasPublicaciones() as ViewResult;

            Assert.IsNotNull(result.Model);
        }
        [Test]
        public void TodasPublicacionesCase05()
        {

            var result = pc.TodasPublicaciones() as ViewResult;

            Assert.IsTrue(result.ViewName.StartsWith("T"));
            Assert.IsTrue(result.ViewName.EndsWith("s"));

            Assert.AreEqual("TodasPublicaciones", result.ViewName);
        }
        [Test]
        public void TodasPublicacionesCase06()
        {

            var result = pc.TodasPublicaciones() as ViewResult;

            Assert.IsEmpty(result.ViewData);

        }
        [Test]
        public void TodasPublicacionesCase07()
        {

            var result = pc.TodasPublicaciones() as ViewResult;

            Assert.IsTrue(result.ViewData.ModelState.IsValid);

        }

        [Test]
        public void TodasPublicacionesCase08()
        {

            var result = pc.TodasPublicaciones() as ViewResult;

            Assert.IsInstanceOf<IndexModel>(result.Model);

        }
        [Test]
        public void TodasPublicacionesCase09()
        {
            Usuario usuario = new Usuario { id = 12 };
            mockAutenticacionRepo.Setup(o => o.GetCurrrentUser()).Returns("Alex");
            mockUsuarioRepo.Setup(o => o.FindByUserName("Alex")).Returns(usuario);
            var result = pc.TodasPublicaciones() as ViewResult;
            var Modelo = result.Model as IndexModel;

            Assert.AreEqual(12, Modelo.UsuarioActivo.id);

        }
        [Test]
        public void TodasPublicacionesCase10()
        {
            DateTime fecha = new DateTime(2021, 3, 11);
            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha);

            var result = pc.TodasPublicaciones() as ViewResult;
            var Modelo = result.Model as IndexModel;

            Assert.AreEqual(fecha, Modelo.fechaActual);

        }
        [Test]
        public void TodasPublicacionesCase11()
        {
            DateTime fecha = new DateTime(2000, 1, 20);
            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha.AddDays(15));

            var result = pc.TodasPublicaciones() as ViewResult;
            var Modelo = result.Model as IndexModel;

            Assert.AreEqual(fecha.AddDays(15), Modelo.fechaActual);

        }
        [Test]
        public void TodasPublicacionesCase12()
        {
            List<Publicacion> publicaciones = new List<Publicacion>();

            DateTime fecha = new DateTime(2000, 1, 20);
            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha.AddDays(15));
            mockPublicacionRepo.Setup(o => o.GetAllPublicaciones()).Returns(publicaciones);
            var result = pc.TodasPublicaciones() as ViewResult;
            var Modelo = result.Model as IndexModel;

            Assert.IsNotNull(Modelo.pubs);

        }
        [Test]
        public void TodasPublicacionesCase13()
        {
            List<Publicacion> publicaciones = new List<Publicacion>();

            DateTime fecha = new DateTime(2000, 1, 20);
            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha.AddDays(15));
            mockPublicacionRepo.Setup(o => o.GetAllPublicaciones()).Returns(publicaciones);
            var result = pc.TodasPublicaciones() as ViewResult;
            var Modelo = result.Model as IndexModel;

            Assert.AreEqual(0, Modelo.pubs.Count);

        }
        [Test]
        public void TodasPublicacionesCase14()
        {
            List<Publicacion> publicaciones = new List<Publicacion> { new Publicacion(), new Publicacion(), new Publicacion() };

            DateTime fecha = new DateTime(2000, 1, 20);
            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha.AddDays(15));
            mockPublicacionRepo.Setup(o => o.GetAllPublicaciones()).Returns(publicaciones);
            var result = pc.TodasPublicaciones() as ViewResult;
            var Modelo = result.Model as IndexModel;

            Assert.AreEqual(3, Modelo.pubs.Count);

        }
        [Test]
        public void TodasPublicacionesCase15()
        {
            List<Publicacion> publicaciones = new List<Publicacion> { new Publicacion(), new Publicacion(), new Publicacion() };

            DateTime fecha = new DateTime(2000, 1, 20);
            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha.AddDays(15));
            mockPublicacionRepo.Setup(o => o.GetAllPublicaciones()).Returns(publicaciones);
            var result = pc.TodasPublicaciones() as ViewResult;
            var Modelo = result.Model as IndexModel;

            Assert.IsInstanceOf<List<Publicacion>>(Modelo.pubs);

        }
        [Test]
        public void TodasPublicacionesCase16()
        {
            List<Publicacion> publicaciones = new List<Publicacion> { new Publicacion(), new Publicacion(), new Publicacion() };
            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario());
            DateTime fecha = new DateTime(2000, 1, 20);

            mockPublicacionRepo.Setup(o => o.GetAllPublicaciones()).Returns(publicaciones);
            var result = pc.TodasPublicaciones() as ViewResult;
            var Modelo = result.Model as IndexModel;

            Assert.IsInstanceOf<Usuario>(Modelo.UsuarioActivo);

        }

        //cases PubsCulminadas

        [Test]
        public void PubsCulminadasCase01()
        {
            var result = pc.PubsCulminadas();

            Assert.IsNotNull(result);
        }


        [Test]
        public void PubsCulminadasCase02()
        {

            var result = pc.PubsCulminadas();

            Assert.IsNotInstanceOf<RedirectToActionResult>(result);
        }
        [Test]
        public void PubsCulminadasCase03()
        {

            var result = pc.PubsCulminadas();

            Assert.IsInstanceOf<ViewResult>(result);
        }
        [Test]
        public void PubsCulminadasCase04()
        {

            var result = pc.PubsCulminadas() as ViewResult;

            Assert.IsNotNull(result.Model);
        }
        [Test]
        public void PubsCulminadasCase05()
        {

            var result = pc.PubsCulminadas() as ViewResult;

            Assert.IsTrue(result.ViewName.StartsWith("T"));
            Assert.IsTrue(result.ViewName.EndsWith("s"));

            Assert.AreEqual("TodasPublicaciones", result.ViewName);
        }
        [Test]
        public void PubsCulminadasCase06()
        {

            var result = pc.PubsCulminadas() as ViewResult;

            Assert.IsEmpty(result.ViewData);

        }
        [Test]
        public void PubsCulminadasCase07()
        {

            var result = pc.PubsCulminadas() as ViewResult;

            Assert.IsTrue(result.ViewData.ModelState.IsValid);

        }

        [Test]
        public void PubsCulminadasCase08()
        {

            var result = pc.PubsCulminadas() as ViewResult;

            Assert.IsInstanceOf<IndexModel>(result.Model);

        }
        [Test]
        public void PubsCulminadasCase09()
        {
            Usuario usuario = new Usuario { id = 12 };
            mockAutenticacionRepo.Setup(o => o.GetCurrrentUser()).Returns("Alex");
            mockUsuarioRepo.Setup(o => o.FindByUserName("Alex")).Returns(usuario);
            var result = pc.PubsCulminadas() as ViewResult;
            var Modelo = result.Model as IndexModel;

            Assert.AreEqual(12, Modelo.UsuarioActivo.id);

        }
        [Test]
        public void PubsCulminadasCase10()
        {
            DateTime fecha = new DateTime(2021, 3, 11);
            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha);

            var result = pc.PubsCulminadas() as ViewResult;
            var Modelo = result.Model as IndexModel;

            Assert.AreEqual(fecha, Modelo.fechaActual);

        }
        [Test]
        public void PubsCulminadasCase11()
        {
            DateTime fecha = new DateTime(2000, 1, 20);
            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha.AddDays(15));

            var result = pc.PubsCulminadas() as ViewResult;
            var Modelo = result.Model as IndexModel;

            Assert.AreEqual(fecha.AddDays(15), Modelo.fechaActual);

        }
        [Test]
        public void PubsCulminadasCase12()
        {
            List<Publicacion> publicaciones = new List<Publicacion>();

            DateTime fecha = new DateTime(2000, 1, 20);
            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha.AddDays(15));
            mockPublicacionRepo.Setup(o => o.GetCulminados()).Returns(publicaciones);
            var result = pc.PubsCulminadas() as ViewResult;
            var Modelo = result.Model as IndexModel;

            Assert.IsNotNull(Modelo.pubs);

        }
        [Test]
        public void PubsCulminadasCase13()
        {
            List<Publicacion> publicaciones = new List<Publicacion>();

            DateTime fecha = new DateTime(2000, 1, 20);
            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha.AddDays(15));
            mockPublicacionRepo.Setup(o => o.GetCulminados()).Returns(publicaciones);
            var result = pc.PubsCulminadas() as ViewResult;
            var Modelo = result.Model as IndexModel;

            Assert.AreEqual(0, Modelo.pubs.Count);

        }
        [Test]
        public void PubsCulminadasCase14()
        {
            List<Publicacion> publicaciones = new List<Publicacion> { new Publicacion(), new Publicacion(), new Publicacion() };

            DateTime fecha = new DateTime(2000, 1, 20);
            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha.AddDays(15));
            mockPublicacionRepo.Setup(o => o.GetCulminados()).Returns(publicaciones);
            var result = pc.PubsCulminadas() as ViewResult;
            var Modelo = result.Model as IndexModel;

            Assert.AreEqual(3, Modelo.pubs.Count);

        }
        [Test]
        public void PubsCulminadasCase15()
        {
            List<Publicacion> publicaciones = new List<Publicacion> { new Publicacion(), new Publicacion(), new Publicacion() };

            DateTime fecha = new DateTime(2000, 1, 20);
            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha.AddDays(15));
            mockPublicacionRepo.Setup(o => o.GetCulminados()).Returns(publicaciones);
            var result = pc.PubsCulminadas() as ViewResult;
            var Modelo = result.Model as IndexModel;

            Assert.IsInstanceOf<List<Publicacion>>(Modelo.pubs);

        }
        [Test]
        public void PubsCulminadasCase16()
        {
            List<Publicacion> publicaciones = new List<Publicacion> { new Publicacion(), new Publicacion(), new Publicacion() };
            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario());
            DateTime fecha = new DateTime(2000, 1, 20);

            mockPublicacionRepo.Setup(o => o.GetCulminados()).Returns(publicaciones);
            var result = pc.PubsCulminadas() as ViewResult;
            var Modelo = result.Model as IndexModel;

            Assert.IsInstanceOf<Usuario>(Modelo.UsuarioActivo);

        }


        //cases MisPostulaciones



        [Test]
        public void MisPostulacionesCase01()
        {
            var result = pc.MisPostulaciones();

            Assert.IsNotNull(result);
        }


        [Test]
        public void MisPostulacionesCase02()
        {

            var result = pc.MisPostulaciones();

            Assert.IsNotInstanceOf<RedirectToActionResult>(result);
        }
        [Test]
        public void MisPostulacionesCase03()
        {

            var result = pc.MisPostulaciones();

            Assert.IsInstanceOf<ViewResult>(result);
        }
        [Test]
        public void MisPostulacionesCase04()
        {

            var result = pc.MisPostulaciones() as ViewResult;

            Assert.IsNotNull(result.Model);
        }
        [Test]
        public void MisPostulacionesCase05()
        {

            var result = pc.MisPostulaciones() as ViewResult;

            Assert.IsTrue(result.ViewName.StartsWith("M"));
            Assert.IsTrue(result.ViewName.EndsWith("s"));

            Assert.AreEqual("MisPostulaciones", result.ViewName);
        }
        [Test]
        public void MisPostulacionesCase06()
        {

            var result = pc.MisPostulaciones() as ViewResult;

            Assert.IsEmpty(result.ViewData);

        }
        [Test]
        public void MisPostulacionesCase07()
        {

            var result = pc.MisPostulaciones() as ViewResult;

            Assert.IsTrue(result.ViewData.ModelState.IsValid);

        }

        [Test]
        public void MisPostulacionesCase08()
        {

            var result = pc.MisPostulaciones() as ViewResult;

            Assert.IsInstanceOf<PostulacionModel>(result.Model);

        }
        [Test]
        public void MisPostulacionesCase09()
        {
            Usuario usuario = new Usuario { id = 12 };
            mockAutenticacionRepo.Setup(o => o.GetCurrrentUser()).Returns("Alex");
            mockUsuarioRepo.Setup(o => o.GetWithPostulacionessByUsername("Alex")).Returns(usuario);
            var result = pc.MisPostulaciones() as ViewResult;
            var Modelo = result.Model as PostulacionModel;

            Assert.AreEqual(12, Modelo.UsuarioActivo.id);

        }
        [Test]
        public void MisPostulacionesCase10()
        {
            DateTime fecha = new DateTime(2021, 3, 11);
            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha);

            var result = pc.MisPostulaciones() as ViewResult;
            var Modelo = result.Model as PostulacionModel;

            Assert.AreEqual(fecha, Modelo.fechaActual);

        }
        [Test]
        public void MisPostulacionesCase11()
        {
            DateTime fecha = new DateTime(2000, 1, 20);
            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha.AddDays(15));

            var result = pc.MisPostulaciones() as ViewResult;
            var Modelo = result.Model as PostulacionModel;

            Assert.AreEqual(fecha.AddDays(15), Modelo.fechaActual);

        }
        [Test]
        public void MisPostulacionesCase12()
        {
            List<Postulacion> postulacioness = new List<Postulacion>();


            Usuario usuario = new Usuario { id = 12, userName = "Mabel", postulaciones = postulacioness };
            mockAutenticacionRepo.Setup(o => o.GetCurrrentUser()).Returns("Mabel");
            mockUsuarioRepo.Setup(o => o.GetWithPostulacionessByUsername("Mabel")).Returns(usuario);
            DateTime fecha = new DateTime(2000, 1, 20);
            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha.AddDays(15));

            var result = pc.MisPostulaciones() as ViewResult;
            var Modelo = result.Model as PostulacionModel;

            Assert.IsNotNull(Modelo.UsuarioActivo);

        }


        [Test]
        public void MisPostulacionesCase13()
        {
            List<Postulacion> postulacioness = new List<Postulacion>();


            Usuario usuario = new Usuario { id = 12, userName = "Mabel", postulaciones = postulacioness };
            mockAutenticacionRepo.Setup(o => o.GetCurrrentUser()).Returns("Mabel");
            mockUsuarioRepo.Setup(o => o.GetWithPostulacionessByUsername("Mabel")).Returns(usuario);
            DateTime fecha = new DateTime(2000, 1, 20);
            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha.AddDays(15));

            var result = pc.MisPostulaciones() as ViewResult;
            var Modelo = result.Model as PostulacionModel;

            Assert.AreEqual(0, Modelo.UsuarioActivo.postulaciones.Count);

        }
        [Test]
        public void MisPostulacionesCase14()
        {
            List<Postulacion> postulacioness = new List<Postulacion> { new Postulacion(), new Postulacion() };


            Usuario usuario = new Usuario { id = 12, userName = "Mabel", postulaciones = postulacioness };
            mockAutenticacionRepo.Setup(o => o.GetCurrrentUser()).Returns("Mabel");
            mockUsuarioRepo.Setup(o => o.GetWithPostulacionessByUsername("Mabel")).Returns(usuario);
            DateTime fecha = new DateTime(2000, 1, 20);
            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha.AddDays(15));

            var result = pc.MisPostulaciones() as ViewResult;
            var Modelo = result.Model as PostulacionModel;

            Assert.AreEqual(2, Modelo.UsuarioActivo.postulaciones.Count);

        }
        [Test]
        public void MisPostulacionesCase15()
        {
            List<Postulacion> postulacioness = new List<Postulacion> { new Postulacion(), new Postulacion() };


            Usuario usuario = new Usuario { id = 12, userName = "Mabel", postulaciones = postulacioness };
            mockAutenticacionRepo.Setup(o => o.GetCurrrentUser()).Returns("Mabel");
            mockUsuarioRepo.Setup(o => o.GetWithPostulacionessByUsername("Mabel")).Returns(usuario);
            DateTime fecha = new DateTime(2000, 1, 20);
            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha.AddDays(15));

            var result = pc.MisPostulaciones() as ViewResult;
            var Modelo = result.Model as PostulacionModel;

            Assert.IsInstanceOf<List<Postulacion>>(Modelo.UsuarioActivo.postulaciones);

        }
        [Test]
        public void MisPostulacionesCase16()
        {
            List<Postulacion> postulacioness = new List<Postulacion> { new Postulacion(), new Postulacion() };


            Usuario usuario = new Usuario { id = 12, userName = "Mabel", postulaciones = postulacioness };
            mockAutenticacionRepo.Setup(o => o.GetCurrrentUser()).Returns("Mabel");
            mockUsuarioRepo.Setup(o => o.GetWithPostulacionessByUsername("Mabel")).Returns(usuario);
            DateTime fecha = new DateTime(2000, 1, 20);
            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha.AddDays(15));

            var result = pc.MisPostulaciones() as ViewResult;
            var Modelo = result.Model as PostulacionModel;

            Assert.IsInstanceOf<Usuario>(Modelo.UsuarioActivo);

        }

        //cases MisAsignados

        [Test]
        public void MisAsignadosCase01()
        {
            var result = pc.MisAsignados();

            Assert.IsNotNull(result);
        }


        [Test]
        public void MisAsignadosCase02()
        {

            var result = pc.MisAsignados();

            Assert.IsNotInstanceOf<RedirectToActionResult>(result);
        }
        [Test]
        public void MisAsignadosCase03()
        {

            var result = pc.MisAsignados();

            Assert.IsInstanceOf<ViewResult>(result);
        }
        [Test]
        public void MisAsignadosCase04()
        {

            var result = pc.MisAsignados() as ViewResult;

            Assert.IsNotNull(result.Model);
        }
        [Test]
        public void MisAsignadosCase05()
        {

            var result = pc.MisAsignados() as ViewResult;

            Assert.IsTrue(result.ViewName.StartsWith("M"));
            Assert.IsTrue(result.ViewName.EndsWith("s"));

            Assert.AreEqual("MisAsignados", result.ViewName);
        }
        [Test]
        public void MisAsignadosCase06()
        {

            var result = pc.MisAsignados() as ViewResult;

            Assert.IsEmpty(result.ViewData);

        }
        [Test]
        public void MisAsignadosCase07()
        {

            var result = pc.MisAsignados() as ViewResult;

            Assert.IsTrue(result.ViewData.ModelState.IsValid);

        }

        [Test]
        public void MisAsignadosCase08()
        {

            var result = pc.MisAsignados() as ViewResult;

            Assert.IsInstanceOf<PostulacionModel>(result.Model);

        }
        [Test]
        public void MisAsignadosCase09()
        {
            Usuario usuario = new Usuario { id = 12 };
            mockAutenticacionRepo.Setup(o => o.GetCurrrentUser()).Returns("Alex");
            mockUsuarioRepo.Setup(o => o.GetWithPostulacionessByUsername("Alex")).Returns(usuario);
            var result = pc.MisAsignados() as ViewResult;
            var Modelo = result.Model as PostulacionModel;

            Assert.AreEqual(12, Modelo.UsuarioActivo.id);

        }
        [Test]
        public void MisAsignadosCase10()
        {
            DateTime fecha = new DateTime(2021, 3, 11);
            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha);

            var result = pc.MisAsignados() as ViewResult;
            var Modelo = result.Model as PostulacionModel;

            Assert.AreEqual(fecha, Modelo.fechaActual);

        }
        [Test]
        public void MisAsignadosCase11()
        {
            DateTime fecha = new DateTime(2000, 1, 20);
            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha.AddDays(15));

            var result = pc.MisAsignados() as ViewResult;
            var Modelo = result.Model as PostulacionModel;

            Assert.AreEqual(fecha.AddDays(15), Modelo.fechaActual);

        }
        [Test]
        public void MisAsignadosCase12()
        {
            List<Postulacion> postulacioness = new List<Postulacion>();


            Usuario usuario = new Usuario { id = 12, userName = "Mabel", postulaciones = postulacioness };
            mockAutenticacionRepo.Setup(o => o.GetCurrrentUser()).Returns("Mabel");
            mockUsuarioRepo.Setup(o => o.GetWithPostulacionessByUsername("Mabel")).Returns(usuario);
            DateTime fecha = new DateTime(2000, 1, 20);
            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha.AddDays(15));

            var result = pc.MisAsignados() as ViewResult;
            var Modelo = result.Model as PostulacionModel;

            Assert.IsNotNull(Modelo.UsuarioActivo);

        }


        [Test]
        public void MisAsignadosCase13()
        {
            List<Postulacion> postulacioness = new List<Postulacion>();


            Usuario usuario = new Usuario { id = 12, userName = "Mabel", postulaciones = postulacioness };
            mockAutenticacionRepo.Setup(o => o.GetCurrrentUser()).Returns("Mabel");
            mockUsuarioRepo.Setup(o => o.GetWithPostulacionessByUsername("Mabel")).Returns(usuario);
            DateTime fecha = new DateTime(2000, 1, 20);
            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha.AddDays(15));

            var result = pc.MisAsignados() as ViewResult;
            var Modelo = result.Model as PostulacionModel;

            Assert.AreEqual(0, Modelo.UsuarioActivo.postulaciones.Count);

        }
        [Test]
        public void MisAsignadosCase14()
        {
            List<Postulacion> postulacioness = new List<Postulacion> { new Postulacion(), new Postulacion() };


            Usuario usuario = new Usuario { id = 12, userName = "Mabel", postulaciones = postulacioness };
            mockAutenticacionRepo.Setup(o => o.GetCurrrentUser()).Returns("Mabel");
            mockUsuarioRepo.Setup(o => o.GetWithPostulacionessByUsername("Mabel")).Returns(usuario);
            DateTime fecha = new DateTime(2000, 1, 20);
            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha.AddDays(15));

            var result = pc.MisAsignados() as ViewResult;
            var Modelo = result.Model as PostulacionModel;

            Assert.AreEqual(2, Modelo.UsuarioActivo.postulaciones.Count);

        }
        [Test]
        public void MisAsignadosCase15()
        {
            List<Postulacion> postulacioness = new List<Postulacion> { new Postulacion(), new Postulacion() };


            Usuario usuario = new Usuario { id = 12, userName = "Mabel", postulaciones = postulacioness };
            mockAutenticacionRepo.Setup(o => o.GetCurrrentUser()).Returns("Mabel");
            mockUsuarioRepo.Setup(o => o.GetWithPostulacionessByUsername("Mabel")).Returns(usuario);
            DateTime fecha = new DateTime(2000, 1, 20);
            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha.AddDays(15));

            var result = pc.MisAsignados() as ViewResult;
            var Modelo = result.Model as PostulacionModel;

            Assert.IsInstanceOf<List<Postulacion>>(Modelo.UsuarioActivo.postulaciones);

        }
        [Test]
        public void MisAsignadosCase16()
        {
            List<Postulacion> postulacioness = new List<Postulacion> { new Postulacion(), new Postulacion() };


            Usuario usuario = new Usuario { id = 12, userName = "Mabel", postulaciones = postulacioness };
            mockAutenticacionRepo.Setup(o => o.GetCurrrentUser()).Returns("Mabel");
            mockUsuarioRepo.Setup(o => o.GetWithPostulacionessByUsername("Mabel")).Returns(usuario);
            DateTime fecha = new DateTime(2000, 1, 20);
            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha.AddDays(15));

            var result = pc.MisAsignados() as ViewResult;
            var Modelo = result.Model as PostulacionModel;

            Assert.IsInstanceOf<Usuario>(Modelo.UsuarioActivo);

        }


        //cases cerrarTrabajo  GET

        [Test]
        public void cerrarTrabajoCase01()
        {
            var result = pc.cerrarTrabajo(2);

            Assert.IsNotNull(result);
        }


        [Test]
        public void cerrarTrabajoCase02()
        {

            var result = pc.cerrarTrabajo(1);

            Assert.IsNotInstanceOf<RedirectToActionResult>(result);
        }
        [Test]
        public void cerrarTrabajoCase03()
        {

            var result = pc.cerrarTrabajo(5);

            Assert.IsInstanceOf<ViewResult>(result);
        }
        [Test]
        public void cerrarTrabajoCase04()
        {

            var result = pc.cerrarTrabajo(11) as ViewResult;

            Assert.IsNotNull(result.Model);
        }
        [Test]
        public void cerrarTrabajoCase05()
        {

            var result = pc.cerrarTrabajo(2) as ViewResult;

            Assert.IsTrue(result.ViewName.StartsWith("c"));
            Assert.IsTrue(result.ViewName.EndsWith("o"));

            Assert.AreEqual("cerrarTrabajo", result.ViewName);
        }
        [Test]
        public void cerrarTrabajoCase06()
        {

            var result = pc.cerrarTrabajo(9) as ViewResult;

            Assert.IsEmpty(result.ViewData);

        }
        [Test]
        public void cerrarTrabajoCase07()
        {

            var result = pc.cerrarTrabajo(8) as ViewResult;

            Assert.IsTrue(result.ViewData.ModelState.IsValid);

        }

        [Test]
        public void cerrarTrabajoCase08()
        {

            var result = pc.cerrarTrabajo(55) as ViewResult;

            Assert.IsInstanceOf<PubModel>(result.Model);

        }
        [Test]
        public void cerrarTrabajoCase09()
        {
            Usuario usuario = new Usuario { id = 55 };
            mockAutenticacionRepo.Setup(o => o.GetCurrrentUser()).Returns("Jorge");
            mockUsuarioRepo.Setup(o => o.FindByUserName("Jorge")).Returns(usuario);



            var result = pc.cerrarTrabajo(13) as ViewResult;
            var Modelo = result.Model as PubModel;

            Assert.AreEqual(55, Modelo.UsuarioActivo.id);

        }
        [Test]
        public void cerrarTrabajoCase10()
        {
            DateTime fecha = new DateTime(2021, 3, 11);
            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha);

            Usuario usuario = new Usuario { id = 55 };
            mockAutenticacionRepo.Setup(o => o.GetCurrrentUser()).Returns("Jorge");
            mockUsuarioRepo.Setup(o => o.FindByUserName("Jorge")).Returns(usuario);



            var result = pc.cerrarTrabajo(8) as ViewResult;
            var Modelo = result.Model as PubModel;

            Assert.AreEqual(fecha, Modelo.fechaActual);

        }
        [Test]
        public void cerrarTrabajoCase11()
        {
            DateTime fecha = new DateTime(2021, 3, 11);
            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha.AddDays(15));

            Usuario usuario = new Usuario { id = 55 };
            mockAutenticacionRepo.Setup(o => o.GetCurrrentUser()).Returns("Jorge");
            mockUsuarioRepo.Setup(o => o.FindByUserName("Jorge")).Returns(usuario);



            var result = pc.cerrarTrabajo(8) as ViewResult;
            var Modelo = result.Model as PubModel;

            Assert.AreEqual(fecha.AddDays(15), Modelo.fechaActual);

        }
        [Test]
        public void cerrarTrabajoCase12()
        {
            DateTime fecha = new DateTime(2021, 3, 11);
            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha);

            Usuario usuario = new Usuario { id = 55 };
            mockAutenticacionRepo.Setup(o => o.GetCurrrentUser()).Returns("Jorge");
            mockUsuarioRepo.Setup(o => o.FindByUserName("Jorge")).Returns(usuario);



            var result = pc.cerrarTrabajo(8) as ViewResult;
            var Modelo = result.Model as PubModel;

            Assert.IsNotNull(Modelo.UsuarioActivo);

        }


        [Test]
        public void cerrarTrabajoCase13()
        {
            Publicacion publicacion = new Publicacion();


            DateTime fecha = new DateTime(2021, 3, 11);
            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha);


            mockAutenticacionRepo.Setup(o => o.GetCurrrentUser()).Returns("Jorge");
            mockUsuarioRepo.Setup(o => o.FindByUserName("Jorge")).Returns(value: null);
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(8)).Returns(publicacion);


            var result = pc.cerrarTrabajo(8) as ViewResult;
            var Modelo = result.Model as PubModel;

            Assert.AreEqual(null, Modelo.UsuarioActivo);

        }
        [Test]
        public void cerrarTrabajoCase14()
        {
            List<Postulacion> postulacioness = new List<Postulacion> { new Postulacion(), new Postulacion() };


            DateTime fecha = new DateTime(2021, 3, 11);
            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha);


            mockAutenticacionRepo.Setup(o => o.GetCurrrentUser()).Returns("Jorge");
            mockUsuarioRepo.Setup(o => o.FindByUserName("Jorge")).Returns(new Usuario());
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(8)).Returns(new Publicacion { postulaciones = postulacioness });


            var result = pc.cerrarTrabajo(8) as ViewResult;
            var Modelo = result.Model as PubModel;

            Assert.AreEqual(2, Modelo.pub.postulaciones.Count);

        }
        [Test]
        public void cerrarTrabajoCase15()
        {
            List<Postulacion> postulacioness = new List<Postulacion> { new Postulacion(), new Postulacion() };


            DateTime fecha = new DateTime(2021, 3, 11);
            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha);


            mockAutenticacionRepo.Setup(o => o.GetCurrrentUser()).Returns("Jorge");
            mockUsuarioRepo.Setup(o => o.FindByUserName("Jorge")).Returns(new Usuario());
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(8)).Returns(new Publicacion { postulaciones = postulacioness });


            var result = pc.cerrarTrabajo(8) as ViewResult;
            var Modelo = result.Model as PubModel;


            Assert.IsInstanceOf<List<Postulacion>>(Modelo.pub.postulaciones);

        }
        [Test]
        public void cerrarTrabajoCase16()
        {
            List<Postulacion> postulacioness = new List<Postulacion> { new Postulacion(), new Postulacion() };


            DateTime fecha = new DateTime(2021, 3, 11);
            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha);


            mockAutenticacionRepo.Setup(o => o.GetCurrrentUser()).Returns("Jorge");
            mockUsuarioRepo.Setup(o => o.FindByUserName("Jorge")).Returns(new Usuario());
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(8)).Returns(new Publicacion { postulaciones = postulacioness });


            var result = pc.cerrarTrabajo(8) as ViewResult;
            var Modelo = result.Model as PubModel;

            Assert.AreEqual(2, Modelo.pub.postulaciones.Count);

            Assert.IsInstanceOf<Usuario>(Modelo.UsuarioActivo);

        }


        //cases cerrarTrabajo Post

        [Test]
        public void cerrarTrabajoPostSuccesCase01()
        {
            var mockFile = new Mock<IFormFile>();

            mockFile.Setup(o => o.Length).Returns(1);
            mockFile.Setup(o => o.ContentType).Returns("image/png");
            mockFile.Setup(o => o.FileName).Returns("Fotaso.png");


            List<IFormFile> files = new List<IFormFile> { mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object };

            mockPostulacionRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(new Postulacion());

            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());

            mockIWebHostEnvironment.Setup(o => o.WebRootPath).Returns("C :\\Users\\Alex\\source\repos\\AppTUMISMITOPE.Test\\wwwrootTest");




            mockManageFiles.Setup(o => o.CombinePaths(It.IsAny<string>(), It.IsAny<string>(), mockFile.Object.FileName)).Returns("files\\fotosEvidencias\\" + mockFile.Object.FileName);
            mockManageFiles.Setup(o => o.CombinePaths(mockIWebHostEnvironment.Object.WebRootPath, It.IsAny<string>())).Returns("C:\\Users\\Alex\\source\\repos\\AppTUMISMITOPE.Test\\wwwroootTest\\files\\fotosEvidencias\\Fotaso.png");

            var result = pc.cerrarTrabajo(new Evidencia(), files);



            Assert.NotNull(result);
        }
        [Test]
        public void cerrarTrabajoPostSuccesCase02()
        {
            var mockFile = new Mock<IFormFile>();

            mockFile.Setup(o => o.Length).Returns(1);
            mockFile.Setup(o => o.ContentType).Returns("image/png");
            mockFile.Setup(o => o.FileName).Returns("Fotaso.png");


            List<IFormFile> files = new List<IFormFile> { mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object };

            mockPostulacionRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(new Postulacion());

            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());

            mockIWebHostEnvironment.Setup(o => o.WebRootPath).Returns("C :\\Users\\Alex\\source\repos\\AppTUMISMITOPE.Test\\wwwrootTest");




            mockManageFiles.Setup(o => o.CombinePaths(It.IsAny<string>(), It.IsAny<string>(), mockFile.Object.FileName)).Returns("files\\fotosEvidencias\\" + mockFile.Object.FileName);
            mockManageFiles.Setup(o => o.CombinePaths(mockIWebHostEnvironment.Object.WebRootPath, It.IsAny<string>())).Returns("C:\\Users\\Alex\\source\\repos\\AppTUMISMITOPE.Test\\wwwroootTest\\files\\fotosEvidencias\\Fotaso.png");

            var result = pc.cerrarTrabajo(new Evidencia(), files);

            Assert.IsNotInstanceOf<ViewResult>(result);
        }


        [Test]
        public void cerrarTrabajoPostSuccesCase03()
        {
            var mockFile = new Mock<IFormFile>();

            mockFile.Setup(o => o.Length).Returns(1);
            mockFile.Setup(o => o.ContentType).Returns("image/png");
            mockFile.Setup(o => o.FileName).Returns("Fotaso.png");


            List<IFormFile> files = new List<IFormFile> { mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object };

            mockPostulacionRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(new Postulacion());

            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());

            mockIWebHostEnvironment.Setup(o => o.WebRootPath).Returns("C :\\Users\\Alex\\source\repos\\AppTUMISMITOPE.Test\\wwwrootTest");


            mockManageFiles.Setup(o => o.CombinePaths(It.IsAny<string>(), It.IsAny<string>(), mockFile.Object.FileName)).Returns("files\\fotosEvidencias\\" + mockFile.Object.FileName);
            mockManageFiles.Setup(o => o.CombinePaths(mockIWebHostEnvironment.Object.WebRootPath, It.IsAny<string>())).Returns("C:\\Users\\Alex\\source\\repos\\AppTUMISMITOPE.Test\\wwwroootTest\\files\\fotosEvidencias\\Fotaso.png");

            var result = pc.cerrarTrabajo(new Evidencia(), files);

            Assert.IsInstanceOf<RedirectToActionResult>(result);
        }
        [Test]
        public void cerrarTrabajoPostSuccesCase04()
        {
            var mockFile = new Mock<IFormFile>();

            mockFile.Setup(o => o.Length).Returns(1);
            mockFile.Setup(o => o.ContentType).Returns("image/png");
            mockFile.Setup(o => o.FileName).Returns("Fotaso.png");


            List<IFormFile> files = new List<IFormFile> { mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object };

            mockPostulacionRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(new Postulacion());

            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());

            mockIWebHostEnvironment.Setup(o => o.WebRootPath).Returns("C :\\Users\\Alex\\source\repos\\AppTUMISMITOPE.Test\\wwwrootTest");


            mockManageFiles.Setup(o => o.CombinePaths(It.IsAny<string>(), It.IsAny<string>(), mockFile.Object.FileName)).Returns("files\\fotosEvidencias\\" + mockFile.Object.FileName);
            mockManageFiles.Setup(o => o.CombinePaths(mockIWebHostEnvironment.Object.WebRootPath, It.IsAny<string>())).Returns("C:\\Users\\Alex\\source\\repos\\AppTUMISMITOPE.Test\\wwwroootTest\\files\\fotosEvidencias\\Fotaso.png");

            var result = pc.cerrarTrabajo(new Evidencia(), files);

            Assert.IsInstanceOf<RedirectToActionResult>(result);
        }
        [Test]
        public void cerrarTrabajoPostSuccesCase05()
        {
            var mockFile = new Mock<IFormFile>();

            mockFile.Setup(o => o.Length).Returns(1);
            mockFile.Setup(o => o.ContentType).Returns("image/png");
            mockFile.Setup(o => o.FileName).Returns("Fotaso.png");


            List<IFormFile> files = new List<IFormFile> { mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object };

            mockPostulacionRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(new Postulacion());

            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());

            mockIWebHostEnvironment.Setup(o => o.WebRootPath).Returns("C :\\Users\\Alex\\source\repos\\AppTUMISMITOPE.Test\\wwwrootTest");


            mockManageFiles.Setup(o => o.CombinePaths(It.IsAny<string>(), It.IsAny<string>(), mockFile.Object.FileName)).Returns("files\\fotosEvidencias\\" + mockFile.Object.FileName);
            mockManageFiles.Setup(o => o.CombinePaths(mockIWebHostEnvironment.Object.WebRootPath, It.IsAny<string>())).Returns("C:\\Users\\Alex\\source\\repos\\AppTUMISMITOPE.Test\\wwwroootTest\\files\\fotosEvidencias\\Fotaso.png");

            var result = pc.cerrarTrabajo(new Evidencia(), files) as RedirectToActionResult;

            Assert.IsTrue(result.ActionName.StartsWith("I"));
            Assert.IsTrue(result.ActionName.EndsWith("x"));
            Assert.AreEqual("Index", result.ActionName);
        }
        [Test]
        public void cerrarTrabajoPostSuccesCase06()
        {
            var mockFile = new Mock<IFormFile>();

            mockFile.Setup(o => o.Length).Returns(1);
            mockFile.Setup(o => o.ContentType).Returns("image/png");
            mockFile.Setup(o => o.FileName).Returns("Fotaso.png");


            List<IFormFile> files = new List<IFormFile> { mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object };

            mockPostulacionRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(new Postulacion());

            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());

            mockIWebHostEnvironment.Setup(o => o.WebRootPath).Returns("C :\\Users\\Alex\\source\repos\\AppTUMISMITOPE.Test\\wwwrootTest");


            mockManageFiles.Setup(o => o.CombinePaths(It.IsAny<string>(), It.IsAny<string>(), mockFile.Object.FileName)).Returns("files\\fotosEvidencias\\" + mockFile.Object.FileName);
            mockManageFiles.Setup(o => o.CombinePaths(mockIWebHostEnvironment.Object.WebRootPath, It.IsAny<string>())).Returns("C:\\Users\\Alex\\source\\repos\\AppTUMISMITOPE.Test\\wwwroootTest\\files\\fotosEvidencias\\Fotaso.png");

            var result = pc.cerrarTrabajo(new Evidencia(), files) as RedirectToActionResult;

            Assert.IsTrue(result.ControllerName.StartsWith("H"));
            Assert.IsTrue(result.ControllerName.EndsWith("e"));
            Assert.AreEqual("Home", result.ControllerName);
        }
        [Test]
        public void cerrarTrabajoPostSuccesCase07()
        {
            var mockFile = new Mock<IFormFile>();

            mockFile.Setup(o => o.Length).Returns(1);
            mockFile.Setup(o => o.ContentType).Returns("image/png");
            mockFile.Setup(o => o.FileName).Returns("Fotaso.png");


            List<IFormFile> files = new List<IFormFile> { mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object };

            mockPostulacionRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(new Postulacion());

            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());

            mockIWebHostEnvironment.Setup(o => o.WebRootPath).Returns("C :\\Users\\Alex\\source\repos\\AppTUMISMITOPE.Test\\wwwrootTest");


            mockManageFiles.Setup(o => o.CombinePaths(It.IsAny<string>(), It.IsAny<string>(), mockFile.Object.FileName)).Returns("files\\fotosEvidencias\\" + mockFile.Object.FileName);
            mockManageFiles.Setup(o => o.CombinePaths(mockIWebHostEnvironment.Object.WebRootPath, It.IsAny<string>())).Returns("C:\\Users\\Alex\\source\\repos\\AppTUMISMITOPE.Test\\wwwroootTest\\files\\fotosEvidencias\\Fotaso.png");

            var result = pc.cerrarTrabajo(new Evidencia(), files) as RedirectToActionResult;

            Assert.IsTrue(pc.ModelState.IsValid);

        }
        [Test]
        public void cerrarTrabajoPostSuccesCase08()
        {
            var mockFile = new Mock<IFormFile>();

            mockFile.Setup(o => o.Length).Returns(1);
            mockFile.Setup(o => o.ContentType).Returns("image/png");
            mockFile.Setup(o => o.FileName).Returns("Fotaso.png");


            List<IFormFile> files = new List<IFormFile> { mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object };

            Postulacion postulacion = new Postulacion { id = 15, estado = 0 };

            mockPostulacionRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(postulacion);

            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());

            mockIWebHostEnvironment.Setup(o => o.WebRootPath).Returns("C :\\Users\\Alex\\source\repos\\AppTUMISMITOPE.Test\\wwwrootTest");


            mockManageFiles.Setup(o => o.CombinePaths(It.IsAny<string>(), It.IsAny<string>(), mockFile.Object.FileName)).Returns("files\\fotosEvidencias\\" + mockFile.Object.FileName);
            mockManageFiles.Setup(o => o.CombinePaths(mockIWebHostEnvironment.Object.WebRootPath, It.IsAny<string>())).Returns("C:\\Users\\Alex\\source\\repos\\AppTUMISMITOPE.Test\\wwwroootTest\\files\\fotosEvidencias\\Fotaso.png");

            var result = pc.cerrarTrabajo(new Evidencia(), files) as RedirectToPageResult;

            //verificamos la asignacion de estado segun la logica
            Assert.AreEqual(3, postulacion.estado);

        }
        [Test]
        public void cerrarTrabajoPostSuccesCase09()
        {
            var mockFile = new Mock<IFormFile>();

            mockFile.Setup(o => o.Length).Returns(1);
            mockFile.Setup(o => o.ContentType).Returns("image/png");
            mockFile.Setup(o => o.FileName).Returns("Fotaso.png");


            List<IFormFile> files = new List<IFormFile> { mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object };

            Postulacion postulacion = new Postulacion { id = 8 };

            mockPostulacionRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(postulacion);

            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());

            mockIWebHostEnvironment.Setup(o => o.WebRootPath).Returns("C :\\Users\\Alex\\source\repos\\AppTUMISMITOPE.Test\\wwwrootTest");


            mockManageFiles.Setup(o => o.CombinePaths(It.IsAny<string>(), It.IsAny<string>(), mockFile.Object.FileName)).Returns("files\\fotosEvidencias\\" + mockFile.Object.FileName);
            mockManageFiles.Setup(o => o.CombinePaths(mockIWebHostEnvironment.Object.WebRootPath, It.IsAny<string>())).Returns("C:\\Users\\Alex\\source\\repos\\AppTUMISMITOPE.Test\\wwwroootTest\\files\\fotosEvidencias\\Fotaso.png");

            var result = pc.cerrarTrabajo(new Evidencia(), files) as RedirectToPageResult;

            //verificamos la asignacion de estado segun la logica
            Assert.AreEqual(3, postulacion.estado);

        }
        [Test]
        public void cerrarTrabajoPostSuccesCase10()
        {
            var mockFile = new Mock<IFormFile>();

            mockFile.Setup(o => o.Length).Returns(1);
            mockFile.Setup(o => o.ContentType).Returns("image/png");
            mockFile.Setup(o => o.FileName).Returns("Fotaso.png");


            List<IFormFile> files = new List<IFormFile> { mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object };

            Postulacion postulacion = new Postulacion { id = 8, estado = 1111111 };

            mockPostulacionRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(postulacion);

            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());

            mockIWebHostEnvironment.Setup(o => o.WebRootPath).Returns("C :\\Users\\Alex\\source\repos\\AppTUMISMITOPE.Test\\wwwrootTest");


            mockManageFiles.Setup(o => o.CombinePaths(It.IsAny<string>(), It.IsAny<string>(), mockFile.Object.FileName)).Returns("files\\fotosEvidencias\\" + mockFile.Object.FileName);
            mockManageFiles.Setup(o => o.CombinePaths(mockIWebHostEnvironment.Object.WebRootPath, It.IsAny<string>())).Returns("C:\\Users\\Alex\\source\\repos\\AppTUMISMITOPE.Test\\wwwroootTest\\files\\fotosEvidencias\\Fotaso.png");

            var result = pc.cerrarTrabajo(new Evidencia(), files) as RedirectToPageResult;

            //verificamos la asignacion de estado segun la logica
            Assert.AreEqual(3, postulacion.estado);

        }
        [Test]
        public void cerrarTrabajoPostSuccesCase11()
        {
            var mockFile = new Mock<IFormFile>();

            mockFile.Setup(o => o.Length).Returns(1);
            mockFile.Setup(o => o.ContentType).Returns("image/png");
            mockFile.Setup(o => o.FileName).Returns("Fotaso.png");


            List<IFormFile> files = new List<IFormFile> { mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object };

            Postulacion postulacion = new Postulacion { id = 15, estado = 0 };

            mockPostulacionRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(postulacion);

            Publicacion publicacion = new Publicacion { id = 65, estado = 0 };
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(publicacion);

            mockIWebHostEnvironment.Setup(o => o.WebRootPath).Returns("C :\\Users\\Alex\\source\repos\\AppTUMISMITOPE.Test\\wwwrootTest");


            mockManageFiles.Setup(o => o.CombinePaths(It.IsAny<string>(), It.IsAny<string>(), mockFile.Object.FileName)).Returns("files\\fotosEvidencias\\" + mockFile.Object.FileName);
            mockManageFiles.Setup(o => o.CombinePaths(mockIWebHostEnvironment.Object.WebRootPath, It.IsAny<string>())).Returns("C:\\Users\\Alex\\source\\repos\\AppTUMISMITOPE.Test\\wwwroootTest\\files\\fotosEvidencias\\Fotaso.png");

            var result = pc.cerrarTrabajo(new Evidencia(), files) as RedirectToPageResult;

            //verificamos la asignacion de estado segun la logica
            Assert.AreEqual(1, publicacion.estado);

        }
        [Test]
        public void cerrarTrabajoPostSuccesCase12()
        {
            var mockFile = new Mock<IFormFile>();

            mockFile.Setup(o => o.Length).Returns(1);
            mockFile.Setup(o => o.ContentType).Returns("image/png");
            mockFile.Setup(o => o.FileName).Returns("Fotaso.png");


            List<IFormFile> files = new List<IFormFile> { mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object };

            Postulacion postulacion = new Postulacion { id = 8 };

            mockPostulacionRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(postulacion);

            Publicacion publicacion = new Publicacion { id = 99 };
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(publicacion);

            mockIWebHostEnvironment.Setup(o => o.WebRootPath).Returns("C :\\Users\\Alex\\source\repos\\AppTUMISMITOPE.Test\\wwwrootTest");


            mockManageFiles.Setup(o => o.CombinePaths(It.IsAny<string>(), It.IsAny<string>(), mockFile.Object.FileName)).Returns("files\\fotosEvidencias\\" + mockFile.Object.FileName);
            mockManageFiles.Setup(o => o.CombinePaths(mockIWebHostEnvironment.Object.WebRootPath, It.IsAny<string>())).Returns("C:\\Users\\Alex\\source\\repos\\AppTUMISMITOPE.Test\\wwwroootTest\\files\\fotosEvidencias\\Fotaso.png");

            var result = pc.cerrarTrabajo(new Evidencia(), files) as RedirectToPageResult;

            //verificamos la asignacion de estado segun la logica
            Assert.AreEqual(1, publicacion.estado);

        }
        [Test]
        public void cerrarTrabajoPostSuccesCase13()
        {
            var mockFile = new Mock<IFormFile>();

            mockFile.Setup(o => o.Length).Returns(1);
            mockFile.Setup(o => o.ContentType).Returns("image/png");
            mockFile.Setup(o => o.FileName).Returns("Fotaso.png");


            List<IFormFile> files = new List<IFormFile> { mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object };

            Postulacion postulacion = new Postulacion { id = 8, estado = 1111111 };

            mockPostulacionRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(postulacion);

            Publicacion publicacion = new Publicacion { id = 65, estado = 1002112 };
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(publicacion);

            mockIWebHostEnvironment.Setup(o => o.WebRootPath).Returns("C :\\Users\\Alex\\source\repos\\AppTUMISMITOPE.Test\\wwwrootTest");


            mockManageFiles.Setup(o => o.CombinePaths(It.IsAny<string>(), It.IsAny<string>(), mockFile.Object.FileName)).Returns("files\\fotosEvidencias\\" + mockFile.Object.FileName);
            mockManageFiles.Setup(o => o.CombinePaths(mockIWebHostEnvironment.Object.WebRootPath, It.IsAny<string>())).Returns("C:\\Users\\Alex\\source\\repos\\AppTUMISMITOPE.Test\\wwwroootTest\\files\\fotosEvidencias\\Fotaso.png");

            var result = pc.cerrarTrabajo(new Evidencia(), files) as RedirectToPageResult;

            //verificamos la asignacion de estado segun la logica
            Assert.AreEqual(1, publicacion.estado);

        }


        [Test]
        public void cerrarTrabajoPostSuccesCase14()
        {
            var mockFile = new Mock<IFormFile>();

            mockFile.Setup(o => o.Length).Returns(1);
            mockFile.Setup(o => o.ContentType).Returns("image/png");
            mockFile.Setup(o => o.FileName).Returns("Fotaso.png");


            List<IFormFile> files = new List<IFormFile> { mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object };

            Postulacion postulacion = new Postulacion { id = 8, estado = 1111111 };

            mockPostulacionRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(postulacion);

            Publicacion publicacion = new Publicacion { id = 65, estado = 1111111 };
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(publicacion);

            mockIWebHostEnvironment.Setup(o => o.WebRootPath).Returns("C :\\Users\\Alex\\source\repos\\AppTUMISMITOPE.Test\\wwwrootTest");


            mockManageFiles.Setup(o => o.CombinePaths(It.IsAny<string>(), It.IsAny<string>(), mockFile.Object.FileName)).Returns("files\\fotosEvidencias\\" + mockFile.Object.FileName);
            mockManageFiles.Setup(o => o.CombinePaths(mockIWebHostEnvironment.Object.WebRootPath, It.IsAny<string>())).Returns("C:\\Users\\Alex\\source\\repos\\AppTUMISMITOPE.Test\\wwwroootTest\\files\\fotosEvidencias\\Fotaso.png");

            var result = pc.cerrarTrabajo(new Evidencia(), files) as RedirectToPageResult;

            //verificamos la asignacion de estado segun la logica
            Assert.AreNotEqual(postulacion.estado, publicacion.estado);

        }

        [Test]
        public void cerrarTrabajoPostSuccesCase15()
        {
            var mockFile = new Mock<IFormFile>();

            mockFile.Setup(o => o.Length).Returns(1);
            mockFile.Setup(o => o.ContentType).Returns("image/png");
            mockFile.Setup(o => o.FileName).Returns("Fotaso.png");


            List<IFormFile> files = new List<IFormFile> { mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object };

            Postulacion postulacion = new Postulacion { id = 8, estado = 1111111 };

            mockPostulacionRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(postulacion);

            Publicacion publicacion = new Publicacion { id = 65, estado = 1111111 };
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(publicacion);

            mockIWebHostEnvironment.Setup(o => o.WebRootPath).Returns("C :\\Users\\Alex\\source\repos\\AppTUMISMITOPE.Test\\wwwrootTest");


            mockManageFiles.Setup(o => o.CombinePaths(It.IsAny<string>(), It.IsAny<string>(), mockFile.Object.FileName)).Returns("files\\fotosEvidencias\\" + mockFile.Object.FileName);
            mockManageFiles.Setup(o => o.CombinePaths(mockIWebHostEnvironment.Object.WebRootPath, It.IsAny<string>())).Returns("C:\\Users\\Alex\\source\\repos\\AppTUMISMITOPE.Test\\wwwroootTest\\files\\fotosEvidencias\\Fotaso.png");

            var result = pc.cerrarTrabajo(new Evidencia(), files) as RedirectToPageResult;

            Assert.IsTrue(pc.ModelState.IsValid);
            //verificamos la cantidad de files
            Assert.AreEqual(6, files.Count);



        }
        [Test]
        public void cerrarTrabajoPostSuccesCase16()
        {
            var mockFile = new Mock<IFormFile>();

            mockFile.Setup(o => o.Length).Returns(1);
            mockFile.Setup(o => o.ContentType).Returns("image/png");
            mockFile.Setup(o => o.FileName).Returns("Fotaso.png");


            List<IFormFile> files = new List<IFormFile> { mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object };

            Postulacion postulacion = new Postulacion { id = 8, estado = 1111111 };

            mockPostulacionRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(postulacion);

            Publicacion publicacion = new Publicacion { id = 65, estado = 1111111 };
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(publicacion);

            mockIWebHostEnvironment.Setup(o => o.WebRootPath).Returns("C :\\Users\\Alex\\source\repos\\AppTUMISMITOPE.Test\\wwwrootTest");


            mockManageFiles.Setup(o => o.CombinePaths(It.IsAny<string>(), It.IsAny<string>(), mockFile.Object.FileName)).Returns("files\\fotosEvidencias\\" + mockFile.Object.FileName);
            mockManageFiles.Setup(o => o.CombinePaths(mockIWebHostEnvironment.Object.WebRootPath, It.IsAny<string>())).Returns("C:\\Users\\Alex\\source\\repos\\AppTUMISMITOPE.Test\\wwwroootTest\\files\\fotosEvidencias\\Fotaso.png");

            var result = pc.cerrarTrabajo(new Evidencia(), files) as RedirectToPageResult;

            //verificamos la cantidad de files
            Assert.IsTrue(pc.ModelState.IsValid);
            Assert.AreEqual(7, files.Count);

            foreach (var file in files)
            {

                Assert.AreNotEqual("image/jpeg", file.ContentType);
            }

        }
        [Test]
        public void cerrarTrabajoPostSuccesCase17()
        {
            var mockFile = new Mock<IFormFile>();

            mockFile.Setup(o => o.Length).Returns(1);
            mockFile.Setup(o => o.ContentType).Returns("image/png");
            mockFile.Setup(o => o.FileName).Returns("Fotaso.png");


            List<IFormFile> files = new List<IFormFile> { mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object };

            Postulacion postulacion = new Postulacion { id = 8, estado = 1111111 };

            mockPostulacionRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(postulacion);

            Publicacion publicacion = new Publicacion { id = 65, estado = 1111111 };
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(publicacion);

            mockIWebHostEnvironment.Setup(o => o.WebRootPath).Returns("C :\\Users\\Alex\\source\repos\\AppTUMISMITOPE.Test\\wwwrootTest");


            mockManageFiles.Setup(o => o.CombinePaths(It.IsAny<string>(), It.IsAny<string>(), mockFile.Object.FileName)).Returns("files\\fotosEvidencias\\" + mockFile.Object.FileName);
            mockManageFiles.Setup(o => o.CombinePaths(mockIWebHostEnvironment.Object.WebRootPath, It.IsAny<string>())).Returns("C:\\Users\\Alex\\source\\repos\\AppTUMISMITOPE.Test\\wwwroootTest\\files\\fotosEvidencias\\Fotaso.png");

            var result = pc.cerrarTrabajo(new Evidencia(), files) as RedirectToPageResult;

            //verificamos la cantidad de files
            Assert.IsTrue(pc.ModelState.IsValid);
            Assert.AreEqual(7, files.Count);

            foreach (var file in files) {

                Assert.AreEqual("image/png", file.ContentType);
            }

        }
        [Test]
        public void cerrarTrabajoPostSuccesCase18()
        {
            var mockFile = new Mock<IFormFile>();

            mockFile.Setup(o => o.Length).Returns(1);
            mockFile.Setup(o => o.ContentType).Returns("image/png");
            mockFile.Setup(o => o.FileName).Returns("Fotaso.png");


            List<IFormFile> files = new List<IFormFile> { mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object };

            Postulacion postulacion = new Postulacion { id = 8, estado = 1111111 };

            mockPostulacionRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(postulacion);

            Publicacion publicacion = new Publicacion { id = 65, estado = 1111111 };
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(publicacion);

            mockIWebHostEnvironment.Setup(o => o.WebRootPath).Returns("C :\\Users\\Alex\\source\repos\\AppTUMISMITOPE.Test\\wwwrootTest");


            mockManageFiles.Setup(o => o.CombinePaths(It.IsAny<string>(), It.IsAny<string>(), mockFile.Object.FileName)).Returns("files\\fotosEvidencias\\" + mockFile.Object.FileName);
            mockManageFiles.Setup(o => o.CombinePaths(mockIWebHostEnvironment.Object.WebRootPath, It.IsAny<string>())).Returns("C:\\Users\\Alex\\source\\repos\\AppTUMISMITOPE.Test\\wwwroootTest\\files\\fotosEvidencias\\Fotaso.png");

            var result = pc.cerrarTrabajo(new Evidencia(), files) as RedirectToPageResult;

            //verificamos la cantidad de files
            Assert.IsTrue(pc.ModelState.IsValid);
            Assert.AreEqual(7, files.Count);

            foreach (var file in files)
            {

                Assert.IsTrue(file.Length > 0);
            }

        }

        [Test]
        public void cerrarTrabajoPostSuccesCase19()
        {
            var mockFile = new Mock<IFormFile>();

            mockFile.Setup(o => o.Length).Returns(1);
            mockFile.Setup(o => o.ContentType).Returns("image/png");
            mockFile.Setup(o => o.FileName).Returns("Fotaso.png");


            List<IFormFile> files = new List<IFormFile> { mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object };

            Postulacion postulacion = new Postulacion { id = 8, estado = 1111111 };

            mockPostulacionRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(postulacion);

            Publicacion publicacion = new Publicacion { id = 65, estado = 1111111 };
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(publicacion);

            mockIWebHostEnvironment.Setup(o => o.WebRootPath).Returns("C :\\Users\\Alex\\source\repos\\AppTUMISMITOPE.Test\\wwwrootTest");

            //ruta
            string rutaRelative = "files\\fotosEvidencias\\" + mockFile.Object.FileName;


            mockManageFiles.Setup(o => o.CombinePaths(It.IsAny<string>(), It.IsAny<string>(), mockFile.Object.FileName)).Returns(rutaRelative);
            mockManageFiles.Setup(o => o.CombinePaths(mockIWebHostEnvironment.Object.WebRootPath, rutaRelative)).Returns("C:\\Users\\Alex\\source\\repos\\AppTUMISMITOPE.Test\\wwwroootTest\\files\\fotosEvidencias\\Fotaso.png");
            FotoEvidencia fotoEvidencia = new FotoEvidencia();
            mockFotoEvidenciaRepo.Setup(o => o.Add(It.IsAny<FotoEvidencia>())).Callback<FotoEvidencia>((objetoParametro) => fotoEvidencia = objetoParametro);

            var result = pc.cerrarTrabajo(new Evidencia(), files) as RedirectToPageResult;

            //verificamos la rutva cambiada



            Assert.AreNotEqual("files\\fotosEvidencias\\" + mockFile.Object.FileName, fotoEvidencia.contenido);


        }
        [Test]
        public void cerrarTrabajoPostSuccesCase20()
        {
            var mockFile = new Mock<IFormFile>();

            mockFile.Setup(o => o.Length).Returns(1);
            mockFile.Setup(o => o.ContentType).Returns("image/png");

            //ponemos un nombre con espacios 
            mockFile.Setup(o => o.FileName).Returns("Fotaso de las calles.png");


            List<IFormFile> files = new List<IFormFile> { mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object };

            Postulacion postulacion = new Postulacion { id = 8, estado = 1111111 };

            mockPostulacionRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(postulacion);

            Publicacion publicacion = new Publicacion { id = 65, estado = 1111111 };
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(publicacion);

            mockIWebHostEnvironment.Setup(o => o.WebRootPath).Returns("C :\\Users\\Alex\\source\repos\\AppTUMISMITOPE.Test\\wwwrootTest");

            //ruta
            string rutaRelative = "files\\fotosEvidencias\\" + mockFile.Object.FileName;


            mockManageFiles.Setup(o => o.CombinePaths(It.IsAny<string>(), It.IsAny<string>(), mockFile.Object.FileName)).Returns(rutaRelative);
            mockManageFiles.Setup(o => o.CombinePaths(mockIWebHostEnvironment.Object.WebRootPath, rutaRelative)).Returns("C:\\Users\\Alex\\source\\repos\\AppTUMISMITOPE.Test\\wwwroootTest\\files\\fotosEvidencias\\Fotaso.png");
            FotoEvidencia fotoEvidencia = new FotoEvidencia();

            //capturamos el paramtertro que recibio en el metodo
            mockFotoEvidenciaRepo.Setup(o => o.Add(It.IsAny<FotoEvidencia>())).Callback<FotoEvidencia>((objetoParametro) => fotoEvidencia = objetoParametro);

            var result = pc.cerrarTrabajo(new Evidencia(), files) as RedirectToPageResult;

            //verificamos la rutva cambiada



            Assert.AreNotEqual("files\\fotosEvidencias\\" + mockFile.Object.FileName, fotoEvidencia.contenido);


        }
        [Test]
        public void cerrarTrabajoPostSuccesCase21()
        {
            var mockFile = new Mock<IFormFile>();

            mockFile.Setup(o => o.Length).Returns(1);
            mockFile.Setup(o => o.ContentType).Returns("image/png");


            mockFile.Setup(o => o.FileName).Returns("Fotaso.png");


            List<IFormFile> files = new List<IFormFile> { mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object };

            Postulacion postulacion = new Postulacion { id = 8, estado = 1111111 };

            mockPostulacionRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(postulacion);

            Publicacion publicacion = new Publicacion { id = 65, estado = 1111111 };
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(publicacion);

            mockIWebHostEnvironment.Setup(o => o.WebRootPath).Returns("C :\\Users\\Alex\\source\repos\\AppTUMISMITOPE.Test\\wwwrootTest");

            //ruta
            string rutaRelative = "files\\fotosEvidencias\\" + mockFile.Object.FileName;


            mockManageFiles.Setup(o => o.CombinePaths(It.IsAny<string>(), It.IsAny<string>(), mockFile.Object.FileName)).Returns(rutaRelative);
            mockManageFiles.Setup(o => o.CombinePaths(mockIWebHostEnvironment.Object.WebRootPath, rutaRelative)).Returns("C:\\Users\\Alex\\source\\repos\\AppTUMISMITOPE.Test\\wwwroootTest\\files\\fotosEvidencias\\Fotaso.png");
            FotoEvidencia fotoEvidencia = new FotoEvidencia();

            //capturamos el paramtertro que recibio en el metodo
            mockFotoEvidenciaRepo.Setup(o => o.Add(It.IsAny<FotoEvidencia>())).Callback<FotoEvidencia>((objetoParametro) => fotoEvidencia = objetoParametro);

            var result = pc.cerrarTrabajo(new Evidencia(), files) as RedirectToPageResult;

            //verificamos la rutva cambiada



            Assert.AreEqual("/files/fotosEvidencias/" + mockFile.Object.FileName, fotoEvidencia.contenido);


        }
        [Test]
        public void cerrarTrabajoPostSuccesCase22()
        {
            var mockFile = new Mock<IFormFile>();

            mockFile.Setup(o => o.Length).Returns(1);
            mockFile.Setup(o => o.ContentType).Returns("image/png");

            //ponemos un nombre con espacios 
            mockFile.Setup(o => o.FileName).Returns("Fotaso de las calles.png");


            List<IFormFile> files = new List<IFormFile> { mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object };

            Postulacion postulacion = new Postulacion { id = 8, estado = 1111111 };

            mockPostulacionRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(postulacion);

            Publicacion publicacion = new Publicacion { id = 65, estado = 1111111 };
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(publicacion);

            mockIWebHostEnvironment.Setup(o => o.WebRootPath).Returns("C :\\Users\\Alex\\source\repos\\AppTUMISMITOPE.Test\\wwwrootTest");

            //ruta
            string rutaRelative = "files\\fotosEvidencias\\" + mockFile.Object.FileName;


            mockManageFiles.Setup(o => o.CombinePaths(It.IsAny<string>(), It.IsAny<string>(), mockFile.Object.FileName)).Returns(rutaRelative);
            mockManageFiles.Setup(o => o.CombinePaths(mockIWebHostEnvironment.Object.WebRootPath, rutaRelative)).Returns("C:\\Users\\Alex\\source\\repos\\AppTUMISMITOPE.Test\\wwwroootTest\\files\\fotosEvidencias\\Fotaso.png");
            FotoEvidencia fotoEvidencia = new FotoEvidencia();

            //capturamos el paramtertro que recibio en el metodo
            mockFotoEvidenciaRepo.Setup(o => o.Add(It.IsAny<FotoEvidencia>())).Callback<FotoEvidencia>((objetoParametro) => fotoEvidencia = objetoParametro);

            var result = pc.cerrarTrabajo(new Evidencia(), files) as RedirectToPageResult;

            //verificamos la rutva cambiada



            Assert.AreEqual("/files/fotosEvidencias/" + "Fotaso%20de%20las%20calles.png", fotoEvidencia.contenido);


        }
        [Test]
        public void cerrarTrabajoPostSuccesCase23()
        {
            var mockFile = new Mock<IFormFile>();

            mockFile.Setup(o => o.Length).Returns(1);
            mockFile.Setup(o => o.ContentType).Returns("image/jpeg");

            //ponemos un nombre con espacios 
            mockFile.Setup(o => o.FileName).Returns("0 1 2 3 4 5.jpeg");


            List<IFormFile> files = new List<IFormFile> { mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object };

            Postulacion postulacion = new Postulacion { id = 8, estado = 1111111 };

            mockPostulacionRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(postulacion);

            Publicacion publicacion = new Publicacion { id = 65, estado = 1111111 };
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(publicacion);

            mockIWebHostEnvironment.Setup(o => o.WebRootPath).Returns("C :\\Users\\Alex\\source\repos\\AppTUMISMITOPE.Test\\wwwrootTest");

            //ruta
            string rutaRelative = "files\\fotosEvidencias\\" + mockFile.Object.FileName;


            mockManageFiles.Setup(o => o.CombinePaths(It.IsAny<string>(), It.IsAny<string>(), mockFile.Object.FileName)).Returns(rutaRelative);
            mockManageFiles.Setup(o => o.CombinePaths(mockIWebHostEnvironment.Object.WebRootPath, rutaRelative)).Returns("C:\\Users\\Alex\\source\\repos\\AppTUMISMITOPE.Test\\wwwroootTest\\files\\fotosEvidencias\\Fotaso.jpeg");
            FotoEvidencia fotoEvidencia = new FotoEvidencia();

            //capturamos el paramtertro que recibio en el metodo
            mockFotoEvidenciaRepo.Setup(o => o.Add(It.IsAny<FotoEvidencia>())).Callback<FotoEvidencia>((objetoParametro) => fotoEvidencia = objetoParametro);

            var result = pc.cerrarTrabajo(new Evidencia(), files) as RedirectToPageResult;

            //verificamos la rutva cambiada



            Assert.AreEqual("/files/fotosEvidencias/" + "0%201%202%203%204%205.jpeg", fotoEvidencia.contenido);


        }

        [Test]
        public void cerrarTrabajoPostSuccesCase24()
        {
            var mockFile = new Mock<IFormFile>();

            mockFile.Setup(o => o.Length).Returns(1);
            mockFile.Setup(o => o.ContentType).Returns("image/jpeg");


            mockFile.Setup(o => o.FileName).Returns("Picc.jpeg");


            List<IFormFile> files = new List<IFormFile> { mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object };

            Postulacion postulacion = new Postulacion { id = 8, estado = 1111111 };

            mockPostulacionRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(postulacion);

            Publicacion publicacion = new Publicacion { id = 65, estado = 1111111 };
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(publicacion);

            mockIWebHostEnvironment.Setup(o => o.WebRootPath).Returns("C :\\Users\\Alex\\source\repos\\AppTUMISMITOPE.Test\\wwwrootTest");


            string rutaRelative = "files\\fotosEvidencias\\" + mockFile.Object.FileName;


            mockManageFiles.Setup(o => o.CombinePaths(It.IsAny<string>(), It.IsAny<string>(), mockFile.Object.FileName)).Returns(rutaRelative);
            mockManageFiles.Setup(o => o.CombinePaths(mockIWebHostEnvironment.Object.WebRootPath, rutaRelative)).Returns("C:\\Users\\Alex\\source\\repos\\AppTUMISMITOPE.Test\\wwwroootTest\\files\\fotosEvidencias\\Picc.jpeg");
            FotoEvidencia fotoEvidencia = new FotoEvidencia();


            mockFotoEvidenciaRepo.Setup(o => o.Add(It.IsAny<FotoEvidencia>())).Callback<FotoEvidencia>((objetoParametro) => fotoEvidencia = objetoParametro);

            var result = pc.cerrarTrabajo(new Evidencia { id = 4 }, files) as RedirectToPageResult;

            //verificamos la rutva cambiada



            Assert.AreEqual(4, fotoEvidencia.idEvidencia);


        }
        [Test]
        public void cerrarTrabajoPostFailCase25()
        {
            var mockFile = new Mock<IFormFile>();

            mockFile.Setup(o => o.Length).Returns(1);
            mockFile.Setup(o => o.ContentType).Returns("image/png");
            mockFile.Setup(o => o.FileName).Returns("Fotaso.png");


            List<IFormFile> files = new List<IFormFile> { mockFile.Object };

            Postulacion postulacion = new Postulacion { id = 8, estado = 1111111 };

            mockPostulacionRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(postulacion);



            var result = pc.cerrarTrabajo(new Evidencia(), files) as RedirectToActionResult;

            Assert.IsFalse(pc.ModelState.IsValid);

        }

        [Test]
        public void cerrarTrabajoPostFailCase26()
        {
            var mockFile = new Mock<IFormFile>();

            mockFile.Setup(o => o.Length).Returns(1);
            mockFile.Setup(o => o.ContentType).Returns("image/png");
            mockFile.Setup(o => o.FileName).Returns("Fotaso.png");


            List<IFormFile> files = new List<IFormFile> { mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object };


            Postulacion postulacion = new Postulacion { id = 8, estado = 1111111 };

            mockPostulacionRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(postulacion);
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());



            var result = pc.cerrarTrabajo(new Evidencia(), files);

            Assert.IsFalse(pc.ModelState.IsValid);
            Assert.IsInstanceOf<ViewResult>(result);

        }
        [Test]
        public void cerrarTrabajoPostFailCase27()
        {
            var mockFile = new Mock<IFormFile>();

            mockFile.Setup(o => o.Length).Returns(1);
            mockFile.Setup(o => o.ContentType).Returns("image/png");
            mockFile.Setup(o => o.FileName).Returns("Fotaso.png");


            List<IFormFile> files = new List<IFormFile> { mockFile.Object };


            Postulacion postulacion = new Postulacion { id = 8, estado = 1111111 };

            mockPostulacionRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(postulacion);
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());


            var result = pc.cerrarTrabajo(new Evidencia(), files);

            Assert.IsFalse(pc.ModelState.IsValid);
            Assert.IsInstanceOf<ViewResult>(result);

        }

        [Test]
        public void cerrarTrabajoPostFailCase28()
        {
            var mockFile = new Mock<IFormFile>();

            mockFile.Setup(o => o.Length).Returns(1);
            mockFile.Setup(o => o.ContentType).Returns("image/png");
            mockFile.Setup(o => o.FileName).Returns("Fotaso.png");


            List<IFormFile> files = new List<IFormFile>();


            Postulacion postulacion = new Postulacion { id = 8, estado = 1111111 };

            mockPostulacionRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(postulacion);
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());






            var result = pc.cerrarTrabajo(new Evidencia(), files) as ViewResult;

            Assert.IsFalse(pc.ModelState.IsValid);
            Assert.IsTrue(result.ViewName.StartsWith("c"));
            Assert.IsTrue(result.ViewName.EndsWith("o"));
            Assert.AreEqual("cerrarTrabajo", result.ViewName);
        }

        [Test]
        public void cerrarTrabajoPostFailCase29()
        {
            var mockFile = new Mock<IFormFile>();

            mockFile.Setup(o => o.Length).Returns(1);
            mockFile.Setup(o => o.ContentType).Returns("image/png");
            mockFile.Setup(o => o.FileName).Returns("Fotaso.png");


            List<IFormFile> files = new List<IFormFile>();


            Postulacion postulacion = new Postulacion { id = 8, estado = 1111111 };

            mockPostulacionRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(postulacion);
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());






            var result = pc.cerrarTrabajo(new Evidencia(), files) as ViewResult;

            Assert.IsFalse(pc.ModelState.IsValid);
            Assert.IsNotNull(result.Model);
        }
        [Test]
        public void cerrarTrabajoPostFailCase30()
        {
            var mockFile = new Mock<IFormFile>();

            mockFile.Setup(o => o.Length).Returns(1);
            mockFile.Setup(o => o.ContentType).Returns("image/png");
            mockFile.Setup(o => o.FileName).Returns("Fotaso.png");


            List<IFormFile> files = new List<IFormFile>();


            Postulacion postulacion = new Postulacion { id = 8, estado = 1111111 };

            mockPostulacionRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(postulacion);
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());






            var result = pc.cerrarTrabajo(new Evidencia(), files) as ViewResult;

            Assert.IsFalse(pc.ModelState.IsValid);
            Assert.IsInstanceOf<PubModel>(result.Model);
        }
        [Test]
        public void cerrarTrabajoPostFailCase31()
        {


            DateTime fecha = new DateTime(2021, 3, 11);
            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha);


            var mockFile = new Mock<IFormFile>();

            mockFile.Setup(o => o.Length).Returns(1);
            mockFile.Setup(o => o.ContentType).Returns("image/png");
            mockFile.Setup(o => o.FileName).Returns("Fotaso.png");


            List<IFormFile> files = new List<IFormFile>();


            Postulacion postulacion = new Postulacion { id = 8, estado = 1111111 };

            mockPostulacionRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(postulacion);
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());






            var result = pc.cerrarTrabajo(new Evidencia(), files) as ViewResult;
            var Modelo = result.Model as PubModel;


            Assert.IsFalse(pc.ModelState.IsValid);
            Assert.IsInstanceOf<DateTime>(Modelo.fechaActual);



        }
        [Test]
        public void cerrarTrabajoPostFailCase32()
        {


            DateTime fecha = new DateTime(2021, 3, 11);
            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha);


            var mockFile = new Mock<IFormFile>();

            mockFile.Setup(o => o.Length).Returns(1);
            mockFile.Setup(o => o.ContentType).Returns("image/png");
            mockFile.Setup(o => o.FileName).Returns("Fotaso.png");


            List<IFormFile> files = new List<IFormFile>();


            Postulacion postulacion = new Postulacion { id = 8, estado = 1111111 };

            mockPostulacionRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(postulacion);
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());






            var result = pc.cerrarTrabajo(new Evidencia(), files) as ViewResult;
            var Modelo = result.Model as PubModel;


            Assert.IsFalse(pc.ModelState.IsValid);
            Assert.AreEqual(fecha, Modelo.fechaActual);



        }
        [Test]
        public void cerrarTrabajoPostFailCase33()
        {


            DateTime fecha = new DateTime(2021, 3, 11);
            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha.AddDays(3));


            var mockFile = new Mock<IFormFile>();

            mockFile.Setup(o => o.Length).Returns(1);
            mockFile.Setup(o => o.ContentType).Returns("image/png");
            mockFile.Setup(o => o.FileName).Returns("Fotaso.png");


            List<IFormFile> files = new List<IFormFile>();


            Postulacion postulacion = new Postulacion { id = 8, estado = 1111111 };

            mockPostulacionRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(postulacion);
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());






            var result = pc.cerrarTrabajo(new Evidencia(), files) as ViewResult;
            var Modelo = result.Model as PubModel;


            Assert.IsFalse(pc.ModelState.IsValid);
            Assert.AreEqual(fecha.AddDays(3), Modelo.fechaActual);



        }
        [Test]
        public void cerrarTrabajoPostFailCase34()
        {


            DateTime fecha = new DateTime(2021, 3, 11);
            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha.AddMinutes(15));


            var mockFile = new Mock<IFormFile>();

            mockFile.Setup(o => o.Length).Returns(1);
            mockFile.Setup(o => o.ContentType).Returns("image/png");
            mockFile.Setup(o => o.FileName).Returns("Fotaso.png");


            List<IFormFile> files = new List<IFormFile>();


            Postulacion postulacion = new Postulacion { id = 8, estado = 1111111 };

            mockPostulacionRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(postulacion);
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());






            var result = pc.cerrarTrabajo(new Evidencia(), files) as ViewResult;
            var Modelo = result.Model as PubModel;


            Assert.IsFalse(pc.ModelState.IsValid);
            Assert.AreEqual(fecha.AddMinutes(15), Modelo.fechaActual);

        }
        [Test]
        public void cerrarTrabajoPostFailCase35()
        {


            DateTime fecha = new DateTime(2021, 3, 11);
            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha.AddMinutes(15));


            var mockFile = new Mock<IFormFile>();

            mockFile.Setup(o => o.Length).Returns(1);
            mockFile.Setup(o => o.ContentType).Returns("image/png");
            mockFile.Setup(o => o.FileName).Returns("Fotaso.png");


            List<IFormFile> files = new List<IFormFile>();


            Postulacion postulacion = new Postulacion { id = 8, estado = 1111111 };

            mockPostulacionRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(postulacion);
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());
            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario());





            var result = pc.cerrarTrabajo(new Evidencia(), files) as ViewResult;
            var Modelo = result.Model as PubModel;


            Assert.IsFalse(pc.ModelState.IsValid);
            Assert.IsInstanceOf<Usuario>(Modelo.UsuarioActivo);
            Assert.IsInstanceOf<Publicacion>(Modelo.pub);

        }
        [Test]
        public void cerrarTrabajoPostFailCase36()
        {


            DateTime fecha = new DateTime(2021, 3, 11);
            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha.AddMinutes(15));


            var mockFile = new Mock<IFormFile>();

            mockFile.Setup(o => o.Length).Returns(1);
            mockFile.Setup(o => o.ContentType).Returns("image/png");
            mockFile.Setup(o => o.FileName).Returns("Fotaso.png");


            List<IFormFile> files = new List<IFormFile>();


            Postulacion postulacion = new Postulacion { id = 8, estado = 1111111 };

            mockPostulacionRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(postulacion);
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion { id = 17 });
            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario { id = 50 });





            var result = pc.cerrarTrabajo(new Evidencia(), files) as ViewResult;
            var Modelo = result.Model as PubModel;


            Assert.IsFalse(pc.ModelState.IsValid);
            Assert.AreEqual(17, Modelo.pub.id);
            Assert.AreEqual(50, Modelo.UsuarioActivo.id);
        }

        [Test]
        public void cerrarTrabajoPostFailCase37()
        {


            DateTime fecha = new DateTime(2021, 3, 11);
            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha.AddMinutes(15));


            var mockFile = new Mock<IFormFile>();

            mockFile.Setup(o => o.Length).Returns(1);
            mockFile.Setup(o => o.ContentType).Returns("image/png");
            mockFile.Setup(o => o.FileName).Returns("Fotaso.png");


            List<IFormFile> files = new List<IFormFile>();


            Postulacion postulacion = new Postulacion { id = 8, estado = 1111111 };

            mockPostulacionRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(postulacion);
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(value: null);
            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(value: null);





            var result = pc.cerrarTrabajo(new Evidencia(), files) as ViewResult;
            var Modelo = result.Model as PubModel;


            Assert.IsFalse(pc.ModelState.IsValid);
            Assert.IsNull(Modelo.pub);
            Assert.IsNull(Modelo.UsuarioActivo);
        }
        [Test]
        public void cerrarTrabajoPostFailCase38()
        {


            DateTime fecha = new DateTime(2021, 3, 11);
            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha.AddMinutes(15));


            var mockFile = new Mock<IFormFile>();

            mockFile.Setup(o => o.Length).Returns(0);
            mockFile.Setup(o => o.ContentType).Returns("image/png");
            mockFile.Setup(o => o.FileName).Returns("Fotaso.png");


            List<IFormFile> files = new List<IFormFile> { mockFile.Object };


            Postulacion postulacion = new Postulacion { id = 8, estado = 1111111 };

            mockPostulacionRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(postulacion);
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(value: null);
            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(value: null);





            var result = pc.cerrarTrabajo(new Evidencia(), files) as ViewResult;


            Assert.IsFalse(pc.ModelState.IsValid);
            Assert.AreEqual("Se requiere almenos 6 fotos", result.ViewData.ModelState["evidencias"].Errors[0].ErrorMessage);

        }
        [Test]
        public void cerrarTrabajoPostFailCase39()
        {


            DateTime fecha = new DateTime(2021, 3, 11);
            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha.AddMinutes(15));


            var mockFile = new Mock<IFormFile>();

            mockFile.Setup(o => o.Length).Returns(1);
            mockFile.Setup(o => o.ContentType).Returns("image/jpg");
            mockFile.Setup(o => o.FileName).Returns("Fotaso.jpg");


            List<IFormFile> files = new List<IFormFile> { mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object };


            Postulacion postulacion = new Postulacion { id = 8, estado = 1111111 };

            mockPostulacionRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(postulacion);
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());
            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario());








            Assert.IsTrue(pc.ModelState.IsValid);

            var exception = Assert.Throws(typeof(Exception), () => pc.cerrarTrabajo(new Evidencia(), files));
            Assert.AreEqual("file with error extension", exception.Message);

        }
        [Test]
        public void cerrarTrabajoPostFailCase40()
        {


            DateTime fecha = new DateTime(2021, 3, 11);
            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha.AddMinutes(15));


            var mockFile = new Mock<IFormFile>();

            mockFile.Setup(o => o.Length).Returns(1);
            mockFile.Setup(o => o.ContentType).Returns("image/svg");
            mockFile.Setup(o => o.FileName).Returns("Fotaso.svg");


            List<IFormFile> files = new List<IFormFile> { mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object };


            Postulacion postulacion = new Postulacion { id = 8, estado = 1111111 };

            mockPostulacionRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(postulacion);
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());
            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario());








            Assert.IsTrue(pc.ModelState.IsValid);

            var exception = Assert.Throws(typeof(Exception), () => pc.cerrarTrabajo(new Evidencia(), files));
            Assert.AreEqual("file with error extension", exception.Message);

        }
        [Test]
        public void cerrarTrabajoPostFailCase41()
        {


            DateTime fecha = new DateTime(2021, 3, 11);
            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha.AddMinutes(15));


            var mockFile = new Mock<IFormFile>();

            mockFile.Setup(o => o.Length).Returns(1);
            mockFile.Setup(o => o.ContentType).Returns("image/gif");
            mockFile.Setup(o => o.FileName).Returns("Fotaso.gif");


            List<IFormFile> files = new List<IFormFile> { mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object };


            Postulacion postulacion = new Postulacion { id = 8, estado = 1111111 };

            mockPostulacionRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(postulacion);
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());
            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario());








            Assert.IsTrue(pc.ModelState.IsValid);

            var exception = Assert.Throws(typeof(Exception), () => pc.cerrarTrabajo(new Evidencia(), files));
            Assert.AreEqual("file with error extension", exception.Message);

        }
        [Test]
        public void cerrarTrabajoPostFailCase42()
        {


            DateTime fecha = new DateTime(2021, 3, 11);
            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha.AddMinutes(15));


            var mockFile = new Mock<IFormFile>();

            mockFile.Setup(o => o.Length).Returns(1);
            mockFile.Setup(o => o.ContentType).Returns("image/bpm");
            mockFile.Setup(o => o.FileName).Returns("Fotaso.bpm");


            List<IFormFile> files = new List<IFormFile> { mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object };


            Postulacion postulacion = new Postulacion { id = 8, estado = 1111111 };

            mockPostulacionRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(postulacion);
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());
            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario());








            Assert.IsTrue(pc.ModelState.IsValid);

            var exception = Assert.Throws(typeof(Exception), () => pc.cerrarTrabajo(new Evidencia(), files));
            Assert.AreEqual("file with error extension", exception.Message);

        }

        [Test]
        public void cerrarTrabajoPostFailCase43()
        {


            DateTime fecha = new DateTime(2021, 3, 11);
            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha.AddMinutes(15));


            var mockFile = new Mock<IFormFile>();

            mockFile.Setup(o => o.Length).Returns(1);
            mockFile.Setup(o => o.ContentType).Returns("image/jpeg");
            mockFile.Setup(o => o.FileName).Returns("Fotaso.jpeg");



            var mockFileBad = new Mock<IFormFile>();
            //incrustamos un file que no deberia generar expception
            mockFileBad.Setup(o => o.Length).Returns(1);
            mockFileBad.Setup(o => o.ContentType).Returns("image/bpm");
            mockFileBad.Setup(o => o.FileName).Returns("Fotaso.bpm");

            List<IFormFile> files = new List<IFormFile> { mockFile.Object, mockFile.Object, mockFile.Object, mockFileBad.Object, mockFile.Object, mockFile.Object };


            Postulacion postulacion = new Postulacion { id = 8, estado = 1111111 };

            mockIWebHostEnvironment.Setup(o => o.WebRootPath).Returns("C :\\Users\\Alex\\source\repos\\AppTUMISMITOPE.Test\\wwwrootTest");

            //ruta
            string rutaRelative = "files\\fotosEvidencias\\" + mockFile.Object.FileName;


            mockManageFiles.Setup(o => o.CombinePaths(It.IsAny<string>(), It.IsAny<string>(), mockFile.Object.FileName)).Returns(rutaRelative);
            mockManageFiles.Setup(o => o.CombinePaths(mockIWebHostEnvironment.Object.WebRootPath, rutaRelative)).Returns("C:\\Users\\Alex\\source\\repos\\AppTUMISMITOPE.Test\\wwwroootTest\\files\\fotosEvidencias\\Fotaso.png");





            mockPostulacionRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(postulacion);
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());
            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario());








            Assert.IsTrue(pc.ModelState.IsValid);

            var exception = Assert.Throws(typeof(Exception), () => pc.cerrarTrabajo(new Evidencia(), files));
            Assert.AreEqual("file with error extension", exception.Message);

        }
        [Test]
        public void cerrarTrabajoPostFailCase44()
        {


            DateTime fecha = new DateTime(2021, 3, 11);
            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha.AddMinutes(15));


            var mockFile = new Mock<IFormFile>();

            mockFile.Setup(o => o.Length).Returns(1);
            mockFile.Setup(o => o.ContentType).Returns("image/jpeg");
            mockFile.Setup(o => o.FileName).Returns("Fotaso.jpeg");



            var mockFileBad = new Mock<IFormFile>();
            //incrustamos un file que no deberia generar expception
            mockFileBad.Setup(o => o.Length).Returns(1);
            mockFileBad.Setup(o => o.ContentType).Returns("image/svg");
            mockFileBad.Setup(o => o.FileName).Returns("Fotaso.svg");

            List<IFormFile> files = new List<IFormFile> { mockFile.Object, mockFileBad.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object };


            Postulacion postulacion = new Postulacion { id = 8, estado = 1111111 };

            mockIWebHostEnvironment.Setup(o => o.WebRootPath).Returns("C :\\Users\\Alex\\source\repos\\AppTUMISMITOPE.Test\\wwwrootTest");

            //ruta
            string rutaRelative = "files\\fotosEvidencias\\" + mockFile.Object.FileName;


            mockManageFiles.Setup(o => o.CombinePaths(It.IsAny<string>(), It.IsAny<string>(), mockFile.Object.FileName)).Returns(rutaRelative);
            mockManageFiles.Setup(o => o.CombinePaths(mockIWebHostEnvironment.Object.WebRootPath, rutaRelative)).Returns("C:\\Users\\Alex\\source\\repos\\AppTUMISMITOPE.Test\\wwwroootTest\\files\\fotosEvidencias\\Fotaso.png");





            mockPostulacionRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(postulacion);
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());
            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario());








            Assert.IsTrue(pc.ModelState.IsValid);

            var exception = Assert.Throws(typeof(Exception), () => pc.cerrarTrabajo(new Evidencia(), files));
            Assert.AreEqual("file with error extension", exception.Message);

        }


        [Test]
        public void cerrarTrabajoPostFailCase45()
        {


            DateTime fecha = new DateTime(2021, 3, 11);
            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha.AddMinutes(15));


            var mockFile = new Mock<IFormFile>();

            mockFile.Setup(o => o.Length).Returns(1);
            mockFile.Setup(o => o.ContentType).Returns("image/jpeg");
            mockFile.Setup(o => o.FileName).Returns("Fotaso.jpeg");



            var mockFileBad = new Mock<IFormFile>();
            //incrustamos un file que no deberia generar expception
            mockFileBad.Setup(o => o.Length).Returns(1);
            mockFileBad.Setup(o => o.ContentType).Returns("image/svg");
            mockFileBad.Setup(o => o.FileName).Returns("Fotaso.svg");

            List<IFormFile> files = new List<IFormFile> { mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFileBad.Object };


            Postulacion postulacion = new Postulacion { id = 8, estado = 1111111 };

            mockIWebHostEnvironment.Setup(o => o.WebRootPath).Returns("C :\\Users\\Alex\\source\repos\\AppTUMISMITOPE.Test\\wwwrootTest");

            //ruta
            string rutaRelative = "files\\fotosEvidencias\\" + mockFile.Object.FileName;


            mockManageFiles.Setup(o => o.CombinePaths(It.IsAny<string>(), It.IsAny<string>(), mockFile.Object.FileName)).Returns(rutaRelative);
            mockManageFiles.Setup(o => o.CombinePaths(mockIWebHostEnvironment.Object.WebRootPath, rutaRelative)).Returns("C:\\Users\\Alex\\source\\repos\\AppTUMISMITOPE.Test\\wwwroootTest\\files\\fotosEvidencias\\Fotaso.png");





            mockPostulacionRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(postulacion);
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());
            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario());








            Assert.IsTrue(pc.ModelState.IsValid);

            var exception = Assert.Throws(typeof(Exception), () => pc.cerrarTrabajo(new Evidencia(), files));
            Assert.AreEqual("file with error extension", exception.Message);

        }


        [Test]
        public void cerrarTrabajoPostFailCase46()
        {


            DateTime fecha = new DateTime(2021, 3, 11);
            mockDateMethods.Setup(o => o.getCurrentDate()).Returns(fecha.AddMinutes(15));


            var mockFile = new Mock<IFormFile>();

            mockFile.Setup(o => o.Length).Returns(1);
            mockFile.Setup(o => o.ContentType).Returns("image/jpeg");
            mockFile.Setup(o => o.FileName).Returns("Fotaso.jpeg");



            var mockFileBad = new Mock<IFormFile>();
            //incrustamos un file que no deberia generar expception
            mockFileBad.Setup(o => o.Length).Returns(0);
            mockFileBad.Setup(o => o.ContentType).Returns("image/jpeg");
            mockFileBad.Setup(o => o.FileName).Returns("Fotaso.jpeg");

            List<IFormFile> files = new List<IFormFile> { mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFile.Object, mockFileBad.Object };


            Postulacion postulacion = new Postulacion { id = 8, estado = 1111111 };

            mockIWebHostEnvironment.Setup(o => o.WebRootPath).Returns("C :\\Users\\Alex\\source\repos\\AppTUMISMITOPE.Test\\wwwrootTest");

            //ruta
            string rutaRelative = "files\\fotosEvidencias\\" + mockFile.Object.FileName;


            mockManageFiles.Setup(o => o.CombinePaths(It.IsAny<string>(), It.IsAny<string>(), mockFile.Object.FileName)).Returns(rutaRelative);
            mockManageFiles.Setup(o => o.CombinePaths(mockIWebHostEnvironment.Object.WebRootPath, rutaRelative)).Returns("C:\\Users\\Alex\\source\\repos\\AppTUMISMITOPE.Test\\wwwroootTest\\files\\fotosEvidencias\\Fotaso.png");





            mockPostulacionRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(postulacion);
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());
            mockUsuarioRepo.Setup(o => o.FindByUserName(It.IsAny<String>())).Returns(new Usuario());








            Assert.IsTrue(pc.ModelState.IsValid);

            var exception = Assert.Throws(typeof(Exception), () => pc.cerrarTrabajo(new Evidencia(), files));
            Assert.AreEqual("file with error extension", exception.Message);

        }



        [Test]
        public void ActualizarEstadoPostulacionesCase01()
        {
            mockPostulacionRepo.Setup(o => o.GetPostulacionesByPublicacion(It.IsAny<int>())).Returns(new List<Postulacion> { new Postulacion { likesPostulacion = new List<LikePostulacion>() } });

            mockDonacionRepo.Setup(o => o.GetByIdPublicacion(It.IsAny<int>())).Returns(new List<Donacion>());
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());
            mockCajaRepo.Setup(o => o.GetCaja()).Returns(new Caja());
            mockPostulacionRepo.Setup(o => o.GetEnVotacionByIdPublicacion(It.IsAny<int>())).Returns(new List<Postulacion>());

            var result = pc.ActualizarEstadoPostulaciones(5);



            Assert.IsNotNull(result);

        }


        [Test]
        public void ActualizarEstadoPostulacionesCase02()
        {
            mockPostulacionRepo.Setup(o => o.GetPostulacionesByPublicacion(It.IsAny<int>())).Returns(new List<Postulacion> { new Postulacion { likesPostulacion = new List<LikePostulacion>() } });

            mockDonacionRepo.Setup(o => o.GetByIdPublicacion(It.IsAny<int>())).Returns(new List<Donacion>());
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());
            mockCajaRepo.Setup(o => o.GetCaja()).Returns(new Caja());
            mockPostulacionRepo.Setup(o => o.GetEnVotacionByIdPublicacion(It.IsAny<int>())).Returns(new List<Postulacion>());

            var result = pc.ActualizarEstadoPostulaciones(5);



            Assert.IsNotInstanceOf<ViewResult>(result);
            Assert.IsNotInstanceOf<RedirectToActionResult>(result);

        }
        [Test]
        public void ActualizarEstadoPostulacionesCase03()
        {
            mockPostulacionRepo.Setup(o => o.GetPostulacionesByPublicacion(It.IsAny<int>())).Returns(new List<Postulacion> { new Postulacion { likesPostulacion = new List<LikePostulacion>() } });

            mockDonacionRepo.Setup(o => o.GetByIdPublicacion(It.IsAny<int>())).Returns(new List<Donacion>());
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(It.IsAny<int>())).Returns(new Publicacion());
            mockCajaRepo.Setup(o => o.GetCaja()).Returns(new Caja());
            mockPostulacionRepo.Setup(o => o.GetEnVotacionByIdPublicacion(It.IsAny<int>())).Returns(new List<Postulacion>());

            var result = pc.ActualizarEstadoPostulaciones(5);



            Assert.IsInstanceOf<List<Postulacion>>(result);
        }
        [Test]
        public void ActualizarEstadoPostulacionesCase04()
        {
            Postulacion postualcion1 = new Postulacion { id = 5, monto = 500, likesPostulacion = new List<LikePostulacion> { new LikePostulacion { id = 29 }, new LikePostulacion { id = 11 } } };
            Postulacion postualcion2 = new Postulacion { id = 53, monto = 300, likesPostulacion = new List<LikePostulacion> { new LikePostulacion { id = 7 } } };


            mockPostulacionRepo.Setup(o => o.GetPostulacionesByPublicacion(It.IsAny<int>())).Returns(new List<Postulacion> { postualcion1, postualcion2 });


            List<Donacion> donaciones = new List<Donacion> { new Donacion { id = 58, monto = 600 }, new Donacion { id = 11, monto = 65 } };
            mockDonacionRepo.Setup(o => o.GetByIdPublicacion(5)).Returns(donaciones);


            Publicacion publicacion = new Publicacion { id = 5 };
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(5)).Returns(publicacion);

            Caja caja = new Caja { monto = 10 };
            mockCajaRepo.Setup(o => o.GetCaja()).Returns(caja);
            mockPostulacionRepo.Setup(o => o.GetEnVotacionByIdPublicacion(It.IsAny<int>())).Returns(new List<Postulacion>());

            var result = pc.ActualizarEstadoPostulaciones(5);



            //verificamos el cambio de datos cuando el monto de donaciones es mayor o igual al monto asignado



            Assert.AreEqual(2, postualcion1.estado);
            Assert.AreEqual(175, caja.monto);
            Assert.AreEqual(500, publicacion.montoAsignado);
        }
        [Test]
        public void ActualizarEstadoPostulacionesCase05()
        {
            Postulacion postualcion1 = new Postulacion { id = 5, monto = 500, likesPostulacion = new List<LikePostulacion> { new LikePostulacion { id = 29 }, new LikePostulacion { id = 11 } } };
            Postulacion postualcion2 = new Postulacion { id = 53, monto = 300, likesPostulacion = new List<LikePostulacion> { new LikePostulacion { id = 7 } } };


            mockPostulacionRepo.Setup(o => o.GetPostulacionesByPublicacion(It.IsAny<int>())).Returns(new List<Postulacion> { postualcion1, postualcion2 });


            List<Donacion> donaciones = new List<Donacion> { new Donacion { id = 58, monto = 300 }, new Donacion { id = 11, monto = 200 } };
            mockDonacionRepo.Setup(o => o.GetByIdPublicacion(5)).Returns(donaciones);


            Publicacion publicacion = new Publicacion { id = 5 };
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(5)).Returns(publicacion);

            Caja caja = new Caja { monto = 10 };
            mockCajaRepo.Setup(o => o.GetCaja()).Returns(caja);
            mockPostulacionRepo.Setup(o => o.GetEnVotacionByIdPublicacion(It.IsAny<int>())).Returns(new List<Postulacion>());

            var result = pc.ActualizarEstadoPostulaciones(5);



            //verificamos el cambio de datos cuando el monto de donaciones es mayor o igual al monto asignado



            Assert.AreEqual(2, postualcion1.estado);
            Assert.AreEqual(10, caja.monto);
            Assert.AreEqual(500, publicacion.montoAsignado);
        }
        [Test]
        public void ActualizarEstadoPostulacionesCase06()
        {
            Postulacion postualcion1 = new Postulacion { id = 5, monto = 500, likesPostulacion = new List<LikePostulacion> { new LikePostulacion { id = 29 } } };
            Postulacion postualcion2 = new Postulacion { id = 53, monto = 300, likesPostulacion = new List<LikePostulacion> { new LikePostulacion { id = 7 }, new LikePostulacion { id = 11 } } };


            mockPostulacionRepo.Setup(o => o.GetPostulacionesByPublicacion(It.IsAny<int>())).Returns(new List<Postulacion> { postualcion1, postualcion2 });


            List<Donacion> donaciones = new List<Donacion> { new Donacion { id = 58, monto = 300 }, new Donacion { id = 11, monto = 200 } };
            mockDonacionRepo.Setup(o => o.GetByIdPublicacion(5)).Returns(donaciones);


            Publicacion publicacion = new Publicacion { id = 5 };
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(5)).Returns(publicacion);

            Caja caja = new Caja { monto = 10 };
            mockCajaRepo.Setup(o => o.GetCaja()).Returns(caja);
            mockPostulacionRepo.Setup(o => o.GetEnVotacionByIdPublicacion(It.IsAny<int>())).Returns(new List<Postulacion>());

            var result = pc.ActualizarEstadoPostulaciones(5);



            //verificamos el cambio de datos cuando el monto de donaciones es mayor o igual al monto asignado



            Assert.AreEqual(2, postualcion2.estado);
            Assert.AreEqual(210, caja.monto);
            Assert.AreEqual(300, publicacion.montoAsignado);
        }
        [Test]
        public void ActualizarEstadoPostulacionesCase07()
        {
            Postulacion postualcion1 = new Postulacion { id = 5, monto = 600, likesPostulacion = new List<LikePostulacion> { new LikePostulacion { id = 29 } } };
            Postulacion postualcion2 = new Postulacion { id = 53, monto = 400, likesPostulacion = new List<LikePostulacion>() };


            mockPostulacionRepo.Setup(o => o.GetPostulacionesByPublicacion(It.IsAny<int>())).Returns(new List<Postulacion> { postualcion1, postualcion2 });


            List<Donacion> donaciones = new List<Donacion> { new Donacion { id = 58, monto = 300 }, new Donacion { id = 11, monto = 300 } };
            mockDonacionRepo.Setup(o => o.GetByIdPublicacion(5)).Returns(donaciones);


            Publicacion publicacion = new Publicacion { id = 5 };
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(5)).Returns(publicacion);

            Caja caja = new Caja { monto = 10 };
            mockCajaRepo.Setup(o => o.GetCaja()).Returns(caja);
            mockPostulacionRepo.Setup(o => o.GetEnVotacionByIdPublicacion(It.IsAny<int>())).Returns(new List<Postulacion>());

            var result = pc.ActualizarEstadoPostulaciones(5);



            //verificamos el cambio de datos cuando el monto de donaciones es mayor o igual al monto asignado



            Assert.AreEqual(2, postualcion1.estado);
            Assert.AreEqual(10, caja.monto);
            Assert.AreEqual(600, publicacion.montoAsignado);
            Assert.AreEqual(3, publicacion.estado);
        }
        [Test]
        public void ActualizarEstadoPostulacionesCase09()
        {

            //teniendo la misma cantidad de likes , ahora ejiremos por fecha
            Postulacion postualcion1 = new Postulacion { id = 5, fechaCreacion = new DateTime(2021, 4, 18), monto = 600, likesPostulacion = new List<LikePostulacion> { new LikePostulacion { id = 29 } } };
            Postulacion postualcion2 = new Postulacion { id = 53, fechaCreacion = new DateTime(2021, 4, 17), monto = 400, likesPostulacion = new List<LikePostulacion> { new LikePostulacion { id = 29 } } };


            mockPostulacionRepo.Setup(o => o.GetPostulacionesByPublicacion(It.IsAny<int>())).Returns(new List<Postulacion> { postualcion1, postualcion2 });


            List<Donacion> donaciones = new List<Donacion> { new Donacion { id = 58, monto = 300 }, new Donacion { id = 11, monto = 300 } };
            mockDonacionRepo.Setup(o => o.GetByIdPublicacion(5)).Returns(donaciones);


            Publicacion publicacion = new Publicacion { id = 5 };
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(5)).Returns(publicacion);

            Caja caja = new Caja { monto = 10 };
            mockCajaRepo.Setup(o => o.GetCaja()).Returns(caja);
            mockPostulacionRepo.Setup(o => o.GetEnVotacionByIdPublicacion(It.IsAny<int>())).Returns(new List<Postulacion>());

            var result = pc.ActualizarEstadoPostulaciones(5);



            //verificamos el cambio de datos cuando el monto de donaciones es mayor o igual al monto asignado



            Assert.AreEqual(2, postualcion2.estado);
            Assert.AreEqual(210, caja.monto);
            Assert.AreEqual(400, publicacion.montoAsignado);
            Assert.AreEqual(3, publicacion.estado);
        }
        [Test]
        public void ActualizarEstadoPostulacionesCase10()
        {

            //teniendo la misma cantidad de likes , ahora ejiremos por fecha
            Postulacion postualcion1 = new Postulacion { id = 5, fechaCreacion = new DateTime(2021, 4, 20), monto = 600, likesPostulacion = new List<LikePostulacion> { new LikePostulacion { id = 29 } } };
            Postulacion postualcion2 = new Postulacion { id = 53, fechaCreacion = new DateTime(2021, 4, 23), monto = 400, likesPostulacion = new List<LikePostulacion> { new LikePostulacion { id = 29 } } };


            mockPostulacionRepo.Setup(o => o.GetPostulacionesByPublicacion(It.IsAny<int>())).Returns(new List<Postulacion> { postualcion1, postualcion2 });


            List<Donacion> donaciones = new List<Donacion> { new Donacion { id = 58, monto = 300 }, new Donacion { id = 11, monto = 300 } };
            mockDonacionRepo.Setup(o => o.GetByIdPublicacion(5)).Returns(donaciones);


            Publicacion publicacion = new Publicacion { id = 5 };
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(5)).Returns(publicacion);

            Caja caja = new Caja { monto = 10 };
            mockCajaRepo.Setup(o => o.GetCaja()).Returns(caja);
            mockPostulacionRepo.Setup(o => o.GetEnVotacionByIdPublicacion(It.IsAny<int>())).Returns(new List<Postulacion>());

            var result = pc.ActualizarEstadoPostulaciones(5);



            //verificamos el cambio de datos cuando el monto de donaciones es mayor o igual al monto asignado



            Assert.AreEqual(2, postualcion1.estado);
            Assert.AreEqual(10, caja.monto);
            Assert.AreEqual(600, publicacion.montoAsignado);
            Assert.AreEqual(3, publicacion.estado);
        }
        [Test]
        public void ActualizarEstadoPostulacionesCase11()
        {

            //teniendo la misma cantidad de likes , ahora ejiremos por fecha
            Postulacion postualcion1 = new Postulacion { id = 5, fechaCreacion = new DateTime(2021, 4, 20), monto = 600, likesPostulacion = new List<LikePostulacion>() };
            Postulacion postualcion2 = new Postulacion { id = 53, fechaCreacion = new DateTime(2021, 4, 23), monto = 400, likesPostulacion = new List<LikePostulacion> { new LikePostulacion { id = 29 } } };


            mockPostulacionRepo.Setup(o => o.GetPostulacionesByPublicacion(It.IsAny<int>())).Returns(new List<Postulacion> { postualcion1, postualcion2 });


            List<Donacion> donaciones = new List<Donacion> { new Donacion { id = 58, monto = 300 }, new Donacion { id = 11, monto = 300 } };
            mockDonacionRepo.Setup(o => o.GetByIdPublicacion(5)).Returns(donaciones);


            Publicacion publicacion = new Publicacion { id = 5 };
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(5)).Returns(publicacion);

            Caja caja = new Caja { monto = 10 };
            mockCajaRepo.Setup(o => o.GetCaja()).Returns(caja);
            mockPostulacionRepo.Setup(o => o.GetEnVotacionByIdPublicacion(It.IsAny<int>())).Returns(new List<Postulacion>());

            var result = pc.ActualizarEstadoPostulaciones(5);



            //verificamos el cambio de datos cuando el monto de donaciones es mayor o igual al monto asignado

            Assert.AreEqual(2, postualcion2.estado);
            Assert.AreEqual(210, caja.monto);
            Assert.AreEqual(400, publicacion.montoAsignado);
            Assert.AreEqual(3, publicacion.estado);
        }
        [Test]
        public void ActualizarEstadoPostulacionesCase12()
        {

            //teniendo la misma cantidad de likes , ahora ejiremos por fecha
            Postulacion postualcion1 = new Postulacion { id = 5, fechaCreacion = new DateTime(2021, 4, 20), monto = 4.20m, likesPostulacion = new List<LikePostulacion>() };
            Postulacion postualcion2 = new Postulacion { id = 53, fechaCreacion = new DateTime(2021, 4, 23), monto = 6.50m, likesPostulacion = new List<LikePostulacion> { new LikePostulacion { id = 29 } } };


            mockPostulacionRepo.Setup(o => o.GetPostulacionesByPublicacion(It.IsAny<int>())).Returns(new List<Postulacion> { postualcion1, postualcion2 });


            List<Donacion> donaciones = new List<Donacion> { new Donacion { id = 58, monto = 50.60 }, new Donacion { id = 11, monto = 1 } };
            mockDonacionRepo.Setup(o => o.GetByIdPublicacion(5)).Returns(donaciones);


            Publicacion publicacion = new Publicacion { id = 5 };
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(5)).Returns(publicacion);

            Caja caja = new Caja { monto = 10 };
            mockCajaRepo.Setup(o => o.GetCaja()).Returns(caja);
            mockPostulacionRepo.Setup(o => o.GetEnVotacionByIdPublicacion(It.IsAny<int>())).Returns(new List<Postulacion>());

            var result = pc.ActualizarEstadoPostulaciones(5);



            //verificamos el cambio de datos cuando el monto de donaciones es mayor o igual al monto asignado

            Assert.AreEqual(2, postualcion2.estado);
            Assert.AreEqual(55.10, caja.monto);
            Assert.AreEqual(6.50, publicacion.montoAsignado);
            Assert.AreEqual(3, publicacion.estado);
        }
        [Test]
        public void ActualizarEstadoPostulacionesCase13()
        {


            Postulacion postualcion1 = new Postulacion { id = 5, fechaCreacion = new DateTime(2021, 4, 20), monto = 4.20m, likesPostulacion = new List<LikePostulacion>() };


            mockPostulacionRepo.Setup(o => o.GetPostulacionesByPublicacion(It.IsAny<int>())).Returns(new List<Postulacion> { postualcion1 });


            List<Donacion> donaciones = new List<Donacion> { new Donacion { id = 58, monto = 50.60 }, new Donacion { id = 11, monto = 1 } };
            mockDonacionRepo.Setup(o => o.GetByIdPublicacion(5)).Returns(donaciones);


            Publicacion publicacion = new Publicacion { id = 5 };
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(5)).Returns(publicacion);

            Caja caja = new Caja { monto = 10 };
            mockCajaRepo.Setup(o => o.GetCaja()).Returns(caja);
            mockPostulacionRepo.Setup(o => o.GetEnVotacionByIdPublicacion(It.IsAny<int>())).Returns(new List<Postulacion>());

            var result = pc.ActualizarEstadoPostulaciones(5);



            //verificamos el cambio de datos cuando el monto de donaciones es mayor o igual al monto asignado

            Assert.AreEqual(2, postualcion1.estado);
            Assert.AreEqual(57.40, caja.monto);
            Assert.AreEqual(4.20, publicacion.montoAsignado);
            Assert.AreEqual(3, publicacion.estado);
        }



        //cuando las donaciones no sobrepasan el monto

        [Test]
        public void ActualizarEstadoPostulacionesCase14()
        {
            Postulacion postualcion1 = new Postulacion { id = 5, monto = 800, likesPostulacion = new List<LikePostulacion> { new LikePostulacion { id = 29 }, new LikePostulacion { id = 11 } } };
            Postulacion postualcion2 = new Postulacion { id = 53, monto = 900, likesPostulacion = new List<LikePostulacion> { new LikePostulacion { id = 7 } } };


            mockPostulacionRepo.Setup(o => o.GetPostulacionesByPublicacion(It.IsAny<int>())).Returns(new List<Postulacion> { postualcion1, postualcion2 });


            List<Donacion> donaciones = new List<Donacion> { new Donacion { id = 58, monto = 600 }, new Donacion { id = 11, monto = 65 } };
            mockDonacionRepo.Setup(o => o.GetByIdPublicacion(5)).Returns(donaciones);


            Publicacion publicacion = new Publicacion { id = 5 };
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(5)).Returns(publicacion);

            Caja caja = new Caja { monto = 10 };
            mockCajaRepo.Setup(o => o.GetCaja()).Returns(caja);
            mockPostulacionRepo.Setup(o => o.GetEnVotacionByIdPublicacion(It.IsAny<int>())).Returns(new List<Postulacion>());

            var result = pc.ActualizarEstadoPostulaciones(5);



            //verificamos el cambio de datos cuando el monto de donaciones es mayor o igual al monto asignado



            Assert.AreEqual(1, postualcion1.estado);
            Assert.AreEqual(10, caja.monto);
            Assert.AreEqual(800, publicacion.montoAsignado);
            Assert.AreEqual(2, publicacion.estado);
        }
        [Test]
        public void ActualizarEstadoPostulacionesCase15()
        {
            Postulacion postualcion1 = new Postulacion { id = 5, monto = 550, likesPostulacion = new List<LikePostulacion> { new LikePostulacion { id = 29 }, new LikePostulacion { id = 11 } } };
            Postulacion postualcion2 = new Postulacion { id = 53, monto = 300, likesPostulacion = new List<LikePostulacion> { new LikePostulacion { id = 7 } } };


            mockPostulacionRepo.Setup(o => o.GetPostulacionesByPublicacion(It.IsAny<int>())).Returns(new List<Postulacion> { postualcion1, postualcion2 });


            List<Donacion> donaciones = new List<Donacion> { new Donacion { id = 58, monto = 300 }, new Donacion { id = 11, monto = 200 } };
            mockDonacionRepo.Setup(o => o.GetByIdPublicacion(5)).Returns(donaciones);


            Publicacion publicacion = new Publicacion { id = 5 };
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(5)).Returns(publicacion);

            Caja caja = new Caja { monto = 10 };
            mockCajaRepo.Setup(o => o.GetCaja()).Returns(caja);
            mockPostulacionRepo.Setup(o => o.GetEnVotacionByIdPublicacion(It.IsAny<int>())).Returns(new List<Postulacion>());

            var result = pc.ActualizarEstadoPostulaciones(5);



            //verificamos el cambio de datos cuando el monto de donaciones es mayor o igual al monto asignado



            Assert.AreEqual(1, postualcion1.estado);
            Assert.AreEqual(10, caja.monto);
            Assert.AreEqual(550, publicacion.montoAsignado);
            Assert.AreEqual(2, publicacion.estado);
        }
        [Test]
        public void ActualizarEstadoPostulacionesCase16()
        {
            Postulacion postualcion1 = new Postulacion { id = 5, monto = 700, likesPostulacion = new List<LikePostulacion> { new LikePostulacion { id = 29 } } };
            Postulacion postualcion2 = new Postulacion { id = 53, monto = 800, likesPostulacion = new List<LikePostulacion> { new LikePostulacion { id = 7 }, new LikePostulacion { id = 11 } } };


            mockPostulacionRepo.Setup(o => o.GetPostulacionesByPublicacion(It.IsAny<int>())).Returns(new List<Postulacion> { postualcion1, postualcion2 });


            List<Donacion> donaciones = new List<Donacion> { new Donacion { id = 58, monto = 300 }, new Donacion { id = 11, monto = 200 } };
            mockDonacionRepo.Setup(o => o.GetByIdPublicacion(5)).Returns(donaciones);


            Publicacion publicacion = new Publicacion { id = 5 };
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(5)).Returns(publicacion);

            Caja caja = new Caja { monto = 10 };
            mockCajaRepo.Setup(o => o.GetCaja()).Returns(caja);
            mockPostulacionRepo.Setup(o => o.GetEnVotacionByIdPublicacion(It.IsAny<int>())).Returns(new List<Postulacion>());

            var result = pc.ActualizarEstadoPostulaciones(5);



            //verificamos el cambio de datos cuando el monto de donaciones es mayor o igual al monto asignado



            Assert.AreEqual(1, postualcion2.estado);
            Assert.AreEqual(10, caja.monto);
            Assert.AreEqual(800, publicacion.montoAsignado);
            Assert.AreEqual(2, publicacion.estado);
        }
        [Test]
        public void ActualizarEstadoPostulacionesCase17()
        {
            Postulacion postualcion1 = new Postulacion { id = 5, monto = 601, likesPostulacion = new List<LikePostulacion> { new LikePostulacion { id = 29 } } };
            Postulacion postualcion2 = new Postulacion { id = 53, monto = 400, likesPostulacion = new List<LikePostulacion>() };


            mockPostulacionRepo.Setup(o => o.GetPostulacionesByPublicacion(It.IsAny<int>())).Returns(new List<Postulacion> { postualcion1, postualcion2 });


            List<Donacion> donaciones = new List<Donacion> { new Donacion { id = 58, monto = 300 }, new Donacion { id = 11, monto = 300 } };
            mockDonacionRepo.Setup(o => o.GetByIdPublicacion(5)).Returns(donaciones);


            Publicacion publicacion = new Publicacion { id = 5 };
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(5)).Returns(publicacion);

            Caja caja = new Caja { monto = 10 };
            mockCajaRepo.Setup(o => o.GetCaja()).Returns(caja);
            mockPostulacionRepo.Setup(o => o.GetEnVotacionByIdPublicacion(It.IsAny<int>())).Returns(new List<Postulacion>());

            var result = pc.ActualizarEstadoPostulaciones(5);



            //verificamos el cambio de datos cuando el monto de donaciones es mayor o igual al monto asignado

            Assert.AreEqual(1, postualcion1.estado);
            Assert.AreEqual(10, caja.monto);
            Assert.AreEqual(601, publicacion.montoAsignado);
            Assert.AreEqual(2, publicacion.estado);
        }
        [Test]
        public void ActualizarEstadoPostulacionesCase18()
        {

            //teniendo la misma cantidad de likes , ahora ejiremos por fecha
            Postulacion postualcion1 = new Postulacion { id = 5, fechaCreacion = new DateTime(2021, 4, 18), monto = 900, likesPostulacion = new List<LikePostulacion> { new LikePostulacion { id = 29 } } };
            Postulacion postualcion2 = new Postulacion { id = 53, fechaCreacion = new DateTime(2021, 4, 17), monto = 1000, likesPostulacion = new List<LikePostulacion> { new LikePostulacion { id = 29 } } };


            mockPostulacionRepo.Setup(o => o.GetPostulacionesByPublicacion(It.IsAny<int>())).Returns(new List<Postulacion> { postualcion1, postualcion2 });


            List<Donacion> donaciones = new List<Donacion> { new Donacion { id = 58, monto = 300 }, new Donacion { id = 11, monto = 300 } };
            mockDonacionRepo.Setup(o => o.GetByIdPublicacion(5)).Returns(donaciones);


            Publicacion publicacion = new Publicacion { id = 5 };
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(5)).Returns(publicacion);

            Caja caja = new Caja { monto = 10 };
            mockCajaRepo.Setup(o => o.GetCaja()).Returns(caja);
            mockPostulacionRepo.Setup(o => o.GetEnVotacionByIdPublicacion(It.IsAny<int>())).Returns(new List<Postulacion>());

            var result = pc.ActualizarEstadoPostulaciones(5);



            //verificamos el cambio de datos cuando el monto de donaciones es mayor o igual al monto asignado

            Assert.AreEqual(1, postualcion2.estado);
            Assert.AreEqual(10, caja.monto);
            Assert.AreEqual(1000, publicacion.montoAsignado);
            Assert.AreEqual(2, publicacion.estado);
        }
        [Test]
        public void ActualizarEstadoPostulacionesCase19()
        {

            //teniendo la misma cantidad de likes , ahora ejiremos por fecha
            Postulacion postualcion1 = new Postulacion { id = 5, fechaCreacion = new DateTime(2021, 4, 20), monto = 320, likesPostulacion = new List<LikePostulacion> { new LikePostulacion { id = 29 } } };
            Postulacion postualcion2 = new Postulacion { id = 53, fechaCreacion = new DateTime(2021, 4, 23), monto = 400, likesPostulacion = new List<LikePostulacion> { new LikePostulacion { id = 29 } } };


            mockPostulacionRepo.Setup(o => o.GetPostulacionesByPublicacion(It.IsAny<int>())).Returns(new List<Postulacion> { postualcion1, postualcion2 });


            List<Donacion> donaciones = new List<Donacion> { new Donacion { id = 58, monto = 10 }, new Donacion { id = 11, monto = 300 } };
            mockDonacionRepo.Setup(o => o.GetByIdPublicacion(5)).Returns(donaciones);


            Publicacion publicacion = new Publicacion { id = 5 };
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(5)).Returns(publicacion);

            Caja caja = new Caja { monto = 0 };
            mockCajaRepo.Setup(o => o.GetCaja()).Returns(caja);
            mockPostulacionRepo.Setup(o => o.GetEnVotacionByIdPublicacion(It.IsAny<int>())).Returns(new List<Postulacion>());

            var result = pc.ActualizarEstadoPostulaciones(5);



            //verificamos el cambio de datos cuando el monto de donaciones es mayor o igual al monto asignado



            Assert.AreEqual(1, postualcion1.estado);
            Assert.AreEqual(0, caja.monto);
            Assert.AreEqual(320, publicacion.montoAsignado);
            Assert.AreEqual(2, publicacion.estado);
        }
        [Test]
        public void ActualizarEstadoPostulacionesCase20()
        {

            //teniendo la misma cantidad de likes , ahora ejiremos por fecha
            Postulacion postualcion1 = new Postulacion { id = 5, fechaCreacion = new DateTime(2021, 4, 20), monto = 900.01m, likesPostulacion = new List<LikePostulacion>() };
            Postulacion postualcion2 = new Postulacion { id = 53, fechaCreacion = new DateTime(2021, 4, 23), monto = 1120.80m, likesPostulacion = new List<LikePostulacion> { new LikePostulacion { id = 29 } } };


            mockPostulacionRepo.Setup(o => o.GetPostulacionesByPublicacion(It.IsAny<int>())).Returns(new List<Postulacion> { postualcion1, postualcion2 });


            List<Donacion> donaciones = new List<Donacion> { new Donacion { id = 58, monto = 300 }, new Donacion { id = 11, monto = 300 } };
            mockDonacionRepo.Setup(o => o.GetByIdPublicacion(5)).Returns(donaciones);


            Publicacion publicacion = new Publicacion { id = 5 };
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(5)).Returns(publicacion);

            Caja caja = new Caja { monto = 0 };
            mockCajaRepo.Setup(o => o.GetCaja()).Returns(caja);
            mockPostulacionRepo.Setup(o => o.GetEnVotacionByIdPublicacion(It.IsAny<int>())).Returns(new List<Postulacion>());

            var result = pc.ActualizarEstadoPostulaciones(5);



            //verificamos el cambio de datos cuando el monto de donaciones es mayor o igual al monto asignado
            Assert.AreEqual(1, postualcion2.estado);
            Assert.AreEqual(0, caja.monto);
            Assert.AreEqual(1120.80, publicacion.montoAsignado);
            Assert.AreEqual(2, publicacion.estado);
        }
        [Test]
        public void ActualizarEstadoPostulacionesCase21()
        {

            //teniendo la misma cantidad de likes , ahora ejiremos por fecha
            Postulacion postualcion1 = new Postulacion { id = 5, fechaCreacion = new DateTime(2021, 4, 20), monto = 4.20m, likesPostulacion = new List<LikePostulacion>() };
            Postulacion postualcion2 = new Postulacion { id = 53, fechaCreacion = new DateTime(2021, 4, 23), monto = 6.50m, likesPostulacion = new List<LikePostulacion> { new LikePostulacion { id = 29 } } };


            mockPostulacionRepo.Setup(o => o.GetPostulacionesByPublicacion(It.IsAny<int>())).Returns(new List<Postulacion> { postualcion1, postualcion2 });


            List<Donacion> donaciones = new List<Donacion> { new Donacion { id = 58, monto = 0.5 }, new Donacion { id = 11, monto = 1 } };
            mockDonacionRepo.Setup(o => o.GetByIdPublicacion(5)).Returns(donaciones);


            Publicacion publicacion = new Publicacion { id = 5 };
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(5)).Returns(publicacion);

            Caja caja = new Caja { monto = 0 };
            mockCajaRepo.Setup(o => o.GetCaja()).Returns(caja);
            mockPostulacionRepo.Setup(o => o.GetEnVotacionByIdPublicacion(It.IsAny<int>())).Returns(new List<Postulacion>());

            var result = pc.ActualizarEstadoPostulaciones(5);



            //verificamos el cambio de datos cuando el monto de donaciones es mayor o igual al monto asignado

            Assert.AreEqual(1, postualcion2.estado);
            Assert.AreEqual(0, caja.monto);
            Assert.AreEqual(6.50, publicacion.montoAsignado);
            Assert.AreEqual(2, publicacion.estado);
        }
        [Test]
        public void ActualizarEstadoPostulacionesCase22()
        {


            Postulacion postualcion1 = new Postulacion { id = 5, fechaCreacion = new DateTime(2021, 4, 20), monto = 4.20m, likesPostulacion = new List<LikePostulacion>() };


            mockPostulacionRepo.Setup(o => o.GetPostulacionesByPublicacion(It.IsAny<int>())).Returns(new List<Postulacion> { postualcion1 });


            List<Donacion> donaciones = new List<Donacion> { new Donacion { id = 58, monto = 4.10 }, new Donacion { id = 11, monto = 0.05 } };
            mockDonacionRepo.Setup(o => o.GetByIdPublicacion(5)).Returns(donaciones);


            Publicacion publicacion = new Publicacion { id = 5 };
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(5)).Returns(publicacion);

            Caja caja = new Caja { monto = 0 };
            mockCajaRepo.Setup(o => o.GetCaja()).Returns(caja);
            mockPostulacionRepo.Setup(o => o.GetEnVotacionByIdPublicacion(It.IsAny<int>())).Returns(new List<Postulacion>());

            var result = pc.ActualizarEstadoPostulaciones(5);



            //verificamos el cambio de datos cuando el monto de donaciones es mayor o igual al monto asignado

            Assert.AreEqual(1, postualcion1.estado);
            Assert.AreEqual(0, caja.monto);
            Assert.AreEqual(4.20, publicacion.montoAsignado);
            Assert.AreEqual(2, publicacion.estado);
        }

        [Test]
        public void ActualizarEstadoPostulacionesCase23()
        {


            Postulacion postualcion1 = new Postulacion { id = 5, fechaCreacion = new DateTime(2021, 4, 20), monto = 4.20m, likesPostulacion = new List<LikePostulacion>() };
            Postulacion postualcion2 = new Postulacion { id = 53, fechaCreacion = new DateTime(2021, 4, 23), monto = 6.50m, likesPostulacion = new List<LikePostulacion> { new LikePostulacion { id = 29 } } };
            Postulacion postualcion3 = new Postulacion { id = 8, fechaCreacion = new DateTime(2021, 4, 20), monto = 4.20m, likesPostulacion = new List<LikePostulacion>() };
            Postulacion postualcion4 = new Postulacion { id = 7, fechaCreacion = new DateTime(2021, 4, 20), monto = 4.20m, likesPostulacion = new List<LikePostulacion>() };

            mockPostulacionRepo.Setup(o => o.GetPostulacionesByPublicacion(It.IsAny<int>())).Returns(new List<Postulacion> { postualcion1 });


            List<Donacion> donaciones = new List<Donacion> { new Donacion { id = 58, monto = 4.10 }, new Donacion { id = 11, monto = 0.05 } };
            mockDonacionRepo.Setup(o => o.GetByIdPublicacion(5)).Returns(donaciones);


            Publicacion publicacion = new Publicacion { id = 5 };
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(5)).Returns(publicacion);

            Caja caja = new Caja { monto = 0 };
            mockCajaRepo.Setup(o => o.GetCaja()).Returns(caja);
        
            mockPostulacionRepo.Setup(o => o.GetEnVotacionByIdPublicacion(5)).Returns(new List<Postulacion> { postualcion1, postualcion3, postualcion4 });
            var result = pc.ActualizarEstadoPostulaciones(5);



            //verificamos el cambio de datos cuando el monto de donaciones es mayor o igual al monto asignado

            Assert.AreEqual(3, result.Count);
        }
        [Test]
        public void ActualizarEstadoPostulacionesCase24()
        {


            Postulacion postualcion1 = new Postulacion { id = 5, fechaCreacion = new DateTime(2021, 4, 20), monto = 4.20m, likesPostulacion = new List<LikePostulacion>() };
            Postulacion postualcion2 = new Postulacion { id = 53, fechaCreacion = new DateTime(2021, 4, 23), monto = 6.50m, likesPostulacion = new List<LikePostulacion> { new LikePostulacion { id = 29 } } };
            Postulacion postualcion3 = new Postulacion { id = 8, fechaCreacion = new DateTime(2021, 4, 20), monto = 4.20m, likesPostulacion = new List<LikePostulacion>() };
            Postulacion postualcion4 = new Postulacion { id = 7, fechaCreacion = new DateTime(2021, 4, 20), monto = 4.20m, likesPostulacion = new List<LikePostulacion>() };
            Postulacion postualcion5 = new Postulacion { id = 9, fechaCreacion = new DateTime(2021, 4, 20), monto = 4.20m, likesPostulacion = new List<LikePostulacion>() };
            mockPostulacionRepo.Setup(o => o.GetPostulacionesByPublicacion(It.IsAny<int>())).Returns(new List<Postulacion> { postualcion1 });


            List<Donacion> donaciones = new List<Donacion> { new Donacion { id = 58, monto = 4.10 }, new Donacion { id = 11, monto = 0.05 } };
            mockDonacionRepo.Setup(o => o.GetByIdPublicacion(5)).Returns(donaciones);


            Publicacion publicacion = new Publicacion { id = 5 };
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(5)).Returns(publicacion);

            Caja caja = new Caja { monto = 0 };
            mockCajaRepo.Setup(o => o.GetCaja()).Returns(caja);

            List<Postulacion> postulaciones = new List<Postulacion> { postualcion1, postualcion3, postualcion4, postualcion5 };
            mockPostulacionRepo.Setup(o => o.GetEnVotacionByIdPublicacion(5)).Returns(postulaciones);
            var result = pc.ActualizarEstadoPostulaciones(5);



            //verificamos el cambio de datos cuando el monto de donaciones es mayor o igual al monto asignado

            Assert.AreEqual(4, result.Count);
            Assert.AreEqual(postulaciones,result);

        }
        [Test]
        public void ActualizarEstadoPostulacionesCase25()
        {


            Postulacion postualcion1 = new Postulacion { id = 5, fechaCreacion = new DateTime(2021, 4, 20), monto = 4.20m, likesPostulacion = new List<LikePostulacion>() };
            
            mockPostulacionRepo.Setup(o => o.GetPostulacionesByPublicacion(It.IsAny<int>())).Returns(new List<Postulacion> { postualcion1 });


            List<Donacion> donaciones = new List<Donacion> { new Donacion { id = 58, monto = 4.10 }, new Donacion { id = 11, monto = 0.05 } };
            mockDonacionRepo.Setup(o => o.GetByIdPublicacion(5)).Returns(donaciones);


            Publicacion publicacion = new Publicacion { id = 5 };
            mockPublicacionRepo.Setup(o => o.GetPublicacionById(5)).Returns(publicacion);

            Caja caja = new Caja { monto = 0 };
            mockCajaRepo.Setup(o => o.GetCaja()).Returns(caja);

       
            mockPostulacionRepo.Setup(o => o.GetEnVotacionByIdPublicacion(5)).Returns(new List<Postulacion>());
            var result = pc.ActualizarEstadoPostulaciones(5);



            //verificamos el cambio de datos cuando el monto de donaciones es mayor o igual al monto asignado

            Assert.AreEqual(0, result.Count);
            

        }
    }
    
}

