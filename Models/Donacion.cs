﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppTUMISMITOPE.Models
{
    public class Donacion
    {
        public int id { get; set; }
        public Double monto { get; set; }
        public string canalPago { get; set; }
        public int idUsuario { get; set; }
        public int idPublicacion { get; set; }
        public DateTime fecha { get; set; }

        public Usuario usuario { get; set; }

    }
}
