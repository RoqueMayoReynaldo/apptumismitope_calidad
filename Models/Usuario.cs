﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppTUMISMITOPE.Models
{
    public class Usuario
    {
        public int id { get; set; }
        public string userName { get; set; }
        public string pass { get; set; }
        public int tipo { get; set; }
        public int estado { get; set; }
        public string correo { get; set; }
        public DateTime fechaCreacion { get; set; }
        public string foto { get; set; }
        public string nombre { get; set; }
        public string apellido { get; set; }
        public DateTime fechaNacimiento { get; set; }
        public string sexo { get; set; }
        public string direccion { get; set; }
        public List<Postulacion> postulaciones { get; set; }

        public PerfilProfesional perfilProfesional { get; set; }

    }
}
