﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppTUMISMITOPE.Models
{
    public class PubModel
    {
        public Publicacion pub { get; set; }
        public Usuario UsuarioActivo { get; set; }
        public DateTime fechaActual { get; set; }
    }
}
