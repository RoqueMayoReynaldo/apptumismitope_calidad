﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppTUMISMITOPE.Models
{
    public class PerfilProfesional
    {
        public int id { get; set; }
        public int dni { get; set; }
        public int telefono { get; set; }
        public string descripcion { get; set; }
        public int reputacion { get; set; }
        public int estado { get; set; }
        public string especialidad { get; set; }
        public string documentacion { get; set; }
        public int idUsuario { get; set; }

    }
}
