﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppTUMISMITOPE.Models
{
    public class ReaccionPublicacion
    {
        public int id { get; set; }
        public int tipo { get; set; }
        public DateTime fechaCreacion { get; set; }
        public int idPublicacion { get; set; }
        public int idUsuario { get; set; }

        public Usuario usuario { get; set; }
    }
}
