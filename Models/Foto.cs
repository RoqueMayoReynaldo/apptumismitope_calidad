﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppTUMISMITOPE.Models
{
    public class Foto
    {
        public int id { get; set; }
        public string contenido { get; set; }
        public string detalle { get; set; }
        public int idPublicacion { get; set; }

    }
}
