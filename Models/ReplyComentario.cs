﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppTUMISMITOPE.Models
{
    public class ReplyComentario
    {
        public int id { get; set; }
        public string contenido { get; set; }
        public DateTime fecha { get; set; }
        public int idUsuario { get; set; }
        public int idComentario { get; set; }

        public Usuario usuario { get; set; }
    }
}
