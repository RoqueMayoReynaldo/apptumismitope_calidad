var $btnComent = $("#sendComent");
$btnComent.click(
    function (e) {
      
        e.preventDefault();
        if ($.trim($("#contentComent").val()) == "" || $("#contentComent").val() == null ) {
            this.abort();
        } 
     var request=   $.ajax({
            url: $("#formComent").attr('action'),
            type: $("#formComent").attr('method'),
            data: {
                idPublicacion: $("#comentLibId").val(),
                idUsuario: $("#comentUsuario").val(),
                contenido: $.trim($("#contentComent").val())
            }
     });
        request.done(
            
            function (com) {
               
                $('#formComent')[0].reset();
                var $listaComentarios = $("#listaComentarios");
                $listaComentarios.append(com);

                var $cantComents = $('#cantidadComents').html();
                var $lenghtText = parseInt($cantComents.length)-11;
                var $num = $cantComents.substring(0,$lenghtText);
                var $nuevoNumero = parseInt($num) + 1;
                
                $cantComents = $nuevoNumero.toString();
                if ($nuevoNumero.toString().length < 2) {

                    $cantComents = "0" + $nuevoNumero.toString();
                }
                
                $('#cantidadComents').html($cantComents + " " +"Comentarios");
                $('#cantidadComentsBox').html($cantComents + " " + "Comentarios");



            }

        );
        request.fail(
            function (com) {
                console.log("fallasteeeeeee");
            }
        );
    }



);


var $reply = $(".reply");
$reply.click(
    function (e) {
        e.preventDefault();
        var $this = $(this);
        var $ident = $this.attr('id');


        var reqForm = $.ajax({
            url: '/Publicacion/FormReply',
            type: 'post',
            data: {
                idComentario:$ident,
                idUsuario: $("#UserActivoId").val()
            }
        }
        )
        reqForm.done(

            function (respuesta) {

               
                var $listaComentarios = $("#listForm-"+$ident);
                $listaComentarios.html(respuesta);

                var $sendReply = $(".sendReply");
                $sendReply.click(

                    function (e) {

                        e.preventDefault();
                    
                        var $this = $(this);
                        var $ident = $this.val();

                        if ($.trim($("#replyContent-" + $ident).val()) == "" || $("#replyContent-" + $ident).val() == null) {

                            $(".alert").remove();
                            $("<div id='BadAlertMSG' role='alert' class='alert alert-danger'> <strong> Fatal! </strong> No se envio,revise su respuesta.</div >").appendTo($("#listForm-" + $ident));
                            this.abort();
                        } 
                        var reqReply = $.ajax(
                            {
                                url: '/Publicacion/SendReply',
                                type: 'post',
                                data: {
                                    idComentario: $('#replyComent-' + $ident).val(),
                                    idUsuario: $("#replyUsuario-" + $ident).val(),
                                    contenido: $("#replyContent-" + $ident).val()
                                }
                            })
                        reqReply.done(
                            function (respuesta) {
                                var $listR = $("#listForm-" + $ident);
                                $listR.before(respuesta);
                                $('#formReply-' + $ident)[0].reset();

                                $(".alert").remove();
                                $("<div id='OkAlertMSG' role='alert' class='alert alert-success'> <strong> Exito! </strong> Se envio tu respuesta.</div >").appendTo($listR);

         
                            }
                        );
                        reqReply.fail(
                            function () {
                               

                                console.log("fallasteeeeeee");
                            }
                        );
                    }
                );
                var $sendReply = $(".cancelReply");
                $sendReply.click(
                    function (e) {
                        e.preventDefault();
                        var $cancelReply = $(this);
                        var $ident = $cancelReply.val();
                        $("#fReply-"+$ident).remove();
                        $(".alert").remove();
                    }



                );

            }

        );
        reqForm.fail(
            function () {
                console.log("fallasteeeeeee");
            }
        );
    }
);


var $btnTodos = $("#linkTodos");

$btnTodos.click(

    function (e) {
       

     /*  alert("hiciste click");*/

        var request = $.ajax(
            {

                url: '/Publicacion/TodasPublicaciones',
                type:'get'


            }

        )
        

        request.done(

            function (respuesta) {


                var $seccionPubs = $("#seccionPublicaciones");
            
              
                           
                $seccionPubs.html(respuesta);

           

               
               



            }



        );


    }






);



var $linkCulminados = $("#linkCulminados");

$linkCulminados.click(

    function (e) {
        e.preventDefault();

        /*  alert("hiciste click");*/

        var request = $.ajax(
            {

                url: '/Publicacion/PubsCulminadas',
                type: 'get'


            }

        )


        request.done(

            function (respuesta) {


                var $seccionPubs = $("#seccionPublicaciones");
                $seccionPubs.html(respuesta);

              

            }



        );


    }






);





var $linkPostulaciones = $("#linkPostulaciones");

$linkPostulaciones.click(

    function (e) {
        e.preventDefault();

         

        var request = $.ajax(
            {

                url: '/Publicacion/MisPostulaciones',
                type: 'get'


            }

        )


        request.done(

            function (respuesta) {


                var $seccionPubs = $("#seccionPublicaciones");
                $seccionPubs.html(respuesta);



            }



        );


    }

);





var $linkPostulaciones = $("#linkAsignados");

$linkPostulaciones.click(

    function (e) {
        e.preventDefault();

       

        var request = $.ajax(
            {

                url: '/Publicacion/MisAsignados',
                type: 'get'


            }

        )


        request.done(

            function (respuesta) {


                var $seccionPubs = $("#seccionPublicaciones");
    
                $seccionPubs.html(respuesta);



            }



        );


    }

);


