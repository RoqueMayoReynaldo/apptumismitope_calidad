var slideIndex = 1;
showSlides(slideIndex);

// Next/previous controls
function plusSlides-galleria(n) {
    showSlides(slideIndex += n);
}

// Thumbnail image controls
function currentSlide(n) {
    showSlides(slideIndex = n);
}

function showSlides(n) {
    var i;
    var slides = document.getElementsByClassName("mySlides-galleria");
    var dots = document.getElementsByClassName("demo-galleria");
    var captionText = document.getElementById("caption-galleria");
    if (n > slides.length) { slideIndex = 1 }
    if (n < 1) { slideIndex = slides.length }
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active-galleria", "");
    }
    slides[slideIndex - 1].style.display = "block";
    dots[slideIndex - 1].className += " active-galleria";
    captionText.innerHTML = dots[slideIndex - 1].alt;
}
