using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using AppTUMISMITOPE.DB;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection.Extensions;
using AppTUMISMITOPE.Authentication;
using AppTUMISMITOPE.Repository;
using AppTUMISMITOPE.FilesService;
using AppTUMISMITOPE.DateServices;

namespace AppTUMISMITOPE
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
        
            services.AddDbContext<AppTumismitoPeContext>(o => o.UseSqlServer(Configuration.GetConnectionString("DevConnection")));

            //configurando el uso de cookies
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme).AddCookie(o=>o.LoginPath="/Welcome/SingIn");

            services.AddTransient<IAutenticacion, Autenticacion>();
            services.AddTransient<IUsuarioRepository, UsuarioRepository>();
            services.AddTransient<IPublicacionRepository, PublicacionRepository>();
            services.AddTransient<IFotoRepository, FotoRepository>();
            services.AddTransient<IManageFiles, ManageFiles>();

            services.AddTransient<ICajaRepository, CajaRepository>();
            services.AddTransient<IComentarioRepository, ComentarioRepository>();
            services.AddTransient<IDonacionRepository, DonacionRepository>();
            services.AddTransient<IEvidenciaRepository, EvidenciaRepository>();
            services.AddTransient<IFotoEvidenciaRepository, FotoEvidenciaRepository>();
            services.AddTransient<IPostulacionRepository, PostulacionRepository>();
            services.AddTransient<ITarjetaRepository, TarjetaRepository>();
            services.AddTransient<IReplyComentarioRepository, ReplyComentarioRepository>();

            services.AddTransient<IDateMethods, DateMethods>();

            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

           

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
