﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AppTUMISMITOPE.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AppTUMISMITOPE.DB.Mapping
{
    public class EvidenciaMap : IEntityTypeConfiguration<Evidencia>

    {
        public void Configure(EntityTypeBuilder<Evidencia> builder)
        {


            builder.ToTable("Evidencia", "dbo");
            builder.HasKey(Evidencia => Evidencia.id);
        
            builder.HasMany(Evidencia=>Evidencia.fotoEvidencias).WithOne().HasForeignKey(o=>o.idEvidencia);

        }
    }
}
