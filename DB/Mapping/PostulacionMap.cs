﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using AppTUMISMITOPE.Models;

namespace AppTUMISMITOPE.DB.Mapping
{
    public class PostulacionMap : IEntityTypeConfiguration<Postulacion>

    {
        public void Configure(EntityTypeBuilder<Postulacion> builder)
        {


            builder.ToTable("Postulacion", "dbo");
            builder.HasKey(Postulacion => Postulacion.id);
            builder.HasOne(Postulacion=>Postulacion.usuario).WithMany(Usuario=>Usuario.postulaciones).HasForeignKey(Postulacion=>Postulacion.idUsuario);
            builder.HasMany(Postulacion=>Postulacion.likesPostulacion).WithOne().HasForeignKey(o=>o.idPostulacion);

            builder.HasOne(Postulacion => Postulacion.evidencia).WithOne().HasForeignKey<Evidencia>(o=>o.idPostulacion);
    

        }
    }
}
