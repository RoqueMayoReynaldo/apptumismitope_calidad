﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AppTUMISMITOPE.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AppTUMISMITOPE.DB.Mapping
{
    public class FotoMap : IEntityTypeConfiguration<Foto>

    {
        public void Configure(EntityTypeBuilder<Foto> builder)
        {


            builder.ToTable("Foto", "dbo");
            builder.HasKey(Foto => Foto.id);
  

        }
    }
}
