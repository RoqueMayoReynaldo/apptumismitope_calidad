﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using AppTUMISMITOPE.Models;

namespace AppTUMISMITOPE.DB.Mapping
{
    public class ComentarioMap : IEntityTypeConfiguration<Comentario>

    {
        public void Configure(EntityTypeBuilder<Comentario> builder)
        {


            builder.ToTable("Comentario", "dbo");
            builder.HasKey(Comentario => Comentario.id);
            builder.HasOne(Comentario=>Comentario.usuario).WithMany().HasForeignKey(o=>o.idUsuario);
            builder.HasMany(Comentario=>Comentario.replyComentarios).WithOne().HasForeignKey(ReplyComentario=>ReplyComentario.idComentario);
            //builder.HasMany(Libro => Libro.bookAutores).WithOne().HasForeignKey(o => o.idBook);

        }
    }
}
