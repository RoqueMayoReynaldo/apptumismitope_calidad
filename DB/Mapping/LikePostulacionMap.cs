﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AppTUMISMITOPE.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AppTUMISMITOPE.DB.Mapping
{
    public class LikePostulacionMap : IEntityTypeConfiguration<LikePostulacion>

    {
        public void Configure(EntityTypeBuilder<LikePostulacion> builder)
        {

            builder.ToTable("LikePostulacion", "dbo");
            builder.HasKey(LikePostulacion => LikePostulacion.id);


        }
    }
}
