﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using AppTUMISMITOPE.Models;

namespace AppTUMISMITOPE.DB.Mapping
{
    public class ReaccionPublicacionMap : IEntityTypeConfiguration<ReaccionPublicacion>

    {
        public void Configure(EntityTypeBuilder<ReaccionPublicacion> builder)
        {


            builder.ToTable("ReaccionPublicacion", "dbo");
            builder.HasKey(ReaccionPublicacion => ReaccionPublicacion.id);
            builder.HasOne(ReaccionPublicacion=>ReaccionPublicacion.usuario).WithMany().HasForeignKey(ReaccionPublicacion=>ReaccionPublicacion.idUsuario);
            //builder.HasMany(Libro => Libro.bookAutores).WithOne().HasForeignKey(o => o.idBook);

        }
    }
}
