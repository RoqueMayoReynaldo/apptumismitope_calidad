﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using AppTUMISMITOPE.Models;

namespace AppTUMISMITOPE.DB.Mapping
{
    public class DonacionMap : IEntityTypeConfiguration<Donacion>

    {
        public void Configure(EntityTypeBuilder<Donacion> builder)
        {


            builder.ToTable("Donacion", "dbo");
            builder.HasKey(Donacion => Donacion.id);
            builder.HasOne(Donacion=> Donacion.usuario).WithMany().HasForeignKey(Donacion=> Donacion.idUsuario);
            //builder.HasMany(Libro => Libro.bookAutores).WithOne().HasForeignKey(o => o.idBook);

        }
    }
}
