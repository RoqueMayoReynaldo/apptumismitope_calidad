﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using AppTUMISMITOPE.Models;

namespace AppTUMISMITOPE.DB.Mapping
{
    public class ReplyComentarioMap : IEntityTypeConfiguration<ReplyComentario>

    {
        public void Configure(EntityTypeBuilder<ReplyComentario> builder)
        {


            builder.ToTable("ReplyComentario", "dbo");
            builder.HasKey(ReplyComentario => ReplyComentario.id);
            builder.HasOne(ReplyComentario => ReplyComentario.usuario).WithMany().HasForeignKey(ReplyComentario => ReplyComentario.idUsuario);
            //builder.HasMany(Libro => Libro.bookAutores).WithOne().HasForeignKey(o => o.idBook);

        }
    }
}
