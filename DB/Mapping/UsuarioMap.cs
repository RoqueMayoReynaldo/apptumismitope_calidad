﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using AppTUMISMITOPE.Models;
namespace AppTUMISMITOPE.DB.Mapping
{
    public class UsuarioMap : IEntityTypeConfiguration<Usuario>

    {
        public void Configure(EntityTypeBuilder<Usuario> builder)
        {


            builder.ToTable("Usuario", "dbo");
            builder.HasKey(Usuario => Usuario.id);
            builder.HasOne(Usuario => Usuario.perfilProfesional).WithOne().HasForeignKey<PerfilProfesional>(o=>o.idUsuario);
            

        }
    }
}
