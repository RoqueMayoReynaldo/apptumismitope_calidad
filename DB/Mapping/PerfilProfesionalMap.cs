﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using AppTUMISMITOPE.Models;

namespace AppTUMISMITOPE.DB.Mapping
{
    public class PerfilProfesionalMap : IEntityTypeConfiguration<PerfilProfesional>

    {
        public void Configure(EntityTypeBuilder<PerfilProfesional> builder)
        {


            builder.ToTable("PerfilProfesional", "dbo");
            builder.HasKey(PerfilProfesional => PerfilProfesional.id);
            //builder.HasMany(Libro => Libro.bookAutores).WithOne().HasForeignKey(o => o.idBook);

        }
    }
}
