﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using AppTUMISMITOPE.Models;
namespace AppTUMISMITOPE.DB.Mapping
{
    public class PublicacionMap : IEntityTypeConfiguration<Publicacion>
    {
        public void Configure(EntityTypeBuilder<Publicacion> builder)
        {


            builder.ToTable("Publicacion", "dbo");
            builder.HasKey(Publicacion => Publicacion.id);
            builder.HasOne(Publicacion=>Publicacion.usuario).WithMany().HasForeignKey(o=>o.idUsuario);
            builder.HasMany(Publicacion=>Publicacion.postulaciones).WithOne(Postulacion=>Postulacion.publicacion).HasForeignKey(o=>o.idPublicacion);
            builder.HasMany(Publicacion => Publicacion.donaciones).WithOne().HasForeignKey(o=>o.idPublicacion);
            builder.HasMany(Publicacion=>Publicacion.reacciones).WithOne().HasForeignKey(o=>o.idPublicacion);
            builder.HasMany(Publicacion=>Publicacion.comentarios).WithOne().HasForeignKey(o=>o.idPublicacion);
            builder.HasMany(Publicacion => Publicacion.fotos).WithOne().HasForeignKey(o => o.idPublicacion);

        }

    }
}
