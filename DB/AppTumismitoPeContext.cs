﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using AppTUMISMITOPE.Models;
using AppTUMISMITOPE.DB.Mapping;
namespace AppTUMISMITOPE.DB

{
    public class AppTumismitoPeContext:DbContext
    {
        public virtual DbSet<Comentario> Comentarios { get; set; }
        public virtual DbSet<Donacion> Donaciones { get; set; }
        public virtual DbSet<PerfilProfesional> PerfilProfesionales { get; set; }
        public virtual DbSet<Postulacion> Postulaciones { get; set; }
        public virtual DbSet<Publicacion> Publicaciones { get; set; }
        public virtual DbSet<ReaccionPublicacion> ReaccionPublicaciones { get; set; }
        public virtual DbSet<Usuario> Usuarios { get; set; }
        public virtual DbSet<Foto> Fotos { get; set; }
        public virtual DbSet<ReplyComentario> ReplyComentarios { get; set; }
        public virtual DbSet<Tarjeta> Tarjetas { get; set; }
        public virtual DbSet<Caja> Cajas { get; set; }
        public virtual DbSet<LikePostulacion> LikePostulaciones { get; set; }
           
        public virtual DbSet<Evidencia> Evidencias { get; set; }
        public virtual DbSet<FotoEvidencia> FotoEvidencias { get; set; }
        public  AppTumismitoPeContext(DbContextOptions<AppTumismitoPeContext> options) : base(options) { }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new ComentarioMap());
            modelBuilder.ApplyConfiguration(new DonacionMap());
            modelBuilder.ApplyConfiguration(new PerfilProfesionalMap());
            modelBuilder.ApplyConfiguration(new PostulacionMap());
            modelBuilder.ApplyConfiguration(new PublicacionMap());
            modelBuilder.ApplyConfiguration(new ReaccionPublicacionMap());
            modelBuilder.ApplyConfiguration(new UsuarioMap());
            modelBuilder.ApplyConfiguration(new FotoMap());
            modelBuilder.ApplyConfiguration(new ReplyComentarioMap());
            modelBuilder.ApplyConfiguration(new TarjetaMap());
            modelBuilder.ApplyConfiguration(new LikePostulacionMap());
            modelBuilder.ApplyConfiguration(new CajaMap());

            modelBuilder.ApplyConfiguration(new EvidenciaMap());
            modelBuilder.ApplyConfiguration(new FotoEvidenciaMap());
        }

    }
}
