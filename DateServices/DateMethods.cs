﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppTUMISMITOPE.DateServices
{

    public interface IDateMethods {


        DateTime getCurrentDate();
    }
    public class DateMethods : IDateMethods
    {
        public DateTime getCurrentDate()
        {
            return DateTime.Now;
        }
    }
}
