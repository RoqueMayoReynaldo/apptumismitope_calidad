﻿using AppTUMISMITOPE.DB;
using AppTUMISMITOPE.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppTUMISMITOPE.Repository
{

    public interface IPostulacionRepository
    {
        Postulacion FindByPublicacionUsuario(int idPub,int idUser );
        void Add(Postulacion postulacion);

        List<Postulacion> GetNoElejidos(int id);
        List<Postulacion> GetEnVotacionByIdPublicacion(int id);
        List<Postulacion> GetPostulacionesByPublicacion(int id);

        void DeleteRange(List<Postulacion> postulaciones);


        Postulacion FindById(int id);

        void UpdateChanges();

    }
    


    public class PostulacionRepository : IPostulacionRepository
    {
        private readonly AppTumismitoPeContext context;

        public PostulacionRepository(AppTumismitoPeContext context)
        {
            this.context = context;
        }

        public Postulacion FindById(int id)
        {
            return context.Postulaciones.FirstOrDefault(o => o.id == id); 
        }
        public List<Postulacion> GetPostulacionesByPublicacion(int id)
        {
            return context.Postulaciones.Include("likesPostulacion").Where(o => o.idPublicacion == id).ToList(); ;
        }

 
        public List<Postulacion> GetEnVotacionByIdPublicacion(int id)
        {
            return context.Postulaciones.Include("likesPostulacion").Where(o => o.idPublicacion == id && o.estado == 0).ToList();
        }

        public List<Postulacion> GetNoElejidos(int id)
        {
            return context.Postulaciones.Include("likesPostulacion").Where(o => o.idPublicacion == id && o.estado != 1).ToList();
        }
        public Postulacion FindByPublicacionUsuario(int idPub, int idUser)
        {
            return context.Postulaciones.FirstOrDefault(o => o.idPublicacion == idPub && o.idUsuario == idUser);
        }


      

        public void Add(Postulacion postulacion)
        {
            context.Postulaciones.Add(postulacion);

            context.SaveChanges();
        }

        public void DeleteRange(List<Postulacion> postulaciones)
        {

            context.Postulaciones.RemoveRange(postulaciones);
            context.SaveChanges();
        }

        public void UpdateChanges()
        {
            context.SaveChanges();
        }
    }
}
