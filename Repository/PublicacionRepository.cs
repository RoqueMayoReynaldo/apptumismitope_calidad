﻿using AppTUMISMITOPE.DB;
using AppTUMISMITOPE.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppTUMISMITOPE.Repository
{
    public interface IPublicacionRepository
    {
        List<Publicacion> GetAllPublicaciones();
        List<Publicacion> GetPublicacionesSinCulminar();
        List<Publicacion> GetCulminados();
        Publicacion GetPublicacionById(int id);
      
        void AddAndSavePublicacion(Publicacion publicacion);
        void UpdateChanges();
      
    }

    public class PublicacionRepository : IPublicacionRepository
    {
        private readonly AppTumismitoPeContext context;

        public PublicacionRepository(AppTumismitoPeContext context )
        {
            this.context = context;
        }

 
        public void AddAndSavePublicacion(Publicacion publicacion)
        {
            context.Publicaciones.Add(publicacion);

            context.SaveChanges();
        }

        public List<Publicacion> GetAllPublicaciones()
        {
            return context.Publicaciones.Include("usuario").Include("fotos").Include("donaciones").Include("reacciones.usuario").
                Include("comentarios.usuario").Include("postulaciones.usuario.perfilProfesional").OrderByDescending(Publicacion => Publicacion.fechaCreacion).
                Where(o => o.estado == 0 || o.estado == 3).ToList();
        }

        public List<Publicacion> GetCulminados()
        {
            return context.Publicaciones.Include("usuario").Include("fotos").Include("donaciones").Include("reacciones.usuario").
                Include("comentarios.usuario").Include("postulaciones.usuario.perfilProfesional").Include("postulaciones.evidencia.fotoEvidencias").
                OrderByDescending(Publicacion => Publicacion.fechaCreacion).Where(o => o.estado == 1).ToList();
        }

        public Publicacion GetPublicacionById(int id)
        {
            return context.Publicaciones.Include("usuario").Include("fotos").Include("donaciones.usuario").Include("reacciones.usuario").
                Include("comentarios.usuario").Include("comentarios.replyComentarios.usuario").Include("postulaciones.usuario.perfilProfesional").Include("postulaciones.likesPostulacion").
                FirstOrDefault(Publicacion => Publicacion.id == id);

        }

        public List<Publicacion> GetPublicacionesSinCulminar()
        {
            return context.Publicaciones.Include("usuario").Include("fotos").Include("donaciones")
                .Include("reacciones.usuario").Include("comentarios.usuario").Include("postulaciones.usuario.perfilProfesional")
                .OrderByDescending(Publicacion => Publicacion.fechaCreacion)
                .Where(o => o.estado == 0 || o.estado == 3 || o.estado == 2).ToList();
        }

     

        public void UpdateChanges()
        {
            context.SaveChanges();
        }
    }
}
