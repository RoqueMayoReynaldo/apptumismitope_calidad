﻿using AppTUMISMITOPE.DB;
using AppTUMISMITOPE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppTUMISMITOPE.Repository
{
    public interface IFotoEvidenciaRepository
    {
        void Add(FotoEvidencia fotoEvidencia);

    }


    public class FotoEvidenciaRepository : IFotoEvidenciaRepository
    {

     

        private readonly AppTumismitoPeContext context;

        public FotoEvidenciaRepository(AppTumismitoPeContext context)
        {
            this.context = context;



        }

        public void Add(FotoEvidencia fotoEvidencia)
        {
            context.FotoEvidencias.Add(fotoEvidencia);
            context.SaveChanges();
        }
    }
}
