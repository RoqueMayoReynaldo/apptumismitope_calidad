﻿using AppTUMISMITOPE.DB;
using AppTUMISMITOPE.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppTUMISMITOPE.Repository
{
    public interface IReplyComentarioRepository
    {
        void AddReply(ReplyComentario replyComentario );
        ReplyComentario FindById(int id);
    }

    public class ReplyComentarioRepository : IReplyComentarioRepository
    {
        private readonly AppTumismitoPeContext context;

        public ReplyComentarioRepository(AppTumismitoPeContext context)
        {
            this.context = context;
        }

        public void AddReply(ReplyComentario replyComentario)
        {
            context.ReplyComentarios.Add(replyComentario);
            context.SaveChanges();
        }

        public ReplyComentario FindById(int id)
        {
            return context.ReplyComentarios.Include("usuario").FirstOrDefault(o => o.id == id);
        }
    }
}
