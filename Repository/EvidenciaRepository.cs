﻿using AppTUMISMITOPE.DB;
using AppTUMISMITOPE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppTUMISMITOPE.Repository
{

    public interface IEvidenciaRepository
    {

        void Add(Evidencia evidencia);
    }
    public class EvidenciaRepository : IEvidenciaRepository
    {
        private readonly AppTumismitoPeContext context;

        public EvidenciaRepository(AppTumismitoPeContext context)
        {
            this.context = context;
        }

        public void Add(Evidencia evidencia)
        {
            context.Evidencias.Add(evidencia);
            context.SaveChanges();
        }
    }
}
