﻿using AppTUMISMITOPE.DB;
using AppTUMISMITOPE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppTUMISMITOPE.Repository
{
    public interface IFotoRepository
    {

        void AddAndSaveFoto(Foto foto);

    }

    public class FotoRepository : IFotoRepository
    {
        private readonly AppTumismitoPeContext context;

        public FotoRepository(AppTumismitoPeContext context )
        {
            this.context = context;
        }

        public void AddAndSaveFoto(Foto foto)
        {
            context.Fotos.Add(foto);
            context.SaveChanges();
        }
    }
}
