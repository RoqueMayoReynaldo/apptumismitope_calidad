﻿using AppTUMISMITOPE.DB;
using AppTUMISMITOPE.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppTUMISMITOPE.Repository
{
    public interface IUsuarioRepository
    {
        Usuario FindById(int id);
        Usuario FindByCredentials(String nombreUsuario,String password);
        Usuario FindByUserName(String userName);
        Usuario GetWithPostulacionessByUsername(String userName);
    
    }

    public class UsuarioRepository : IUsuarioRepository
    {
        private readonly AppTumismitoPeContext context;

        public UsuarioRepository(AppTumismitoPeContext context)
        {
            this.context = context;
        }


        public Usuario FindByCredentials(string nombreUsuario,string password)
        {
            return context.Usuarios.FirstOrDefault(o=>o.userName==nombreUsuario && o.pass==password);
        }

        public Usuario FindById(int id)
        {
            return context.Usuarios.FirstOrDefault(o => o.id == id);
        }

        public Usuario FindByUserName(string userName)
        {
            return context.Usuarios.FirstOrDefault(o => o.userName == userName);
        }

        public Usuario GetWithPostulacionessByUsername(string userName)
        {
            return context.Usuarios.Include("postulaciones.publicacion").Include("postulaciones.publicacion.usuario").
                Include("postulaciones.publicacion.fotos").Include("postulaciones.publicacion.donaciones").
                Include("postulaciones.publicacion.reacciones.usuario").Include("postulaciones.publicacion.comentarios.usuario").
                Include("postulaciones.publicacion.postulaciones.usuario.perfilProfesional").FirstOrDefault(User => User.userName == userName);
        }
    }
}
