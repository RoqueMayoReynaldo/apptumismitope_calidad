﻿using AppTUMISMITOPE.DB;
using AppTUMISMITOPE.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppTUMISMITOPE.Repository
{
    public interface IComentarioRepository
    {
        Comentario FindById(int id);
        void AddComentario(Comentario comentario);

    }

    public class ComentarioRepository : IComentarioRepository
    {
        private readonly AppTumismitoPeContext context;

        public ComentarioRepository(AppTumismitoPeContext context )
        {
            this.context = context;
        }

        public void AddComentario(Comentario comentario)
        {
            context.Comentarios.Add(comentario);
            context.SaveChanges();
        }

        public Comentario FindById(int id)
        {
            return context.Comentarios.Include("usuario").FirstOrDefault(o => o.id == id);
        }
    }
}
