﻿using AppTUMISMITOPE.DB;
using AppTUMISMITOPE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppTUMISMITOPE.Repository
{
    public interface ITarjetaRepository
    {

        Tarjeta GetTarjetaByModel(Tarjeta t);
        void UpdateChanges();
    }

  
      

    public class TarjetaRepository : ITarjetaRepository
    {
        private readonly AppTumismitoPeContext context;

        public TarjetaRepository(AppTumismitoPeContext context)
        {
            this.context = context;
        }

        public Tarjeta GetTarjetaByModel(Tarjeta t)
        {

            return context.Tarjetas.FirstOrDefault(o => o.numero == t.numero && o.nombre == t.nombre && o.apellido == t.apellido &&
            o.mesVencimiento == t.mesVencimiento && o.anioVencimiento == t.anioVencimiento && o.codigoSeguridad == t.codigoSeguridad);
        }

        public void UpdateChanges()
        {
            context.SaveChanges();
        }
    }
}
