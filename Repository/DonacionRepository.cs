﻿using AppTUMISMITOPE.DB;
using AppTUMISMITOPE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppTUMISMITOPE.Repository
{
    public interface IDonacionRepository
    {

        void Add(Donacion donacion );

        List<Donacion> GetByIdPublicacion(int id);
    }

    public class DonacionRepository : IDonacionRepository
    {
        private readonly AppTumismitoPeContext context;

        public DonacionRepository(AppTumismitoPeContext context)
        {
            this.context = context;
        }

        public void Add(Donacion donacion)
        {
            context.Donaciones.Add(donacion);
            context.SaveChanges();
        }

        public List<Donacion> GetByIdPublicacion(int id)
        {
            return context.Donaciones.Where(o => o.idPublicacion == id).ToList();
        }
    }
}
