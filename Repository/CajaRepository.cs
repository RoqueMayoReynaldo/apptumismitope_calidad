﻿using AppTUMISMITOPE.DB;
using AppTUMISMITOPE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppTUMISMITOPE.Repository
{
    public interface ICajaRepository
    {

        Caja GetCaja(); 
    }

    public class CajaRepository : ICajaRepository
    {
        private readonly AppTumismitoPeContext context;

        public CajaRepository(AppTumismitoPeContext context)
        {
            this.context = context;
        }

        public Caja GetCaja()
        {
            return context.Cajas.FirstOrDefault();
        }
    }
}
