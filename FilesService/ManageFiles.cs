﻿using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AppTUMISMITOPE.FilesService
{
    public interface IManageFiles
    {
        String CombinePaths(string path1,string path2,string path3);
        String CombinePaths(string path1, string path2);

    }
    public class ManageFiles : IManageFiles
    {
        

    
        public string CombinePaths(string path1, string path2, string path3)
        {
            return Path.Combine(path1, path2, path3);
        }

        public string CombinePaths(string path1, string path2)
        {
            return Path.Combine(path1, path2);
        }

    }
}
