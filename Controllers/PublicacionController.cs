﻿using AppTUMISMITOPE.DB;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AppTUMISMITOPE.Models;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using AppTUMISMITOPE.Authentication;
using AppTUMISMITOPE.Repository;
using AppTUMISMITOPE.FilesService;
using AppTUMISMITOPE.DateServices;

namespace AppTUMISMITOPE.Controllers
{


   

    [Authorize]
    public class PublicacionController : Controller
    {
        // GET: PublicacionController

    
        private IWebHostEnvironment hosting;
        private readonly IAutenticacion autenticacion;
        private readonly IUsuarioRepository usuarioRepo;
        private readonly IPublicacionRepository publicacionRepo;
        private readonly IPostulacionRepository postulacionRepo;
        private readonly IComentarioRepository comentarioRepo;
        private readonly IReplyComentarioRepository replyRepo;
        private readonly ITarjetaRepository tarjetaRepo;
        private readonly IDonacionRepository donacionRepo;
        private readonly ICajaRepository cajaRepo;
        private readonly IEvidenciaRepository evidenciaRepo;
        private readonly IFotoEvidenciaRepository fotoEvidenciaRepo;
        private readonly IManageFiles manageFiles;
        private readonly IDateMethods dateMethods;

        public PublicacionController(
            IWebHostEnvironment hosting,
            IAutenticacion autenticacion,
            IUsuarioRepository usuarioRepo,
            IPublicacionRepository publicacionRepo,
            IPostulacionRepository postulacionRepo,
            IComentarioRepository comentarioRepo,
            IReplyComentarioRepository replyRepo,
            ITarjetaRepository tarjetaRepo,
            IDonacionRepository donacionRepo,
            ICajaRepository cajaRepo,
            IEvidenciaRepository evidenciaRepo,
            IFotoEvidenciaRepository fotoEvidenciaRepo, 
            IManageFiles manageFiles,
            IDateMethods dateMethods)
        {
            
            this.hosting= hosting;
            this.autenticacion = autenticacion;
            this.usuarioRepo = usuarioRepo;
            this.publicacionRepo = publicacionRepo;
            this.postulacionRepo = postulacionRepo;
            this.comentarioRepo = comentarioRepo;
            this.replyRepo = replyRepo;
            this.tarjetaRepo = tarjetaRepo;
            this.donacionRepo = donacionRepo;
            this.cajaRepo = cajaRepo;
            this.evidenciaRepo = evidenciaRepo;
            this.fotoEvidenciaRepo = fotoEvidenciaRepo;
            this.manageFiles = manageFiles;
            this.dateMethods = dateMethods;
        }

        [HttpGet]
        public ActionResult Detalle(int id)
        {
            //comentado actualizarPostulaciones();

           
            string conectUser = autenticacion.GetCurrrentUser();

            Usuario us = usuarioRepo.FindByUserName(conectUser);
            PubModel PubModel = new PubModel
            {
                pub = publicacionRepo.GetPublicacionById(id),
                UsuarioActivo = usuarioRepo.FindByUserName(conectUser),
                fechaActual=DateTime.Now
            };
           

           



            PubModel.pub.vista++;
            publicacionRepo.UpdateChanges();

            //verificamos si existe postulacion del usuario
       
            Postulacion postulacion = postulacionRepo.FindByPublicacionUsuario(id,us.id);

            ViewBag.Postulo =  postulacion != null ? true : false;

            return View("Detalle", PubModel);

        }


        [HttpPost]
        public ViewResult Comentar(Comentario comentario)
        {

            // comentado actualizarPostulaciones();

            comentario.fecha = DateTime.Now;
            comentarioRepo.AddComentario(comentario);

           
            Comentario com = comentarioRepo.FindById(comentario.id);

            return View("Comentar",com);

        }


        [HttpGet]
        public ActionResult Postulaciones(int id)
        {
            // comentado actualizarPostulaciones();

            string conectUser = autenticacion.GetCurrrentUser();


            PubModel PubModel = new PubModel
            {
                pub = publicacionRepo.GetPublicacionById(id),
                UsuarioActivo = usuarioRepo.FindByUserName(conectUser)

            };
   
            return View("Postulaciones", PubModel);

        }

        [HttpPost]
        public ViewResult FormReply (int idComentario,int idUsuario)
        {
            //comentado actualizarPostulaciones();

            
                Usuario us = usuarioRepo.FindById(idUsuario);
                ViewBag.idComent = idComentario;
                return View("FormReply", us);
            
           
               
            

   

        }

        [HttpPost]
        public ViewResult SendReply(ReplyComentario replyComentario)
        {
            //comentado actualizarPostulaciones();



            if (replyComentario!=null)
            {
                replyComentario.fecha = dateMethods.getCurrentDate();
                replyRepo.AddReply(replyComentario);

                ReplyComentario rc = replyRepo.FindById(replyComentario.id);

                return View("SendReply", rc);
            }
            else
            {
                throw new Exception("error for parameter null");

            }

 

        }


        [HttpPost]
        public IActionResult setDonacion(Tarjeta t,Donacion donacion)
        {
            //comentado actualizarPostulaciones();


            Tarjeta tj = tarjetaRepo.GetTarjetaByModel(t);

            if (tj==null)
            {
                ModelState.AddModelError("Mensaje","Verifique datos de tarjeta");
            }
            else if (donacion.monto >tj.saldo || tj.saldo==0)
            {
                ModelState.AddModelError("Mensaje", "Saldo insuficiente");
            }

            if (ModelState.IsValid)
            {
                tj.saldo -= donacion.monto;
                tarjetaRepo.UpdateChanges();
                donacion.fecha = DateTime.Now;

                donacionRepo.Add(donacion);

            
                var idPub = new { id = donacion.idPublicacion };
                 
                return RedirectToAction("Detalle", idPub);
            }

            //lo mismo que el metodo detalle

            string conectUser = autenticacion.GetCurrrentUser();

            Usuario us = usuarioRepo.FindByUserName(conectUser);
            PubModel PubModel = new PubModel
            {
                pub = publicacionRepo.GetPublicacionById(donacion.idPublicacion),
                UsuarioActivo = usuarioRepo.FindByUserName(conectUser),
                fechaActual = dateMethods.getCurrentDate()
            };






            PubModel.pub.vista++;
            publicacionRepo.UpdateChanges();

            //verificamos si existe postulacion del usuario

            Postulacion postulacion = postulacionRepo.FindByPublicacionUsuario(donacion.idPublicacion, us.id);

            ViewBag.Postulo = postulacion != null ? true : false;

            return View("Detalle", PubModel);



    

        }



        [HttpPost]
        public IActionResult Postular(Postulacion postulacion,int cantidadPostulaciones)
        {
            //comentado actualizarPostulaciones();

            //[Required(ErrorMessage = "Monto requerido")]
            //public Decimal monto { get; set; }
            //[Required(ErrorMessage = "Dias requerido")]
            //public int diasRealizacion { get; set; }

            //public int estado { get; set; }
            //[Required(ErrorMessage = "Descripcion requerida")]
            //public string descripcion { get; set; }


            if (string.IsNullOrEmpty(postulacion.descripcion))
            {
                ModelState.AddModelError("descripcion", "Descripcion requerida");
            }
            if (postulacion.monto==0)
            {
                ModelState.AddModelError("monto", "Monto requerido");
            }
            if (postulacion.diasRealizacion == 0)
            {
                ModelState.AddModelError("diasRealizacion", "Dias requerido");
            }

            if (ModelState.IsValid)
            {
                postulacion.fechaCreacion = dateMethods.getCurrentDate();
                postulacion.estado = 0;

                postulacionRepo.Add(postulacion);


                if (cantidadPostulaciones==0)
                {
                    Publicacion publicacion = publicacionRepo.GetPublicacionById(postulacion.idPublicacion);

                    publicacion.limitePostulaciones = dateMethods.getCurrentDate().AddDays(7);
            
                    publicacionRepo.UpdateChanges();
                }

          
            }


            //lo mismo que el metodo detalle

            string conectUser = autenticacion.GetCurrrentUser();

            Usuario us = usuarioRepo.FindByUserName(conectUser);
            PubModel PubModel = new PubModel
            {
                pub = publicacionRepo.GetPublicacionById(postulacion.idPublicacion),
                UsuarioActivo = usuarioRepo.FindByUserName(conectUser),
                fechaActual = dateMethods.getCurrentDate()
            };






            PubModel.pub.vista++;
            publicacionRepo.UpdateChanges();

            //verificamos si existe postulacion del usuario

            Postulacion postu = postulacionRepo.FindByPublicacionUsuario(postulacion.idPublicacion, us.id);

            ViewBag.Postulo = postu != null ? true : false;

            return View("Detalle", PubModel);


           

        }


        [HttpGet]
        public ViewResult TodasPublicaciones()
        {
            //comentado actualizarPostulaciones();


            string conectUser = autenticacion.GetCurrrentUser();


            IndexModel indexModel = new IndexModel
            {
                pubs = publicacionRepo.GetAllPublicaciones(),
                UsuarioActivo = usuarioRepo.FindByUserName(conectUser),
                fechaActual = dateMethods.getCurrentDate()
            };

            return View("TodasPublicaciones",indexModel);

        }



        [HttpGet]
        public ViewResult PubsCulminadas()
        {
            //comentado actualizarPostulaciones();


            string conectUser = autenticacion.GetCurrrentUser();


            IndexModel indexModel = new IndexModel
            {
                pubs = publicacionRepo.GetCulminados(),
                UsuarioActivo = usuarioRepo.FindByUserName(conectUser),
                fechaActual = dateMethods.getCurrentDate()
            };
            

            return View("TodasPublicaciones", indexModel);

        }




        [HttpGet]
        public ViewResult MisPostulaciones()
        {

            //comentado actualizarPostulaciones();


            string conectUser = autenticacion.GetCurrrentUser();

            
            PostulacionModel postuModel = new PostulacionModel
            {
                
                UsuarioActivo = usuarioRepo.GetWithPostulacionessByUsername(conectUser),
                fechaActual = dateMethods.getCurrentDate()
            };


            //Console.WriteLine("Tiene postulaciones: "+postuModel.UsuarioActivo.postulaciones.Count());
            return View("MisPostulaciones", postuModel);

        }




        [HttpGet]
        public ViewResult MisAsignados()
        {

            //comentado actualizarPostulaciones();


            string conectUser = autenticacion.GetCurrrentUser();

            PostulacionModel postuModel = new PostulacionModel
            {

                UsuarioActivo = usuarioRepo.GetWithPostulacionessByUsername(conectUser),
                fechaActual = dateMethods.getCurrentDate()
            };


          
            return View("MisAsignados", postuModel);

        }



        

        //used
        public List<Postulacion> ActualizarEstadoPostulaciones(int id)
        {

            List <Postulacion> postulaciones = postulacionRepo.GetPostulacionesByPublicacion(id);

            int maxLikes = postulaciones.Max(o => o.likesPostulacion.Count());


            postulaciones = postulaciones.Where(o => o.likesPostulacion.Count()==maxLikes).ToList();

            DateTime fechaMin = postulaciones.Min(o=>o.fechaCreacion);

            Postulacion postulacion = postulaciones.FirstOrDefault(o => o.fechaCreacion == fechaMin);

            //logica para cambiar de estados segun situacion de publicaion y postulacion
            Publicacion publicacion = publicacionRepo.GetPublicacionById(id);


            List<Donacion> donaciones = donacionRepo.GetByIdPublicacion(id);

            Double sumaDonaciones = donaciones.Sum(o => o.monto);

            Double montoRestante = 0;
            if (sumaDonaciones >= (Double)postulacion.monto)
            {
                montoRestante = sumaDonaciones - (Double)postulacion.monto;

                //ya tiene monto completo y trabajador asignado
                publicacion.estado = 3;
                publicacion.montoAsignado = (Double)postulacion.monto;
                postulacion.estado = 2;
                //envio de dinero restante a caja
                Caja caja = cajaRepo.GetCaja();
                caja.monto += montoRestante;
            }
            if (sumaDonaciones < (Double)postulacion.monto)
            {
                publicacion.estado = 2;
                publicacion.montoAsignado = (Double)postulacion.monto;
                postulacion.estado = 1;

            }

            publicacionRepo.UpdateChanges();

            List<Postulacion> postulacionesRemover = postulacionRepo.GetEnVotacionByIdPublicacion(id);

            //guardar lista
            List<Postulacion> posts = new List<Postulacion>();
            posts.AddRange(postulacionesRemover);

            postulacionRepo.DeleteRange(postulacionesRemover);


            return posts;


        }


        [HttpGet]
        public ViewResult cerrarTrabajo(int id)
        {
            //comentado actualizarPostulaciones();



            string conectUser = autenticacion.GetCurrrentUser();

       
            PubModel PubModel = new PubModel
            {
                pub = publicacionRepo.GetPublicacionById(id),
                UsuarioActivo = usuarioRepo.FindByUserName(conectUser),
                fechaActual = dateMethods.getCurrentDate()
            };

        

           

            return View("cerrarTrabajo", PubModel);


           
        }

        [HttpPost]
        public IActionResult cerrarTrabajo(Evidencia evidencia,List<IFormFile> files) {


            if (files.Count<6)
            {
                ModelState.AddModelError("evidencias","Se requiere almenos 6 fotos");
            }

            if (ModelState.IsValid)
            {
                evidenciaRepo.Add(evidencia);

                Postulacion postulacion = postulacionRepo.FindById(evidencia.idPostulacion);
                postulacion.estado = 3;

                postulacionRepo.UpdateChanges();

                Publicacion publicacion = publicacionRepo.GetPublicacionById(postulacion.idPublicacion);
                publicacion.estado = 1;
                publicacionRepo.UpdateChanges();

                foreach (IFormFile file in files)
                {

                    string relativePath = "";
                   

                    if (file.Length > 0 && (file.ContentType == "image/png" || file.ContentType == "image/jpeg"))
                    {
                        relativePath = manageFiles.CombinePaths("files", "fotosEvidencias", file.FileName);
                        string filePath = manageFiles.CombinePaths(hosting.WebRootPath, relativePath);
                        var stream = new FileStream(filePath, FileMode.Create);
                        file.CopyTo(stream);
                        stream.Close();

                        relativePath= "/" + relativePath.Replace('\\', '/');


                        FotoEvidencia f = new FotoEvidencia()
                        {

                            idEvidencia = evidencia.id,
                            contenido = relativePath.Replace(" ", "%20")




                        };

                        fotoEvidenciaRepo.Add(f);

                    }
                    else
                    {
                        throw new Exception("file with error extension");
                    }

               
                }
                return RedirectToAction("Index","Home");
               
            }

            Postulacion pos = postulacionRepo.FindById(evidencia.idPostulacion);



            //
            string conectUser = autenticacion.GetCurrrentUser();

            PubModel PubModel = new PubModel
            {
                pub = publicacionRepo.GetPublicacionById(pos.idPublicacion),
                UsuarioActivo = usuarioRepo.FindByUserName(conectUser),
                fechaActual = dateMethods.getCurrentDate()
            };





            return View("cerrarTrabajo", PubModel);


        //    return cerrarTrabajo(pos.idPublicacion);
        }

        //public string SaveFile(IFormFile file)
        //{


        //    string relativePath = "";

        //    if (file.Length > 0 && (file.ContentType == "image/png" || file.ContentType == "image/jpeg"))
        //    {
        //        relativePath = manageFiles.CombinePaths("files", "fotosEvidencias", file.FileName);
        //        string filePath = manageFiles.CombinePaths(hosting.WebRootPath, relativePath);
        //        var stream = new FileStream(filePath, FileMode.Create);
        //        file.CopyTo(stream);
        //        stream.Close();
        //        return "/" + relativePath.Replace('\\', '/');
        //    }
        //    else
        //    {
        //        throw new Exception("file with error extension");
        //    }

        //}


     

        // comentado actualizar

        
        //public void actualizarPostulaciones()
        //{




        //    List<Publicacion> publicaciones = context.Publicaciones.Where(o => o.estado == 0).ToList();

        //    foreach (Publicacion pub in publicaciones)
        //    {



        //        DateTime fechaActual = DateTime.Now;
        //        if (fechaActual >= pub.limitePostulaciones)
        //        {


        //            List<Postulacion> postulaciones = context.Postulaciones.Include("likesPostulacion").Where(o => o.idPublicacion == pub.id).ToList();

        //            int maxLikes = postulaciones.Max(o => o.likesPostulacion.Count());



        //            postulaciones = postulaciones.Where(o => o.likesPostulacion.Count() == maxLikes).ToList();

        //            DateTime fechaMin = postulaciones.Min(o => o.fechaCreacion);

        //            Postulacion postulacion = postulaciones.FirstOrDefault(o => o.fechaCreacion == fechaMin);

        //            //logica para cambiar de estados segun situacion de publicaion y postulacion
        //            Publicacion publicacion = context.Publicaciones.FirstOrDefault(o => o.id == pub.id);


        //            List<Donacion> donaciones = context.Donaciones.Where(o => o.idPublicacion == pub.id).ToList();
        //            Double sumaDonaciones = donaciones.Sum(o => o.monto);

        //            Double montoRestante = 0;
        //            if (sumaDonaciones >= (Double)postulacion.monto)
        //            {
        //                montoRestante = sumaDonaciones - (Double)postulacion.monto;

        //                //ya tiene monto completo y trabajador asignado
        //                publicacion.estado = 3;
        //                publicacion.montoAsignado = (Double)postulacion.monto;
        //                postulacion.estado = 2;
        //                //envio de dinero restante a caja
        //                Caja caja = context.Cajas.FirstOrDefault();
        //                caja.monto += montoRestante;
        //            }
        //            if (sumaDonaciones < (Double)postulacion.monto)
        //            {
        //                publicacion.estado = 2;
        //                publicacion.montoAsignado = (Double)postulacion.monto;
        //                postulacion.estado = 1;

        //            }


        //            context.SaveChanges();

        //            List<Postulacion> postulacionesRemover = context.Postulaciones.Include("likesPostulacion").Where(o => o.idPublicacion == pub.id && o.estado == 0).ToList();

        //            //guardar lista
        //            List<Postulacion> posts = new List<Postulacion>();
        //            posts.AddRange(postulacionesRemover);

        //            context.Postulaciones.RemoveRange(postulacionesRemover);

        //            context.SaveChanges();


        //        }






        //    }


        //}


    }
}
