﻿using AppTUMISMITOPE.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AppTUMISMITOPE.DB;
using System.Security.Cryptography;
using System.Text;
using Microsoft.Extensions.Configuration;
using AppTUMISMITOPE.Repository;
using AppTUMISMITOPE.Authentication;

namespace AppTUMISMITOPE.Controllers
{
    public class WelcomeController : Controller
    {
        //private AppTumismitoPeContext context;
        //private IConfiguration configuration;

        private readonly IUsuarioRepository usuarioRepo;
        private readonly IAutenticacion autenticacion;

        //Inyeactamos ICONFIGURATION para poder usar el valor de la llave para la contraseña alojada en appsetings.json
        //public WelcomeController(AppTumismitoPeContext context,IConfiguration configuration)
        //{
        //    this.context = context;
        //    this.configuration = configuration;
        //}


        public WelcomeController(IUsuarioRepository usuarioRepo,IAutenticacion autenticacion)
        {
            this.usuarioRepo = usuarioRepo;
            this.autenticacion = autenticacion;
        }



        [HttpGet]
        public IActionResult SingIn()
        {
            return View("SingIn");
        }

        [HttpPost]
        public IActionResult SingIn(String username,String password)
        {


            Usuario user = usuarioRepo.FindByCredentials(username,password);


            if (user == null)
            {
                ModelState.AddModelError("msj","User not found bro");
                //temp data solo existe durante un unico request
                TempData["MensajeLogeo"] = "Usuario y contraseña incorrectos";
                return RedirectToAction("SingIn");

            }
            else {


                autenticacion.IniciarSesion(username);
                
                return RedirectToAction("Index", "Home");

            }

            
        }

        //public string CrearHash(string pass)
        //{
        //    pass = pass + configuration.GetValue<string>("Key");  //obtenemos el key desde appsetting.json,esto es muy util cuando este en produccion
        //    var sha = SHA512.Create();
        //    var bytes = Encoding.Default.GetBytes(pass);
        //    var hash = sha.ComputeHash(bytes);
        //    return Convert.ToBase64String(hash) ;
        //}


    }
}
